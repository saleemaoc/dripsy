// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};
/** ------------------------
 Builders
 ------------------------**/
Alloy.Globals.DaoBuilder = require('daos/DaoBuilder');
Alloy.Globals.ServiceBuilder = require('services/ServiceBuilder');
/** ------------------------
 Resources
 ------------------------**/
Alloy.Globals.Dimens = require('dimen');
Alloy.Globals.Colors = require('colors');
Alloy.Globals.Integers = require('integer');
Alloy.Globals.Fonts = require('fonts');
Alloy.Globals.Images = require('images');
/** ------------------------
 Managers
 ------------------------**/
Alloy.Globals.AppRoute = new (require('AppRoute'))();
Alloy.Globals.AppProperties = require('AppProperties');
Alloy.Globals.NetworkManager = new (require('NetworkManager'))();
/** ------------------------
 Utils
 ------------------------**/
Alloy.Globals.moment = require('moment');
Alloy.Globals.momentTZ = require('moment-timezone');

Alloy.Globals.FormatUtils = require('FormatUtils');
Alloy.Globals.JsUtils = require('JsUtils');
Alloy.Globals.TiUtils = require('TiUtils');
Alloy.Globals.IsAndroid = OS_ANDROID;
Alloy.Globals.IsIOS = OS_IOS;
/** ------------------------
 Test
 ------------------------**/
//(require('Test')).run();
/** ------------------------
 Social
 ------------------------**/
Alloy.Globals.FacebookHelper = require('FacebookHelper');
/**
 * Paypal
 */
Alloy.Globals.PaypalHelper = require('PaypalHelper');

/** ------------------------
 Pushwoosh
 ------------------------**/
Alloy.Globals.PushWooshHelper = require('PushWooshHelper');
if (OS_IOS) {

	Alloy.Globals.Foreground = true;

	Ti.App.addEventListener('resume', function(e) {
		_.delay(function() {
			Alloy.Globals.AppProperties.setNotificationCheckDate();
			Alloy.Globals.Foreground = true;
			Alloy.Globals.PushWooshHelper.clearNotifications();
		}, 500);

	});

	Ti.App.addEventListener('pause', function(e) {
		Alloy.Globals.Foreground = false;
	});

} else {
	Ti.Android.currentActivity.addEventListener('resume', function(e) {
		Alloy.Globals.PushWooshHelper.clearNotifications();
	});
}

/** ------------------------
 User
 ------------------------**/
var maxDateBirth = new Date();
maxDateBirth.setFullYear(maxDateBirth.getFullYear() - Alloy.CFG.constants.user_min_year, maxDateBirth.getMonth(), maxDateBirth.getDate());
var defaultDateBirth = new Date();
defaultDateBirth.setFullYear(defaultDateBirth.getFullYear() - Alloy.CFG.constants.user_default_year, defaultDateBirth.getMonth(), defaultDateBirth.getDate());

Alloy.Globals.UserDates = {
	MaxDateBirth : maxDateBirth,
	DefaultDateBirth : defaultDateBirth
};

(require('GlobalLoader')).register();

/**
 * ImageView Properties
 */
//and to use contentmodes constants via alloy
var AvImageview = require("av.imageview");
Alloy.Globals.CONTENT_MODE_FIT = AvImageview.CONTENT_MODE_ASPECT_FIT;
Alloy.Globals.CONTENT_MODE_FILL = AvImageview.CONTENT_MODE_ASPECT_FILL;
