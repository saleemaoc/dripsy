/** ---------
 Constants
 -------- **/
var TAG = 'widgets/co.clarika.label';
/** ---------
 Fields
 -------- **/
/**
 * // Arguments passed into this controller can be accessed via the `$.args` object directly or:
 */
var args = $.args;

var labeltype = null;
var isGift = false;
/** ---------
 Methods
 -------- **/
/**
 * apply properties
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {
		if (_.has(properties, 'label')) {
			_.extend($.label, properties.label);
		}
		//data binding
		if (_.has(properties, 'text')) {
			$.label.text = properties.text;
		}

		if (_.has(properties, 'labeltype')) {
			labelType = properties.labeltype;
		}

		if (_.has(properties, 'isgift')) {
			isGift = properties.isgift;
		}

		_.extend($.widget, _.omit(properties, 'label', 'text', 'labeltype', 'isgift'));
	}
};

/**
 * To initialize widget
 */
var init = function() {
	applyProperties(args);
	//gc
	args = null;
	updateUI();
};

/**
 * update ui
 */
var updateUI = function() {
	if (_.isNumber(labelType)) {
		if (isGift) {
			switch(labelType) {
			case Alloy.CFG.constants.gift_status.accepted:
				acceptStyle();
				break;
			case Alloy.CFG.constants.gift_status.pending:
			case Alloy.CFG.constants.transaction_status.pending_accept:
				pendingStyle();
				break;
			case Alloy.CFG.constants.gift_status.rejected:
			case Alloy.CFG.constants.transaction_status.rejected:
			case Alloy.CFG.constants.transaction_status.gift_rejected:
				rejectedStyle();
				break;
			case Alloy.CFG.constants.transaction_status.cancel:
				cancelStyle();
				break;
			case  Alloy.CFG.constants.gift_status.defeated:
				defeatedStatus();
				break;
			}
		} else {
			switch(labelType) {
			case Alloy.CFG.constants.transaction_status.accept:
			case Alloy.CFG.constants.transaction_status.gift_acepted:
				acceptStyle();
				break;
			case Alloy.CFG.constants.transaction_status.cancel:
				cancelStyle();
				break;
			case Alloy.CFG.constants.transaction_status.pending:
			case Alloy.CFG.constants.transaction_status.pending_accept:
				pendingStyle();
				break;
			case Alloy.CFG.constants.transaction_status.rejected:
			case Alloy.CFG.constants.transaction_status.gift_rejected:
				rejectedStyle();
				break;
			case Alloy.CFG.constants.transaction_status.defeated:
				defeatedStatus();
				break;
			}
		}
	}
};

/**
 * update UI widget with accept style
 */
var acceptStyle = function() {
	$.widget.backgroundColor = Alloy.Globals.Colors.accent;
};

/**
 * update UI widget with cancel style
 */
var cancelStyle = function() {
	$.widget.backgroundColor = Alloy.Globals.Colors.secondary;
	$.widget.borderColor = Alloy.Globals.Colors.secondary;
};

/**
 * update UI widget with pending style
 */
var pendingStyle = function() {
	$.widget.backgroundColor = 'transparent';
	$.widget.borderColor = Alloy.Globals.Colors.white;
	$.widget.borderWidth = '1dp';
};

/**
 * update UI widget with rejected style
 */
var rejectedStyle = function() {
	$.widget.backgroundColor = Alloy.Globals.Colors.red;
	$.widget.borderColor = Alloy.Globals.Colors.red;
};

var defeatedStatus = function() {
	$.widget.backgroundColor = Alloy.Globals.Colors.red;
	$.widget.borderColor = Alloy.Globals.Colors.red;
};

//initialize widget
init();

/** ---------
 Public
 -------- **/
exports.applyProperties = applyProperties;
