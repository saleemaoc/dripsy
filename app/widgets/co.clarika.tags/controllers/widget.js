/*
NightToNight
Created by Juan Ignacio Agu on 2016-09-02.
Copyright 2016 Juan Ignacio Agu. All rights reserved.
*/

/** ---------
 Constants
 -------- **/
var TAG = 'widget/co.clarika.tags';

/** ---------
 Fields
 -------- **/

// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var editMode = true;
var tags = [];
var collection;

/** ---------
 Methods
 -------- **/

/**
 * apply properties to widget
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		if (_.has(properties, 'tags')) {
			tags = properties.tags;
		}

		if (_.has(properties, 'editMode')) {
			editMode = properties.editMode;
		}

		if (_.has(properties, 'collection')) {
			collection = properties.collection;
		}

		_.extend($.widget, _.omit(properties, 'tags', 'editMode', 'collection'));
	}
};

/**
 * apply listeners to widget
 */
var applyListeners = function() {
	$.btn_add.addEventListener('click', onClickAdd);

};

var init = function() {
	applyListeners();
	applyProperties(args);
	//gc
	args = null;
	updateViewByMode();
};

var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
	} catch(e) {
	}
};

/**
 * update view by edit mode
 */
var updateViewByMode = function() {
	if (!editMode) {
		$.tags.right = 0;
		$.btn_add.visible = false;
	}
};

var refreshTags = function() {
	$.tags.removeAllChildren();
	showLoader();
	if (_.isArray(tags)) {
		_.map(tags, function(tag) {
			addTagView(tag);
			//gc
			tagController = null;
			tag = null;
		});
	}
	_.delay(hideLoader, 500);
};

/**
 * return tags
 * @return {Array}
 */
var getTags = function() {
	return tags;
};

/**
 * set and refresh tags
 * @param {Array} tags
 */
var setTags = function(_tags) {
	tags = _tags;
	_.defer(refreshTags);
};

/**
 * add tag view
 * @param {Alloy.Model.Tag} tag
 */
var addTagView = function(tag) {
	//create controller
	var tagController = Widget.createController('tag', {
		tag : tag,
		title : {
			text : tag.get('name').toUpperCase()
		}
	});
	//add listener
	tagController.on('click', onClickTag);
	if (editMode) {
		tagController.on('longclick:tag', onLongClick);
	}
	//add in view
	$.tags.add(tagController.getView());

};

/**
 * add tag
 * @param {Alloy.Model.Tag} tag
 */
var addTag = function(tag) {
	tags.push(tag);
	_.defer(addTagView, tag);
};

var showLoader = function() {
	$.content_tags.visible = false;
	$.loader.show();
};

var hideLoader = function() {
	$.loader.hide();
	$.content_tags.visible = true;
};

/** ---------
 Listeners
 -------- **/

/**
 * Fired when user click on tag
 * @param {Object} e
 */
var onClickTag = _.debounce(function(e) {
	$.trigger('click:tag', e);
}, Alloy.Globals.Integers.debounce, true);

/**
 * Fired when user click on add button
 */
var onClickAdd = _.debounce(function(e) {
	var addController = Widget.createController('win_tag', {
		collection : collection
	});
	addController.on('success:tag', onSuccessTag);
	addController.on('create:tag', onCreateTag);
	if (OS_ANDROID) {
		addController.getView().open();
	} else {

		var navWindow = Titanium.UI.iOS.createNavigationWindow({
			window : addController.getView()
		});

		addController.on('navclose', function(e) {
			navWindow.close();
			//gc
			navWindow = null;
		});

		navWindow.open();
	}
	//gc
	addController = null;
}, Alloy.Globals.Integers.debounce, true);

/**
 * when user selected tag in list
 */
var onSuccessTag = _.debounce(function(e) {
	$.trigger('success:tag', e);
}, Alloy.Globals.Integers.debounce, true);

/**
 * when user create tag
 */
var onCreateTag = _.debounce(function(e) {
	$.trigger('create:tag', e);
}, Alloy.Globals.Integers.debounce, true);

/**
 * Fired when user click on add button
 */
var onLongClick = _.debounce(function(e) {

	var tag = e.tag;
	var tagName = tag.getName();

	var dialog = Ti.UI.createAlertDialog({
		message : String.format(L('question_delete_tag', 'Do you want to eliminate the interest of %s'), tagName),
		buttonNames : [L('cancel', 'Cancel'), L('accept', 'Accept')],
		cancel : 0
	});

	dialog.addEventListener('click', function(e) {
		switch(e.index) {
		case 1:
			$.trigger('remove:tag', {
				tag : tag
			});
			break;
		}
	});

	dialog.show();

	//gc
	dialog = null;
}, Alloy.Globals.Integers.debounce, true);

_.defer(init);

/** ---------
 Public
 -------- **/
exports.applyProperties = applyProperties;
exports.addEventListener = $.on;
exports.removeEventListener = $.off;
exports.cleanup = cleanup;
exports.addTag = addTag;
exports.refreshTags = refreshTags;
