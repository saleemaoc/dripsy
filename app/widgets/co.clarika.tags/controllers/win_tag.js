/**
 * @author jagu
 */
/** ------------------------
 Fields
 ------------------------**/
var TAG = 'widget/co.clarika.tags/controllers/win_tag';
/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;

var limit = Alloy.CFG.paggination_default || 20;
var collection;
var httpCK;
var tagName;
//onsearch
var timeOutOnSearch = null;
var popular = false;
/** ------------------------
 Dependencies
 ------------------------**/
/**
 * get service
 * @return {Service}
 */
var getService = function() {
	return Alloy.Globals.ServiceBuilder.getTagService().inject(getDao());
};

/**
 * get dao
 * @return {Dao}
 */
var getDao = function() {
	return Alloy.Globals.DaoBuilder.getTagDao();
};

/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {
		if (_.has(properties, 'collection')) {
			collection = properties.collection;
		}
		_.extend($.win_tag, _.omit(properties, 'collection'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
		if (httpCK) {
			httpCK.abort();
		}
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
	$.list.addEventListener('itemclick', onClickItem);
	if (OS_IOS) {
		$.search.addEventListener('change', onSearch);
	}
};

/**
 * initialize controller
 */
var init = function(e) {
	applyProperties(args);
	applyListeners();
	//gc
	args = null;
	configureToolbar(e);
	_.delay(getData, Alloy.Globals.Integers.SetDataDelay);
	//create http instance
	httpCK = new (require('HttpCK'))();
};

/**
 * configure toolbar
 */
var configureToolbar = function(e) {
	if (OS_ANDROID) {
		var activity = e.source.getActivity();
		activity.onCreateOptionsMenu = onCreateOptionsMenu;
		activity.invalidateOptionsMenu();
		activity.getActionBar().displayHomeAsUp = true;
		activity.getActionBar().onHomeIconItemSelected = onHomeIconItemSelected;
		activity = null;
	}
};

if (OS_ANDROID) {
	/**
	 * on create options menu
	 * @param {Object} e
	 */
	var onCreateOptionsMenu = function(e) {
		e.menu.clear();
		var searchView = Titanium.UI.Android.createSearchView({
			hintText : L('typing_tag', 'Typing tag...'),
			iconifiedByDefault : false,
			focusable : true
		});

		searchView.addEventListener('change', onSearch);
		e.menu.add({
			actionView : searchView,
			icon : Alloy.Globals.Images.ic_action_search_24dp,
			showAsAction : Ti.Android.SHOW_AS_ACTION_ALWAYS
		});
		//gc
		searchView = null;
	};

	/**
	 * on home icon item selected
	 */
	var onHomeIconItemSelected = _.debounce(function(e) {
		$.win_tag.close();
	}, Alloy.Globals.Integers.debounce, true);
}

var getData = function() {
	showLoader();
	httpCK.abortByTag(getService().TAGS.TAG_GET);

	if (validateTagName()) {
		popular = false;
		findTagByName(tagName);
	} else {
		findPopularList();
	}
};

var findPopularList = function() {
	if (!popular && collection) {
		popular = true;
		collection.reset();
		var params = {
			popular : true
		};
		getService().get(params, onSuccessService, onErrorService, httpCK);
	}
};

/**
 * find tag by name
 */
var findTagByName = function() {
	var params = {
		name : tagName
	};
	getService().get(params, onSuccessService, onErrorService, httpCK);
};

/**
 * on success popular list to server
 * @param {Object} response
 */
var onSuccessService = function(response) {
	findLocalData();
};

/**
 * on error popular list to server
 * @param {Object} error
 */
var onErrorService = function(error) {

};

var findLocalData = function() {
	collection.reset();
	if (_.isString(tagName) && !_.isEmpty(tagName)) {
		popular = false;
		getDao().findByName(tagName, true, onSuccessData, onErrorData, collection);
	} else {
		getDao().findPopular(true, onSuccessData, onErrorData, collection);
	}

};

/**
 * @param {Object} e
 */
var onSuccessData = function(e) {
	//declare section
	var sections = [];
	var section = Titanium.UI.createListSection();
	//set items
	var items = [];

	if (validateTagName() && !existTagNameInCollection(e.models)) {
		items.push({
			name : {
				text : tagName
			}
		});
	}

	if (_.isArray(e.models) && (!_.isEmpty(e.models) || !_.isEmpty(items))) {

		_.map(e.models, function(tag) {
			items.push({
				name : {
					text : tag.get('name')
				}
			});
		});

		//set section
		section.setItems(items);
		sections.push(section);

		$.list.setSections(sections);
		hideEmptyResult();
	} else {

		showEmptyResult();
		$.list.setSections([]);

	}
	hideLoader();

};

/**
 * @param {Object} e
 */
var onErrorData = function(e) {
	(require('TiLog')).error(TAG + ' onErrorData()', JSON.stringify(e));
	hideLoader();
	showEmptyResult();
};

/**
 * show loader and hide list
 */
var showLoader = function() {
	$.loader.show();
	$.list.visible = false;
};

/**
 * hide loader and show list
 */
var hideLoader = function() {
	$.loader.hide();
	$.list.show();
};

/**
 * show empty result
 */
var showEmptyResult = function() {

};

/**
 * hide empty result
 */
var hideEmptyResult = function() {

};

var showHelper = function() {

};

var validateTagName = function() {
	return _.isString(tagName) && !_.isEmpty(tagName);
};

/**
 * check if exist tag name in current collection or result collection
 * @param {Array} models
 * @return {Boolean}
 */
var existTagNameInCollection = function(models) {
	if (!_.isArray(models)) {
		models = collection.models;
	}
	return !_.isEmpty(_.filter(models, function(tag) {
		return tag.getName().toLowerCase() == tagName.toLowerCase();
	}));
};
/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * when window is opened
 */
var onOpen = function(e) {
	_.defer(init, e);
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
};

/**
 * @param {Object} e
 */
var onClickItem = _.debounce(function(e) {
	var existTag = validateTagName();
	if (existTag && e.itemIndex == 0) {
		$.trigger('create:tag', {
			name : tagName
		});
	} else {
		var index = e.itemIndex;
		if (existTag) {
			index--;
		}
		var tag = collection.models[index];
		$.trigger('success:tag', {
			tag : tag
		});
	}

	close();
}, Alloy.Globals.Integers.debounce, true);

/**
 * tag search
 */
var onSearch = function(e) {
	tagName = e.source.value;

	if (timeOutOnSearch != null) {
		clearTimeout(timeOutOnSearch);
		timeOutOnSearch = null;
	}
	if ((require('JsUtils')).hasWhiteSpace(tagName)) {

		if (tagName.trim().length > 0) {
			$.trigger('create:tag', {
				name : tagName.trim()
			});
			close();
		}

	} else {
		showLoader();
		timeOutOnSearch = setTimeout(function() {
			_.defer(getData);
		}, 1000);
	}
};

var close = function() {
	if (OS_ANDROID) {
		$.win_tag.close();
	} else {
		$.trigger('navclose', {});
	}

};
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.cleanup = cleanup;
