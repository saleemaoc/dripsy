/*
NightToNight
Created by Juan Ignacio Agu on 2016-09-02.
Copyright 2016 Juan Ignacio Agu. All rights reserved.
*/

/** ---------
 Constants
 -------- **/
var TAG = 'widget/co.clarika.tags/tag';

/** ---------
 Fields
 -------- **/

// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var tag;

/** ---------
 Methods
 -------- **/

/**
 * apply properties to widget
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		if (_.has(properties, 'tag')) {
			tag = properties.tag;
		}

		if (_.has(properties, 'title')) {
			_.extend($.lbl_title, properties.title);
		}

		_.extend($.tag, _.omit(properties, 'tag'));
	}
};

/**
 * apply listeners to widget
 */
var applyListeners = function() {
	$.tag.addEventListener('click', onClickTag);
	$.tag.addEventListener('longpress', onLongClick);
};

var init = function() {
	applyListeners();
	applyProperties(args);
	//gc
	args = null;
};

/** ---------
 Listeners
 -------- **/

/**
 * Fired when user click on tag
 * @param {Object} e
 */
var onClickTag = _.debounce(function(e) {
	$.trigger('click:tag', _.extend(e, {
		tag : tag
	}));
}, Alloy.Globals.Integers.debounce, true);

/**
 * Fired when user click on tag
 * @param {Object} e
 */
var onLongClick = _.debounce(function(e) {
	$.trigger('longclick:tag', _.extend(e, {
		tag : tag
	}));
}, Alloy.Globals.Integers.debounce, true);

_.defer(init);
/** ---------
 Public
 -------- **/
exports.applyProperties = applyProperties;
exports.addEventListener = $.on;
exports.removeEventListener = $.off;
