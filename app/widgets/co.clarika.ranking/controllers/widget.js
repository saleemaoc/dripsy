/*
NightToNight
Created by Juan Ignacio Agu on 2016-09-02.
Copyright 2016 Juan Ignacio Agu. All rights reserved.
*/

/** ---------
 Constants
 -------- **/
var TAG = 'widgets/co.clarika.image';
/** ---------
 Fields
 -------- **/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var value = 0;
/** ---------
 Methods
 -------- **/

/**
 * apply properties to widget
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		if (_.has(properties, 'label')) {
			_.extend($.label, properties.label);
		}

		if (_.has(properties, 'value')) {
			value = properties.value;
		}

		_.extend($.label, _.omit(properties, 'label'));
	}
};

/**
 * apply listeners to widget
 */
var applyListeners = function() {
};

var init = function() {
	applyListeners();
	applyProperties(args);
	//gc
	args = null;
	updateUI();
};

/** ---------
 Listeners
 -------- **/

/**
 * @param {Object} e
 */
var onClickStar = function(e) {
	calculateValueById(e.source.id);
	$.trigger('change', {
		value : value
	});
	updateUI();
};

var updateUI = function() {
	$.icon_rank_5.image = value == 5 ? Alloy.Globals.Images.ic_star_24dp : Alloy.Globals.Images.ic_star_normal_24dp;
	$.icon_rank_4.image = value >= 4 ? Alloy.Globals.Images.ic_star_24dp : Alloy.Globals.Images.ic_star_normal_24dp;
	$.icon_rank_3.image = value >= 3 ? Alloy.Globals.Images.ic_star_24dp : Alloy.Globals.Images.ic_star_normal_24dp;
	$.icon_rank_2.image = value >= 2 ? Alloy.Globals.Images.ic_star_24dp : Alloy.Globals.Images.ic_star_normal_24dp;
	$.icon_rank_1.image = value >= 1 ? Alloy.Globals.Images.ic_star_24dp : Alloy.Globals.Images.ic_star_normal_24dp;
};

/**
 * @param {String} id
 */
var calculateValueById = function(id) {
	switch(id) {
	case $.rank_1.id:
		value = 1;
		break;
	case $.rank_2.id:
		value = 2;
		break;
	case $.rank_3.id:
		value = 3;
		break;
	case $.rank_4.id:
		value = 4;
		break;
	case $.rank_5.id:
		value = 5;
		break;
	}
};

var getValue = function() {
	return value;
};

/**
 * @param {Integer} v
 */
var setValue = function(v) {
	value = v;
	updateUI();
};

_.defer(init);
/** ---------
 Public
 -------- **/
exports.applyProperties = applyProperties;
exports.setValue = setValue;
exports.getValue = getValue;
exports.addEventListener = $.on;
exports.removeEventListener = $.off;
