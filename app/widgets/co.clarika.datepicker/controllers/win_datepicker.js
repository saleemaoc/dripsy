/** ------------------------
 Constants
 ------------------------**/
var TAG = 'co.clarika.datepicker/win_datepicker';
/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;

var value = new Date();
var english = !(Ti.Locale.currentLanguage == 'es');
var contentPicker;

var maxDate = new Date();
var minDate = new Date();

var moment = require('/alloy/moment');


/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {
		if (_.has(properties, 'picker')) {
			
			_.extend($.date_picker, properties.picker);
		
			if (_.has(properties.picker, 'maxDate')) {
				setMaxDate(properties.picker.maxDate);
			}

			if (_.has(properties.picker, 'minDate')) {
				setMinDate(properties.picker.minDate);
			}

		}

	}
		
	if (_.has(properties, 'value')) {
		value = properties.value;
	}

	_.extend($.win_datepicker, _.omit(properties, 'picker', 'cancel', 'accept', 'v_picker', 'value'));
	
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
	$.win_datepicker.addEventListener('click', onCancel);	
};

/**
 * initialize controller
 */
var init = function(e) {
	generateMinDate();
	applyProperties(args);
	//gc
	args = null;

	configureValues();
	setValue(value);
	applyListeners();

};

var configureValues = function() {
	
};
var setValue = function(value) {
	
	var columns = [{
	    field: "date",
	    format: "{0: yyyy-MM-dd}"
	}];

	$.date_picker.setColumns([columns]);
		
	if (_.isDate(value)) {
		$.date_picker.setValue(value);
	}
};

var validateDate = function() {
	
};

/**
 * set value for max date propertie
 * @param {Date}
 */
var setMaxDate = function(d) {
	if (_.isDate(d)) {
		maxDate = d;
		if (!_.isDate(minDate) || minDate.getTime() > maxDate.getTime()) {
			minDate = generateMinDate();
		}
	}
};

/**
 * set value for min date propertie
 * @param {Date}
 */
var setMinDate = function(d) {
	Ti.API.info('setMinDate', d.toString());
	if (_.isDate(d) && maxDate.getTime() > d) {
		minDate = d;
	}
};

/**
 * generate value for min date using max date
 */
var generateMinDate = function() {
	var now = new Date();
	now.setFullYear(maxDate.getFullYear() - 100, now.getMonth(), now.getDate());
	Ti.API.info('generateMinDate', now.toString());
	return now;
};
/** ------------------------
 Listeners
 ------------------------**/

/**
 * when window is opened
 */
var onOpen = function(e) {
	// $.date_picker.setValue(new Date);
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
};

/**
 * on cancel
 * @param {Object} e
 */
var onCancel = _.debounce(function(e) {
	$.trigger('cancel', e);
	$.win_datepicker.close();
}, 1000, true);

/**
 * on accept
 * @param {Object} e
 */
var onAccept = _.debounce(function(e) {
	$.trigger('accept', {
		value : value
	});
	//hoy -18
	var eighteenYearsAgo = moment().subtract(18, "years");
    
    //fecha de nacimiento
    var birthday = moment(value);
    if (eighteenYearsAgo.isAfter(birthday)) {
        //return "okay, you're good";    
    }
    else {
    	(require('TiUtils')).toast("Debe ser mayor de edad");
        return "fecha invalida";    
    }
	$.win_datepicker.close();
}, 1000, true);

/**
 * on change date picker value
 * @param {Object} e
 */
var onChangeDatePicker = function(e) {
	value = e.value;
};


_.defer(init, {});
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.cleanup = cleanup;
