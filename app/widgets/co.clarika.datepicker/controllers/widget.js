/** --------
 Constants
 -------- **/
var TAG = 'co.clarika.datepicker';
/** --------
 Fields
 -------- **/

var args = $.args;
var value = null;
var formatDateToShow = Ti.Locale.currentLanguage == 'es' ? 'DD/MM/YYYY' : 'MM/DD/YYYY';
var formatDateToGet = null;
var propertiesToPicker;
var isChange = false;
/** --------
 Methods
 -------- **/
/**
 * apply properties to widget
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		if (_.has(properties, 'label')) {
			_.extend($.label, properties.label);
		}

		if (_.has(properties, 'hint_text')) {
			_.extend($.hint_text, properties.hint_text);
		}

		if (_.has(properties, 'formatDateToShow')) {
			formatDateToShow = properties.formatDateToShow;
		}

		if (_.has(properties, 'formatDateToGet')) {
			formatDateToGet = properties.formatDateToGet;
		}

		if (_.has(properties, 'value')) {
			setValue(properties.value);
		}

		if (_.has(properties, 'valueWithoutRefreshLabel')) {
			value = properties.valueWithoutRefreshLabel;
		}
		if (_.has(properties, 'picker')) {
			propertiesToPicker = properties.picker;
		}

		if (_.has(properties, 'custom_height')) {
			$.widget.height = properties.custom_height;
		}
		_.extend($.widget, _.omit(properties, 'label', 'value', 'formatDateToShow', 'formatDateToGet', 'picker', 'hint_text', 'height', 'valueWithoutRefreshLabel'));

	}
};

/**
 * refresh value
 */
var refreshValueInLabel = function() {
	if (_.isDate(value)) {
		$.hint_text.visible = false;
		$.label.text = (require('alloy/moment'))(value).format(formatDateToShow);
	} else {
		Ti.API.error('NOTrefreshValueInLabel', JSON.stringify(value));
	}
};

/**
 * get value selected
 */
var getValue = function() {
	if (!_.isNull(formatDateToGet)) {
		return (require('alloy/moment'))(value).format(formatDateToGet);
	} else {
		return value;
	}
};

/**
 * @param {Date} value
 * set value
 */
var setValue = function(_value) {
	value = _value;
	refreshValueInLabel();
};

var init = function() {
	applyProperties(args);
	//gc
	args = null;
};

init({});


var getIsChange=function(){
	return isChange;
};
/** --------
 Listeners
 -------- **/
/**
 * on click to widget
 * @param {Object} e
 */
var onClick = _.debounce(function(e) {
	$.trigger('click', e);

	var datePickerController = Widget.createController('win_datepicker', {
		picker : propertiesToPicker,
		value : value
	});

	datePickerController.on('accept', onAccept);
	datePickerController.getView().open();

	//gc
	datePickerController = null;
}, 1000, true);

var onAccept = function(e) {
	isChange = true;
	$.trigger('accept', e);
	setValue(e.value);
};

/** --------
 Public
 -------- **/
exports.applyProperties = applyProperties;
exports.getValue = getValue;
exports.setValue = setValue;
exports.getIsChange=getIsChange;