/** --------
 Constants
 -------- **/
var TAG = 'co.clarika.stripe';

var PREFERENCES_CARD_LAST_4 = "card_last_4";
var PREFERENCES_CARD_BRAND = "card_brand";
var PREFERENCES_CARD_EXP_MONTH = "card_exp_month";
var PREFERENCES_CARD_EXP_YEAR = "card_exp_year";
var PREFERENCES_CARD_TOKEN = "card_token";
/** --------
 Fields
 -------- **/

var args = $.args;
var currentCard = null;
var payment = null;

var enableChange = true;

var success = false;
/** --------
 Methods
 -------- **/

/**
 * apply properties to widget
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		if (_.has(properties, 'card')) {
			currentCard = properties.card;
		}

		if (_.has(properties, 'payment')) {
			payment = properties.payment;
		}

		if (_.has(properties, 'transactionAmount')) {
			$.lbl_currency_amount.text = properties.transactionAmount;
		}

		_.extend($.widget, _.omit(properties, 'card', 'payment', 'transactionAmount'));

	}
};

var beforeOpen = function() {
	configurePaymentMethods();
};

var init = function(e) {
	configureToolbar(e);
	applyProperties(args);
	applyListeners();
	loadListCard();
	//gc
	args = null;
};

var applyListeners = function() {
	$.btn_add_new_card.on('click', onAddNewCard);
	$.list_card.addEventListener('itemclick', onClickCard);

	Alloy.Globals.PaypalHelper.paypal.addEventListener(Alloy.Globals.PaypalHelper.paypal.SUCCESS, onSuccessPayment);
	Alloy.Globals.PaypalHelper.paypal.addEventListener(Alloy.Globals.PaypalHelper.paypal.ERROR, onErrorPayment);
	Alloy.Globals.PaypalHelper.paypal.addEventListener(Alloy.Globals.PaypalHelper.paypal.CANCELED, onCancelPayment);
};
/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
		Alloy.Globals.PaypalHelper.paypal.removeEventListener(Alloy.Globals.PaypalHelper.paypal.SUCCESS, onSuccessPayment);
		Alloy.Globals.PaypalHelper.paypal.removeEventListener(Alloy.Globals.PaypalHelper.paypal.ERROR, onErrorPayment);
		Alloy.Globals.PaypalHelper.paypal.removeEventListener(Alloy.Globals.PaypalHelper.paypal.CANCELED, onCancelPayment);
	} catch(e) {
	}
};

/**
 * configure toolbar
 */
var configureToolbar = function(e) {
	if (OS_ANDROID) {
		var activity = $.widget.getActivity();
		activity.getActionBar().displayHomeAsUp = true;
		activity.getActionBar().onHomeIconItemSelected = onHomeIconItemSelected;
		//gc
		activity = null;
	}

};

var onBack = function() {
	if (OS_IOS) {
		$.trigger('cancel');
	}
	$.win_cart_drink.close();
};

if (OS_ANDROID) {
	/**
	 * on home icon item selected
	 */
	var onHomeIconItemSelected = _.debounce(function(e) {
		$.trigger('cancel');
		close();
	}, Alloy.Globals.Integers.debounce, true);
}

/**
 * open window
 */
var open = function() {
	if (OS_IOS) {
		Alloy.Globals.AppRoute.openWindow($.widget.getView);
	} else {
		$.widget.open();
	}

};

var close = function() {
	if (OS_IOS) {
		Alloy.Globals.AppRoute.getNavigationWindow().closeWindow($.widget.getView);
	} else {
		$.widget.close();
	}
};

var loadListCard = function() {
	var card = getCard();
	var sections = [];
	var section = Ti.UI.createListSection();
	if (!_.isNull(card)) {
		section.setItems([{
			lbl_last_card_number : {
				text : card.cardLast4
			},
			img_card : {
				image : getLogoByBrand(card.brand)
			},
			lbl_exp_date : {
				text : card.expMonth + "/" + card.expYear
			}
		}]);
	}

	sections.push(section);
	$.list_card.sections = sections;
};

/**
 * get logo by card brand
 * @param {string} brand
 */
var getLogoByBrand = function(brand) {
	switch(brand) {
	case 'Visa':
		return Alloy.Globals.Images.ic_card_visa;
		break;
	case 'Mastercard':
		return Alloy.Globals.Images.ic_card_master_card;
		break;
	case 'amex':
		return Alloy.Globals.Images.ic_card_amex;
		break;
	default:
		return Alloy.Globals.Images.ic_card;
		break;
	}
};
/**
 * save new card
 * @param {Object} card
 */
var saveNewCard = function(card) {

	if (!_.isNull(card)) {

		Ti.App.Properties.setString(PREFERENCES_CARD_LAST_4, card.cardLast4);
		Ti.App.Properties.setString(PREFERENCES_CARD_BRAND, card.brand);
		Ti.App.Properties.setString(PREFERENCES_CARD_EXP_MONTH, card.expMonth);
		Ti.App.Properties.setString(PREFERENCES_CARD_EXP_YEAR, card.expYear);
		Ti.App.Properties.setString(PREFERENCES_CARD_TOKEN, card.token);

		//clear token
		card.token = null;
		currentCard = card;
	} else {
		//clear card
		Ti.App.Properties.setString(PREFERENCES_CARD_LAST_4, null);
		Ti.App.Properties.setString(PREFERENCES_CARD_BRAND, null);
		Ti.App.Properties.setString(PREFERENCES_CARD_EXP_MONTH, null);
		Ti.App.Properties.setString(PREFERENCES_CARD_EXP_YEAR, null);
		Ti.App.Properties.setString(PREFERENCES_CARD_TOKEN, null);
	}

};

/**
 * get card
 */
var getCard = function() {

	if (currentCard == null && _.isString(Ti.App.Properties.getString(PREFERENCES_CARD_TOKEN, null))) {

		currentCard = {
			cardLast4 : Ti.App.Properties.getString(PREFERENCES_CARD_LAST_4, ''),
			brand : Ti.App.Properties.getString(PREFERENCES_CARD_BRAND, ''),
			expMonth : Ti.App.Properties.getString(PREFERENCES_CARD_EXP_MONTH, ''),
			expYear : Ti.App.Properties.getString(PREFERENCES_CARD_EXP_YEAR, ''),
		};

		return currentCard;
	} else {
		return currentCard;
	}
};
var payWithCard = _.debounce(function(e) {
	if (!_.isNull(getCard())) {
		success = true;
		$.trigger('success', {
			paymentMethod : Alloy.CFG.Stripe.method,
			code : Ti.App.Properties.getString(PREFERENCES_CARD_TOKEN)
		});
		close();
	} else {
		(require('TiUtils')).toast(L('payment_with_card_not_valid', 'Please select a valid card'));
	}

});

var configurePaymentMethods = function() {
	var hasPaypal = false;
	var hasStripe = false;
	if (_.isArray(Alloy.Globals.CurrentCountryPaymentMethods) && !_.isEmpty(Alloy.Globals.CurrentCountryPaymentMethods)) {
		hasPaypal = Alloy.Globals.CurrentCountryPaymentMethods.indexOf(Alloy.CFG.Paypal.method) != -1;
		hasStripe = Alloy.Globals.CurrentCountryPaymentMethods.indexOf(Alloy.CFG.Stripe.method) != -1;
	}

	if (!hasStripe) {
		$.list_card.hide();
		$.list_card.height = 0;
		$.btn_add_new_card.hide();
		$.btn_add_new_card.applyProperties({
			height : 0
		});
	}

};
/** --------
 Listeners
 -------- **/
/**
 * when window is opened
 */
var onOpen = function(e) {
	_.defer(init, e);
	success = false;
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	if (!success) {
		$.trigger('cancel');
	}

	cleanup();
};

var onAddNewCard = _.debounce(function(e) {
	var cardStripeController = Widget.createController('card_stripe');

	//listeners
	cardStripeController.on('success', onSuccessAddNewCard);
	cardStripeController.open();
	cardStripeController = null;
});

var onSuccessAddNewCard = function(e) {
	var card = e.card;
	saveNewCard(card);
	loadListCard();
	payWithCard();
};
/** ----
 Paypal
 ---- **/
/**
 * pay with paypal
 */
var onPayPaypal = _.debounce(function(e) {
	(require('TiUtils')).questionAlert(L('paypal_payment_conditions', 'Purchases through PayPal are paid at the time, not when they are used / consumed. Do you agree?'), 'Paypal', payWithPaypay);
}, Alloy.Globals.Integers.debounce, true);

var payWithPaypay = function() {
	if (payment != null) {
		Alloy.Globals.PaypalHelper.paypal.onBuy(payment);
	}
};
/**
 * on success payment transaction
 * @param {Object} e
 */
var onSuccessPayment = _.debounce(function(e) {

	var response = OS_ANDROID ? JSON.parse(e.response) : e.response;
	var paymentOperationCode = response.response.id;

	$.trigger('success', {
		paymentMethod : Alloy.CFG.Paypal.method,
		code : paymentOperationCode
	});
	success = true;
	close();
}, Alloy.Globals.Integers.debounce, true);

/**
 * on error payment transaction
 * @param {Object} error
 */
var onErrorPayment = _.debounce(function(error) {
	(require('TiLog')).error(TAG, 'onErrorPayment -> ' + JSON.stringify(error));
	(require('TiUtils')).toast(L('error_process_payment', 'An error occurred while processing your purchase, please try again.'));
}, Alloy.Globals.Integers.debounce, true);

/**
 * on cancel payment transaction
 * @param {Object} e
 */
var onCancelPayment = _.debounce(function(e) {
}, Alloy.Globals.Integers.debounce, true);

/**
 * when selected an exist card
 */
var onClickCard = _.debounce(function(e) {
	payWithCard();
});

var STRIPE_TYPE = {
	api_connection_error : 'api_connection_error',
	api_error : 'api_error',
	authentication_error : 'authentication_error',
	card_error : 'card_error',
	invalid_request_error : 'invalid_request_error',
	rate_limit_error : 'rate_limit_error',
	validation_error : 'validation_error'
};

var STRIPE_CODE = {
	invalid_number : 'invalid_number',
	invalid_expiry_month : 'invalid_expiry_month',
	invalid_expiry_year : 'invalid_expiry_year',
	invalid_cvc : 'invalid_cvc',
	invalid_swipe_data : 'invalid_swipe_data',
	incorrect_number : 'incorrect_number',
	expired_card : 'expired_card',
	incorrect_cvc : 'incorrect_cvc',
	incorrect_zip : 'incorrect_zip',
	card_declined : 'card_declined',
	missing : 'missing',
	processing_error : 'processing_error'
};

var analizeError = function(error) {
	var message = L('stripe_generic_error', 'The transaction could not be completed please try again later');
	if (_.isObject(error) && _.has(error, 'type')) {
		switch(error.type) {
		case STRIPE_TYPE.card_error:
			message = analizeCardError(error);
			break;
		default:
			message = L('stripe_generic_error_with_card', 'There was an error processing your payment with your card');
			break;

		}
	}
	return message;
};

var analizeCardError = function(error) {
	var message = L('stripe_generic_error_with_card', 'There was an error processing your payment with your card');
	switch(error.code) {
	case STRIPE_CODE.invalid_number:
		message = L('stripe_invalid_number', 'Invalid card number');
		break;
	case STRIPE_CODE.invalid_expiry_month:
		message = L('stripe_invalid_number', 'Invalid expiry month');
		break;
	case STRIPE_CODE.invalid_expiry_year:
		message = L('stripe_invalid_number', 'Invalid expiry year');
		break;
	case STRIPE_CODE.invalid_cvc:
		message = L('stripe_invalid_cvc', 'Invalid CVC');
		break;
	case STRIPE_CODE.invalid_swipe_data:
		message = L('invalid_swipe_data', "The card's swipe data is invalid");
		break;
	case STRIPE_CODE.incorrect_number:
		message = L('stripe_incorect_number', 'Incorrect card number');
		break;
	case STRIPE_CODE.expired_card:
		message = L('stripe_expired_card', 'The card has expired');
		break;
	case STRIPE_CODE.incorrect_cvc:
		message = L('stripe_incorrect_cvc', "The card's security code is incorrect");
		break;
	case STRIPE_CODE.incorrect_zip:
		message = L('stripe_incorrect_zip', "The card's zip code failed validation");
		break;
	case STRIPE_CODE.card_declined:
		message = L('stripe_card_declined', "The card was declined");
		break;
	case STRIPE_CODE.missing:
		message = L('stripe_missing', "There is no card on a customer that is being charged");
		break;
	case STRIPE_CODE.processing_error:
		message = L('stripe_processing_error', "An error occurred while processing the card.");
		break;
	}
	return message;
};

beforeOpen();
/** --------
 Public
 -------- **/
exports.applyProperties = applyProperties;
exports.open = open;
exports.close = close;
exports.saveNewCard = saveNewCard;
exports.analizeError = analizeError;
exports.analizeCardError = analizeCardError;
