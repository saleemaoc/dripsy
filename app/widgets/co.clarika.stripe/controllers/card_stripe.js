/** --------
 Constants
 -------- **/
var TAG = 'co.clarika.stripe/card_stripe';
var expiryDateRegex = /^(0[1-9]|1[0-2])\/?([0-9]{4}|[0-9]{2})$/;
var SEPARATOR_EXPIRY_DATE = "/";
/** --------
 Fields
 -------- **/

var args = $.args;
var stripe = require('co.clarika.stripe');
var Mask = require('mask');
var enableChangeExpDate = true;
var enableChangeCardNumber = true;

var cardNumber1 = "";
var cardNumber2 = "";
var cardNumber3 = "";
var cardNumber4 = "";
var expDate = "";
/** --------
 Dependencies
 -------- **/
/**
 * return dependencie to service
 */
var getService = function() {
	return Alloy.Globals.ServiceBuilder.getCardService();
};
/** --------
 Methods
 -------- **/

/**
 * apply properties to widget
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		_.extend($.card_stripe, _.omit(properties));

	}
};

var init = function(e) {
	applyProperties(args);
	applyListeners();

	//gc
	args = null;
};

var applyListeners = function() {
	$.txt_exp_date.addEventListener('change', onChangeExpDate);

	$.btn_add_card.addEventListener('click', onSaveCard);
	$.btn_close.addEventListener('click', close);
	$.btn_info_exp_date.addEventListener('click', onInfoExpDate);
	$.btn_info_cvv.addEventListener('click', onInfoCVV);

	$.txt_card_number_1.addEventListener('change', onChangeCardNumber1);
	$.txt_card_number_2.addEventListener('change', onChangeCardNumber2);
	$.txt_card_number_3.addEventListener('change', onChangeCardNumber3);
	$.txt_card_number_4.addEventListener('change', onChangeCardNumber4);
};
/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
	} catch(e) {
	}
};

/**
 * open window
 */
var open = function() {
	$.card_stripe.open();
};

var close = function() {
	$.card_stripe.close();
};

var showLoader = function() {
	$.v_modal.hide();
	$.loader.show();
};

var hideLoader = function() {
	$.v_modal.show();
	$.loader.hide();
};

var hideKeyboard = function() {
	if (OS_IOS) {
		$.txt_card_number_1.blur();
		$.txt_card_number_2.blur();
		$.txt_card_number_3.blur();
		$.txt_card_number_4.blur();
		$.txt_exp_date.blur();
		$.txt_cvv.blur();
	} else {
		Ti.UI.Android.hideSoftKeyboard();
	}
};

/*var formatCardNumber = function(cardNumber) {

 if (_.isString(cardNumber) && !_.isEmpty(cardNumber)) {
 cardNumber = cardNumber.replace(/ /g, '');
 cardNumber = checkSpaceAtPositionAndInsert(cardNumber, 4);
 cardNumber = checkSpaceAtPositionAndInsert(cardNumber, 9);
 cardNumber = checkSpaceAtPositionAndInsert(cardNumber, 14);
 }
 return cardNumber;
 };

 var checkSpaceAtPositionAndInsert = function(str, position) {
 var SPACE = " ";
 if (str.length > position && str[position] != SPACE) {
 return insert(str, position, SPACE);
 } else {
 return str;
 }
 };
 var insert = function(str, index, value) {
 return str.substr(0, index) + value + str.substr(index);
 };*/

var getCardNumber = function() {
	return ($.txt_card_number_1.getValue() + $.txt_card_number_2.getValue() + $.txt_card_number_3.getValue() + $.txt_card_number_4.getValue()).trim();
};
/** --------
 Listeners
 -------- **/
/**
 * when window is opened
 */
var onOpen = function(e) {
	_.defer(init, e);
	$.txt_card_number_1.focus();
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
};

var onChangeExpDate = function(e) {
	if (enableChangeExpDate) {
		var newValue = Mask.mask($.txt_exp_date, Mask.generic, {
			regex : expiryDateRegex,
			syntax : '$1/$2'
		});

		if (newValue != e.value) {
			enableChangeExpDate = false;
			$.txt_exp_date.value = newValue;
		} else {
			enableChangeExpDate = true;
		}
	} else {
		if (expDate != e.value && e.value.length == 5) {
			$.txt_cvv.focus();
		}
		enableChangeExpDate = true;
	}
	expDate = e.value;

};

var onChangeCardNumber = function(e) {
	if (enableChangeCardNumber) {

		var newValue = formatCardNumber(e.value);
		if (newValue != e.value) {
			enableChangeCardNumber = false;
			$.txt_card_number.value = newValue;
		} else {
			enableChangeCardNumber = true;
		}
	} else {
		if (!_.isEmpty(e.value)) {
			var last = e.value.length;
			$.txt_card_number.setSelection(last, last);
		}

		enableChangeCardNumber = true;
	}

};

var onSaveCard = _.debounce(function(error) {

	if (validateFields()) {

		var expiryDate = $.txt_exp_date.getValue();
		var splitExpiryDate = expiryDate.split(SEPARATOR_EXPIRY_DATE);
		var value = stripe.setCard({
			publishableKey : Alloy.CFG.Stripe.key,
			cardNumber : getCardNumber(),
			month : splitExpiryDate[0],
			expiryYear : splitExpiryDate[1],
			cvc : $.txt_cvv.getValue()
		});

		if (OS_IOS || validateCard()) {
			hideKeyboard();

			if (Alloy.Globals.NetworkManager.checkConnection()) {
				showLoader();
				stripe.requestForToken({
					success : function success(e) {

						addUserCard({
							cardLast4 : stripe.cardLast4,
							brand : stripe.brand,
							expMonth : stripe.expMonth,
							expYear : stripe.expYear,
							token : e.token
						});

					},
					failure : onErrorAddCard
				});
			}
		}
	}
});

var validateFields = function() {

	var cardNumber = getCardNumber();
	var expiryDate = $.txt_exp_date.getValue();
	var cvv = $.txt_cvv.getValue();

	if (_.isEmpty(cardNumber)) {
		(require('TiUtils')).toast(L('card_number_required', 'Card number is required'));
		return false;
	} else if (_.isEmpty(expiryDate)) {
		(require('TiUtils')).toast(L('expiry_date_required', 'Expiry date is required'));
		return false;
	} else if (!expiryDateRegex.test(expiryDate) || expiryDate.indexOf(SEPARATOR_EXPIRY_DATE) == -1) {
		(require('TiUtils')).toast(L('expiry_date_format_error', 'Incorrect date format. e.g: MM/YY '));
	} else if (_.isEmpty(cvv)) {
		(require('TiUtils')).toast(L('cvv_required', 'CVC is required'));
		return false;
	} else if (cvv.length <= 2) {
		(require('TiUtils')).toast(L('cvv_length', 'CVC must have more than 2 numbers'));
	} else {
		return true;
	}
};

var validateCard = function() {
	if (!stripe.validateNumber()) {
		(require('TiUtils')).toast(L('card_number_invalid', 'Invalid card number'));
		return false;
	} else if (!stripe.validateExpiryDate()) {
		(require('TiUtils')).toast(L('expiry_date_invalid', 'Invalid expiry date'));
		return false;
	} else if (!stripe.validateCVC) {
		(require('TiUtils')).toast(L('cvv_invalid', 'Invalid CVC'));
		return false;
	} else {
		return true;
	}
};

var onInfoExpDate = _.debounce(function(e) {
	Titanium.UI.createAlertDialog({
		title : '',
		message : L('info_exp_date', 'Enter the expiration date of your card.')
	}).show();
});

var onInfoCVV = _.debounce(function(e) {
	Titanium.UI.createAlertDialog({
		title : '',
		message : L('info_cvv', 'Enter the security code on the back of your card.')
	}).show();
});

/**
 * send card to backend
 * @param {Object} card
 */
var addUserCard = function(card) {
	var params = {
		last_card : card.cardLast4,
		brand : card.brand,
		exp_month : card.expMonth,
		exp_year : card.expYear,
		token : card.token
	};
	getService().addCard(params, function(e) {
		$.trigger('success', {
			card : card
		});
		close();
	}, onErrorAddCard);
};

var onErrorAddCard = function(e) {
	hideLoader();
	(require('TiUtils')).toast(L('error_add_new_card', 'Card details are incorrect, please try again or contact your provider.'));
};

var onNextToExpDate = _.debounce(function(e) {
	$.txt_card_number_4.blur();
	$.txt_exp_date.focus();
});

var onNextToCVV = _.debounce(function(e) {
	$.txt_exp_date.blur();
	$.txt_cvv.focus();
});

var onChangeCardNumber1 = function(e) {
	if (cardNumber1.length < 4 && e.value.trim().length == 4) {
		$.txt_card_number_2.focus();
	}
	cardNumber1 = e.value;
};

var onChangeCardNumber2 = function(e) {
	if (cardNumber2.length < 4 && e.value.trim().length == 4) {
		$.txt_card_number_3.focus();
	}
	if (cardNumber2.length == 1 && e.value.trim().length == 0) {
		$.txt_card_number_1.focus();
	}
	cardNumber2 = e.value;
};

var onChangeCardNumber3 = function(e) {
	if (cardNumber3.length < 4 && e.value.trim().length == 4) {
		$.txt_card_number_4.focus();
	}
	if (cardNumber3.length == 1 && e.value.trim().length == 0) {
		$.txt_card_number_2.focus();
	}
	cardNumber3 = e.value;
};

var onChangeCardNumber4 = function(e) {
	if (cardNumber4.length < 4 && e.value.trim().length == 4) {
		$.txt_exp_date.focus();
	}
	if (cardNumber4.length == 1 && e.value.trim().length == 0) {
		$.txt_card_number_3.focus();
	}
	cardNumber4 = e.value;
};
/** --------
 Public
 -------- **/
exports.applyProperties = applyProperties;
exports.open = open;
exports.close = close;
