/**
 * @author jagu
 */

/** ---------
 Constants
 -------- **/
var TAG = 'widgets/co.clarika.textarea';
/** ---------
 Fields
 -------- **/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var enterKey = true;
/** ---------
 Methods
 -------- **/

/**
 * apply properties to widget
 * @param {Object} properties
 */
var applyProperties = function(properties) {

	if (_.isObject(properties)) {

		if (OS_IOS && _.has(properties, 'iOSHintText')) {
			_.extend($.hint_text, properties.iOSHintText);
			$.hint_text.text = properties.hintText;
		}
		if (_.has(properties, 'enterKey')) {
			enterKey = properties.enterKey;
		}
		_.extend($.text_area, _.omit(properties, 'iOSHintText', 'left', 'top', 'bottom', 'height', 'right', 'enterKey'));
		_.extend($.widget, _.omit(properties, 'iOSHintText', 'hintText', 'color', 'font', 'autocapitalization', 'autocorrect', 'enterKey'));
	}
};

/**
 * apply listeners to widget
 */
var applyListeners = function() {
	if (OS_IOS) {
		$.text_area.addEventListener('change', onChange);
	}
	if (!enterKey && OS_ANDROID) {
		$.text_area.addEventListener('return', function() {
		});
	}
};

var init = function() {
	applyListeners();
	applyProperties(args);
	//gc
	args = null;
};

/**
 * on change text area value
 * @param {Object} e
 */
var onChange = function(e) {
	$.hint_text.visible = (e.value.trim().length == 0);
};

/**
 * get value
 * @return {String}
 */
var getValue = function() {
	return $.text_area.getValue();
};

/**
 * set value
 * @param {String}
 */
var setValue = function(value) {

	if (_.isString(value)) {
		$.text_area.setValue(value);
		if (OS_IOS) {
			onChange({
				value : value
			});
		}
	}
};

init();

/** ---------
 Public
 -------- **/
exports.applyProperties = applyProperties;
exports.addEventListener = $.on;
exports.removeEventListener = $.off;
exports.getValue = getValue;
exports.setValue = setValue;
