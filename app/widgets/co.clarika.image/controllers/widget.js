/*
NightToNight
Created by Juan Ignacio Agu on 2016-09-02.
Copyright 2016 Juan Ignacio Agu. All rights reserved.
*/

/** ---------
 Constants
 -------- **/
var TAG = 'widgets/co.clarika.image';
/** ---------
 Fields
 -------- **/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
/** ---------
 Methods
 -------- **/

/**
 * apply properties to widget
 * @param {Object} properties
 */
var applyProperties = function(properties) {

	if (_.isObject(properties)) {

		if (_.has(properties, 'loader')) {
			_.extend($.loader, properties.loader);
		}

		if (_.has(properties, 'border')) {
			_.extend($.border, properties.border);
		}

		if (_.has(properties, 'image')) {
			if (_.has(properties.image, 'defaultImage') && _.isString(properties.image.defaultImage)) {
				setDefaultImage(properties.image.defaultImage);
			}
			if (_.has(properties.image, 'borderRadius')) {
				$.image.borderRadius = properties.image.borderRadius;
			}
			if (_.has(properties.image, 'image')) {
				setImage(properties.image.image);
			}

			_.extend($.image, _.omit(properties.image, 'image', 'borderRadius', 'defaultImage', 'brokenLinkImage'));
		}

		if (_.has(properties, 'defaultimagebinding')) {
			setDefaultImage(properties.defaultimagebinding);
		}
		if (_.has(properties, 'imagebinding')) {
			setImage(properties.imagebinding);
		}

		_.extend($.widget, _.omit(properties, 'loader', 'border', 'image', 'imagebinding'));
	}
};

/**
 * apply listeners to widget
 */
var applyListeners = function() {
	$.image.addEventListener('load', onLoad);
	$.image.addEventListener('error', onError);
};

var init = function() {
	applyListeners();
	applyProperties(args);
	//gc
	args = null;
	//refresh();
};

var refresh = function() {
	//nothing
};
/** ---------
 Listeners
 -------- **/

/*
 * Fired when either the initial image and/or all of the images in an animation are loaded.
 * @param {Object} e
 */
var onLoad = function(e) {
	$.trigger('load', e);
	$.loader.hide();
};

/*
 * Fired when an image fails to load.
 * @param {Object} e
 */
var onError = function(e) {
	$.trigger('error', e);
};

var setDefaultImage = function(defaultImage) {
	$.image.defaultImage = defaultImage;
	$.image.brokenLinkImage = defaultImage;
	if (OS_ANDROID) {
		$.image.setDefaultImage(defaultImage);
		$.image.setBrokenLinkImage(defaultImage);
	}
};

var setImage = function(image) {
	if (_.isNull(image) || !Alloy.Globals.JsUtils.validateURL(image)) {
		image = Alloy.CFG.constants.mock_url_broken_link_image;
	}

	$.image.image = image;
	if (OS_ANDROID) {
		$.image.setImage(image);
	}

};
init();
/** ---------
 Public
 -------- **/
exports.applyProperties = applyProperties;
exports.addEventListener = $.on;
exports.removeEventListener = $.off;
exports.refresh = refresh;
exports.setDefaultImage = setDefaultImage;
exports.setImage = setImage; 