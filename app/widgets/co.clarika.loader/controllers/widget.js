/** ------------------------
 Fields
 ------------------------**/
var args = arguments[0] || {};
var EVENTS = {
	OPEN : 'loaderIsOpen',
	BACK : 'androidback',
	CLOSE : 'loaderIsClose'
};
var cancelable = true;
/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {
		if (_.has(properties, 'content')) {
			_.extend($.content, properties.content);
		}
		if (_.has(properties, 'activityIndicator')) {
			_.extend($.activityIndicator, properties.activityIndicator);
		}
		if (_.has(properties, 'message')) {
			_.extend($.message, properties.message);
		}

		if (_.has(properties, 'cancelable')) {
			cancelable = properties.cancelable;
		}

		_.extend($.widget, _.omit(properties, 'content', 'activityIndicator', 'message', 'cancelable'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {

	if (OS_ANDROID && !cancelable) {
		$.widget.addEventListener('androidback', function(e) {
			$.trigger(EVENTS.BACK, e);
		});

	}
};

/**
 * initialize controller
 */
var init = function() {
	applyProperties(args);
	applyListeners();
};

/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * when window is opened
 */
var onOpen = function(e) {

	$.trigger(EVENTS.OPEN, _.extend({
		message : $.message
	}, e));
	$.activityIndicator.show();
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	$.trigger(EVENTS.CLOSE, e);
	cleanup();
};

/**
 * open window
 */
var show = function() {
	$.widget.open();
};
/**
 * close window
 */
var hide = function() {
	$.widget.close();
};

/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.cleanup = cleanup;
exports.events = EVENTS;
exports.show = show;
exports.hide = hide;

init({});
