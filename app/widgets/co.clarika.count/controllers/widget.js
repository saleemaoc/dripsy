/*
NightToNight
Created by Juan Ignacio Agu on 2016-09-02.
Copyright 2016 Juan Ignacio Agu. All rights reserved.
*/

/** ---------
 Constants
 -------- **/
var TAG = 'widgets/co.clarika.count';
/** ---------
 Fields
 -------- **/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
/** ---------
 Methods
 -------- **/

/**
 * apply properties to widget
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		if (_.has(properties, 'count')) {
			_.extend($.count, properties.count);
		}

		if (_.has(properties, 'buttonLess')) {
			_.extend($.btn_less, properties.buttonLess);
		}

		if (_.has(properties, 'buttonMore')) {
			_.extend($.btn_more, properties.buttonMore);
		}

		_.extend($.widget, _.omit(properties, 'count', 'buttonLess', 'buttonMore'));
	}
};

/**
 * apply listeners to widget
 */
var applyListeners = function() {
	$.btn_less.addEventListener('click', onClickLess);
	$.btn_more.addEventListener('click', onClickMore);
};

var init = function() {
	applyListeners();
	applyProperties(args);
	//gc
	args = null;
};

var getValue = function() {
	return parseInt($.lbl_count.text);
};

/**
 * set value
 * @param {Integer} value
 */
var setValue = function(value) {
	$.lbl_count.text = value;
};
/** ---------
 Listeners
 -------- **/

/**
 * @param {Object} e
 */
var onClickLess = function(e) {
	var value = getValue();
	if (value > 0) {
		value--;
		$.lbl_count.text = value;
		$.trigger('click:less', {
			value : value
		});
	}

};

/**
 * @param {Object} e
 */
var onClickMore = function(e) {
	var value = getValue();
	value++;
	$.lbl_count.text = value;
	$.trigger('click:more', {
		value : value
	});
};

init();
/** ---------
 Public
 -------- **/
exports.applyProperties = applyProperties;
exports.addEventListener = $.on;
exports.removeEventListener = $.off;
exports.getValue = getValue;
exports.setValue = setValue;
