/*
NightToNight
Created by Juan Ignacio Agu on 2016-09-02.
Copyright 2016 Juan Ignacio Agu. All rights reserved.
*/

/** ---------
 Constants
 -------- **/
var TAG = 'widgets/co.clarika.image.map';
/** ---------
 Fields
 -------- **/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var image = null;
/** ---------
 Methods
 -------- **/

/**
 * apply properties to widget
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		if (_.has(properties, 'loader')) {
			_.extend($.loader, properties.loader);
		}

		if (_.has(properties, 'border')) {
			_.extend($.border, properties.border);
		}

		if (_.has(properties, 'imagebinding')) {
			image = properties.imagebinding;
		}
		if (_.has(properties, 'image')) {
			_.extend($.image, _.omit(properties.image, 'image'));
			if (_.has(properties.image, 'image')) {
				image = properties.image.image;
			}
		}

		_.extend($.widget, _.omit(properties, 'loader', 'border', 'image'));
	}
};

/**
 * apply listeners to widget
 */
var applyListeners = function() {
	if (OS_ANDROID) {
		$.image.addEventListener('load', onLoad);
	}
};

var init = function() {
	applyListeners();
	applyProperties(args);
	//gc
	args = null;
	refresh();
};

var refresh = function() {
	if (image != null) {
		$.loader.show();
		if (OS_ANDROID) {
			if (Alloy.Globals.JsUtils.validateURL(image)) {
				$.image.image = encodeURI(image);
			} else {
				$.image.image = image;
				onLoad();
			}

		} else {
			$.image.backgroundImage = encodeURI(image);
			onLoad();
		}
	}
};
/** ---------
 Listeners
 -------- **/

/*
 * Fired when either the initial image and/or all of the images in an animation are loaded.
 * @param {Object} e
 */
var onLoad = function(e) {
	$.trigger('load', e);
	$.loader.hide();
};

/*
 * Fired when an image fails to load.
 * @param {Object} e
 */
var onError = function(e) {
	$.trigger('error', e);
};

var setDefaultImage = function(image) {
	$.image.image = image;
};
init();
/** ---------
 Public
 -------- **/
exports.applyProperties = applyProperties;
exports.addEventListener = $.on;
exports.removeEventListener = $.off;
exports.refresh = refresh;
exports.setDefaultImage = setDefaultImage;
