/*
NightToNight
Created by Juan Ignacio Agu on 2016-09-02.
Copyright 2016 Juan Ignacio Agu. All rights reserved.
*/

/** ---------
 Constants
 -------- **/
var TAG = 'widgets/co.clarika.button.circle.label';
/** ---------
 Fields
 -------- **/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
/** ---------
 Methods
 -------- **/
/**
 * apply properties to widget
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		if (_.has(properties, 'circle')) {
			_.extend($.circle, properties.circle);
		}

		if (_.has(properties, 'icon')) {
			_.extend($.icon, properties.icon);
		}

		if (_.has(properties, 'label')) {
			_.extend($.label, properties.label);
		}

		_.extend($.widget, _.omit(properties, 'circle', 'icon', 'label'));
	}
};

/**
 * apply listeners to widget
 */
var applyListeners = function() {
};

var init = function() {
	applyListeners();
	applyProperties(args);
	//gc
	args = null;
};

var hide = function() {
	$.widget.visible = false;
};

var show = function() {
	$.widget.visible = true;
};
/** ---------
 Listeners
 -------- **/

/**
 * @param {Object} e
 */
var onClick = function(e) {
	$.trigger('click', e);
};

_.defer(init);
/** ---------
 Public
 -------- **/
exports.applyProperties = applyProperties;
exports.addEventListener = $.on;
exports.removeEventListener = $.off;
exports.hide = hide;
exports.show = show;
