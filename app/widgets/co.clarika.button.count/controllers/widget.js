/*
NightToNight
Created by Juan Ignacio Agu on 2016-09-02.
Copyright 2016 Juan Ignacio Agu. All rights reserved.
*/

/** ---------
 Constants
 -------- **/
var TAG = 'widgets/co.clarika.button.count';
/** ---------
 Fields
 -------- **/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
/** ---------
 Methods
 -------- **/

/**
 * apply properties to widget
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		if (_.has(properties, 'line')) {
			_.extend($.line, properties.line);
		}

		if (_.has(properties, 'count')) {
			_.extend($.count, properties.count);
		}

		if (_.has(properties, 'title')) {
			_.extend($.title, properties.title);
		}

		_.extend($.widget, _.omit(properties, 'line', 'count', 'title'));
	}
};

/**
 * apply listeners to widget
 */
var applyListeners = function() {
	$.widget.addEventListener('click', onClick);
};

var init = function() {
	applyListeners();
	applyProperties(args);
	//gc
	args = null;
};

/** ---------
 Listeners
 -------- **/
var onClick = function(e) {
	$.trigger('click', e);
};

init({});
/** ---------
 Public
 -------- **/
exports.applyProperties = applyProperties;
exports.addEventListener = $.on;
exports.removeEventListener = $.off;
