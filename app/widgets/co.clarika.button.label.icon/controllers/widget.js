/** --------
 Fields
 -------- **/
var args = $.args;

var color = Alloy.Globals.Colors.accents;
var colorSelected = '#00796B';
var uppercase = true;
/** --------
 Methods
 -------- **/

/**
 * apply properties to widget
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		if (_.has(properties, 'uppercase')) {
			uppercase = properties.uppercase;
		}

		if (_.has(properties, 'icon')) {
			_.extend($.icon, properties.icon);
		}

		if (_.has(properties, 'icon_right')) {
			$.icon_right.visible = true;
			$.icon_right.width = '24dp';
			_.extend($.icon_right, properties.icon_right);
		}

		if (_.has(properties, 'label')) {
			_.extend($.label, _.omit(properties.label, 'text'));
			if (_.has(properties.label, 'text')) {

				$.label.text = uppercase ? properties.label.text.toUpperCase() : properties.label.text;
			}
		}

		if (_.has(properties, 'button')) {
			_.extend($.button, properties.button);
		}

		//colors
		if (_.has(properties, 'color')) {
			color = properties.color;
			$.button.backgroundColor = color;
		}

		if (_.has(properties, 'colorSelected')) {
			colorSelected = properties.colorSelected;
		}

		_.extend($.widget, _.omit(properties, 'icon', 'label', 'button', 'color', 'colorSelected', 'uppercase'));
	}
};

var show = function() {
	$.widget.show();
};

var hide = function() {
	$.widget.hide();
};
/** --------
 Listeners
 -------- **/
/**
 * on click button
 * @param {Object} e
 */
var onClick = function(e) {

	$.trigger('click', e);

	$.button.backgroundColor = colorSelected;
	_.delay(function() {
		$.button.backgroundColor = color;
	}, 250);

};

//init
applyProperties(args);
//gc
args = null;
/** --------
 Public
 -------- **/
exports.applyProperties = applyProperties;
exports.addEventListener = $.on;
exports.removeEventListener = $.off;
exports.show = show;
exports.hide = hide;
