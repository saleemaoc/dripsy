/**
 * @author jagu
 * For Andorid add in Manifest :  <uses-permission android:name="android.android.permission.CAMERA"/>
 */

/** ------------------------
 Constants
 ------------------------**/
var TAG = 'com.juanagu.take.picture';
var PATH = Titanium.Filesystem.getApplicationDataDirectory();
var EXTENSION = '.jpg';
var THUMBNAIL_EXTENSION = "_thumbnail";
var parentWindow = null;
var activity = null;
var defaultImage = '';
/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;

var options = {
	showControls : OS_IOS,
	saveToPhotoGallery : true,
	allowEditing : OS_IOS,
	mediaTypes : [Ti.Media.MEDIA_TYPE_PHOTO],
	autohide : true,
	autorotate : true,
	animated : OS_ANDROID,
	success : function(e) {
		onSuccess(e);
	},
	cancel : function(e) {
		onCancel(e);
	},
	error : function(e) {
		onError(e);
	}
};

var thumbnailPath = null;
var imagePath = null;
var imageURL = null;
var name = (require('JsUtils')).getGUID('_');
//image sizes
var imageMaxSize = Alloy.CFG.constants.user_photo_avatar;
var imageWidth = null;
var imageHeight = null;

//thumbnail sizes
var thumbnailMaxSize = Alloy.CFG.constants.user_photo_thumbnail;
var thumbnailWidth = null;
var thumbnailHeight = null;

var editMode = true;
var needDownload = false;
/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {
		if (_.has(properties, 'options')) {
			_.extend(options, properties.options);
		}

		if (_.has(properties, 'name')) {
			name = properties.name;
		}

		if (_.has(properties, 'imagePath')) {
			setImagePath(properties.imagePath);
		}

		if (_.has(properties, 'thumbnailPath')) {
			setThumbnailPath(properties.thumbnailPath);
		}

		if (_.has(properties, 'imageMaxSize')) {
			imageMaxSize = properties.imageMaxSize;
		}

		if (_.has(properties, 'thumbnailMaxSize')) {
			thumbnailMaxSize = properties.thumbnailMaxSize;
		}

		if (_.has(properties, 'editMode')) {
			editMode = properties.editMode;
		}

		if (_.has(properties, 'imageURL')) {
			setImageURL(properties.imageURL);
		}

		if (OS_ANDROID && _.has(properties, 'fab')) {
			$.fab.applyProperties(properties.fab);
		}

		if (_.has(properties, 'image')) {
			_.extend($.image, _.omit(properties.image, 'image', 'defaultImage'));
			if (_.has(properties.image, 'image')) {
				thumbnailPath = properties.image.image;
			}
			if (_.has(properties.image, 'defaultImage')) {
				defaultImage = properties.image.defaultImage;
			}
			refreshImage();
		}

		if (_.has(properties, 'icon')) {
			_.extend($.icon, properties.icon);
		}

		if (_.has(properties, 'content')) {
			_.extend($.content, properties.content);
		}

		if (_.has(properties, 'shadow_top')) {
			_.extend($.shadow_top, properties.shadow_top);
		}

		if (_.has(properties, 'shadow_bottom')) {
			_.extend($.shadow_bottom, properties.shadow_bottom);
		}

		if (OS_IOS && _.has(properties, 'parentWindow')) {
			parentWindow = properties.parentWindow;
		}

		if (OS_ANDROID && _.has(properties, 'activity')) {
			activity = properties.activity;
		}

		_.extend($.widget, _.omit(properties, 'options', 'imagePath', 'thumbnailPath', 'name', 'fab', 'imageMaxSize', 'thumbnailMaxsize', 'editMode', 'parentWindow', 'imageURL', 'fab', 'image', 'icon', 'content', 'shadow_top', 'shadow_bottom', 'activity'));
	}

};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
		parentWindow = null;
		activity = null;
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
	$.image.addEventListener('click', onClickImage);
	if (editMode && OS_ANDROID) {
		$.fab.on('click', onClickCamera);
	}
};

/**
 * initialize controller
 */
var init = function() {
	applyProperties(args);
	applyListeners();
	//gc
	args = null;
	_.delay(updateUI, Alloy.Globals.Integers.PreventDetachedView);
};

/**
 * get height component
 */
var getHeight = function() {
	return $.widget.rect.height;
};
/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * when window is opened
 */
var onOpen = function(e) {
	if (OS_IOS) {
		if (_.isNull(parentWindow)) {
			parentWindow = e.source;
		}
	} else if (OS_ANDROID) {
		$.fab.onOpen(e);
	}
	_.defer(init);
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
	if (OS_ANDROID) {
		$.fab.onClose(e);
	}
};

/**
 * Fired as soon as the device detects a touch gesture.
 * @param {Object} e
 */
var onTouchStart = function(e) {

};

/**
 * when user clicked in camera
 * @param {Object} e
 */
var onClickCamera = _.debounce(function(e) {
	if (editMode) {
		$.trigger('clickCamera', e);
		openOptionDialog();
	}
}, Alloy.Globals.Integers.debounce, true);

/**
 * when user clicked in image
 * @param {Object} e
 */
var onClickImage = _.debounce(function(e) {
	$.trigger('clickImage', e);
	if (_.isString(imagePath) || (_.isString(imageURL) && !_.isEmpty(imageURL))) {
		Widget.createController('win_zoom', {
			image : {
				image : imagePath || imageURL
			}
		}).getView().open();
	}
}, Alloy.Globals.Integers.debounce, true);

/**
 * open option dialog with options camera or gallery
 */
var openOptionDialog = function() {
	var dialog = Titanium.UI.createOptionDialog({
		//title of dialog
		title : L('select_source', 'Select source'),
		//options
		options : [L('gallery', 'Gallery'), L('cancel', 'Cancel')],
		//index of cancel button
		cancel : 1
	});

	dialog.addEventListener('click', onClickOptionDialog);
	dialog.show();
	//gc
	dialog = null;
};

/**
 * when user clicked in option dialog
 * @param {Object} e
 */
var onClickOptionDialog = function(e) {
	if (OS_IOS || Ti.Filesystem.hasStoragePermissions()) {
		switch(e.index) {

		case 0:
			openGallery();
			break;
		}
	} else {
		Ti.Filesystem.requestStoragePermissions(openOptionDialog);
	}
};

/**
 * open device camera
 */
var openCamera = function() {
	var hasCameraPermissions = Ti.Media.hasCameraPermissions();
	if (hasCameraPermissions) {
		Titanium.Media.showCamera(options);
	} else {
		Ti.Media.requestCameraPermissions(function(e) {
			if (e.success) {
				Titanium.Media.showCamera(options);
			}
		});
	}
};

/**
 * open device gallery
 */
var openGallery = function() {
	if (OS_IOS) {
		var hasGalleryPermissions = Ti.Media.hasPhotoGalleryPermissions();
		if (hasGalleryPermissions) {
			Titanium.Media.openPhotoGallery(options);
		} else {
			Ti.Media.requestPhotoGalleryPermissions(function(e) {
				if (e.success) {
					Titanium.Media.openPhotoGallery(options);
				}
			});
		}
	} else {
		var hasCameraPermissions = Ti.Media.hasCameraPermissions();
		if (hasCameraPermissions) {
			Titanium.Media.openPhotoGallery(options);
		} else {
			Ti.Media.requestCameraPermissions(function(e) {
				if (e.success) {
					Titanium.Media.openPhotoGallery(options);
				}
			});
		}
	}

};

/**
 * on success take picture
 * @param {Object} e
 */
var onSuccess = function(e) {
	$.trigger('success', e);
	if (OS_IOS) {
		showLoader();
		_.defer(saveImage, e.media);
	} else {
		crop(e.media);
	}
};

/**
 * on cancel take picture
 * @param {Object} e
 */
var onCancel = function(e) {
	$.trigger('cancel', e);
};

/**
 * on error take picture
 * @param {String} e
 */
var onError = function(e) {
	$.trigger('error', e);
};

var crop = function(media) {

	var imageCropper = require('se.hyperlab.imagecropper');

	imageCropper.open({
		image : media,
		aspect_x : 1,
		aspect_y : 1,
		//max_x : 300,
		//max_y : 300,
		size : imageMaxSize, // for square images with aspect_x = aspect_y = 1
		error : function(e) {
			Ti.API.error('[CROPPING ERROR] ' + e);
		},
		success : function(e) {
			saveImage(e.image);
		},
		cancel : function(e) {
			Ti.API.info('[CROPPING CANCEL] ' + e);
		}
	});
};
/**
 * save image in locale storage and create a thumbnail
 * @param {TiBlob} media
 */
var saveImage = function(media) {
	calculateSize(media.width, media.height);
	if (OS_ANDROID) {
		var f = Titanium.Filesystem.getFile(PATH, name + EXTENSION);
		f.write(media);

		var size = imageHeight >= imageWidth ? imageWidth : imageHeight;
		var nativePath = f.nativePath;
		var ImageFactory = require('fh.imagefactory');
		ImageFactory.rotateResizeImage(nativePath, size, Alloy.CFG.constants.user_photo_quality);
		ImageFactory = null;
		setImagePath(nativePath);
		saveThumbnail(media);
	} else {
		try {

			var ImageFactory = require('ti.imagefactory');

			media = ImageFactory.imageAsResized(media, {
				width : imageWidth,
				height : imageHeight
			});

		} catch(e) {
			Ti.API.error(TAG, 'resize error, ti.imagefactory is required');
		}

		try {
			var f = Titanium.Filesystem.getFile(PATH, name + EXTENSION);
			f.write(media);
			setImagePath(f.nativePath);

			$.trigger('successImage', {
				path : f.nativePath
			});
			saveThumbnail(media);
		} catch(error) {
			Ti.API.error(TAG, 'save image error -> ' + error);
			$.trigger('error', {
				error : error
			});
		}
	}
};

/**
 * resize image and save thumbnail
 * @param {TiBlob} media
 */
var saveThumbnail = function(media) {
	var f = Titanium.Filesystem.getFile(PATH, name + THUMBNAIL_EXTENSION + EXTENSION);
	try {

		var ImageFactory = require('ti.imagefactory');
		f.write(ImageFactory.imageAsResized(media, {
			width : thumbnailWidth,
			height : thumbnailHeight,
			format : ImageFactory.PNG
		}));

		//gc
		ImageFactory = null;

	} catch(e) {

		var resize = (Titanium.UI.createImageView({
				width : thumbnailWidth,
				height : thumbnailHeight,
				image : imagePath,
				autorotate : true,
				defaultImage : 'none'
			})).toImage();

		f.write( OS_ANDROID ? resize.media : resize);

	}

	setThumbnailPath(f.nativePath);

	$.trigger('successThumbnail', {
		path : f.nativePath
	});

	hideLoader();
};

/**
 * calculate size to image and thumbnail
 * @param {Integer} realWidth
 * @param {Integer} realHeight
 */
var calculateSize = function(realWidth, realHeight) {

	//calculate size to image
	var sizeToImage = calculateSizeToMaxSize(realWidth, realHeight, imageMaxSize);
	imageWidth = sizeToImage.width;
	imageHeight = sizeToImage.height;

	//calculate size to thumbnail
	var sizeToThumbnail = calculateSizeToMaxSize(realWidth, realHeight, thumbnailMaxSize);
	thumbnailWidth = sizeToThumbnail.width;
	thumbnailHeight = sizeToThumbnail.height;
};

/**
 *
 * calculate size depending maxSize
 * @param {Integer} realWidth
 * @param {Integer} realHeight
 * @param {Integer} maxSize
 * @return {Object} {width:N,height:M}
 *
 */
var calculateSizeToMaxSize = function(realWidth, realHeight, maxSize) {
	var result = {
		width : realWidth,
		height : realHeight
	};
	//valido si es necesario realizar un resize
	if (realWidth > maxSize || realHeight > maxSize) {
		//si width y height son iguales le seteo a los dos el tamaño maximo permitido
		if (realWidth == realHeight) {
			result.width = maxSize;
			result.height = maxSize;
		}
		//si width es mayor a height seteo width como tamaño mayor y calculo height teniendo en cuenta el porcentaje de width que tuve que reducir
		else if (realWidth > realHeight) {

			var percent = (maxSize * 100) / realWidth;
			result.width = maxSize;
			result.height = (percent * realHeight ) / 100;

		}
		//se realiza el mismo calculo que arriba pero invirtiendo parametros
		else {

			var percent = (maxSize * 100) / realHeight;
			result.height = maxSize;
			result.width = (percent * realWidth ) / 100;

		}
	}
	return result;
};

/**
 * set image url
 * @param {String} url
 */
var setImageURL = function(url) {
	imageURL = url;
	refreshImage();
};

/**
 * set image path
 * @param {String} path
 */
var setImagePath = function(path) {
	imagePath = path;
};

/**
 * set thumbnail path
 * @param {String} path
 */
var setThumbnailPath = function(path) {
	thumbnailPath = path;
	refreshImage();
};

/**
 * get image path
 * @return {String}
 */
var getImagePath = function() {
	return imagePath;
};

/**
 * get thumbnail path
 * @return {String}
 */
var getThumbnailPath = function() {
	return thumbnailPath;
};

/**
 * refresh image in imageView
 */
var refreshImage = function() {
	var image = "";

	if (_.isString(thumbnailPath) && (OS_IOS || Alloy.Globals.JsUtils.validateURL(thumbnailPath))) {
		image = thumbnailPath;
	} else {
		if (_.isString(thumbnailPath) && !_.isEmpty(thumbnailPath)) {
			defaultImage = thumbnailPath;
		}
		image = Alloy.CFG.constants.mock_url_broken_link_image;
	}
	if (OS_ANDROID) {
		$.image.defaultImage = defaultImage;
		$.image.brokenLinkImage = defaultImage;

		$.image.setDefaultImage(defaultImage);
		$.image.setBrokenLinkImage(defaultImage);

		$.image.image = image;

		$.image.setImage(image);
	} else {
		$.image.defaultImage = defaultImage;
		$.image.image = image;
	}
	$.image.height = Ti.UI.SIZE;
	$.image.width = Ti.UI.FILL;
};

/**
 * show loader
 */
var showLoader = function() {
	$.loader.show();
	$.icon.visible = false;
	$.v_download.visible = false;
};

/**
 * hide loader
 */
var hideLoader = function() {
	$.loader.hide();
};

/**
 * update UI
 */
var updateUI = function() {
	if (!editMode && OS_ANDROID) {
		if (OS_ANDROID) {
			$.fab.applyProperties({
				visible : false
			});
		} else if (OS_IOS && !_.isNull(parentWindow)) {
			parentWindow.setRightNavButton(null);
		}

	}
	if (OS_IOS && !_.isNull(parentWindow) && editMode) {
		var btnCamera = Titanium.UI.createButton({
			title : L('take_picture', 'Take picture'),
			systemButton : Ti.UI.iOS.SystemButton.CAMERA,
		});

		btnCamera.addEventListener('click', onClickCamera);
		var currentRightNavButtons = parentWindow.getRightNavButtons();
		currentRightNavButtons.push(btnCamera);
		parentWindow.setRightNavButtons(currentRightNavButtons);
		//gc
		btnCamera = null;

	}
	//gc
	parentWindow = null;
};

var getImageDataByIntent = function(intent) {
	return intent.getBlobExtra("data");

};
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.onOpen = onOpen;
exports.onClose = onClose;
exports.cleanup = cleanup;
exports.getImagePath = getImagePath;
exports.getThumbnailPath = getThumbnailPath;
exports.refreshImage = refreshImage;
exports.getHeight = getHeight;
