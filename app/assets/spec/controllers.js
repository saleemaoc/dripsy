//Setup module to run Behave tests
require('behave').andSetup(this);

var name_controllers = ['win_login', 'partial_login','win_home', 'partial_home', 'partial_menu'];

describe('Create all controllers', function() {
	for (var i = 0,
	    j = name_controllers.length; i < j; i++) {
		var name_controller = name_controllers[i];
		it.eventually('Create controller ' + name_controller, function(e) {
			var controller = Alloy.createController(name_controller);
			expect(controller).notToBe(undefined);
		});
	};
});
