//Setup module to run Behave tests
require('behave').andSetup(this);

var PATH_DAO = 'daos/';
var name_daos = ['UserDao'];

describe('Test Daos', function() {

	for (var i = 0,
	    j = name_daos.length; i < j; i++) {
		var dao_name = name_daos[i];
		it('Test dao ' + dao_name, function() {

			var dao = new (require(PATH_DAO.concat(dao_name)))();

			expect(dao).notToBe(undefined);
			var response = dao.find();
			expect(response).notToBe(undefined);
			expect(response.models).notToBe(undefined);

		});
	};

});
