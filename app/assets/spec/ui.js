//Setup module to run Behave tests
require('behave').andSetup(this);

var generic_test_controllers = ['architecture/win_example'];
describe('Generic test to controllers', function() {

	it('Open controllers', function() {
		for (var i = 0,
		    j = generic_test_controllers.length; i < j; i++) {
			var controller = generic_test_controllers[i];
			Alloy.createController(controller).getView().open();

		};
	});

	/*it.eventually('*** logs in user', function(done) {

	}, 10000);*/
	/*var index = null;

	it('creates a index controller', function() {
	index = Alloy.createController('index');
	expect(index).notToBe(null);
	});

	it('creates a scrollableview for you and me and the rest of the world', function() {
	expect(index.getView("scrollableViewID")).notToBe(undefined);
	});

	*/
	// go nuts! :-)

});
