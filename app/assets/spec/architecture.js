//Setup module to run Behave tests
require('behave').andSetup(this);

var win_controllers = [{
	path : 'win_login'
},{
	path : 'win_home'
}];

var partial_controllers = [{
	path : 'partial_login'
},{
	path : 'partial_home'
}, {
	path : 'partial_menu'
}];

var partial_list_controllers = [];

describe('Win controllers', function() {

	for (var i = 0,
	    j = win_controllers.length; i < j; i++) {
		var win_controller = win_controllers[i];
		var controller = Alloy.createController(win_controller.path);
		it('Checking mandatory public methods ' + win_controller, function() {
			expect(controller.cleanup).notToBe(undefined);
			expect(controller.applyProperties).notToBe(undefined);
		});
	};

});

describe('Partial controllers', function() {

	for (var i = 0,
	    j = partial_controllers.length; i < j; i++) {
		var partial_controller = partial_controllers[i];
		var controller = Alloy.createController(partial_controller.path);
		it('Checking mandatory public methods ' + partial_controller, function() {

			expect(controller.onOpen).notToBe(undefined);
			expect(controller.onClose).notToBe(undefined);
			expect(controller.cleanup).notToBe(undefined);
			expect(controller.applyProperties).notToBe(undefined);

		});
	};

});

/*var index = null;

it('creates a index controller', function() {
index = Alloy.createController('index');
expect(index).notToBe(null);
});

it('creates a scrollableview for you and me and the rest of the world', function() {
expect(index.getView("scrollableViewID")).notToBe(undefined);
});

*/
// go nuts! :-)
