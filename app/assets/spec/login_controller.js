//Setup module to run Behave tests
require('behave').andSetup(this);

describe('Controller Login Test', function() {

	it('Open controllers', function() {
		var login = Alloy.createController('partial_login');
		expect(login).notToBe(null);
		expect(login.validateFields(null, null)).toBe(false);
	});
});