/**
 * @author jagu
 *
 */
/** -----------------------------
 * Constants
 ------------------------------**/
var TAG = 'controllers/partial_user_registration_edition';
/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var genderID = null;

/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {
		if (_.has(properties, 'txt_first_name')) {
			_.extend($.txt_first_name, properties.txt_first_name);
		}

		if (_.has(properties, 'txt_last_name')) {
			_.extend($.txt_last_name, properties.txt_last_name);
		}
		if (_.has(properties, 'txt_email')) {
			_.extend($.txt_email, properties.txt_email);
		}
		_.extend($.partial_user_registration_edition, _.omit(properties, 'txt_first_name', 'txt_last_name', 'txt_email'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
	$.picker_gender.on('change', onChangeGender);
	$.picker_gender.on('click', onClickGender);
};

/**
 * initialize controller
 */
var init = function() {
	applyProperties(args);
	applyListeners();
	applyPropertiesToDatePicker();
	//gc
	args = null;
};

var getLastName = function() {
	var lastName = $.txt_last_name.getValue();
	return lastName;
};

/**
 * @params {String} lastName
 */
var setLastName = function(lastName) {
	$.txt_last_name.setValue(lastName);
};

var getFirstName = function() {
	var firstName = $.txt_first_name.getValue();
	return firstName;
};
/**
 * @params {String} firstName
 */
var setFirstName = function(firstName) {
	$.txt_first_name.setValue(firstName);
};

var getEmail = function() {
	var email = $.txt_email.getValue();
	return email;
};

/**
 * @params {String} email
 */
var setEmail = function(email) {
	$.txt_email.setValue(email);
};

var onChangeGender = function(e) {
	genderID = e.row.id;
	$.trigger("changeGender", {
		gender : genderID
	});
};

var getGender = function() {
	return genderID;
};

/**
 * @params {Integer} value
 */
var setGender = function(value) {
	genderID = value;
	$.picker_gender.applyProperties({
		options : [{
			title : L('gender', 'Gender')
		}, {
			id : Alloy.CFG.constants.gender.male,
			title : L('male', 'Male')
		}, {
			id : Alloy.CFG.constants.gender.female,
			title : L('female', 'Female')
		}],
		selectedByID : genderID
	});
};

var getDateBirth = function() {
	var dateBirth = $.date_birth.getValue();
	return dateBirth;
};

var getDateBirthIsChange = function() {
	return $.date_birth.getIsChange();
};
/**
 * @params {String} dateBirth
 */
var setDateBirth = function(dateBirth) {
	$.date_birth.setValue(dateBirth);
};

var applyPropertiesToDatePicker = function() {
	$.date_birth.applyProperties({
	});
};

var hideKeyboard = function() {
	$.txt_first_name.blur();
	$.txt_last_name.blur();
	$.txt_email.blur();
};

var disableEmail = function() {
	$.txt_email.editable = false;
};
/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * when window is opened
 */
var onOpen = function(e) {
	if(OS_IOS){
		init(e);
	}else{
		_.defer(init, e);
	}
	$.picker_gender.onOpen(e);
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
};

var onClickGender = function() {
	if (OS_IOS) {
		$.trigger('hidekeyboard', {});
		hideKeyboard();
	}
};
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.onOpen = onOpen;
exports.onClose = onClose;
exports.cleanup = cleanup;
exports.getLastName = getLastName;
exports.getFirstName = getFirstName;
exports.getEmail = getEmail;
exports.getGender = getGender;
exports.getDateBirth = getDateBirth;
exports.getDateBirthIsChange = getDateBirthIsChange;
exports.setFirstName = setFirstName;
exports.setLastName = setLastName;
exports.setEmail = setEmail;
exports.setGender = setGender;
exports.setDateBirth = setDateBirth;
exports.hideKeyboard = hideKeyboard;
exports.disableEmail = disableEmail;
