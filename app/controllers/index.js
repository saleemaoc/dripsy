if ((require('AppProperties')).getUserIsLogged()) {
	Alloy.Globals.AppRoute.openHome();
} else {
	Alloy.Globals.AppRoute.openLogin();
}
Alloy.Globals.PushWooshHelper.configure();
Alloy.Globals.PushWooshHelper.initialize();

