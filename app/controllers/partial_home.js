/**
 * @author jagu
 *
 */
/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
/**
 * 1: List
 * 2: Map
 */
var viewType = 1;
var currentController = null;
var currentPage = 0;
/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		if (_.has(properties, 'viewType')) {
			viewType = properties.viewType;
		}

		_.extend($.partial_home, _.omit(properties, 'viewType'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
	} catch(e) {
	}
};
/**
 * apply listeners to controller
 */
var applyListeners = function() {
};

/**
 * initialize controller
 */
var init = function() {
	applyProperties(args);
	applyListeners();
	//gc
	args = null;
	getCurrentPosition();
};

var getCurrentPosition = function() {
	'use strict';
	if (OS_ANDROID) {
		// Use library to handle run-time permissions
		(require('permissions')).requestLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE, function(e) {

			if (e.success) {
				// In some cases the library will already have displayed a dialog, in other cases we receive a message to alert
				// Get our current position
				Ti.Geolocation.getCurrentPosition(function(e) {

					// FIXME: https://jira.appcelerator.org/browse/TIMOB-19071
					if (e.success && !e.error) {
						(require('LocationUtils')).setCoords(e.coords);
					}
					refreshView();

				});
			} else {
				refreshView();
			}

		});
	} else {

		var versionIOS = Ti.Platform.version.split(".")[0];
		versionIOS = parseInt(versionIOS);
		if (versionIOS == 11) {
			//FIXME: https://jira.appcelerator.org/browse/TIMOB-25322
			refreshView();
		} else {
			// FIXME: https://jira.appcelerator.org/browse/TIMOB-19071
			Ti.Geolocation.getCurrentPosition(function(e) {
				if (e.success && !e.error) {
					(require('LocationUtils')).setCoords(e.coords);
				}
				refreshView();
			});
		}
	}
};

var setGlobals = function() {
	Alloy.Globals.CurrentDate = function() {
		return new Date();
	};
	Alloy.Globals.CurrentDay = function() {
		return (require('JsUtils')).getDayStringToDate(Alloy.Globals.CurrentDate());
	};

};

/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * when window is opened
 */
var onOpen = function(e) {
	if (OS_IOS) {
		init(e);
	} else {
		_.defer(init, e);
	}
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
};

/**
 * on resume view
 * @param {Object} e
 */
var onResume = function(e) {
	setGlobals();
};

var refreshView = function() {

	$.partial_home.removeAllChildren();
	Ti.App.fireEvent('closekeyboard');
	var params = {
		currentPage : currentPage
	};

	if (!_.isNull(currentController)) {
		params.collectionBars = currentController.getCollectionBars();
		params.collectionNightclubs = currentController.getCollectionNightclubs();

	}

	currentController = Alloy.createController(viewType == 1 ? 'partial_establishment_list_mode' : 'partial_establishment_map_mode', params);
	currentController.on('scrollend', onScrollend);
	$.partial_home.add(currentController.getView());

	currentController.onOpen({});
};

/**
 * @param {Integer} type
 */
var setViewType = function(type) {
	viewType = type;
	refreshView();
};

/**
 * on search view
 * @param {Object} e
 */
var onSearch = function(e) {
	if (!_.isNull(currentController) && _.isFunction(currentController.onSearch)) {
		currentController.onSearch(e);
	}

};

/**
 * @param {Object} e
 */
var onScrollend = function(e) {
	currentPage = e.currentPage;
	//clear search value
	$.trigger('changetab', e);
};
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.onOpen = onOpen;
exports.onClose = onClose;
exports.cleanup = cleanup;
exports.setViewType = setViewType;
exports.onSearch = onSearch;
exports.onResume = onResume;
