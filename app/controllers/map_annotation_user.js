// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
/* --------
 Constants
 -------- */
var TAG = 'controllers/map_annotation_user';

/* --------
Methods
-------- */

/**
 * apply properties
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		if (_.has(properties, 'image')) {
			$.image.applyProperties({
				image : properties.image
			});
			$.image.refresh();
		}
		if (_.has(properties, 'border')) {
			_.extend($.border, properties.border);
		};

		if (_.has(properties, 'border_transparent')) {
			_.extend($.border_transparent, properties.border_transparent);
		};

		_.extend($.map_annotation_user, _.omit(properties, 'image', 'border', 'border_transparent'));

	}
};

var applyListeners = function() {
	$.image.on('load', function(e) {
		$.trigger('load', e);
	});
};

/**
 * hide component
 */
var hide = function() {
	$.image.applyProperties({
		visible : false
	});
	$.border_transparent.hide({
		animated : OS_IOS
	});
	$.map_annotation_user.hide({
		animated : OS_IOS
	});
};

//init
applyListeners();
applyProperties(args);

//gc
args = null;

/* --------
 Public
 -------- */
exports.applyProperties = applyProperties;
exports.hide = hide;
