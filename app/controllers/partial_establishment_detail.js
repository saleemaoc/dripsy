/**
 * @author jagu
 *
 */

/** ------------------------
 Constants
 ------------------------**/
var TAG = 'controllers/partial_establishment_detail';
/**
 * Delays
 */
var DELAY_GET_IMAGES = 1000;
var DELAY_GET_ASSISTANT = 500;
var DELAY_SEND_RANKING = 500;

/** ------------------------
 Fields
 ------------------------**/

/**
 * Arguments passed into this controller can be accessed via the `$.args` object directly or:
 */
var args = $.args;
/**
 * establishment model reference
 */
var establishment = null;
/**
 * establishment id
 */
var establishmentID = null;
/**
 * flag to indicat if user here in establishment
 */
var imHere = false;
/**
 * flag to indicate if user going to establishment
 */
var imGoing = false;
/**
 * flag to check is user filled the gender before click on "I'm going/I'm Here"
 * surinder
 */
var hasGender = false;
/**
 * http library
 */
var httpCK;
/**
 * store de user ranking
 */
var userRanking;
/**
 * timeout to send ranking
 */
var timeoutSendRanking = null;
/**
 * store user presence
 */
var userPresence = null;
/**
 * assist type
 */

var typeAssist = 0;
/**
 * reference to loader
 */
var loader;

var imagesDownload = false;
/** ------------------------
 Dependencies
 ------------------------**/

/**
 * return establishment dependencie to service
 * @return {EstablishmentService}
 */
var getEstablishmentService = function() {
	return Alloy.Globals.ServiceBuilder.getEstablishmentService();
};

/**
 * return establishment dao
 */
var getEstablishmentDao = function() {
	return Alloy.Globals.DaoBuilder.getEstablishmentDao();
};
/**
 * service to manage establishment images
 * @return {EstablishmentImageService}
 */
var getImageService = function() {
	return Alloy.Globals.ServiceBuilder.getEstablishmentImageService().inject(getImageDao());
};

/**
 * image to manage establishment images
 * @return {EstablishmentImageDao}
 */
var getImageDao = function() {
	return Alloy.Globals.DaoBuilder.getEstablishmentImageDao();
};

/**
 * service to manage user presence
 * @return {UserPresenceService}
 */
var getUserPresenceService = function() {
	return require('services/UserPresenceService');
};

/**
 * dao to manager user presence
 * @return {UserPresenceDao}
 */
var getUserPresenceDao = function() {
	return new (require('daos/UserPresenceDao'))();
};

/**
 * dao for user [Surinder]
 */
var getUserDao = function(){
	return Alloy.Globals.DaoBuilder.getUserDao();
};
var getUserService = function() {
	return Alloy.Globals.ServiceBuilder.getUserService();
};

/** ------------------------
 Methods
 ------------------------**/

/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		if (_.has(properties, 'establishment')) {
			establishment = properties.establishment;
		}

		if (_.has(properties, 'establishmentID')) {
			establishmentID = properties.establishmentID;
		}

		_.extend($.partial_establishment_detail, _.omit(properties, 'establishment', 'establishmentID'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
		Ti.Network.removeEventListener('change', onNetwork);
		if (httpCK) {
			httpCK.abort();
		}
	} catch(e) {
	}
};
/**
 * apply listeners to controller
 */
var applyListeners = function() {
	$.ranking.on('change', onChangeRate);
	$.btn_women.on('click', onClickWomen);
	$.btn_men.on('click', onClickMen);
	$.btn_contact_info.on('click', onContactInfo);
	$.btn_price_list.on('click', onPriceList);
	//$.btn_take_me.on('click', onTakeMe);
	$.partial_establishment_detail.addEventListener('scroll', onScroll);
	$.v_description.addEventListener('click', onContactInfo);
	$.v_error_images.addEventListener('click', getImages);
	Ti.Network.addEventListener('change', onNetwork);
};

var beforeOpen = function() {
	var buttons = [$.btn_contact_info, $.btn_price_list/*, $.btn_take_me*/];

	buttons.forEach(function(btn) {
		btn.applyProperties({
			width : Alloy.Globals.Dimens.establishment_detail_buttons_width()
		});
	});
};
/**
 * initialize controller
 */
var init = function() {
	applyProperties(args);
	applyListeners();
	//gc
	args = null;
	//create http instance
	httpCK = new (require('HttpCK'))();
	//set data to establishment
	_.delay(setData, Alloy.Globals.Integers.SetDataDelay);
	//refersh data to service
	refreshInformationToService();
};

/**
 * set data
 */
var setData = function() {

	if (_.isNumber(establishmentID) && !_.isObject(establishment)) {
		//find establishment by id
		establishment = getEstablishmentDao().findByID(establishmentID);
	}

	if (_.isObject(establishment)) {

		//refresh data
		$.establishment.fetch({
			id : establishment.id
		});
		//gc
		establishment = null;

		isOpen = $.establishment.isOpenToday();
		refreshAssistantUI();
		//apply properties to ranking component
		$.ranking.setValue($.establishment.get('user_ranking'));

		userRanking = $.establishment.get('user_ranking');
		configureUI();

	}

};

/**
 * configure UI
 */
var configureUI = function() {

	if (!($.establishment.getShowPriceList() == 1) || !isOpen || !$.establishment.isStripeConnected()) {
		$.v_content_buttons.remove($.btn_price_list.getView());
	}

	if (($.establishment.getAllowSalesTicket() == 1)) {
		$.btn_ticket.show({
			animated : OS_IOS
		});

	} else {
		$.btn_ticket.height = 0;
	}

	if (!$.establishment.notOpenToday()) {
		$.v_assistant.show({
			animated : OS_IOS
		});

	} else {
		$.v_assistant.height = 0;
	}

	$.v_content_buttons.show({
		animated : OS_IOS
	});

};
/**
 * refresh assistant ui components
 */
var refreshAssistantUI = function() {
	//apply value for female count button
	$.btn_women.applyProperties({
		count : {
			text : $.establishment.get('female_count')
		}
	});
	//apply value for male count button
	$.btn_men.applyProperties({
		count : {
			text : $.establishment.get('male_count')
		}
	});
};
/**
 * update ui for buttons going and im here
 */
var updateUserPresenceUI = function() {

	//Im here
	if (imHere) {

		$.removeClass($.btn_im_a_here, 'button_transparent');
		$.addClass($.btn_im_a_here, 'button_tertiary button_two_in_line right button_font_small');

	} else {

		$.removeClass($.btn_im_a_here, 'button_tertiary');
		$.addClass($.btn_im_a_here, 'button_transparent button_two_in_line right button_font_small');

	}

	//Im going
	if (imGoing) {

		$.removeClass($.btn_going, 'button_transparent');
		$.addClass($.btn_going, 'button_tertiary button_two_in_line left button_font_small');

	} else {

		$.removeClass($.btn_going, 'button_tertiary');
		$.addClass($.btn_going, 'button_transparent button_two_in_line left button_font_small');

	}
};

/**
 * @param {Function} success
 */
var onQuestionAssist = function(type, success) {
	typeAssist = type;
	//Descomentar esto cuando se quiera aplicar la preguntar de visibilidad de usuario y borrar el success del final
	/*var dialog = Titanium.UI.createAlertDialog({
	 message : L('establishment_assist_question', 'Show matches your assistance in place?'),
	 buttonNames : [L('cancel', 'Cancel'), L('yes', 'Yes'), L('no', 'No')],
	 cancel : 0
	 });

	 dialog.addEventListener('click', function(e) {
	 switch(e.index) {
	 case 1:
	 case 2:
	 success({
	 type : type,
	 show : (e.index == 1)
	 });
	 break;
	 }
	 });
	 dialog.show();
	 //gc
	 dialog = null;*/
	success({
		type : typeAssist,
		show : true
	});
};

/**
 * refresh male count, female count, images and user asssist.
 */
var refreshInformationToService = function() {
	showImageLoader();
	//prevent much work on the main thread
	_.delay(getImages, DELAY_GET_IMAGES);
	_.delay(getAssistant, DELAY_GET_ASSISTANT);
	
	// Check user filled the gender [Surinder] 	
	var userDao = getUserDao().findByID(Alloy.Globals.UserID);
	if( userDao.getGender() == 1 || userDao.getGender() == 2 ){
		hasGender = userDao.getGender();
	}
};

var getImageParams = function() {
	return {
		establishment_id : $.establishment.get('id')
	};
};
/**
 * get images to service
 */
var getImages = function() {
	if (Ti.Network.online) {
		showImageLoader();
		getImageService().get(getImageParams(), onSuccessGetImages, onErrorGetImages, httpCK);
	}
};

/**
 * on success get images to service
 * @param {Object} response
 */
var onSuccessGetImages = function(response) {
	updateImages();
	imagesDownload = true;
};

/**
 *on error get images to service
 * @param {Object} error
 */
var onErrorGetImages = function(error) {
	showErrorImages();
};
var showErrorImages = function() {
	hideImageLoader();
	$.v_error_images.visible = true;

};

var updateImages = function() {
	$.v_error_images.visible = false;
	getImageDao().findByEstablishmentID($.establishment.get('id'), true, onSuccessUpdateImages, onErrorUpdateImages, $.collection_establishment_images);
};

/**
 * on success update images to database
 * @param {Object} response
 */
var onSuccessUpdateImages = function(response) {
	hideImageLoader();
};

/**
 * on error update images to database
 * @param {String} error
 */
var onErrorUpdateImages = function(error) {
	hideImageLoader();
	showErrorImages();
};

/**
 * show loader_image and hide scrollableview with images
 */
var showImageLoader = function() {
	$.loader_images.show();
	$.scrollable_view_images.visible = false;
	$.v_error_images.visible = false;
};

/**
 * hide loader_image and show scrollableview with images
 */
var hideImageLoader = function() {
	$.loader_images.hide();
	$.scrollable_view_images.show();
};

/**
 * get assistants to establishment
 */
var getAssistant = function() {

	var establishmentID = $.establishment.get('id');

	var params = {
		id : establishmentID,
	};
	getUserPresenceService().getAssistant(params, onSuccessGetAssistant, onErrorGetAssistant, httpCK);
};

/**
 * on success get assistant
 * @param {Object} response
 */
var onSuccessGetAssistant = function(response) {
	$.establishment.updateAssistant(response.male_count, response.female_count);
	refreshAssistantUI();
	updateUserPresence(response.user_presence);
};

/**
 * on error get assistant
 * @param {Object} error
 */
var onErrorGetAssistant = function(error) {
	(require('TiLog')).error(TAG, 'onErrorGetAssistant -> ' + JSON.stringify(error));
};

/**
 * update local user presence by service
 * @param {Object} userPresenceService
 */
var updateUserPresence = function(userPresenceService) {
	if (_.isObject(userPresenceService)) {
		userPresence = getUserPresenceDao().updateData(userPresenceService);
	} else {
		userPresence = null;
	}

	imHere = false;
	imGoing = false;

	if (_.isObject(userPresence)) {

		switch(userPresence.get('type_presence')) {
		case Alloy.CFG.constants.user_presence.type_presence.here:
			imHere = true;
			break;
		case Alloy.CFG.constants.user_presence.type_presence.going:
			imGoing = true;
			break;
		}
	}

	updateUserPresenceUI();
};
/**
 * create a timeout with delay to prevent send multiples request in short time
 */
var notifyRanking = function() {
	if (!_.isNull(timeoutSendRanking)) {
		clearTimeout(timeoutSendRanking);
		timeoutSendRanking = null;
	}
	timeoutSendRanking = setTimeout(sendUserRanking, DELAY_SEND_RANKING);
};

/**
 * send to service user ranking
 */
var sendUserRanking = function() {
	var params = {
		ranking : userRanking,
		establishment_id : $.establishment.get('id'),
	};
	getEstablishmentService().sendUserRanking($.establishment.get('user_ranking_id'), params, onSuccessSendUserRanking, onErrorSendUserRanking, httpCK);
};

/**
 * on succcess send user ranking
 * @param {Object} response
 */
var onSuccessSendUserRanking = function(response) {
	//only persist to database
	$.establishment.updateUserRanking(response.id, userRanking);
};

/**
 * on error send user ranking
 * @param {Object} error
 */
var onErrorSendUserRanking = function(error) {

	(require('TiLog')).error(TAG, 'onErrorPostUserRanking -> ' + JSON.stringify(error));

	//revert user ranking persist
	userRanking = $.establishment.get('user_ranking');
	$.ranking.setValue(userRanking, false);

};

/**
 * show loader
 */
var showLoader = function(params) {
	loader = Alloy.createWidget('co.clarika.loader', params || {});
	loader.show();
};

/**
 * hide loader
 */
var hideLoader = function() {
	if (loader) {
		loader.hide();
		loader = null;
	}

};

/**
 * remove assist to establishment
 */
var removeAssist = function() {
	if (Titanium.Network.online) {
		if (_.isObject(userPresence)) {
			showLoader({
				message : {
					text : L('delete_your_assistance', 'Delete your assistance...'),
				}
			});
			getUserPresenceService().destroy(userPresence.get('id'), onSuccessDeleteAssist, onErrorDeleteAssist, httpCK);
		} else {
			removeLocalAssist();
		}
	} else {
		(require('TiUtils')).toast(L('error_network_connection_no_internet', "Oops! looks like you don't have Internet connection."));
	}
};

/**
 * remove local referente to user presence
 */
var removeLocalAssist = function() {
	userPresence = null;
	imHere = false;
	imGoing = false;
	updateUserPresenceUI();
};

/**
 * when user delete successfully your assist
 * @param {Object} response
 */
var onSuccessDeleteAssist = function(response) {
	removeLocalAssist();
	hideLoader();
	getAssistant();
};

/**
 * on error delete assist
 * @param {Object} error
 */
var onErrorDeleteAssist = function(error) {
	(require('TiLog')).error(TAG, 'onErrorRemove -> ' + JSON.stringify(error));
	(require('TiUtils')).toast(L('error_delete_assist', 'An error occurred deleting assistance'));
	hideLoader();
};

/**
 * on success assist
 * @param {Object} response
 */
var onSuccessAssist = function(response) {
	hideLoader();
	updateUserPresence(response);
	//update establishment assistant
	getAssistant();
};

/**
 * on error assist
 * @param {Object} error
 */
var onErrorAssist = function(error) {
	(require('TiLog')).error(TAG, 'onErrorAssist -> ' + JSON.stringify(error));
	hideLoader();
	switch(error.code) {
	case Alloy.CFG.Http.AssistExistOtherEstablishment:
		showQuestionToForceAssist(error.message);
		break;
	default:
		(require('TiUtils')).toast(L('error_send_assist', 'An error occured when submitting your assistance'));
		break;
	}

};

/**
 * show a dialog with question to force assist
 * @param {String} currentEstablishmentName
 */
var showQuestionToForceAssist = function(currentEstablishmentName) {

	var dialog = Ti.UI.createAlertDialog({
		cancel : 1,
		buttonNames : [L('yes', 'Yes'), L('no', 'No')],
		title : L('change_establishment_assist', 'Change venue assistance'),
		message : String.format(L('change_establishment_assist_question', "Did you leave %s and are you in %s?\nYou'll only be able to receive gifts for %s."), currentEstablishmentName.replace(/[^\w\s]/gi, ''), $.establishment.getName(), $.establishment.getName())
	});

	dialog.addEventListener('click', function onShowQuestionToForceAssistClick(e) {
		if (e.index == 0) {
			changeForceEstablishmentAssist();
		}
	});
	dialog.show();
	dialog = null;
};

var changeForceEstablishmentAssist = function() {
	sendAssist({
		type : typeAssist,
		show : true,
		force : true
	});
};

/**
 * on choose the gender into dialog option
 */
var flagClickedBtn = false;
var onChooseGender = function(evt){
	// 	Index [0:Male,1:Female]
	console.log('You choosed: ', evt.index);
	if( evt.index == 0 || evt.index == 1 ){
		var userDao = getUserDao().findByID(Alloy.Globals.UserID);
		
		var params = {
			first_name : userDao.getFirstName(),
			last_name : userDao.getLastName(),
			email : userDao.getMail(),
			gender : evt.index+1,
			date_birth: userDao.getDateBirth()
		};
		
		if(OS_ANDROID){
			Ti.UI.Android.hideSoftKeyboard();
		}	
		showLoader();
		console.log('UserID: ', Alloy.Globals.UserID);
		console.log('Params: ', params );
		getUserService().edit(Alloy.Globals.UserID, params, function(){
			// Called onSuccess 
			hideLoader();		
			userDao.setGender(params.gender);
			hasGender = params.gender;
			console.log('success');
			if(flagClickedBtn == 'onGoing'){
				onGoing();
			}
			if(flagClickedBtn == 'onImHere'){
				onImHere();
			}
		}, function(error){
			hideLoader();
			console.log('error', error);
			// Called onError
		}, httpCK);	
	};
};

/**
 * on going
 */
var onGoing = _.debounce(function() {
	if (Titanium.Network.online) {
		if(!hasGender){
			flagClickedBtn = 'onGoing';
			$.gender_dialog.show();
		} else if (!imGoing) {
			onQuestionAssist(Alloy.CFG.constants.user_presence.type_presence.going, sendAssist);
		} else {
			removeAssist();
		}
	} else {
		(require('TiUtils')).toast(L('error_network_connection_no_internet', "Oops! looks like you don't have Internet connection."));
	}
}, Alloy.Globals.Integers.debounce, true);

/**
 * open app to transport user to establishment
 */
var openTransportToEstablishment = function() {

	var pickup = {
		latitude : Alloy.Globals.CoordLatitude,
		longitude : Alloy.Globals.CoordLongitude,
		nickname : Alloy.Globals.UserLogged.getNickName()
	};

	var dropoff = {
		latitude : $.establishment.getLatitude(),
		longitude : $.establishment.getLongitude(),
		nickname : $.establishment.getName(),
		formatted_address : $.establishment.getFullAddress()
	};

	if ((require('TaxiServicesHelper')).isUberEnabled()) {
		(require('Uber')).setPickup(pickup, dropoff);
	} else {
		(require('TiUtils')).toast(L('taxi_services_disabled_by_city', 'There are no services available for the actual city'));
	}
};
/**
 * get current location
 */
var getLocation = function() {

	showLoader({
		message : {
			text : L('getting_your_location', 'Getting your location...')
		}
	});

	// Use library to handle run-time permissions
	(require('permissions')).requestLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE, function(e) {

		if (e.success) {
			// In some cases the library will already have displayed a dialog, in other cases we receive a message to alert
			// Get our current position
			Ti.Geolocation.getCurrentPosition(function(e) {

				// FIXME: https://jira.appcelerator.org/browse/TIMOB-19071
				if (e.success && !e.error) {
					(require('LocationUtils')).setCoords(e.coords);
					openTransportToEstablishment();
				} else if ((require('LocationUtils')).hasLocation()) {
					(require('TiUtils')).toast(L('error_get_exact_location_take_me', 'We could not get your current location exact'));
					openTransportToEstablishment();
				} else {
					(require('TiUtils')).toast(L('error_get_location_take_me', 'error getting your location'));

				}
				_.delay(hideLoader, Alloy.Globals.Integers.PreventDetachedView);

			});
		} else {
			_.delay(hideLoader, Alloy.Globals.Integers.PreventDetachedView);
			(require('TiUtils')).toast(L('error_get_location_take_me', 'error getting your location'));

		}

	});
};

/**
 * send assist to server
 * @param {Object} params {show:true or false, type: 1 or 2}}
 */
var sendAssist = _.debounce(function(params) {

	//create params
	var id = null;
	if (_.isObject(userPresence)) {
		id = userPresence.get('id');
	}

	params.user_id = Alloy.Globals.UserID;
	params.establishment_id = $.establishment.get('id');
	params.type_presence = params.type;

	//gc
	params.type = null;

	showLoader({
		message : {
			text : L('sending_assistance', 'Sending assistance...')
		}
	});
	if (params.force) {
		getUserPresenceService().force(id, params, onSuccessAssist, onErrorAssist, httpCK);
	} else {
		//send to server
		getUserPresenceService().add(id, params, onSuccessAssist, onErrorAssist, httpCK);
	}

}, Alloy.Globals.Integers.debounce, true);

var scrollToBottom = function() {
	$.partial_establishment_detail.scrollToBottom();
};
/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * when window is opened
 */
var onOpen = function(e) {
	if (OS_IOS) {
		init(e);
	} else {
		_.defer(init, e);
	}
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
};

/**
 * on im here
 */
var onImHere = _.debounce(function(e) {
	if (Titanium.Network.online) {
		if(!hasGender){
			flagClickedBtn = 'onImHere';
			$.gender_dialog.show();
		}else if (!imHere) {
			if (isOpen) {
				onQuestionAssist(Alloy.CFG.constants.user_presence.type_presence.here, sendAssist);
			} else {
				(require('TiUtils')).toast(L('error_establishment_not_open', 'The establishment is not yet open'));
			}
		} else {
			removeAssist();
		}
	} else {
		(require('TiUtils')).toast(L('error_network_connection_no_internet', "Oops! looks like you don't have Internet connection."));
	}
}, Alloy.Globals.Integers.debounce, true);

/**
 * open buy ticket
 */
var onBuyTicketTable = _.debounce(function(e) {

	var establishmentID = $.establishment.get('id');
	var subtitle = $.establishment.getName();

	Alloy.Globals.AppRoute.openBuyTicket({
		establishmentID : establishmentID,
		subtitle : subtitle
	});

}, Alloy.Globals.Integers.debounce, true);

/**
 * Fire when user change rate
 * @param {Object} e
 */
var onChangeRate = function(e) {
	//temporally user ranked
	if (e.value != userRanking && e.value != $.establishment.get('user_ranking')) {
		userRanking = e.value;
		//abort if exist any request
		if (_.isObject(httpCK)) {
			httpCK.abortByTag(getEstablishmentService().TAGS.TAG_SEND_RANKING);
		}
		notifyRanking();
	}

};

/**
 * open women list
 */
var onClickWomen = _.debounce(function(e) {
	if ($.establishment.get('female_count') > 0) {

		var establishmentID = $.establishment.get('id');

		Alloy.Globals.AppRoute.openUsersPresence({
			title : L('women', 'Women'),
			subtitle : $.establishment.get('name'),
			gender : Alloy.CFG.constants.gender.female,
			establishmentID : establishmentID
		});
	}
}, Alloy.Globals.Integers.debounce, true);

/**
 * open men list
 */
var onClickMen = _.debounce(function(e) {
	if ($.establishment.get('male_count') > 0) {

		var establishmentID = $.establishment.get('id');
		var subtitle = $.establishment.getName();

		Alloy.Globals.AppRoute.openUsersPresence({
			title : L('men', 'Men'),
			subtitle : subtitle,
			gender : Alloy.CFG.constants.gender.male,
			establishmentID : establishmentID
		});
	}
}, Alloy.Globals.Integers.debounce, true);

/**
 * open contact info
 */
var onContactInfo = _.debounce(function(e) {

	var subtitle = $.establishment.getName();

	Alloy.Globals.AppRoute.openContactInfo({
		establishment : $.establishment,
		subtitle : subtitle
	});
}, Alloy.Globals.Integers.debounce, true);

/**
 * open price list
 */
var onPriceList = _.debounce(function(e) {

	var establishmentID = $.establishment.get('id');
	var subtitle = $.establishment.getName();

	var priceListController = Alloy.Globals.AppRoute.openPriceList({
		subtitle : subtitle,
		establishmentID : establishmentID,
	});
	priceListController.on('transaction:success', onSuccessTransaction);

	//gc
	priceListController = null;

}, Alloy.Globals.Integers.debounce, true);

/**
 * Fired when user scroll
 * @param {Object} e
 */
var onScroll = function(e) {
	$.trigger('scroll', e);
};

/**
 * When user click take me
 * @param {Object} e
 */
var onTakeMe = _.debounce(function(e) {
	getLocation();
}, Alloy.Globals.Integers.debounce, true);

/**
 * on success transaction
 * @param {Object} e
 */
var onSuccessTransaction = function(e) {
	var transactionID = e.transactionID;
	Alloy.Globals.AppRoute.openTransactionDetail({
		transactionID : transactionID
	});
};

var onNetwork = function() {
	if (Ti.Network.online) {
		refreshInformationToService();
	} else {
		hideImageLoader();
		if (!imagesDownload) {
			showErrorImages();
		}
	}
};

beforeOpen();
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.onOpen = onOpen;
exports.onClose = onClose;
exports.cleanup = cleanup;
exports.onBuyTicketTable = onBuyTicketTable;