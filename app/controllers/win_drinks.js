/** ------------------------
 Fields
 ------------------------**/
/**
 * Arguments passed into this controller can be accessed via the `$.args` object directly or:
 */
var args = $.args;
/**
 * subtitle
 */
var subtitle = '';
/**
 * count drink to cart
 */
var countDrinks = 0;

/**
 * to send gift
 */
var gift = null;
/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {
		if (_.has(properties, 'subtitle')) {
			subtitle = properties.subtitle;
		}
		if (_.has(properties, 'establishmentID')) {
			establishmentID = properties.establishmentID;

			$.drinks.applyProperties({
				establishmentID : establishmentID
			});
		}

		if (_.has(properties, 'gift')) {
			gift = properties.gift;
		}

		_.extend($.win_drinks, _.omit(properties, 'establishmentID', 'gift'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
	$.drinks.on('update:toolbar', onUpdateToolbar);
	if (OS_IOS) {
		$.search.addEventListener('change', onSearch);
	}
	if (OS_ANDROID) {
		$.win_drinks.addEventListener('androidback', onBack);
	}
};

/**
 * initialize controller
 */
var init = function(e) {
	applyProperties(args);
	//gc
	args = null;
	configureToolbar(e);
	applyListeners();
	$.drinks.init(e);

};
/**
 * configure toolbar
 */
var configureToolbar = function(e) {
	if (OS_ANDROID) {
		var activity = $.win_drinks.getActivity();
		activity.onCreateOptionsMenu = onCreateOptionsMenu;
		activity.invalidateOptionsMenu();
		activity.getActionBar().setDisplayShowHomeEnabled(true);
		activity.getActionBar().displayHomeAsUp = true;
		activity.getActionBar().onHomeIconItemSelected = onHomeIconItemSelected;
		activity.getActionBar().setSubtitle(subtitle);

		activity = null;
	} else {
		configureNavButtons();
	}
};

/**
 * configure nav bar for ios
 */
var configureNavButtons = function() {

	var rightNavButton = Ti.UI.createButton({
		title : L('buy', 'Buy'),
		image : getIconChartWithBadge()
	});

	rightNavButton.addEventListener('click', onCart);
	$.win_drinks.setRightNavButton(rightNavButton);

	//gc
	rightNavButton = null;
};

var onBack = function() {
	$.win_drinks.close();
};

if (OS_ANDROID) {

	/**
	 * on create options menu
	 * @param {Object} e
	 */
	var onCreateOptionsMenu = function(e) {
		e.menu.clear();
		var searchView = Titanium.UI.Android.createSearchView({
			hintText : L('search', 'Search'),
		});

		searchView.addEventListener('change', onSearch);
		e.menu.add({
			title : L('search', 'Search'),
			actionView : searchView,
			icon : Alloy.Globals.Images.ic_action_search_24dp,
			showAsAction : Ti.Android.SHOW_AS_ACTION_IF_ROOM | Ti.Android.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW
		});
		//gc
		searchView = null;
		var cartItem = e.menu.add({
			title : L('buy', 'Buy'),
			showAsAction : Ti.Android.SHOW_AS_ACTION_IF_ROOM,
			icon : getIconChartWithBadge()
		});

		cartItem.addEventListener('click', onCart);
		//gc
		cartItem = null;
	};
	/**
	 * on home icon item selected
	 */
	var onHomeIconItemSelected = _.debounce(function(e) {
		onBack();
	}, Alloy.Globals.Integers.debounce, true);
}

/**
 * get icon with badge count
 * @return {String}
 */
var getIconChartWithBadge = function() {
	switch(countDrinks) {
	case 0:
		return Alloy.Globals.Images.ic_action_cart;
		break;
	case 1:
		return Alloy.Globals.Images.ic_action_cart_1;
		break;
	case 2:
		return Alloy.Globals.Images.ic_action_cart_2;
		break;
	case 3:
		return Alloy.Globals.Images.ic_action_cart_3;
		break;
	case 4:
		return Alloy.Globals.Images.ic_action_cart_4;
		break;
	case 5:
		return Alloy.Globals.Images.ic_action_cart_5;
		break;
	case 6:
		return Alloy.Globals.Images.ic_action_cart_6;
		break;
	case 7:
		return Alloy.Globals.Images.ic_action_cart_7;
		break;
	case 8:
		return Alloy.Globals.Images.ic_action_cart_8;
		break;
	case 9:
		return Alloy.Globals.Images.ic_action_cart_9;
		break;
	default:
		return Alloy.Globals.Images.ic_action_cart_more;
		break;
	}
};
/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * when window is opened
 */
var onOpen = function(e) {
	if (OS_IOS) {
		init(e);
	} else {
		_.defer(init, e);
	}
	$.drinks.onOpen(e);
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	$.drinks.onClose(e);
	cleanup();
};

/**
 * Fired when user search in toolbar
 * @param {Object} e
 */
var onSearch = function(e) {
	var value = e.source.value;
	$.drinks.onSearch(value);

};

/**
 * open cart
 */
var onCart = _.debounce(function(e) {
	if (countDrinks > 0) {
		var purchaseStore = $.drinks.getPurchaseStore();

		//open cart to buy drinks
		var cartController = Alloy.Globals.AppRoute.openCartDrink({
			purchaseStore : purchaseStore,
			subtitle : subtitle,
			gift : gift
		});

		cartController.on('gift:success', onSuccessGift);
		cartController.on('transaction:success', onSuccessTransaction);

		//gc
		cartController = null;
		purchaseStore = null;
	} else {
		(require('TiUtils')).toast(L('error_cart_empty', 'You did not select any drink'));
	}
}, Alloy.Globals.Integers.debounce, true);

/**
 * on update toolbar
 * @param {Object} e
 */
var onUpdateToolbar = function(e) {
	//check if need change icon
	if (countDrinks != e.count) {

		countDrinks = e.count;

		if (OS_ANDROID) {
			var activity = $.win_drinks.getActivity();
			activity.invalidateOptionsMenu();
			//gc
			activity = null;
		} else {
			configureNavButtons();
		}
	}
};

/**
 * on success send gift
 * @param {Object} e
 */
var onSuccessGift = function(e) {
	$.win_drinks.close();
	$.trigger('gift:success', e);
};

/**
 * on success buy drinks
 */
var onSuccessTransaction = function(e) {
	$.win_drinks.close();
	$.trigger('transaction:success', e);
};

/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.cleanup = cleanup;
