/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var showBack = false;

if (OS_ANDROID) {
	var abx = require('com.alcoapps.actionbarextras');
}
/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {

	if (_.isObject(properties)) {
		if (_.has(properties, 'showBack')) {
			showBack = properties.showBack;
		}
		_.extend($.win_select_country_city, _.omit(properties, 'showBack'));
	}

};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();

		$.select_country_city.off('success', onSuccess);
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
	if (OS_ANDROID) {
		if (!showBack) {
			$.win_select_country_city.addEventListener('androidback', function() {
			});
		} else {
			$.win_select_country_city.addEventListener('androidback', onBack);
		}
	}

	$.select_country_city.on('success', onSuccess);
};

/**
 * initialize controller
 */
var init = function(e) {
	applyProperties(args);
	configureToolbar(e);
	applyListeners();
	//gc
	args = null;
};

var beforeOpen = function() {
	applyProperties(args);
	//gc
	args = null;
};

/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * when window is opened
 */
var onOpen = function(e) {
	if (OS_IOS) {
		init(e);
	} else {
		_.defer(init, e);
	}
	$.select_country_city.onOpen(e);
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	$.select_country_city.onClose(e);
	cleanup();
};

var onBack = function() {
	$.win_select_country_city.close();
};

/**
 * configure toolbar
 */
var configureToolbar = function(e) {
	if (OS_ANDROID) {
		if (showBack) {

			abx.setBackgroundColor(Alloy.Globals.Colors.action_bar_transparent);
			abx.setElevation(0);

			var activity = $.win_select_country_city.getActivity();
			console.log("Adroid Activity:: ", activity);
			activity.getActionBar().show();
			activity.getActionBar().setDisplayShowHomeEnabled(true);
			activity.getActionBar().displayHomeAsUp = showBack;
			activity.getActionBar().onHomeIconItemSelected = onHomeIconItemSelected;
			activity = null;
		} else {
			var activity = $.win_select_country_city.getActivity();
			console.log("Adroid Activity:: ", activity);
			$.win_select_country_city.getActivity().getActionBar().hide();
		}
	}
};

if (OS_ANDROID) {
	/**
	 * on home icon item selected
	 */
	var onHomeIconItemSelected = _.debounce(function(e) {
		onBack();
	}, Alloy.Globals.Integers.debounce, true);
}

var onSuccess = function() {
	onClose();
};
beforeOpen();
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.cleanup = cleanup;
