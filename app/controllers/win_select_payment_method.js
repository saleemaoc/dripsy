/** ------------------------
 Constants
 ------------------------**/
var TAG = 'controllers/win_select_payment';
/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var payment = null;

/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {
		if (_.has(properties, 'payment')) {
			payment = properties.payment;
		}
		_.extend($.win_select_payment_method, _.omit(properties, 'payment'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
};

/**
 * initialize controller
 */
var init = function(e) {
	applyProperties(args);
	//gc
	args = null;
	applyListeners();
	showLoader();
	checkPermissions();
};

var checkPermissions = function() {

	// check permission

	var readPhoneState = 'android.permission.READ_PHONE_STATE';
	var hasReadPhoneState = Ti.Android.hasPermission(readPhoneState);

	// request permission
	if (!hasReadPhoneState) {
		Ti.Android.requestPermissions([readPhoneState], function(e) {
			if (e.success) {
				configurePayments();
			} else {
				checkPermissions();
			}
		});
	} else {
		configurePayments();
	}
};
/**
 * configure payments
 */
var configurePayments = function() {

	if (_.isObject(payment)) {
		addClarika();
	}
};

var addClarika = function() {
	var btnClarika = Ti.UI.createButton({
		title : 'Clarika'
	});

	btnClarika.addEventListener('click', onClarika);
	$.v_payments.add(btnClarika);

	//gc
	btnClarika = null;
};
/**
 * show loader
 */
var showLoader = function() {
	$.loader.show();
};
/**
 * hide loader
 */
var hideLoader = function() {
	$.loader.hide();
};
/** ------------------------
 Listeners
 ------------------------**/

/**
 * when window is opened
 */
var onOpen = function(e) {
	if(OS_IOS){
		init(e);
	}else{
		_.defer(init, e);
	}
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
};

/**
 * on success payment
 * @param {Object} e
 */
var onSuccess = function(e) {
	$.trigger('success', e);
	$.win_select_payment_method.close();
};

/**
 * on error  payment
 * @param {Object} e
 */
var onError = function(e) {
	$.trigger('error', e);
	$.win_select_payment_method.close();
};

/**
 * on cancel payment
 * @param {Object} e
 */
var onCancel = function(e) {
	$.trigger('cancel', e);
};

var onClarika = function() {
	onSuccess({
		transactionID : 'PruebaClarika123'
	});
};
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.cleanup = cleanup;
