/**
 * @author jagu
 *
 */
/** ------------------------
 Constants
 ------------------------**/
var TAG = 'controllers/partial_transaction_detail';
/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var transaction = null;
var transactionID = null;

/** ------------------------
 Dependencies
 ------------------------**/

/**
 * get dao dependency
 * @return {Dao}
 */
var getDao = function() {
	return Alloy.Globals.DaoBuilder.getTransactionDao();
};

/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		if (_.has(properties, 'transaction')) {
			transaction = properties.transaction;

			$.transaction_detail_list.applyProperties({
				transactionID : transaction.get('id')
			});
		}

		if (_.has(properties, 'transactionID')) {

			transactionID = properties.transactionID;

			$.transaction_detail_list.applyProperties({
				transactionID : transactionID
			});
		}

		_.extend($.partial_transaction_detail, _.omit(properties, 'transaction', 'transactionID'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
	$.v_establishment_detail.addEventListener('click', onInfo);
};

/**
 * initialize controller
 */
var init = function() {
	applyProperties(args);
	//gc
	args = null;
	applyListeners();
	showLoader();
	//get data
	_.delay(updateData, Alloy.Globals.Integers.SetDataDelay);
};

/**
 * find data if transaction is null
 */
var updateData = function() {
	if (!_.isObject(transaction)) {
		transaction = getDao().findByID(transactionID);
	}
	updateUI();
};

/**
 * update UI components
 */
var updateUI = function() {
	if (_.isObject(transaction)) {
		$.transaction.set(transaction.attributes);
		transaction = null;
	}
	//update image in widget
	$.image.applyProperties({
		image : {
			image : $.transaction.getEstablishmentImage()
		}
	});
	$.image.refresh();
	//alert($.transaction.getTotalAmount());
	
	if ($.transaction.isPending() && !$.transaction.getIsGift()) {
		//generate qr
		$.lbl_code_qr_help.show({
			animated : OS_IOS
		});
		addQRImage();
	} else {
		hideQR();
	}
	hideLoader();
};

var addQRImage = function() {
	$.v_qr.removeAllChildren();
	$.v_qr.add((require('JsUtils')).createQRCodeImageView($.transaction.getTransactionCode(), Alloy.Globals.Dimens.transaction_qr_generator, onErrorGenerateQR));
};

/**
 * hide qr components
 */
var hideQR = function() {
	$.v_qr.hide({
		animated : OS_IOS
	});
	$.v_transaction_code.hide({
		animated : OS_IOS
	});
	$.v_qr.height = 0;
	$.v_transaction_code.height = 0;
};

/**
 * show loader and hide page
 */
var showLoader = function() {
	$.loader.show();
	$.page.hide({
		animated : OS_IOS
	});
};

var hideLoader = function() {
	$.loader.hide();
	$.page.show({
		animated : OS_IOS
	});
};

/** ------------------------
 Listeners
 ------------------------**/

/**
 * when window is opened
 * @param {Object} e
 */
var onOpen = function(e) {
	if (OS_IOS) {
		init(e);
	} else {
		_.defer(init, e);
	}
	$.transaction_detail_list.onOpen(e);
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
	$.transaction_detail_list.onClose(e);
};

/**
 *  on error generate QR
 * @param {Object} e
 */
var onErrorGenerateQR = function(e) {
	addQRImage();
};

/**
 * on information
 * @param {Object} e
 */
var onInfo = _.debounce(function(e) {
	var establishmentID = $.transaction.getEstablishmentID();

	var establishment = Alloy.Globals.DaoBuilder.getEstablishmentDao().findByID(establishmentID);

	var params = {
		establishment : establishment
	};

	if (OS_IOS) {
		params.title = establishment.getName();
	}

	Alloy.Globals.AppRoute.openEstablishmentDetail(params);

}, Alloy.Globals.Integers.debounce, true);
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.onOpen = onOpen;
exports.onClose = onClose;
exports.cleanup = cleanup;
