/**
 * @author jagu
 *
 */
/** ------------------------
 Constants
 ------------------------**/
var TAG = 'controller/partial_menu';

var getPendingNotificationTransaction = function() {
	return Alloy.Globals.AppProperties.getPendingTransaction();
};

var getPendingNotificationChat = function() {
	return Alloy.Globals.AppProperties.getPendingChat();
};

var MENU_STRUCTURE = [{
	name : null,
	items : [{
		icon : {
			image : Alloy.Globals.Images.ic_money_24dp
		},
		title : {
			text : L('my_transactions', 'My transactions')
		},
		notification : getPendingNotificationTransaction
	}, {
		icon : {
			image : Alloy.Globals.Images.ic_chat_24dp
		},
		title : {
			text : L('chat', 'Chat')
		},
		notification : getPendingNotificationChat
	}, {
		icon : {
			image : Alloy.Globals.Images.ic_location_24dp
		},
		title : {
			text : L('change_location', 'Change location')
		}
	}]
}, {
	name : L('settings', 'Settings'),
	items : [{
		title : {
			text : L('privacy_policy', 'Privacy policy')
		}
	}, {
		title : {
			text : L('information', 'Information')
		}
	}, {
		disabled : Alloy.Globals.Facebook.loggedIn,
		title : {
			text : L('change_password', 'Change password')
		}
	}, {
		title : {
			text : L('logout', 'Log out')
		}
	}]
}];

//index
var MY_TRANSACTIONS = 0;
var MESSAGES = 1;
var CHANGE_LOCATION = 2;
var PRIVACE_POLICY = 3;
var INFORMATION = 4;
var CHANGE_PASSWORD = Alloy.Globals.Facebook.loggedIn ? 99999 : 5;
var LANGUAGE = 9999;
var LOGOUT = Alloy.Globals.Facebook.loggedIn ? 5 : 6;

/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
/**
 * flag to indicate if need refresh data
 */
var firstTime = true;

var drawer = null;

var itemsController = [];

var pendingTransaction = getPendingNotificationTransaction();
var pendingChat = getPendingNotificationChat();
/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {
		_.extend($.partial_menu, _.omit(properties));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
		Ti.App.removeEventListener('pending_chat', onPendingChat);
		Ti.App.removeEventListener('pending_transaction', onPendingTransaction);
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
	$.options.addEventListener('click', onItemClick);
	$.header.on('click', onClickHeader);
	Ti.App.addEventListener('pending_chat', onPendingChat);
	Ti.App.addEventListener('pending_transaction', onPendingTransaction);
};

/**
 * initialize controller
 */
var init = function() {
	applyProperties(args);
	applyListeners();
	//gc
	args = null;
	loadHeaderInfo();
	loadSections();
};

/**
 * load section into tableview
 */
var loadSections = function() {

	for (var i = 0,
	    j = MENU_STRUCTURE.length; i < j; i++) {
		var sectionStructure = MENU_STRUCTURE[i];
		var section = Ti.UI.createTableViewSection();
		if (!_.isNull(sectionStructure.name)) {
			section.headerView = Alloy.createController('tableview_section_menu', {
				title : {
					text : sectionStructure.name.toUpperCase()
				}
			}).getView();
		}

		section = loadItems(section, sectionStructure.items);
		$.options.appendSection(section);
		//gc
		section = null;
	};
	//gc
	MENU_STRUCTURE = null;
};

/**
 * load items into section
 * @param {TableviewSection} section
 * @param {Array} items
 * @return {TableviewSection}
 */
var loadItems = function(section, items) {

	for (var i = 0,
	    j = items.length; i < j; i++) {
		var item = items[i];
		if (!item.disabled) {

			var controller = Alloy.createController('tableview_row_menu_item', item);

			if (_.isFunction(item.notification)) {
				itemsController.push({
					controller : controller,
					notification : item.notification
				});

			}
			section.add(controller.getView());
		}
	};

	return section;
};

var loadHeaderInfo = function() {

	var params = {
		full_name : {
			text : Alloy.Globals.UserLogged.getFullName()
		}
	};

	var photo = Alloy.Globals.UserLogged.getPhotoMini();
	photo = _.isNull(photo) || !Alloy.Globals.JsUtils.validateURL(photo) ? Alloy.CFG.constants.mock_url_broken_link_image : photo;

	params.image = {
		defaultImage : Alloy.Globals.UserLogged.getPhotoDefault(),
		image : photo,
	};

	params.background_image = {
		defaultImage : Alloy.Globals.UserLogged.getBackgroundDefault(),
		image : photo,
	};
	$.header.applyProperties(params);
};

/**
 * on resume
 * @param {Object} e
 */
var onResume = function(e) {
	if (OS_IOS || !firstTime) {
		loadHeaderInfo();
	}
	firstTime = false;
	pendingTransaction = getPendingNotificationTransaction();
	pendingChat = getPendingNotificationChat();
	refreshItemsControllers();

};

/**
 * set drawer
 */
var setDrawer = function(d) {
	drawer = d;
};

var closeDrawer = function() {
	if (!_.isNull(drawer)) {
		drawer.toggleLeftWindow();
	}
};

var refreshItemsControllers = function() {
	if (_.isArray(itemsController) && !_.isEmpty(itemsController)) {
		itemsController.map(function(itemController) {
			itemController.controller.setNotification(itemController.notification());
		});

	}
};

/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * when window is opened
 */
var onOpen = function(e) {
	if (OS_IOS) {
		init(e);
	} else {
		_.defer(init, e);
	}
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
};

/**
 * Fired when a table row is clicked by the user.
 * @param {Object} e
 */
var onItemClick = _.debounce(function(e) {
	Ti.App.fireEvent('closekeyboard');
	var index = e.index;
	switch(index) {
	case MY_TRANSACTIONS:
		Alloy.Globals.AppRoute.openMyTransactions().on('close', onBack);
		break;
	case MESSAGES:
		Alloy.Globals.AppRoute.openChat().on('close', onBack);
		break;
	case CHANGE_LOCATION:
		Alloy.Globals.AppRoute.openSelectCountryCity({
			showBack : true
		});
		break;
	case PRIVACE_POLICY:
		Alloy.Globals.AppRoute.openPoliticsAndPrivacy();
		break;
	case INFORMATION:
		Alloy.Globals.AppRoute.openInformationScreen();
		break;
	case CHANGE_PASSWORD:
		Alloy.Globals.AppRoute.openChangePassword();
		break;
	case LANGUAGE:
		break;
	case LOGOUT:
		onLogout();
		break;
	};
	closeDrawer();
}, Alloy.Globals.Integers.debounce, true);

var onClickHeader = _.debounce(function(e) {
	var userProfileController = Alloy.Globals.AppRoute.openUserProfile({
		user : Alloy.Globals.UserLogged
	});
	if (OS_IOS) {
		userProfileController.on('ios:back', onResume);
	}
	//gc
	userProfileController;
	closeDrawer();
}, Alloy.Globals.Integers.debounce, true);

var onLogout = _.debounce(function(e) {
	(require('AppProperties')).logout();
	Ti.App.fireEvent('logout');
}, Alloy.Globals.Integers.debounce, true);

var onPendingChat = function(e) {
	if ((e.value == true && pendingChat == false) || (e.value == false && pendingChat == true)) {
		pendingChat = e.value;
		refreshItemsControllers();
	}
};

var onPendingTransaction = function(e) {

	if ((e.value == true && pendingTransaction == false) || (e.value == false && pendingTransaction == true)) {
		pendingTransaction = e.value;
		refreshItemsControllers();
	}
};

var onBack = function() {
	Alloy.Globals.AppRoute.setCurrentRoute('win_home');
};
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.onOpen = onOpen;
exports.onClose = onClose;
exports.cleanup = cleanup;
exports.onResume = onResume;
exports.setDrawer = setDrawer;
