/** ---------
 Fields
 -------- **/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var female = false;
/** ---------
 Methods
 -------- **/
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		if (_.has(properties, 'female')) {
			female = properties.female;
		}
		if (_.has(properties, 'user_1')) {
			$.user_1.applyProperties(formatData(properties.user_1));
		}

		if (_.has(properties, 'user_2')) {
			$.user_2.applyProperties(formatData(properties.user_2));
		}

		if (_.has(properties, 'user_3')) {
			$.user_3.applyProperties(formatData(properties.user_3));
		}

		_.extend($.tableview_row_user_presence, _.omit(properties, 'isFemaleList', 'user_1', 'user_2', 'user_3'));
	}
};

var init = function() {
	applyProperties(args);
	//gc
	args = null;
};

/**
 * @param {Object} data
 */
var formatData = function(data) {
	var result = {};

	if (_.has(data, 'user')) {

		result.visible = true;
		var userPresence = Alloy.Globals.DaoBuilder.getUserPresenceDao().getModel();
		userPresence.mapping(data);
		result.userPresence = userPresence;
		//gc
		userPresence = null;

		var user = result.userPresence.getUser();
		result.name = {
			text : (user.getFirstName() + '\n' + user.getLastName()).toUpperCase()
		};
		result.image = {
			image : Alloy.Globals.JsUtils.validateURL(user.getPhotoMini()) ? user.getPhotoMini() : Alloy.CFG.constants.mock_url_broken_link_image,
			defaultImage : user.getPhotoDefault()
		};
	}
	if (_.has(data, 'type_presence')) {
		result.state_circle = {
			borderColor : data.type_presence == Alloy.CFG.constants.user_presence.type_presence.going ? Alloy.Globals.Colors.type_presence_going : Alloy.Globals.Colors.type_presence_here
		};
	}

	result.female = female;
	return result;
};
//initialize row
init();

/** ---------
 Public
 -------- **/
exports.applyProperties = applyProperties;
