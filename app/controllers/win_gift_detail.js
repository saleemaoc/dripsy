/** ------------------------
 Constants
 ------------------------**/
var TAG = 'controllers/win_gift_detail';
/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {
		if (_.has(properties, 'gift')) {
			$.gift_detail.applyProperties({
				gift : properties.gift
			});
		}
		_.extend($.win_gift_detail, _.omit(properties, 'gift'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
		Ti.App.removeEventListener('closegift', close);
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
	Ti.App.addEventListener('closegift', close);
	if (OS_ANDROID) {
		$.win_gift_detail.addEventListener('androidback', onBack);
	}
};

/**
 * initialize controller
 */
var init = function(e) {
	applyListeners();
	configureToolbar(e);
};

var onBack = function() {
	$.win_gift_detail.close();
};

/**
 * configure toolbar
 */
var configureToolbar = function(e) {
	if (OS_ANDROID) {
		var activity = $.win_gift_detail.getActivity();
		activity.getActionBar().displayHomeAsUp = true;
		activity.getActionBar().onHomeIconItemSelected = onHomeIconItemSelected;
		//gc
		activity = null;
	}

};

if (OS_ANDROID) {
	/**
	 * on home icon item selected
	 */
	var onHomeIconItemSelected = _.debounce(function(e) {
		onBack();
	}, Alloy.Globals.Integers.debounce, true);
}

/*
 *execute before open window
 */
var beforeOpen = function() {
	applyProperties(args);
	//gc
	args = null;
};
/** ------------------------
 Listeners
 ------------------------**/
/**
 * when window is opened
 */
var onOpen = function(e) {
	if (OS_IOS) {
		init(e);
	} else {
		_.defer(init, e);
	}
	$.gift_detail.onOpen(e);
	Ti.App.fireEvent('hide_loader');
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	if (OS_IOS) {
		$.trigger('ios:back', {});
	}
	cleanup();
	$.gift_detail.onClose(e);

};

var close = function() {
	$.win_gift_detail.close();
};
beforeOpen();
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.cleanup = cleanup;
