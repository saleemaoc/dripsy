/**
 * @author jagu
 *
 */
/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var establishment;
/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {
		_.extend($.partial_establishment_detail_map, properties);
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
	} catch(e) {
	}
};
/**
 * apply listeners to controller
 */
var applyListeners = function() {
	$.v_establishment_info.addEventListener('click', onClickEstablishment);
	$.v_right.addEventListener('click', onClickEstablishment);
	//$.v_website.addEventListener('click', onWebsite);
};

/**
 * initialize controller
 */
var init = function() {
	applyProperties(args);
	applyListeners();
	//gc
	args = null;
};

var show = function() {
	$.partial_establishment_detail_map.show();
};

var hide = function() {
	$.partial_establishment_detail_map.hide();
};

var findEstablishment = function(establishmentID) {
	establishment = Alloy.Globals.DaoBuilder.getEstablishmentDao().findByID(establishmentID);
	loadInformation();
};

var loadInformation = function() {

	if (establishment) {
		DEFAULT_VALUE = '-';
		//name
		$.lbl_name.text = establishment.getName().toUpperCase();

		//city
		var city = establishment.getCity();
		$.lbl_city.text = _.isEmpty(city) ? DEFAULT_VALUE : city;

		//address
		var address = establishment.getAddress();
		$.lbl_address.text = _.isEmpty(address) ? DEFAULT_VALUE : address;

		//website
		var website = establishment.getWebsite();
		$.lbl_website.text = _.isEmpty(website) ? DEFAULT_VALUE : website;

		//open
		var isOpen = establishment.getIsOpen(Alloy.Globals.CurrentDay(), Alloy.Globals.CurrentDate());
		$.lbl_open.text = isOpen.message;

		//ranking
		var ranking = establishment.getRanking();
		var star_enabled = 1;
		var star_disabled = 0.5;

		$.rank_1.opacity = ranking >= 1 ? star_enabled : star_disabled;
		$.rank_2.opacity = ranking >= 2 ? star_enabled : star_disabled;
		$.rank_3.opacity = ranking >= 3 ? star_enabled : star_disabled;
		$.rank_4.opacity = ranking >= 4 ? star_enabled : star_disabled;
		$.rank_5.opacity = ranking >= 5 ? star_enabled : star_disabled;

	}

};

/** ------------------------
 Listeners
 ------------------------**/

/**
 * when window is opened
 */
var onOpen = function(e) {
	init(e);
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
};

/**
 * on website click
 * @param {Object} e
 */
var onWebsite = _.debounce(function(e) {
	if (_.isString(establishment.getWebsite()) && !_.isEmpty(establishment.getWebsite())) {
		(require('TiUtils')).openUrl(establishment.getWebsite());
	}

}, Alloy.Globals.Integers.debounce, true);

/**
 * When user click in establishment info
 * @param {Object} e
 */
var onClickEstablishment = _.debounce(function(params) {

	var params = {
		establishment : establishment
	};
	if (OS_IOS) {
		params.title = establishment.getName();
	}
	Alloy.Globals.AppRoute.openEstablishmentDetail(params);
}, Alloy.Globals.Integers.debounce, true);
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.onOpen = onOpen;
exports.onClose = onClose;
exports.cleanup = cleanup;
exports.show = show;
exports.hide = hide;
exports.findEstablishment = findEstablishment;
