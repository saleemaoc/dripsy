/**
 * @author jagu
 *
 */
/** ------------------------
 Constants
 ------------------------**/
var TAG = 'controllers/partial_conversation_detail';
/**
 * 5 seconds
 */
var DELAY_FIND_NEW_MESSAGES = 5000;
/**
 * ms
 */
var DELAY_TO_SENT_NEW_MESSAGES = 500;
/**
 * 1 second
 */
var DELAY_GET_NEW_MESSAGES_FIRST_TIME = 1000;

/**
 * Min length message
 */
var MIN_LENGTH_MESSAGE = 2;
/**
 * delay to scroll to bottom
 */
var DELAY_SCROLL_TO_BOTTOM = 250;
/** ------------------------
 Fields
 ------------------------**/
/**
 * Arguments passed into this controller can be accessed via the `$.args` object directly or:
 */
var args = $.args;
/**
 * conversation id to find messages
 */
var conversationID = null;

/**
 * http library
 */
var httpCK;
/**
 * limit to query
 */
var limit = 7;
/**
 * offset to query
 */
var offset = 0;
/**
 * time out to find new messages
 */
var timeOutfindNewMessages = null;
/**
 * flag to indicate if find first time
 */
var firstTimeFindingMessages = true;

/**
 * indicate state to conection
 */
var online = Ti.Network.online;
/**
 * to user id
 */
var toUserID = null;
/**
 * content messages
 */
var messages = $.local_collection;
/**
 * time out to sent new messages to service
 */
var timeoutSentNewMessagesToService = null;
/**
 * request to send new messages
 */
var requestToSendNewMessages = null;
/**
 * first visible item index when user using scroll
 */
var firstVisibleItemIndex = -1;
/**
 * Indicates whether old messages may exist
 */
var needCheckExistMessages = false;
/**
 * Indicates whether to search for existing messages
 */
var loadingExistMessages = false;
/**
 * flag to indicate first time to refresh exist messages
 */
var isFirsttimeRefreshExistMessages = true;
/**
 * scroll to bottom after get new messages
 */
var needScrollToBottomNewMessages = false;
var currentIndexScroll = -1;
var enableHideKeyboad = true;
/** ------------------------
 Dependencies
 ------------------------**/

/**
 * get service dependency
 * @return {Service}
 */
var getService = function() {
	return Alloy.Globals.ServiceBuilder.getMessageService().inject(getDao());
};
/**
 * get dao dependency
 * @return {Dao}
 */
var getDao = function() {
	return Alloy.Globals.DaoBuilder.getMessageDao();
};

/** ------------------------
 Methods
 ------------------------**/

/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		if (_.has(properties, 'conversationID')) {
			conversationID = properties.conversationID;
		}

		if (_.has(properties, 'toUserID')) {
			toUserID = properties.toUserID;
		}

		_.extend($.partial_conversation_detail, _.omit(properties, 'conversationID', 'toUserID'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();

		//abort all request
		if (httpCK) {
			httpCK.abort();
		}
		//remove event to check connection
		Ti.Network.removeEventListener('change', onNetwork);

		//clear process to find new messages
		clearFindNewMessages();
		clearTimeoutToSentNewMessagesToService();
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
	Ti.Network.addEventListener('change', onNetwork);
	//send message
	$.fab_send_message.on('click', onSendMessage);
	$.txt_send_message.addEventListener('return', onSendMessage);
	//scroll to refresh
	$.list.addEventListener('scrollend', onScrollEnd);
	$.list.addEventListener('scrollstart', onScrollStart);
	$.txt_send_message.addEventListener('focus', scrollViewToBottom);
	$.txt_send_message.addEventListener('click', scrollViewToBottom);
	//list
	$.list.addEventListener('itemclick', onClickItem);
	if (OS_IOS) {
		$.v_list.addEventListener('click', hideKeyboard);
		$.txt_send_message.addEventListener('change', onChangeSendMessageText);
	}

};

/**
 * initialize controller
 */
var init = function() {
	applyProperties(args);
	//gc
	args = null;
	applyListeners();
	configureUI();

	httpCK = new (require('HttpCK'))();
	showLoader();
	_.delay(refreshExistMessages, Alloy.Globals.Integers.SetDataDelay);

};

/**
 * configure UI
 */
var configureUI = function() {
	$.v_list.height = $.partial_conversation_detail.rect.height - 68;
};

/**
 * show view
 */
var show = function() {
	$.partial_conversation_detail.show();
};

/**
 * hide view
 */
var hide = function() {
	$.partial_conversation_detail.hide();
};

/**
 * get query to find messages in local storage
 * @return {String}
 */
var getQuery = function() {
	return ' limit ' + limit + ' offset ' + messages.models.length;
};

/**
 * update data finding in local storage
 */
var refreshExistMessages = function() {
	loadingExistMessages = true;
	if (isFirsttimeRefreshExistMessages) {
		Alloy.Globals.LastDateMessageDrawInCurrentConversation = null;
		Alloy.Globals.LastIdMessageDrawInCurrentConversation = null;
	}
	getDao().findMessagesByConversationID(conversationID, getQuery(), true, successRefreshExistMessages, errorRefreshExistMessages);
};

/**
 * success refresh exist messages
 * @param {Alloy.Collection} collection
 */
var successRefreshExistMessages = function(collection) {
	if (collection.models.length >= limit) {
		needCheckExistMessages = true;
	}

	//checkeo si tengo que mantener el scroll index ya que si no el scroll esta siempre en el index 0.

	if (!_.isEmpty(messages.models) && !_.isEmpty(collection.models)) {
		currentIndexScroll = collection.models.length;
	}

	//add messages
	messages.add(collection.models);
	//scroll al ultimo mensaje o mantengo el scroll que habia antes de agregar items viejos
	autoScrollAfterAddItems();

	isFirsttimeRefreshExistMessages = false;
	loadingExistMessages = false;
	//execute process
	_.delay(function() {
		executeProcessToFindMessages();
		executeTimerToSentNewMessages();
	}, 100);

	hideLoader();
};

/**
 * position scroll after add items
 */
var autoScrollAfterAddItems = function() {
	if (isFirsttimeRefreshExistMessages) {
		scrollToBottom();
	} else if (currentIndexScroll != -1) {
		scrollToIndex(currentIndexScroll);
	}
};
/**
 * error refresh exist messages
 * @param {Object} error
 */
var errorRefreshExistMessages = function(error) {
	(require('TiLog')).error(TAG, 'errorGetData ->' + JSON.stringify(error));
	executeProcessToFindMessages();
	executeTimerToSentNewMessages();
	loadingExistMessages = false;
};

/**
 * get row by model
 * @return {Model} message
 */
var getRowByModel = function(message) {
	//get data to binding
	return message.transform();
};

/**
 * execute process to find messages
 */
var executeProcessToFindMessages = function() {
	clearFindNewMessages();
	timeOutfindNewMessages = setTimeout(findNewMessages, firstTimeFindingMessages ? Alloy.Globals.Integers.SetDataDelay : DELAY_FIND_NEW_MESSAGES);
	firstTimeFindingMessages = false;
};

/**
 * clear timeout to find new messages
 */
var clearFindNewMessages = function() {
	if (!_.isNull(timeOutfindNewMessages)) {
		clearTimeout(timeOutfindNewMessages);
		timeOutfindNewMessages = null;
	}
	if (httpCK) {
		httpCK.abortByTag(getService().TAGS.TAG_GET);
	}
};

/**
 * get params to get new messages
 */
var getParamsToNewMessages = function() {

	var params = {
		conversation_id : conversationID
	};

	lastMessage = findLastMessageWithServerID();
	if (_.isObject(lastMessage)) {
		params.message_id = lastMessage.getServerID();
	}

	return params;
};

/**
 * find new messages
 */
var findNewMessages = function() {
	clearFindNewMessages();
	getService().get(getParamsToNewMessages(), successNewMessages, errorNewMessages, httpCK);
};

/**
 * success get new messages
 * @param {Object} response
 */
var successNewMessages = function(response) {
	needScrollToBottomNewMessages = _.isArray(response) && !_.isEmpty(response);
	var lastMessageID = null;

	if (!_.isEmpty(messages.models)) {
		var lastIndex = messages.models.length - 1;
		var lastMessage = messages.models[lastIndex];
		lastMessageID = lastMessage.getID();
	}

	getDao().findNewMessagesToOtherUserByConversationIDAndLastMessageID(conversationID, lastMessageID, null, true, successFindNewMessagesInDB, errorFindNewMessagesInDB, messages);
};

/**
 * error get new messages
 */
var errorNewMessages = function(error) {
	executeProcessToFindMessages();
	analizeDeleteError(error.code);
};

/**
 * success find new messages in db
 * @param {Object} response
 */
var successFindNewMessagesInDB = function(response) {
	if (needScrollToBottomNewMessages) {
		_.delay(scrollToBottom, DELAY_SCROLL_TO_BOTTOM);
	}
	needScrollToBottomNewMessages = false;
	executeProcessToFindMessages();
};

/**
 * error find new messages in db
 * @param {Object} response
 */
var errorFindNewMessagesInDB = function(error) {
	executeProcessToFindMessages();
};

/**
 * validate message
 * @param {String} message
 */
var validateMessage = function(message) {
	var messageLength = message.trim().length;
	return messageLength > 0;
};

/**
 * update local new message
 * @param {Message} message
 */
var updateLocalNewMessage = function(message) {
	messages.push(message);
	scrollToBottom();
};

/**
 * scroll to bottom of the list
 */
var scrollToBottom = function() {
	enableHideKeyboad = false;
	var index = messages.models.length - 1;
	scrollToIndex(index);
	if (OS_IOS) {
		scrollViewToBottom();
	}

};

var scrollViewToBottom = function() {
	if (OS_IOS) {
		_.delay(function() {
			$.partial_conversation_detail.scrollToBottom();
			enableHideKeyboad = true;
		}, 500);
	}
};

/**
 * scroll to index of the list
 */
var scrollToIndex = function(index) {
	$.list.scrollToItem(0, index, {
		animated : false
	});
};

/**
 * find and sent new messages to service
 */
var sentNewMessagesToService = function() {

	if (!_.isNull(requestToSendNewMessages)) {
		httpCK.abortRequest(requestToSendNewMessages);
		requestToSendNewMessages = null;
	}

	var messagesToService = getDao().findMessagesToSentByConversationID(conversationID);

	if (!_.isEmpty(messagesToService.models)) {
		requestToSendNewMessages = getService().send(messagesToService.models, onSuccessSentNewMessagesToService, onErrorSentNewMessagesToService, httpCK);
	}
};

/**
 * on success sent new messages to service
 * @param {Object} response
 */
var onSuccessSentNewMessagesToService = function(response) {
};

/**
 * on error sent new messages to service
 * @param {Object} error
 */
var onErrorSentNewMessagesToService = function(error) {
	analizeDeleteError(error.code);
};

/**
 * find last message with serverID
 */
var findLastMessageWithServerID = function() {
	if (!_.isEmpty(messages.models)) {

		var lastIndex = messages.models.length - 1;
		for (var i = lastIndex,
		    j = 0; i > j; i--) {
			var message = messages.models[i];
			if (message.getServerID() > 0) {
				return message;
			}
		};
	} else {
		return null;
	}

};

/**
 * clear timeout to sent new messages to service
 */
var clearTimeoutToSentNewMessagesToService = function() {
	if (!_.isNull(timeoutSentNewMessagesToService)) {
		clearTimeout(timeoutSentNewMessagesToService);
		timeoutSentNewMessagesToService = null;
	}
};

/**
 * execute timer to sent new messages
 */
var executeTimerToSentNewMessages = function() {
	timeoutSentNewMessagesToService = _.delay(sentNewMessagesToService, DELAY_TO_SENT_NEW_MESSAGES);
};

var showLoader = function() {
	$.loader.show();
	$.list.hide({
		animated : OS_IOS
	});
};

var hideLoader = function() {
	$.loader.hide();
	$.list.show({
		animated : OS_IOS
	});
};

/**
 * open user profile
 * @param {Integer} userID
 */
var openUserProfile = function(userID) {
	Alloy.Globals.AppRoute.openUserProfile({
		userID : userID
	});
};

var hideKeyboard = function() {
	if (OS_IOS) {
		$.txt_send_message.blur();
	} else {
		Ti.UI.Android.hideSoftKeyboard();
	}

};

var deleteConversation = function() {
	Alloy.Globals.DaoBuilder.getConversationDao().deleteByID(conversationID);
};
var analizeDeleteError = function(code) {
	switch(code) {
	case Alloy.CFG.Http.ConversationNotFound:

		deleteConversation();
		Alloy.Globals.TiUtils.toast(L('conversation_deleted_alert', 'The conversation was deleted'));
		$.trigger('conversation:not_found');
		cleanup();
		break;
	}
};
/** ------------------------
 Listeners
 ------------------------**/

/**
 * when window is opened
 */
var onOpen = function(e) {
	if (OS_IOS) {
		init(e);
	} else {
		_.defer(init, e);
	}
	$.fab_send_message.onOpen(e);
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
};

/**
 * on network state change
 * @param {Object} e
 */
var onNetwork = function(e) {

	if (!e.online && online) {
		online = false;
		$.fab_send_message.disabled();
	} else if (!online) {
		$.fab_send_message.enabled();
		online = true;
	}
};

/**
 * send new message
 * @param {Object} e
 */
var onSendMessage = _.debounce(function(e) {
	if (online) {
		var message = $.txt_send_message.getValue();
		if (validateMessage(message)) {
			var message = getDao().insert(conversationID, Alloy.Globals.UserID, toUserID, Alloy.Globals.UserLogged.getPhotoOrDefault(), message);
			var EMPTY = "";
			$.txt_send_message.setValue(EMPTY);
			if (OS_IOS) {
				onChangeSendMessageText({
					value : EMPTY
				});
			}
			updateLocalNewMessage(message);

			clearTimeoutToSentNewMessagesToService();
			executeTimerToSentNewMessages();
			if (OS_ANDROID) {
				$.txt_send_message.focus();
			}
		}
	}
}, Alloy.Globals.Integers.debounce, true);

/**
 * start scroll
 */
var onScrollStart = function(e) {
	firstVisibleItemIndex = e.firstVisibleItemIndex;
	currentIndexScroll = -1;
	if (enableHideKeyboad) {
		hideKeyboard();
	}
};

/**
 * finish scroll
 */
var onScrollEnd = function(e) {
	if (needCheckExistMessages && !loadingExistMessages && firstVisibleItemIndex >= e.firstVisibleItemIndex && e.firstVisibleItemIndex == 0) {
		refreshExistMessages();
	}
	//Clear first visible item index
	firstVisibleItemIndex = -1;
	enableHideKeyboad = true;
};

/**
 * When user click item
 * @param {Object} e
 */
var onClickItem = _.debounce(function(e) {
	var index = e.itemIndex;
	var bindId = e.bindId;
	var message = messages.models[index];

	switch(bindId) {
	case 'user_photo':
		var userID = message.getFromUserID();
		openUserProfile(userID);
		break;
	}
	hideKeyboard();
}, Alloy.Globals.Integers.debounce, true);

/**
 * Fired when user change message to send
 * @param {Object} e
 */
var onChangeSendMessageText = function(e) {
	$.lbl_hint_text_txt_send_message.visible = (e.value.length == 0);
};
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.onOpen = onOpen;
exports.onClose = onClose;
exports.cleanup = cleanup;
exports.show = show;
exports.hide = hide;
