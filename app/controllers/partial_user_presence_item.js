/**
 * @author jagu
 *
 */
/** ------------------------
 Fields
 ------------------------**/
/**
 * Arguments passed into this controller can be accessed via the `$.args` object directly or:
 */
var args = $.args;
var userPresence = null;

/** ------------------------
 Methods
 ------------------------**/

/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {

	if (_.isObject(properties)) {

		if (_.has(properties, 'image')) {
			$.image.applyProperties({
				image : properties.image
			});
		}

		if (_.has(properties, 'name')) {
			_.extend($.name, properties.name);
		}

		if (_.has(properties, 'state_border')) {
			_.extend($.state_border, properties.state_border);
		}

		if (_.has(properties, 'state_circle')) {
			_.extend($.state_circle, properties.state_circle);
		}

		if (_.has(properties, 'name')) {
			_.extend($.name, properties.name);
		}

		if (_.has(properties, 'userPresence')) {
			userPresence = properties.userPresence;
		}

		_.extend($.partial_user_presence_item, _.omit(properties, 'image', 'userPresence', 'name', 'state_circle', 'state_border'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
	} catch(e) {
	}
};
/**
 * apply listeners to controller
 */
var applyListeners = function() {
	$.partial_user_presence_item.addEventListener('click', onClick);
};

/**
 * initialize controller
 */
var init = function() {
	applyProperties(args);
	applyListeners();
	//gc
	args = null;
};

/** ------------------------
 Listeners
 ------------------------**/
/**
 * click user
 */
var onClick = _.debounce(function(e) {

	var user = userPresence.getUser();

	Alloy.Globals.AppRoute.openUserProfile({
		user : user,
		userPresence : userPresence
	});

});
init();
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.cleanup = cleanup;
