/** --------
 Fields
 -------- **/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;

/** --------
 Methods
 -------- **/

/**
 * apply properties to header section
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {
		if (_.has(properties, 'title')) {
			_.extend($.title, properties.title);
		}
		_.extend($.tableview_section_menu, _.omit(properties, 'title'));
	}
};

var init = function() {
	applyProperties(args);
	//gc
	args = null;
};

//initialize
init();

/** --------
 Public
 -------- **/
exports.applyProperties = applyProperties;

