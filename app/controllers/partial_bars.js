/**
 * @author jagu
 *
 */
/** ------------------------
 Constants
 ------------------------**/
var TAG = 'controllers/partial_bars';
/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;

/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {
		if (_.has(properties, 'name')) {
			$.bar_list.applyProperties({
				name : properties.name
			});
		}

		if (_.has(properties, 'autoSearch')) {
			$.bar_list.applyProperties({
				autoSearch : properties.autoSearch
			});
		}
		_.extend($.partial_bars, _.omit(properties, 'name'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
	} catch(e) {

	}
};
/**
 * apply listeners to controller
 */
var applyListeners = function() {
};

/**
 * initialize controller
 */
var init = function() {
	applyProperties(args);
	applyListeners();
	//gc
	args = null;
};

/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * when window is opened
 */
var onOpen = function(e) {
	if(OS_IOS){
		init(e);
	}else{
		_.defer(init, e);
	}
	$.bar_list.onOpen(e);
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	$.bar_list.onClose(e);
	cleanup();
};

var onSearch = function(e) {
	$.bar_list.onSearch(e);
};

/**
 * fire search
 * @param {String} name
 */
var fireSearch = function(name) {
	$.bar_list.fireSearch(name);
};
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.onOpen = onOpen;
exports.onClose = onClose;
exports.cleanup = cleanup;
exports.getCollection = $.bar_list.getCollection;
exports.onSearch = onSearch;
exports.fireSearch = fireSearch;
