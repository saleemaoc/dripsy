/**
 * @author jagu
 *
 */

/** ------------------------
 Constants
 ------------------------**/

var TAG = 'controllers/partial_establishment_map';

/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var collection = Alloy.Globals.DaoBuilder.getEstablishmentDao().getCollection();
var type = null;
Alloy.Globals.Map = require('ti.map');
var currentAnnotation = null;
var userAnnotation = null;
/**
 * this flag only use on Android
 */
var firstTimeLoadUser = OS_ANDROID;
/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {
		if (_.has(properties, 'collection')) {
			collection = properties.collection;
		}
		if (_.has(properties, 'type')) {
			type = properties.type;
		}
		_.extend($.partial_establishment_map, _.omit(properties, 'collection', 'type'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
};

/**
 * initialize controller
 */
var init = function() {
	applyProperties(args);
	applyListeners();
	//gc
	args = null;
	loadMap();
};

var loadMap = function() {
	if (collection.models.length == 0) {
		getData();
	} else {
		loadInformation();
	}
};

/**
 * load establishment in map
 */
var loadInformation = function() {
	if ((require('LocationUtils')).hasLocation()) {
		getUserAnnotation();
	} else {
		(require('TiUtils')).toast(L('failed_to_get_location', 'Failed to get location, activate your GPS'));
		$.map.applyProperties({
			region : {
				latitudeDelta : 0.9,
				longitudeDelta : 0.9
			}
		});
	}
	collection.map(function(establishment) {
		$.map.addAnnotation(Alloy.Globals.Map.createAnnotation(establishment.getAnnotationMap()));
	});
};

/**
 * get data for establishment
 */
var getData = function() {
	(new (require('daos/EstablishmentDao'))()).find('', true, onSuccessData, onErrorData, collection);
};

var onSuccessData = function(e) {
	loadInformation();
};

var onErrorData = function(e) {

};

var getCollection = function() {
	return collection;
};

var getUserAnnotation = function() {
	var imgUser = Ti.UI.createImageView({
		image : Alloy.Globals.UserLogged.getPhotoOrDefault()
	});
	var image = imgUser.getImage();
	//HACK: call two time because the first time when load image not show in annotation
	var mapAnnotationUserController = Alloy.createController('map_annotation_user', {
		image : {
			image : image
		},
	});

	if (firstTimeLoadUser) {
		mapAnnotationUserController.on('load', function() {
			$.map.removeAnnotation(userAnnotation);
			getUserAnnotation();
		});

		mapAnnotationUserController.hide();
	}
	var mapAnnotationUserView = mapAnnotationUserController.getView();

	userAnnotation = Alloy.Globals.Map.createAnnotation({
		latitude : Alloy.Globals.CoordLatitude,
		longitude : Alloy.Globals.CoordLongitude,
		customView : mapAnnotationUserView
	});

	$.map.addAnnotation(userAnnotation);

	//gc
	mapAnnotationUserController = null;
	mapAnnotationUserView = null;
	firstTimeLoadUser = false;
};
/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * when window is opened
 */
var onOpen = function(e) {
	if(OS_IOS){
		init(e);
	}else{
		_.defer(init, e);
	}
	$.establishment_detail.onOpen(e);
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
};

/**
 * on click map
 * @param {Object} e
 */
var onClickMap = function(e) {
	if (currentAnnotation && e.annotation.establishment_id && currentAnnotation.establishment_id == e.annotation.establishment_id) {
		currentAnnotation = null;
		$.establishment_detail.hide();
	} else if (e.annotation.establishment_id) {

		if (currentAnnotation) {
			currentAnnotation.setImage(Alloy.Globals.Images.map_pin_annotation);
		}

		currentAnnotation = e.annotation;
		currentAnnotation.setImage(Alloy.Globals.Images.map_pin_annotation_selected);

		$.establishment_detail.findEstablishment(e.annotation.establishment_id);
		$.establishment_detail.show();

	}

};

/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.onOpen = onOpen;
exports.onClose = onClose;
exports.cleanup = cleanup;
exports.getCollection = getCollection;