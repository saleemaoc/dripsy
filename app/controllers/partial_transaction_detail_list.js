/**
 * @author jagu
 */

/** ------------------------
 Constants
 ------------------------**/
var TAG = 'controller/partial_transaction_detail_list';
/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var limit = Alloy.CFG.paggination_default || 20;
var collection = $.local_collection;
var transactionID;
var withoutPrice = false;
/** ------------------------
 Dependencies
 ------------------------**/

/**
 * return dependencie to dao
 */
var getDao = function() {
	return Alloy.Globals.DaoBuilder.getTransactionDetailDao();
};

/** ------------------------
 Methods
 ------------------------**/

/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {
		if (_.has(properties, 'transactionID')) {
			transactionID = properties.transactionID;
		}
		if (_.has(properties, 'withoutPrice')) {
			withoutPrice = properties.withoutPrice;
		}
		_.extend($.partial_transaction_detail_list, _.omit(properties, 'transactionID', 'withoutPrice'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
};

/**
 * initialize controller
 */
var init = function() {
	applyProperties(args);
	applyListeners();
	//gc
	args = null;
	showLoader();

	//get data
	_.delay(updateData, Alloy.Globals.Integers.SetDataDelay);
};

/**
 * get query to fetch
 */
var getQuery = function() {
	return ' limit ' + limit + ' offset ' + collection.models.length;
};

/**
 * update data in list
 */
var updateData = function() {
	(require('TiLog')).info(TAG, ' updateData()');
	getDao().findByTransactionID(transactionID, getQuery(), true, onSuccessData, onErrorData, collection);
};

/**
 * @param {Object} e
 */
var onSuccessData = function(e) {
	(require('TiLog')).info(TAG + ' onSuccessData()', JSON.stringify(e));
	hideLoader();
	if (withoutPrice) {
		$.list.setDefaultItemTemplate('without_price');
	}
};

/**
 * @param {Object} e
 */
var onErrorData = function(e) {
	(require('TiLog')).error(TAG + ' onErrorData()', JSON.stringify(e));
	hideLoader();
};

/**
 * show loader and hide list
 */
var showLoader = function() {
	$.loader.show();
	$.list.hide({
		animated : OS_IOS
	});
};

var hideLoader = function() {
	$.loader.hide();
	$.list.show();
};

/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * when window is opened
 */
var onOpen = function(e) {
	if (OS_IOS) {
		init(e);
	} else {
		_.defer(init, e);
	}
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
};

/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.onOpen = onOpen;
exports.onClose = onClose;
exports.cleanup = cleanup;
