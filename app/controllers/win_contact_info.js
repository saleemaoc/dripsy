/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var subtitle = '';
/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {
		if (_.has(properties, 'establishment')) {
			$.contact_info.applyProperties({
				establishment : properties.establishment
			});

		}

		if (_.has(properties, 'subtitle')) {
			subtitle = properties.subtitle;
		}
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
	} catch(e) {
	}
};

var onBack = function() {
	$.win_contact_info.close();
};

/**
 * configure toolbar
 */
var configureToolbar = function(e) {
	if (OS_ANDROID) {
		var activity = $.win_contact_info.getActivity();
		activity.getActionBar().setDisplayShowHomeEnabled(true);
		activity.getActionBar().displayHomeAsUp = true;
		activity.getActionBar().onHomeIconItemSelected = onHomeIconItemSelected;
		activity.getActionBar().setSubtitle(subtitle);
		activity = null;
	}
};
if (OS_ANDROID) {
	/**
	 * on home icon item selected
	 */
	var onHomeIconItemSelected = _.debounce(function(e) {
		onBack();
	}, Alloy.Globals.Integers.debounce, true);
}
/**
 * apply listeners to controller
 */
var applyListeners = function() {
	if (OS_ANDROID) {
		$.win_contact_info.addEventListener('androidback', onBack);
	}
};

/**
 * initialize controller
 */
var init = function(e) {
	applyProperties(args);
	//gc
	args = null;
	configureToolbar(e);
	applyListeners();

};

/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * when window is opened
 */
var onOpen = function(e) {
	if(OS_IOS){
		init(e);
	}else{
		_.defer(init, e);
	}
	$.contact_info.onOpen(e);
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	$.contact_info.onClose(e);
	cleanup();
};

/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.cleanup = cleanup;
