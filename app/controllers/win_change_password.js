/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
if (OS_ANDROID) {
	var abx = require('com.alcoapps.actionbarextras');
}
/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
	$.changePassword.on('success', onSuccess);
	$.changePassword.on('error', onError);
	if (OS_ANDROID) {
		$.win_change_password.addEventListener('androidback', onBack);
	}
};

var onSuccess = function(e) {
	(require('TiUtils')).toast(L('change_password_successful', 'Change password successfully'));
	$.trigger('success', e);
	$.win_change_password.close();
};

var onError = function() {
};

/**
 * initialize controller
 */
var init = function(e) {
	configureToolbar(e);
	applyProperties(args);
	applyListeners();
	//gc
	args = null;
};

/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * when window is opened
 */
var onOpen = function(e) {
	if(OS_IOS){
		init(e);
	}else{
		_.defer(init, e);
	}
	$.changePassword.onOpen(e);
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	$.changePassword.onClose(e);
	cleanup();
};

/**
 * configure toolbar
 */
var configureToolbar = function(e) {
	if (OS_ANDROID) {
		abx.setBackgroundColor(Alloy.Globals.Colors.action_bar_transparent);
		abx.setElevation(0);
		var activity = $.win_change_password.getActivity();
		activity.getActionBar().displayHomeAsUp = true;
		activity.getActionBar().onHomeIconItemSelected = onHomeIconItemSelected;
		//gc
		activity = null;
	}

};

var onBack = function() {
	$.win_change_password.close();
};

if (OS_ANDROID) {
	/**
	 * on home icon item selected
	 */
	var onHomeIconItemSelected = _.debounce(function(e) {
		onBack();
	}, Alloy.Globals.Integers.debounce, true);
}

/**
 * press ok button
 */
var onOk = function(e) {
	$.changePassword.onChangePassword();
};

/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.cleanup = cleanup;
