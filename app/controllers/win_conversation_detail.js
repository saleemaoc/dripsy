/** ------------------------
 Constants
 ------------------------**/
var TAG = 'controllers/win_conversation_detail';
/**
 * 10 seconds
 */
var DELAY_GET_CONVERSATION_STATE = 5000;
/** ------------------------
 Fields
 ------------------------**/
/**
 * Arguments passed into this controller can be accessed via the `$.args` object directly or:
 */
var args = $.args;
/**
 * conversation
 */
var conversation = null;
/**
 * loader
 */
var loader = null;
/**
 * http library
 */
var httpCK;
/**
 * time out to refresh conversation state
 */
var timeOutGetConversationState = null;
/** ------------------------
 Dependencies
 ------------------------**/
/**
 * get service dependency
 * @return {Service}
 */
var getService = function() {
	return Alloy.Globals.ServiceBuilder.getConversationService().inject(getDao());
};

/**
 * get dao dependency
 * @return {Dao}
 */
var getDao = function() {
	return Alloy.Globals.DaoBuilder.getConversationDao();
};
/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		if (_.has(properties, 'conversation')) {
			conversation = properties.conversation;
		}

		_.extend($.win_conversation_detail, _.omit(properties, 'conversation'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
		//abort all request
		if (httpCK) {
			httpCK.abort();
		}
		Ti.App.removeEventListener('closeconversation', onCloseConversation);
		clearGetConversationState();
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
	Ti.App.addEventListener('closeconversation', onCloseConversation);
	if (OS_ANDROID) {
		$.win_conversation_detail.addEventListener('androidback', onBack);
	}
	
	$.conversation_detail.on('conversation:not_found', onCloseConversation);
};

/**
 * initialize controller
 */
var init = function(e) {
	applyProperties(args);
	//gc
	args = null;
	applyListeners();
	configureToolbar(e);
	httpCK = new (require('HttpCK'))();
	_.delay(configureUI, Alloy.Globals.Integers.PreventDetachedView);
};

/**
 * configure toolbar
 */
var configureToolbar = function(e) {
	if (OS_ANDROID) {
		var activity = $.win_conversation_detail.getActivity();
		activity.getActionBar().displayHomeAsUp = true;
		activity.getActionBar().onHomeIconItemSelected = onHomeIconItemSelected;
		activity.onCreateOptionsMenu = onCreateOptionsMenu;
		activity.invalidateOptionsMenu();
		//gc
		activity = null;
	} else {
		configureNavButtons();
	}

};

var onBack = function() {
	$.win_conversation_detail.close();
};

if (OS_ANDROID) {
	/**
	 * on home icon item selected
	 */
	var onHomeIconItemSelected = _.debounce(function(e) {
		onBack();
	}, Alloy.Globals.Integers.debounce, true);

	/**
	 * on create options menu
	 * @param {Object} e
	 */
	var onCreateOptionsMenu = function(e) {
		e.menu.clear();
		var deleteItem = e.menu.add({
			title : L('delete', 'Delete'),
			showAsAction : Ti.Android.SHOW_AS_ACTION_IF_ROOM,
			icon : Alloy.Globals.Images.ic_action_delete_24dp
		});

		deleteItem.addEventListener('click', onDeleteConversation);
		//gc
		deleteItem = null;
	};
}

var configureNavButtons = function() {
	var rightNavButton = Ti.UI.createButton({
		title : L('delete', 'Delete'),
		image : Alloy.Globals.Images.ic_action_delete_24dp
	});

	rightNavButton.addEventListener('click', onDeleteConversation);
	$.win_conversation_detail.setRightNavButton(rightNavButton);

	//gc
	rightNavButton = null;
};
/**
 * configure UI
 */
var configureUI = function() {
	if (_.isObject(conversation)) {
		if (conversation.isPending()) {
			if (conversation.getToUserID() == Alloy.Globals.UserID) {

				//create controller to invitation
				var conversationID = conversation.getID();
				var conversationInvitationController = Alloy.createController('partial_conversation_invitation', {
					conversationID : conversationID,
					left : Alloy.Globals.Dimens.padding_horizontal,
					right : Alloy.Globals.Dimens.padding_horizontal,
					top : Alloy.Globals.Dimens.padding_vertical,
					autoOpen : true
				});

				//configure listeners
				conversationInvitationController.on('invitation:success', onSuccessInvitation);
				conversationInvitationController.on('invitation:loader', showLoader);
				conversationInvitationController.on('invitation:hide', hideLoader);

				//add view to window
				$.v_content_pending_invitation.add(conversationInvitationController.getView());
				//gc
				conversationInvitationController = null;
			} else {
				//mostrar pagina indicando que la solicitud esta pendiente
				$.v_content_pending_invitation.add(Alloy.createController('partial_conversation_pending_invitation').getView());
				_.delay(executeConversationState, Alloy.Globals.Integers.SetDataDelay);
			}
		} else {
			showConveresationDetail();
		}
	}
};

/**
 * on success invitation
 * @param {Object} e
 */
var onSuccessInvitation = function(e) {
	//update conversation
	if (_.isObject(e.conversation)) {
		conversation = e.conversation;
	}

	if (conversation.getState() == Alloy.CFG.constants.conversation_state.rejected) {
		$.win_conversation_detail.close();
	} else {
		$.win_conversation_detail.remove($.v_content_pending_invitation);
		showConveresationDetail();
	}

};

/**
 * show loader
 */
var showLoader = function() {
	$.loader.show();
};

/**
 * hide loader
 */
var hideLoader = function() {
	$.loader.hide();
};

/**
 * show conversation detail
 */
var showConveresationDetail = function() {

	var conversationID = conversation.getID();

	var toUserID = conversation.getOtherUserID();

	$.conversation_detail.applyProperties({
		conversationID : conversationID,
		toUserID : toUserID
	});
	$.v_content_pending_invitation.removeAllChildren();
	$.conversation_detail.onOpen({});
	$.conversation_detail.show();
};

var executeConversationState = function() {
	clearGetConversationState();
	if (conversation.isPending()) {
		timeOutGetConversationState = setTimeout(getConversationState, DELAY_GET_CONVERSATION_STATE);
	}
};

/**
 * find new messages
 */
var getConversationState = function() {
	clearGetConversationState();
	getService().getByID(conversation.getID(), successGetConversationState, errorGetConversationState, httpCK);
};

var clearGetConversationState = function() {
	if (!_.isNull(timeOutGetConversationState)) {
		clearTimeout(timeOutGetConversationState);
		timeOutGetConversationState = null;
	}
	if (httpCK) {
		httpCK.abortByTag(getService().TAGS.TAG_GET_BY_ID);
	}
};

var successGetConversationState = function(response) {
	conversation = getDao().updateModel(response);
	if (conversation.isPending()) {
		executeConversationState();
	} else {
		configureUI();
	}
};

var errorGetConversationState = function(error) {
	executeConversationState();
};

/**
 * delete conversation
 */
var deleteConversation = function() {
	loader = Alloy.createWidget('co.clarika.loader', {
		message : {
			text : L('deleting_conversation', 'Deleting conversation...')
		}
	});
	loader.show();

	var conversationID = conversation.getID();
	getService().destroy(conversationID, onSuccessDeleteConversation, onErrorDeleteConversation, httpCK);
};

/**
 * on success delete conversation
 */
var onSuccessDeleteConversation = function(response) {
	hideWindowLoader();
	$.win_conversation_detail.close();
};

/**
 * on success delete conversation
 */
var onErrorDeleteConversation = function(error) {
	(require('TiLog')).error(TAG, 'onErrorDeleteConversation -> ' + JSON.stringify(error));
	hideWindowLoader();
	(require('TiUtils')).toast(L('deleting_conversation_error', 'An error occurred while trying to delete the conversation'));

};

/**
 * hide loader
 */
var hideWindowLoader = function() {
	if (!_.isNull(loader)) {
		loader.hide();
		loader = null;
	}
};
/** ------------------------
 Listeners
 ------------------------**/

/**
 * when window is opened
 */
var onOpen = function(e) {
	if(OS_IOS){
		init(e);
	}else{
		_.defer(init, e);
	}
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	$.trigger('close', e);
	$.conversation_detail.onClose(e);
	if (OS_IOS) {
		$.trigger('ios:back', {});
	}
	cleanup();

};

/**
 * on delete conversation
 * @param {Object} e
 */
var onDeleteConversation = function(e) {
	var dialog = Titanium.UI.createAlertDialog({
		message : L('conversation_delete_question', 'Do you want to delete the conversation?'),
		buttonNames : [L('cancel', 'Cancel'), L('yes', 'Yes')],
		cancel : 0
	});

	dialog.addEventListener('click', function(e) {
		switch(e.index) {
		case 1:
			deleteConversation();
			break;
		}
	});
	dialog.show();
	//gc
	dialog = null;
};

var onCloseConversation = function() {
	$.win_conversation_detail.close();
};
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.cleanup = cleanup;
