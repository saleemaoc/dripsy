/**
 * @author jagu
 */
/** ------------------------
 Fields
 ------------------------**/
var TAG = 'controller/win_home';
if (OS_ANDROID) {
	var abx = require('com.alcoapps.actionbarextras');
}
/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;

/**
 * 1: List
 * 2: Map
 */
var viewType = 1;

if (OS_ANDROID) {
	var searchView;
}

/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		Ti.App.removeEventListener('closekeyboard', onHideKeyboard);
		Ti.App.removeEventListener('logout', onLogout);
		// remove all event listeners on the controller
		$.off();
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
	if (OS_IOS) {
		$.search.addEventListener('change', onSearch);
	}
	$.home.on('changetab', onChangeTab);
	Ti.App.addEventListener('closekeyboard', onHideKeyboard);
	Ti.App.addEventListener('logout', onLogout);
	
};

/**
 * initialize controller
 */
var init = function(e) {
	applyProperties(args);
	applyListeners();
	//gc
	args = null;
	configureToolbar(e);
};

var beforeOpen = function() {
	Alloy.Globals.PushWooshHelper.initialize();
	if (OS_IOS) {
		Alloy.Globals.AppRoute.setNavigationWindow($.navigationDrawer);
		$.menu.setDrawer($.drawer);
	} else if (OS_ANDROID) {
		$.drawer.on('openDrawer', onOpen);

	}
	$.drawer.on('closeDrawer', onClose);
	$.drawer.on('resumeDrawer', onResume);
};

/**
 * configure toolbar
 */
var configureToolbar = function(e) {
	if (OS_ANDROID) {
		var activity = e.source.getActivity();
		activity.onCreateOptionsMenu = onCreateOptionsMenu;
		activity.invalidateOptionsMenu();
		activity = null;
		abx.setElevation(0);
	} else {
		configureNavButtons();
	}
};

if (OS_ANDROID) {
	/**
	 * on create options menu
	 * @param {Object} e
	 */
	var onCreateOptionsMenu = function(e) {
		e.menu.clear();
		if (viewType == 1) {
			searchView = Titanium.UI.Android.createSearchView({
				hintText : L('search', 'Search'),
			});

			searchView.addEventListener('change', onSearch);
			e.menu.add({
				title : L('search', 'Search'),
				actionView : searchView,
				icon : Alloy.Globals.Images.ic_action_search_24dp,
				showAsAction : Ti.Android.SHOW_AS_ACTION_IF_ROOM | Ti.Android.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW
			});

		}
		var mapItem = e.menu.add({
			title : viewType == 1 ? L('map', 'Map') : L('list', 'List'),
			showAsAction : Ti.Android.SHOW_AS_ACTION_IF_ROOM,
			icon : viewType == 1 ? Alloy.Globals.Images.ic_action_pin_24dp : Alloy.Globals.Images.ic_action_list_24dp
		});

		mapItem.addEventListener('click', onSwitchView);
		//gc
		mapItem = null;
	};
}

var configureNavButtons = function() {
	var rightNavButton = Ti.UI.createButton({
		title : viewType == 1 ? L('map', 'Map') : L('list', 'List'),
		image : viewType == 1 ? Alloy.Globals.Images.ic_action_pin_24dp : Alloy.Globals.Images.ic_action_list_24dp
	});

	rightNavButton.addEventListener('click', onSwitchView);
	$.win_center.setRightNavButton(rightNavButton);

	//gc
	rightNavButton = null;

};
var refreshToolbar = function() {
	if (OS_ANDROID) {
		$.drawer.window.getActivity().invalidateOptionsMenu();
	} else {
		configureNavButtons();
	}
};

var checkIfEnabledGPS = function() {
	if (viewType == 1) {
		if (!Titanium.Geolocation.locationServicesEnabled) {
			(require('LocationUtils')).questionOpenSettings();
			return false;
		} else {
			return true;
		}
	} else {
		return true;
	}
};
/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * when window is opened
 */
var onOpen = function(e) {
	if (OS_IOS) {
		init(e);
	} else {
		_.defer(init, e);
	}
	$.menu.onOpen(e);
	$.home.onOpen(e);
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	Alloy.Globals.AppProperties.updateNotificationCheckDate();
	cleanup();
	$.home.onClose(e);
	$.menu.onClose(e);

};

/**
 * restaurant search
 */
var onSearch = function(e) {
	$.home.onSearch(e);
};

var onResume = function(e) {
	Alloy.Globals.PushWooshHelper.clearNotifications();
	findNotifications();
	Alloy.Globals.AppRoute.setCurrentRoute('win_home');
	$.home.onResume(e);
	$.menu.onResume(e);
	
};

var findNotifications = function() {

	if (Alloy.Globals.FirstTimeFindNotification) {
		Alloy.Globals.ServiceBuilder.getNotificationService().get(function(e) {
			Alloy.Globals.FirstTimeFindNotification = false;
		}, function() {
			//nothing
		});
	}

};

var onSwitchView = function(e) {
	if (checkIfEnabledGPS()) {
		viewType = viewType == 1 ? 2 : 1;
		refreshToolbar();
		$.home.setViewType(viewType);
		if (OS_IOS) {
			if (viewType == 2) {

				$.search.hide();

				$.home.applyProperties({
					top : 0
				});

			} else {

				$.home.applyProperties({
					top : Alloy.Globals.Dimens.search_bar_height
				});

				$.search.show();
			}
		};
	}
};

var onToogle = function(e) {
	onHideKeyboard();
	if (OS_IOS) {
		$.drawer.toggleLeftWindow();
	}

};

/**
 * on change tab
 * @param {Object}
 */
var onChangeTab = function(e) {
	if (OS_IOS) {
		$.search.setValue("");
	}
};

/**
 * android back
 * @param {Object} e
 */
var onBack = function(e) {
	(require('TiUtils')).toast('androidback');
};

/**
 * hide keyboard
 * @param {Object} e
 */
var onHideKeyboard = function(e) {
	if (OS_ANDROID) {
		if (searchView) {
			searchView.blur();
		}
		Ti.UI.Android.hideSoftKeyboard();
	} else {
		$.search.blur();
	}
};

var onLogout = function() {
	$.drawer.close();
};
beforeOpen();
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.cleanup = cleanup;
