/** ------------------------
 Constants
 ------------------------**/
var TAG = 'controllers/win_my_transactions';
/** ------------------------
 Fields
 ------------------------**/
if (OS_ANDROID) {
	var abx = require('com.alcoapps.actionbarextras');
}
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
/**
 * flag to initialize controllers
 */
var firstTimeGift = true;
var firstTimeTransaction = true;
/**
 * page selected
 */
var currentPage = 0;
/**
 * indicate first time to call onresume
 */
var firstTimeResume = true;

/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {
		if (_.has(properties, 'userID')) {
			$.my_transactions.applyProperties({
				userID : properties.userID
			});
			$.gifts.applyProperties({
				userID : properties.userID
			});
		}
		if (_.has(properties, 'currentPage')) {
			currentPage = properties.currentPage;
		}

	}
};

/**
 * before open window
 */
var beforeOpen = function() {
	if (OS_ANDROID) {
		$.win_my_transactions.getActivity().onResume = onResume;
	} else if (OS_IOS) {
		$.my_transactions.on('ios:back', onResume);
		$.gifts.on('ios:back', onResume);
	}

	applyProperties(args);
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
	$.scrollable_view.addEventListener('scrollend', onScrollEnd);
	if (OS_ANDROID) {
		$.win_my_transactions.addEventListener('androidback', onBack);
	}
};

/**
 * initialize controller
 */
var init = function(e) {
	applyProperties(args);
	//gc
	args = null;
	applyListeners();
	configureToolbar(e);
	$.tabs.setScrollableView($.scrollable_view);
	$.scrollable_view.setCurrentPage(currentPage);
	if (currentPage == 0) {
		initializeTransactions();
	} else {
		initializeGifts();
	}
};

var onBack = function() {
	$.win_my_transactions.close();
};

/**
 * configure toolbar
 */
var configureToolbar = function(e) {
	if (OS_ANDROID) {
		var activity = $.win_my_transactions.getActivity();
		activity.getActionBar().displayHomeAsUp = true;
		activity.getActionBar().onHomeIconItemSelected = onHomeIconItemSelected;
		//gc
		activity = null;
		abx.setElevation(0);
	}

};

if (OS_ANDROID) {
	/**
	 * on home icon item selected
	 */
	var onHomeIconItemSelected = _.debounce(function(e) {
		onBack();
	}, Alloy.Globals.Integers.debounce, true);
}

/**
 * initialize gift controller
 */
var initializeGifts = function() {
	firstTimeGift = false;
	$.gifts.onOpen({});
};

var initializeTransactions = function() {
	firstTimeTransaction = false;
	$.my_transactions.onOpen({});
};

/**
 * Fired when the view has stopped moving completely.
 * @param {Object} e
 */
var onScrollEnd = function(e) {
	currentPage = e.currentPage;
	if (firstTimeTransaction && currentPage == 0) {
		initializeTransactions();
	} else if (firstTimeGift && currentPage == 1) {
		initializeGifts();
	}
};

/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * when window is opened
 */
var onOpen = function(e) {
	if(OS_IOS){
		init(e);
	}else{
		_.defer(init, e);
	}

	//clear transaction badge
	Alloy.Globals.AppProperties.setPendingTransaction(false);
};

/**
 * on resume window
 */
var onResume = function(e) {
	Alloy.Globals.AppRoute.setCurrentRoute('win_my_transactions');
	if (!firstTimeResume) {
		switch(currentPage) {
		case 0:
			$.my_transactions.onResume(e);
			break;
		case 1:
			$.gifts.onResume(e);
			break;
		}
	}
	firstTimeResume = false;
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	$.trigger('close', {});
	cleanup();
	$.my_transactions.onClose(e);
	$.gifts.onClose(e);
};

/**
 * when user select tab
 * @param {Object} e
 */
var onSelectTab = function(e) {
	if (firstTimeTransaction && e.tag == 0) {
		initializeTransactions();
	} else if (firstTimeGift && e.tab == 1) {
		initializeGifts();
	}
};

beforeOpen();
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.cleanup = cleanup;
