/** ---------
 Constants
 -------- **/
var TAG = 'controllers/tableview_row_gift';
/** ---------
 Fields
 -------- **/
/**
 *Arguments passed into this controller can be accessed via the `$.args` object directly or:
 */
var args = $.args;
/** ---------
 Methods
 -------- **/
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		if (_.has(properties, 'image')) {
			$.image.applyProperties(properties.image);
			$.image.refresh();
		}

		if (_.has(properties, 'description')) {
			_.extend($.lbl_description, properties.description);
		}

		if (_.has(properties, 'state')) {
			$.label_state.applyProperties(properties.state);
		}

		if (_.has(properties, 'date')) {
			_.extend($.lbl_date, properties.date);
		}

		_.extend($.tableview_row_gift, _.omit(properties, 'image', 'description', 'state', 'date'));
	}
};

var init = function() {
	applyProperties(args);
	//gc
	args = null;
};

//initialize row
init();

/** ---------
 Public
 -------- **/
exports.applyProperties = applyProperties;
