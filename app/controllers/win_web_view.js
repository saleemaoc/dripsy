/**
 * @author jagu
 */
/** ------------------------
 Fields
 ------------------------**/
var TAG = 'controller/win_web_view';
/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {
		if (_.has(properties, 'url')) {
			setUrl(properties.url);
		}
		_.extend($.win_web_view, _.omit(properties, 'url'));
	}
};

/*
 * @params url
 */
var setUrl = function(url) {
	showLoader();
	$.web_view.url = url;
};

/**
 *show loader
 */
var showLoader = function() {
	$.loader.show();
	$.web_view.hide();
};

/**
 *hide loader
 */
var hideLoader = function() {
	$.loader.hide();
};

var onLoad = function() {
	if (Titanium.Network.networkType == Titanium.Network.NETWORK_NONE) {
		onError();
	} else {
		$.web_view.show();
		hideLoader();
	}
};

/**
 *show error view
 */
var onError = function() {
	Ti.API.error('error');
	$.web_view.hide();
	hideLoader();
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
	} catch(e) {
	}
};

/**
 * configure toolbar
 */
var configureToolbar = function(e) {
	if (OS_ANDROID) {
		var activity = $.win_web_view.getActivity();
		activity.getActionBar().setDisplayShowHomeEnabled(true);
		activity.getActionBar().displayHomeAsUp = true;
		activity.getActionBar().onHomeIconItemSelected = onHomeIconItemSelected;
		activity = null;
	}
};

var onBack = function() {
	$.win_web_view.close();
};

if (OS_ANDROID) {
	/**
	 * on home icon item selected
	 */
	var onHomeIconItemSelected = _.debounce(function(e) {
		onBack();
	}, Alloy.Globals.Integers.debounce, true);
}
/**
 * apply listeners to controller
 */
var applyListeners = function() {
	$.web_view.addEventListener('onLoad', onLoad);
	$.web_view.addEventListener('onError', onError);
	if (OS_ANDROID) {
		$.win_web_view.addEventListener('androidback', onBack);
	}
};

/**
 * initialize controller
 */
var init = function(e) {
	configureToolbar(e);
	applyProperties(args);
	applyListeners();
	//gc
	args = null;
};

/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * when window is opened
 */
var onOpen = function(e) {
	init(e);
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
};

var onBack = function(e) {
	$.win_web_view.close();
};
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.cleanup = cleanup;
