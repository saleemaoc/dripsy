/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
/**
 * toolbar subtitle
 */
var subtitle = '';
/**
 * purchase store
 */
var purchaseStore = [];
/**
 * to send gift
 */
var gift = null;
/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		if (_.has(properties, 'subtitle')) {
			subtitle = properties.subtitle;
		}

		if (_.has(properties, 'purchaseStore')) {
			purchaseStore = properties.purchaseStore;
		}

		if (_.has(properties, 'gift')) {
			gift = properties.gift;
		}

		_.extend($.win_cart_drink, _.omit(properties, 'subtitle', 'purchaseStore'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
	$.cart.on('gift:success', onSuccessGift);
	$.cart.on('transaction:success', onSuccessTransaction);
	if (OS_ANDROID) {
		$.win_cart_drink.addEventListener('androidback', onBack);
	}
};

/**
 * initialize controller
 */
var init = function(e) {
	applyProperties(args);
	applyListeners();
	//gc
	args = null;
	configureToolbar(e);
	updateData();
};

/**
 * configure toolbar
 */
var configureToolbar = function(e) {
	if (OS_ANDROID) {
		var activity = $.win_cart_drink.getActivity();
		activity.getActionBar().displayHomeAsUp = true;
		activity.getActionBar().onHomeIconItemSelected = onHomeIconItemSelected;
		activity.getActionBar().setSubtitle(subtitle);
		//gc
		activity = null;
	}

};

var onBack = function() {
	$.win_cart_drink.close();
};

if (OS_ANDROID) {
	/**
	 * on home icon item selected
	 */
	var onHomeIconItemSelected = _.debounce(function(e) {
		onBack();
	}, Alloy.Globals.Integers.debounce, true);
}

/**
 * update data in cart controller
 */
var updateData = function() {
	$.cart.applyProperties({
		purchaseStore : purchaseStore,
		gift : gift
	});
	$.cart.updateData();
};
/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * when window is opened
 */
var onOpen = function(e) {
	if(OS_IOS){
		init(e);
	}else{
		_.defer(init, e);
	}
	$.cart.onOpen(e);

};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	$.cart.onClose(e);
	cleanup();
};

/**
 * on success send gift
 * @param {Object} e
 */
var onSuccessGift = function(e) {
	$.win_cart_drink.close();
	$.trigger('gift:success', {});
};

/**
 * on success buy drinks
 * @param {Object} e
 */
var onSuccessTransaction = function(e) {
	$.win_cart_drink.close();
	$.trigger('transaction:success', e);
};

/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.cleanup = cleanup;
