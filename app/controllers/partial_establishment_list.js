/**
 * @author jagu
 */

/** ------------------------
 Constants
 ------------------------**/
var TAG = 'controller/partial_establishment_list';
/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
/**
 * limit to establishment query
 */
var limit = Alloy.CFG.paggination_default || 20;
/**
 * collection
 */
var collection = $.local_collection;
/**
 * http library
 */
var httpCK;
/**
 * type to establishment
 */
var type = null;
/**
 * name to filter establishment
 */
var name = '';
/**
 * flag to indicate state to loader
 */
var loading = false;
/**
 * refresh flags
 */
var isPullToRefresh = false;
var isScrolling = false;
/**
 * timeout to save search
 */
var timeOutOnSearch = null;

var autoSearch = true;
/** ------------------------
 Dependencies
 ------------------------**/

/**
 * return dependencie to service
 */
var getService = function() {
	return Alloy.Globals.ServiceBuilder.getEstablishmentService().inject(getDao());
};

/**
 * return dependencie to dao
 */
var getDao = function() {
	return new Alloy.Globals.DaoBuilder.getEstablishmentDao();
};

/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {
		if (_.has(properties, 'type')) {
			type = properties.type;
		}

		if (_.has(properties, 'name')) {
			name = properties.name;
		}

		if (_.has(properties, 'collection')) {
			collection = properties.collection;
			$.local_collection = collection;
		}

		if (_.has(properties, 'autoSearch')) {
			autoSearch = properties.autoSearch;
		}

		_.extend($.partial_establishment_list, _.omit(properties, 'type', 'collection', 'name', 'autoSearch'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
		$.is.cleanup();
		if (httpCK) {
			httpCK.abort();
		}
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
	$.list.addEventListener('itemclick', onClickItem);
};

/**
 * initialize controller
 */
var init = function() {
	applyProperties(args);
	applyListeners();
	//gc
	args = null;
	//initialize inifinite scroll
	$.is.init($.list);
	//create http instance
	httpCK = new (require('HttpCK'))();
	if (collection.models.length == 0) {
		showLoader();
	}
	if (autoSearch) {
		//get data
		_.delay(getData, Alloy.Globals.Integers.SetDataDelay);
	}
};

/**
 * get params to get
 */
var getParamsToGet = function() {

	return {
		city_id : Alloy.Globals.CurrentCity,
		name : name,
		type : type,
		limit : limit,
		offset : collection.models.length
	};

};

var getExtraParams = function() {
	//if is pull to refresh ignore cache
	var extraParams = {
		ttl : !isPullToRefresh
	};
	return extraParams;
};

/**
 * get data
 */
var getData = function() {
	var options = getExtraParams();
	getService().get(getParamsToGet(), onSuccessGet, onErrorGet, httpCK, options);
};

/**
 * success get service
 * @param {Object} response
 */
var onSuccessGet = function(response) {

	if (response.results.length < limit) {
		_.delay(removeScrolling, Alloy.Globals.Integers.PreventDetachedView);
	}

	hidePullToRefresh();
	updateData();
};

/**
 * when service display an error
 * @param {String} error
 */
var onErrorGet = function(error) {
	(require('TiLog')).error(TAG + ' onErrorGet()', error);
	hidePullToRefresh();
	updateData();
};

var getQuery = function() {
	return ' and city_id = ' + Alloy.Globals.CurrentCity + ' and name LIKE "%' + name + '%" limit ' + limit + ' offset ' + collection.models.length;
};

/**
 * update data in list
 */
var updateData = function() {
	if (collection.models.length == 0) {
		hideLoader();
	}
	if (type == Alloy.CFG.constants.establishment_type.bar) {
		getDao().findBars(getQuery(), true, onSuccessData, onErrorData, collection);
	} else {
		getDao().findNightclubs(getQuery(), true, onSuccessData, onErrorData, collection);
	}

};

/**
 * @param {Object} e
 */
var onSuccessData = function(e) {
	(require('TiLog')).info(TAG + ' onSuccessData()', JSON.stringify(e));
	showEmptyMessage();
	hideLoaderScrolling();
};

/**
 * @param {Object} e
 */
var onErrorData = function(e) {
	(require('TiLog')).error(TAG + ' onErrorData()', JSON.stringify(e));
	hideLoaderScrolling();
};

/**
 * Show main loader and hide list
 */
var showLoader = function() {
	if (!loading) {
		loading = true;
		$.loader.show();
		$.list.hide({
			animated : OS_IOS
		});
		$.empty.hide();
	}
};

/**
 * Hide main loader and show list
 */
var hideLoader = function() {
	if (loading) {
		loading = false;
		$.loader.hide();
		$.list.show({
			animated : OS_IOS
		});
	}
};

/**
 * Fire pull to refresh
 */
var refresh = function() {
	$.ptr.refresh();
};

/**
 * check if need show empty message
 */
var showEmptyMessage = function() {
	if (collection.models.length > 0) {
		$.list.visible = true;
		$.empty.hide();
	} else {
		$.empty.show();
	}

};
/**
 * hide pull to refresh is showing
 */
var hidePullToRefresh = function() {
	if (isPullToRefresh) {
		isPullToRefresh = false;
		$.ptr.hide();
	}
};

/**
 * detached infinite scrolling
 */
var removeScrolling = function() {
	isScrolling = false;
	$.is.state($.is.DONE);
};

/**
 * hide loader scrolling
 */
var hideLoaderScrolling = function() {
	if (isScrolling) {
		isScrolling = false;
		$.is.state($.is.SUCCESS);
	}
};

/**
 * get collection
 */
var getCollection = function() {
	return collection;
};

/**
 * abort request to get establishments
 */
var cancelGetEstablishments = function() {
	if (_.isObject(httpCK)) {
		httpCK.abortByTag(getService().TAGS.TAG_GET);
	}
};

/**
 * reset collection
 */
var collectionReset = function() {
	if (collection.models.length > 0) {
		collection.reset();
	}
};

/**
 * search establishment by change name
 */
var searchByName = function() {
	collectionReset();
	showLoader();

	if (timeOutOnSearch != null) {
		clearTimeout(timeOutOnSearch);
		timeOutOnSearch = null;
	}

	timeOutOnSearch = setTimeout(function() {
		cancelGetEstablishments();
		getData();
	}, Alloy.Globals.Integers.search);
};
/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * when window is opened
 */
var onOpen = function(e) {
	if (OS_IOS) {
		init(e);
	} else {
		_.defer(init, e);
	}
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
};

/**
 * When user tap pull to refresh
 * @param {Object} e
 */
var onPullToRefresh = function(e) {
	if (!isPullToRefresh) {
		isPullToRefresh = true;
		cancelGetEstablishments();
		getDao().destroyAllByType(type);
		collectionReset();
		showLoader();
		getData();
	}
};

/**
 * when user scrolling to end
 * @param {Object} e
 */
var onEndScrolling = function(e) {
	isScrolling = true;
	getData();
};

/**
 * on click item
 * @param {Object} e
 */
var onClickItem = function(e) {
	var model = collection.models[e.itemIndex];
	$.trigger('itemClick', {
		establishment : model
	});
	var params = {
		establishment : model
	};
	if (OS_IOS) {
		params.title = model.getName();
	}
	Alloy.Globals.AppRoute.openEstablishmentDetail(params);
	//gc
	model = null;
};

/**
 * on search
 * @param {Object} e
 */
var onSearch = function(e) {

	if (e.source.value != name) {
		name = e.source.value;
		searchByName();
	}
};

/**
 * fire search
 * @param {String} value
 */
var fireSearch = function(value) {
	name = value;
	searchByName();
};
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.onOpen = onOpen;
exports.onClose = onClose;
exports.cleanup = cleanup;
exports.refresh = refresh;
exports.onSearch = onSearch;
exports.getCollection = getCollection;
exports.fireSearch = fireSearch;
