/**
 * @author jagu
 *
 */
/** ------------------------
 Constants
 ------------------------**/
var TAG = 'controllers/partial_conversation_invitation';
/** ------------------------
 Fields
 ------------------------**/
/**
 * Arguments passed into this controller can be accessed via the `$.args` object directly or:
 */
var args = $.args;
/**
 * conversation id to accept or reject invitation
 */
var conversationID = null;
/**
 * http library
 */
var httpCK;
/** ------------------------
 Dependencies
 ------------------------**/

/**
 * return conversation dependencie to service
 * @return {Service}
 */
var getService = function() {
	return Alloy.Globals.ServiceBuilder.getConversationService().inject(getDao());
};

/**
 * return conversation dependencie to dao
 * @return {Dao}
 */
var getDao = function() {
	return Alloy.Globals.DaoBuilder.getConversationDao();
};

/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		if (_.has(properties, 'conversationID')) {
			conversationID = properties.conversationID;
		}

		_.extend($.partial_conversation_invitation, _.omit(properties, 'conversationID'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
		if (httpCK) {
			httpCK.abort();
		}
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
	$.btn_accept.addEventListener('click', onAccept);
	$.btn_reject.addEventListener('click', onReject);
};

/**
 * initialize controller
 */
var init = function() {
	applyProperties(args);
	//gc
	args = null;
	//create http instance
	httpCK = new (require('HttpCK'))();
	applyListeners();

};

/**
 * show loader
 */
var showLoader = function() {
	$.partial_conversation_invitation.hide({
		animated : OS_IOS
	});
	$.trigger('invitation:loader');
};

/**
 * hide loader
 */
var hideLoader = function() {
	$.partial_conversation_invitation.show({
		animated : OS_IOS
	});
	$.trigger('invitation:hide');
};

/**
 * on success accept or reject
 * @param {Object} response
 */
var onSuccess = function(response) {
	(require('TiLog')).info(TAG, 'onSuccess -> ' + JSON.stringify(response));

	var conversation = getDao().findByID(response.id);

	hideLoader();

	$.trigger('invitation:success', {
		response : response,
		conversation : conversation
	});

};

/**
 * on error accept or reject
 * @param {Object} error
 */
var onError = function(error) {
	(require('TiLog')).error(TAG, 'onError -> ' + JSON.stringify(error));
	(require('TiUtils')).toast(L('error_conversation_invitation', 'An error occurred while trying to send your reply'));
	hideLoader();
};

/** ------------------------
 Listeners
 ------------------------**/

/**
 * when window is opened
 */
var onOpen = function(e) {
	if (OS_IOS) {
		init(e);
	} else {
		_.defer(init, e);
	}
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
};

/**
 * on accept invitation
 * @param {Object} params
 */
var onAccept = _.debounce(function(params) {
	showLoader();
	getService().accept(conversationID, onSuccess, onError, httpCK);
}, Alloy.Globals.Integers.debounce, true);

/**
 * on reject invitation
 * @param {Object} params
 */
var onReject = _.debounce(function(params) {
	showLoader();
	getService().reject(conversationID, onSuccess, onError, httpCK);
}, Alloy.Globals.Integers.debounce, true);

if (args.autoOpen) {
	init();
}
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.onOpen = onOpen;
exports.onClose = onClose;
exports.cleanup = cleanup;
