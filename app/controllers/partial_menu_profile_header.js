/*
NightToNight
Created by Juan Ignacio Agu on 2016-09-02.
Copyright 2016 Juan Ignacio Agu. All rights reserved.
*/

/** ---------
 Constants
 -------- **/
var TAG = 'controllers/partial_menu_profile_header';
/** ---------
 Fields
 -------- **/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
/** ---------
 Methods
 -------- **/

/**
 * apply properties to widget
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		if (_.has(properties, 'full_name')) {
			_.extend($.lbl_full_name, properties.full_name);
		}

		if (_.has(properties, 'state')) {
			_.extend($.lbl_state, properties.state);
		}

		if (_.has(properties, 'image')) {
			_.extend($.img_avatar, _.omit(properties.image, 'image', 'defaultImage', 'brokenLinkImage'));

			if (_.has(properties.image, 'defaultImage')) {
				if (OS_IOS) {
					$.img_avatar.defaultImage = properties.image.defaultImage;
					$.img_avatar.brokenLinkImage = properties.image.defaultImage;
				} else if (OS_ANDROID) {
					$.img_avatar.setDefaultImage(properties.image.defaultImage);
					$.img_avatar.setBrokenLinkImage(properties.image.defaultImage);
				}

			}
			if (_.has(properties.image, 'image') && Alloy.Globals.JsUtils.validateURL(properties.image.image)) {
				if (OS_IOS) {
					$.img_avatar.image = properties.image.image;
				} else {
					$.img_avatar.setImage(properties.image.image);
				}
			}

		}
		if (_.has(properties, 'background_image')) {
			_.extend($.background_image, _.omit(properties.background_image, 'image', 'defaultImage', 'brokenLinkImage'));

			if (_.has(properties.background_image, 'defaultImage')) {
				if (OS_IOS) {
					$.background_image.defaultImage = properties.background_image.defaultImage;
					$.background_image.brokenLinkImage = properties.background_image.defaultImage;
				} else {
					$.background_image.setDefaultImage(properties.background_image.defaultImage);
					$.background_image.setBrokenLinkImage(properties.background_image.defaultImage);
				}

			}
			if (_.has(properties.background_image, 'image') && Alloy.Globals.JsUtils.validateURL(properties.background_image.image)) {
				if (OS_IOS) {
					$.background_image.image = properties.background_image.image;
				} else {
					$.background_image.setImage(properties.background_image.image);
				}

			}

		}

		_.extend($.partial_menu_profile_header, _.omit(properties, 'full_name', 'state', 'image', 'background_image'));
	}
};

/**
 * apply listeners to widget
 */
var applyListeners = function() {
	$.partial_menu_profile_header.addEventListener('click', onClick);
};

var init = function() {
	applyListeners();
	applyProperties(args);
	//gc
	args = null;
};

/** ---------
 Listeners
 -------- **/

/**
 * @param {Object} e
 */
var onClick = function(e) {
	$.trigger('click', e);
};

(function configureUI() {
})();
init({});
/** ---------
 Public
 -------- **/
exports.applyProperties = applyProperties;
exports.addEventListener = $.on;
exports.removeEventListener = $.off;
