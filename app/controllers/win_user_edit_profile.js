/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
//action bar extra only for android
if (OS_ANDROID) {
	var abx = require('com.alcoapps.actionbarextras');
}
/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {
		if (_.has(properties, 'userID')) {
			$.edit_profile.applyProperties({
				userID : properties.userID
			});
			$.edit_profile.refresh();
		} else if (_.has(properties, 'user')) {
			$.edit_profile.applyProperties({
				user : properties.user
			});
			$.edit_profile.refresh();
		}

		_.extend($.win_user_edit_profile, _.omit(properties, 'userID', 'user'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
	} catch(e) {
	}
};

var onBack = function() {
	$.win_user_edit_profile.close();
};

/**
 * configure toolbar
 */
var configureToolbar = function(e) {
	if (OS_ANDROID) {
		abx.setBackgroundColor(Alloy.Globals.Colors.action_bar_transparent);
		abx.setElevation(0);
		var activity = $.win_user_edit_profile.getActivity();
		activity.getActionBar().displayHomeAsUp = true;
		activity.getActionBar().onHomeIconItemSelected = onHomeIconItemSelected;
		//gc
		activity = null;
	}
};

/**
 * on home icon item selected
 */
var onHomeIconItemSelected = _.debounce(function(e) {
	onBack();
}, Alloy.Globals.Integers.debounce, true);

/**
 * apply listeners to controller
 */
var applyListeners = function() {
	$.edit_profile.on('success', onSuccess);
	$.edit_profile.on('error', onError);
	$.edit_profile.on('scroll', onScroll);
	if (OS_ANDROID) {
		$.win_user_edit_profile.addEventListener('androidback', onBack);
	}
};

/**
 * on success edit profile
 * @param {Object} e
 */
var onSuccess = function(e) {
	$.win_user_edit_profile.close();
};

/**
 * on error edit profile
 * @param {Object} e
 */
var onError = function(error) {
};

/**
 * initialize controller
 */
var init = function(e) {
	configureToolbar(e);
	applyProperties(args);
	applyListeners();
	//gc
	args = null;
	configureActivity();
};

var configureActivity = function() {
	if (OS_ANDROID) {
		var currentActivity = $.win_user_edit_profile.getActivity();
		$.edit_profile.applyProperties({
			currentActivity : currentActivity
		});
		//gc
		currentActivity = null;
	}
};
/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * when window is opened
 */
var onOpen = function(e) {
	if(OS_IOS){
		init(e);
	}else{
		_.defer(init, e);
	}
	$.edit_profile.onOpen(e);
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {

	$.edit_profile.onClose(e);
	if (OS_IOS) {
		$.trigger('ios:back', {});
	}
	cleanup();

};
/**
 * press ok data
 */
var onOk = function(e) {
	$.edit_profile.onEditProfile();
};

/**
 * Fired when user scroll
 */
var onScroll = function(e) {
	if (OS_ANDROID) {
		abx.setBackgroundColor((require('TiUtils')).calculateActionBarBackgroundColor(e, Alloy.Globals.Dimens.registration_photo_height));
	}
};
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.cleanup = cleanup;
