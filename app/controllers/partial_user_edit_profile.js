/**
 * @author pflores - jagu
 *
 */
/** -----------------------------
 * Constants
 ------------------------------**/
var TAG = 'controllers/partial_user_edit_profile';
/** ------------------------
 Fields
 ------------------------**/
/**
 * Arguments passed into this controller can be accessed via the `$.args` object directly or:
 */
var args = $.args;
var httpCK;
var currentCountryID = -1;
var user;
var userID;

/**
 * flag to prevent two loaders
 */
var needLoader = true;
/**
 * flag to indicate if user change photo
 */
var userPhotoIsChange = false;
/**
 * load countries one time
 */
var needLoadCountries = true;

var newInterestsToService = [];
/** ------------------------
 Dependencies
 ------------------------**/
/**
 * get country service
 */
var getCountryService = function() {
	return Alloy.Globals.ServiceBuilder.getCountryService().inject(getCountryDao());
};

/**
 * get city service
 */
var getCityService = function() {
	return Alloy.Globals.ServiceBuilder.getCityService().inject(getCityDao());
};

/**
 * get country dao
 */
var getCountryDao = function() {
	return Alloy.Globals.DaoBuilder.getCountryDao();
};

var getCityDao = function() {
	return Alloy.Globals.DaoBuilder.getCityDao();
};
/**
 * get user dao
 */
var getUserDao = function() {
	return Alloy.Globals.DaoBuilder.getUserDao();
};

/**
 * get user service
 */
var getUserService = function() {
	return Alloy.Globals.ServiceBuilder.getUserService();
};

var getTagDao = function() {
	return Alloy.Globals.DaoBuilder.getTagDao();
};

var getTagService = function() {
	return Alloy.Globals.ServiceBuilder.getTagService().inject(getTagDao());
};

/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		if (_.has(properties, 'user')) {
			user = properties.user;
		}

		if (_.has(properties, 'userID')) {
			userID = properties.userID;
		}

		if (_.has(properties, 'currentActivity')) {
			$.photo.applyProperties({
				activity : properties.currentActivity
			});
		}
		_.extend($.partial_user_edit_profile, _.omit(properties, 'user', 'userID', 'currentActivity'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
		if (httpCK) {
			httpCK.abort();
		}
		$.interests.cleanup();
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
	$.registration_edition.on('changeGender', onChangeGender);
	$.registration_edition.on('hidekeyboard', hideKeyboard);
	$.country_picker.on('change', onChangeCountry);
	$.city_picker.on('change', onChangeCity);
	$.city_picker.on('click', onClickComboBox);
	$.country_picker.on('click', onClickComboBox);
	$.interests.on('create:tag', onCreateTag);
	$.interests.on('success:tag', onSuccessTag);
	$.interests.on('remove:tag', onRemoveTag);
	$.partial_user_edit_profile.addEventListener('scroll', onScroll);
	$.photo.on('success', onSuccessPhoto);
};

/**
 * initialize controller
 */
var init = function() {
	applyProperties(args);
	applyListeners();
	//gc
	args = null;
	httpCK = new (require('HttpCK'))();
	loaderInfo();
};

/**
 * load user info to view
 */
var loaderInfo = function() {
	if (needLoader && _.isObject(user)) {

		needLoader = false;
		$.txt_state.setValue(user.getState());

		$.registration_edition.setFirstName(user.getFirstName());
		$.registration_edition.setLastName(user.getLastName());
		$.registration_edition.setEmail(user.getMail());
		$.registration_edition.setGender(user.getGender());
		$.registration_edition.setDateBirth((require('FormatUtils')).getDateToString(user.get('date_birth')));
		$.photo.applyProperties({
			image : {
				image : user.getPhoto(),
				defaultImage : user.getBackgroundDefault()
			}
		});

		$.txt_occupation.setValue(user.getOccupation());
		$.txt_university.setValue(user.getUniversity());
		$.txt_wyw.setValue(user.getWhereWork());
		$.txt_phone.setValue(user.getPhone());
		$.txt_favorite_drink.setValue(user.getFavoriteDrink());
		//interest
		refreshTags();

		_.delay(hideLoader, 100);

		if (needLoadCountries) {
			_.delay(getCountryData, 500);
			needLoadCountries = false;
		}
	}
};

var refreshTags = function() {
	var tagsByUser = Alloy.Globals.DaoBuilder.getUserTagDao().findTagsByUser(user.get('id'));
	$.interests.applyProperties({
		tags : tagsByUser,
		collection : getTagDao().getCollection()
	});
	$.interests.refreshTags();
};
/**
 * refresh user information
 */
var refresh = function() {
	if (!_.isObject(user) && _.isNumber(userID)) {
		findUser();
	}
	loaderInfo();
};

/**
 * find user by id
 */
var findUser = function() {
	user = getUserDao().findByID(userID, false);
};

/**
 * get country data
 */

var getCountryData = function() {
	(require('TiLog')).info(TAG, ' getCountryData()');
	$.country_picker.showLoader();
	$.city_picker.showLoader();
	getCountryService().get(getParamsToGetCountry(), onSuccessGetCountry, onErrorGetCountry, httpCK);
};

var getParamsToGetCountry = function() {
	return {
	};
};

/**
 * on success get country
 * @param {Object} response
 */
var onSuccessGetCountry = function(reponse) {
	(require('TiLog')).info(TAG, ' onSuccessGetCountry()');
	_.defer(configureCountry);
};

/**
 * on error get country
 * @param {Object} error
 */
var onErrorGetCountry = function(error) {
	(require('TiLog')).error(TAG + ' onErrorGetCountry()', error);
	getCountryData();
};

/**
 * configure country picker
 */
var configureCountry = function() {
	$.country_picker.applyProperties({
		options : $.country_picker.formatDataToPicker(getCountryDao().findWithOrder(), 'id', 'name', true, L('country', 'Country')),
		selectedByID : user.get('country_id')
	});
	if (user.get('city_id') != -1) {
		getCityData();
	} else {
		configureCity();
	}
	$.country_picker.hideLoader();

};

var getCityData = function() {
	showLoaderCity();
	(require('TiLog')).info(TAG, ' getCountryData()');
	getCityService().get(getParamsToCity(), onSuccessGetCity, onErrorGetCity, httpCK);
};

/**
 * on success get city
 * @param {Object} response
 */
var onSuccessGetCity = function(reponse) {
	(require('TiLog')).info(TAG, ' onSuccessGetCity()');
	configureCity();
};

var getParamsToCity = function() {
	var params = {
		country_id : user.get('country_id'),
	};
	return params;

};
/**
 * on error get city
 * @param {Object} error
 */
var onErrorGetCity = function(error) {
	(require('TiLog')).error(TAG + ' onErrorGetCity()', error);
	getCityData();
};
/**
 * configure city picker
 */
var configureCity = function() {

	var countryID = user.get('country_id');
	var cityID = user.get('city_id');

	var label = L('city', 'City');

	$.city_picker.applyProperties({
		options : $.city_picker.formatDataToPicker(getCityDao().findByCountryID(countryID), 'id', 'name', true, label),
		selectedByID : cityID
	});
	$.city_picker.hideLoader();
};
/**
 * edit user profile data
 */
var onEditProfile = _.debounce(function(e) {
	checkToSendEditProfile();
}, Alloy.Globals.Integers.debounce, true);

var checkToSendEditProfile = function() {
	newInterestsToService = getTagDao().getNewInterests();
	if (_.isArray(newInterestsToService.models) && newInterestsToService.models.length > 0) {
		showLoader();
		sendNewInterest();
	} else {
		editUser();
	}
};

var sendNewInterest = function() {
	var params = newInterestsToService.models[0].toService();
	getTagService().send(params, successOnSendNewInterest, errorOnSendNewInterest, httpCK);
};

/**
 *@param {Object} response
 */
var successOnSendNewInterest = function(response) {
	_.defer(checkToSendEditProfile);
};

var errorOnSendNewInterest = function(error) {
	(require('TiLog')).error(TAG, 'errorOnSendNewInterest, error -> ' + JSON.stringify(error));
	hideLoader();
	(require('TiUtils')).toast(L('error_user_edit_profile', 'An error occurred while trying to edit your profile.'));
};

var editUser = function() {
	/**
	 * get form information
	 */

	var state = $.txt_state.getValue();
	var firstName = $.registration_edition.getFirstName();
	var lastName = $.registration_edition.getLastName();
	var email = $.registration_edition.getEmail();
	var gender = $.registration_edition.getGender();
	var dateBirth = (require('FormatUtils')).formatDateToDateFormatService($.registration_edition.getDateBirth());
	var telephone = $.txt_phone.getValue();
	var university = $.txt_university.getValue();
	var occupation = $.txt_occupation.getValue();
	var wyw = $.txt_wyw.getValue();
	var favoriteDrinks = $.txt_favorite_drink.getValue();

	if (validateFields(firstName, lastName, email, gender, dateBirth)) {

		var params = {
			state : state,
			first_name : firstName,
			last_name : lastName,
			email : email,
			gender : (gender == null || gender == -1) ? null : gender,
			date_birth : dateBirth,
			phone : telephone,
			university : university,
			occupation : occupation,
			where_work : wyw,
			favorite_drink : favoriteDrinks
		};

		params.interests_id = getInterestsToService();

		if (_.isNumber(user.get('country_id')) && user.get('country_id') != -1) {
			params.country_id = user.get('country_id');
		}

		if (_.isNumber(user.get('city_id')) && user.get('city_id') != -1) {
			params.city_id = user.get('city_id');
		}

		if (userPhotoIsChange) {
			var photo = $.photo.getImagePath();
			var file = Ti.Filesystem.getFile(photo);
			file = Ti.Utils.base64encode(file.read()).toString();
			params.image = file;
			//gc
			file = null;
			photo = null;
		}

		$.partial_user_edit_profile.scrollTo(0, 0);
		showLoader();
		console.log('Update data: ',params);
		getUserService().edit(user.getID(), params, onSuccess, onError, httpCK);
	}
};

/**
 * get photo height
 */
var getPhotoHeight = function() {
	return $.photo.getHeight();
};

/**
 * get interest to send service
 * @return {Array}
 */
var getInterestsToService = function() {

	var tags = Alloy.Globals.DaoBuilder.getUserTagDao().findTagsByUser(user.getID());
	var tagsIDs = [];
	_.map(tags, function(tag) {
		if (tag.getServerID() != -1) {
			tagsIDs.push(tag.getServerID());
		}
	});
	return tagsIDs;

};

/**
 * @param {Integer} gender
 */
var setDefaultImageByGender = function(gender) {
	var defaultImageByGender = Alloy.Globals.Images.avatar_background_men;

	if (gender == 2) {
		defaultImageByGender = Alloy.Globals.Images.avatar_background_female;
	}
	$.photo.applyProperties({
		image : {
			defaultImage : defaultImageByGender
		}
	});

};

/**
 * @param {String} firstName
 * @param {String} lastName
 * @param {String} email
 * @param {Integer} gender
 * @param {String} dateBirth
 */
var validateFields = function(firstName, lastName, email, gender, dateBirth) {
	if (!_.isString(firstName) || _.isEmpty(firstName.trim())) {
		(require('TiUtils')).toast(L('first_name_required', 'First Name is required'));
		return false;
	} else if (!_.isString(lastName) || _.isEmpty(lastName.trim())) {
		(require('TiUtils')).toast(L('last_name_required', 'Last name is required'));
		return false;
	} else if ((!require('JsUtils').validateEmail(email))) {
		(require('TiUtils')).toast(L('login_error_email', 'Email is not valid'));
		return false;
	} /*else if (gender == null || gender == -1) {
		(require('TiUtils')).toast(L('gender_required', 'Gender is required'));
		return false;
	}*/ else if (dateBirth == null) {
		(require('TiUtils')).toast(L('date_birth_required', 'Date of birth is required'));
		return false;
	}
	return true;
};

var hideKeyboard = function() {
	if (OS_IOS) {
		$.txt_state.blur();
		$.txt_phone.blur();
		$.txt_university.blur();
		$.txt_occupation.blur();
		$.txt_wyw.blur();
	}
};
/** ------------------------
 Listeners
 ------------------------**/

/**
 * Fired when user change country
 * @param {Object} e
 */
var onChangeCountry = function(e) {
	if (e.row.id != user.get('country_id')) {
		user.set('country_id', e.row.id);
		user.set('city_id', -1);
		getCityData();
	}
};

var showLoaderCity = function() {
	var label = L('city', 'City');
	$.city_picker.applyProperties({
		options : $.city_picker.formatDataToPicker([], 'id', 'name', true, label),
		selectedByID : user.get('city_id')
	});
	$.city_picker.showLoader();
};
/**
 * Fired when user change city
 * @param {Object} e
 */
var onChangeCity = function(e) {
	user.set('city_id', e.row.id);
};

/**
 * on success edit profile
 * @param {Object} response
 */
var onSuccess = function(response) {
	user.mapping(response);
	//update global user logged
	Alloy.Globals.UserLogged = user;
	$.trigger('success', {
		user : user
	});
};

/**
 * on error edit user
 * @param {Object} error
 */
var onError = function(error) {
	(require('TiLog')).error(TAG, ' onErrorRegistration -> ' + JSON.stringify(error));
	hideLoader();
	var message = "";
	switch(error.code) {
	case Alloy.CFG.Http.Unauthorized:
		message = L('user_unauthorized', 'User unauthorized');
		break;
	case Alloy.CFG.Http.CustomYearsRequired:
		message = String.format(L('error_years_min_required', 'Your age must be greater than %s years'), error.message);
		break;
	default:
		message = L('error_user_edit_profile', 'An error occurred while trying to edit your profile.');
		break;
	}

	(require('TiUtils')).toast(message);
};

/**
 * show loader and hide page
 */
var showLoader = function() {
	$.loader.show();
	$.page.hide({
		animated : OS_IOS
	});
};

var hideLoader = function() {
	$.loader.hide();
	$.page.show({
		animated : OS_IOS
	});
};

/**
 * Change default Avatar (Female or Male)
 */
var onChangeGender = function(e) {
	var photo = user.get('photo');
	if (photo == null || _.isEmpty(photo)) {
		setDefaultImageByGender(e.gender);
	}
};

/**
 * when window is opened
 */
var onOpen = function(e) {
	//notify components

	$.photo.onOpen(e);
	$.registration_edition.onOpen(e);
	$.country_picker.onOpen(e);
	$.city_picker.onOpen(e);

	if (OS_IOS) {
		init(e);
	} else {
		_.defer(init, e);
	}

};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
};

/**
 * on create new tag
 * @param {Object} e
 */
var onCreateTag = function(e) {
	var tag = getTagDao().createTagToUser(e.name, Alloy.Globals.UserID);
	if (_.isObject(tag)) {
		$.interests.addTag(tag);
	}
};

/**
 * on success tag
 * @param {Object} e
 */
var onSuccessTag = function(e) {
	var tag = Alloy.Globals.DaoBuilder.getUserTagDao().addTagToUser(e.tag, Alloy.Globals.UserID);
	if (_.isObject(tag)) {
		$.interests.addTag(e.tag);
	}
	//gc
	tagDao = null;
};

/**
 * Fired when user scroll
 * @param {Object} e
 */
var onScroll = function(e) {
	$.trigger('scroll', e);
};

/**
 * Fired when user change photo
 * @param {Object} e
 */
var onSuccessPhoto = function(e) {
	userPhotoIsChange = true;
};

/**
 * on remove tag
 * @param {Object} e
 */
var onRemoveTag = function(e) {
	var tag = e.tag;
	Alloy.Globals.DaoBuilder.getUserTagDao().removeTagToUser(e.tag.getID(), Alloy.Globals.UserID);
	_.defer(refreshTags);
};

var onClickComboBox = function() {
	hideKeyboard();
	$.registration_edition.hideKeyboard();
};

(function configureUI() {
	$.photo.applyProperties({
		//top : Alloy.Globals.Dimens.header_avatar_edition_top(),
		height : Alloy.Globals.Dimens.profile_header_avatar_background_size()
	});
	//$.loader.top = Alloy.Globals.Dimens.padding_top_half_height();
})();
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.onOpen = onOpen;
exports.onClose = onClose;
exports.cleanup = cleanup;
exports.onEditProfile = onEditProfile;
exports.refresh = refresh;
exports.getPhotoHeight = getPhotoHeight;
