/** ------------------------
 Constants
 ------------------------**/
var TAG = 'controllers/win_example';
/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {

	if (_.isObject(properties)) {
		if (_.has(properties, 'transaction')) {
			$.transaction_detail.applyProperties({
				transaction : properties.transaction
			});
		}

		if (_.has(properties, 'transactionID')) {
			$.transaction_detail.applyProperties({
				transactionID : properties.transactionID
			});
		}

		_.extend($.win_transaction_detail, _.omit(properties, 'transaction', 'transactionID'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
	if (OS_ANDROID) {
		$.win_transaction_detail.addEventListener('androidback', onBack);
	}
};

/**
 * initialize controller
 */
var init = function(e) {
	applyProperties(args);
	//gc
	args = null;
	applyListeners();
	configureToolbar(e);
};

/**
 * configure toolbar
 */
var configureToolbar = function(e) {
	if (OS_ANDROID) {
		var activity = $.win_transaction_detail.getActivity();
		activity.getActionBar().displayHomeAsUp = true;
		activity.getActionBar().onHomeIconItemSelected = onHomeIconItemSelected;
		//gc
		activity = null;
	}

};

var onBack = function() {
	$.win_transaction_detail.close();
};

if (OS_ANDROID) {
	/**
	 * on home icon item selected
	 */
	var onHomeIconItemSelected = _.debounce(function(e) {
		onBack();
	}, Alloy.Globals.Integers.debounce, true);
}

/**
 * before windows open
 */
var beforeOpen = function() {
	applyProperties(args);
};
/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * when window is opened
 */
var onOpen = function(e) {
	if (OS_IOS) {
		init(e);
	} else {
		_.defer(init, e);
	}
	$.transaction_detail.onOpen(e);
	Ti.App.fireEvent('hide_loader');
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	if (OS_IOS) {
		$.trigger('ios:back', {});
	}
	$.transaction_detail.onClose(e);
	cleanup();
};

beforeOpen();
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.cleanup = cleanup;
