/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
if (OS_ANDROID) {
	var abx = require('com.alcoapps.actionbarextras');
}

/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {
		if (_.has(properties, 'establishment')) {
			$.establishment_detail.applyProperties({
				establishment : properties.establishment
			});
		}

		if (_.has(properties, 'establishmentID')) {
			$.establishment_detail.applyProperties({
				establishmentID : properties.establishmentID
			});
		}

		_.extend($.win_establishment_detail, _.omit(properties, 'establishment', 'establishmentID'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
	if (OS_ANDROID) {
		$.win_establishment_detail.addEventListener('androidback', onBack);
	}
};

/**
 * initialize controller
 */
var init = function(e) {
	Ti.App.fireEvent('closekeyboard');
	applyProperties(args);
	//gc
	args = null;
	configureToolbar(e);
	applyListeners();
};

/**
 * configure toolbar
 */
var configureToolbar = function(e) {
	if (OS_ANDROID) {
		abx.setBackgroundColor(Alloy.Globals.Colors.action_bar_transparent);
		abx.setElevation(0);
		var activity = $.win_establishment_detail.getActivity();
		activity.onCreateOptionsMenu = onCreateOptionsMenu;
		activity.invalidateOptionsMenu();
		activity.getActionBar().displayHomeAsUp = true;
		activity.getActionBar().onHomeIconItemSelected = onHomeIconItemSelected;
		//gc
		activity = null;
	}

};

var onBack = function() {
	$.win_establishment_detail.close();
};

if (OS_ANDROID) {

	/**
	 * on create options menu
	 * @param {Object} e
	 */
	var onCreateOptionsMenu = function(e) {
		e.menu.clear();
		/*var buyItem = e.menu.add({
			title : L('buy_ticket_or_table', 'Buy ticket/table'),
			showAsAction : Ti.Android.SHOW_AS_ACTION_IF_ROOM,
		});

		buyItem.addEventListener('click', onBuy);
		//gc
		buyItem = null;*/
	};
	/**
	 * on home icon item selected
	 */
	var onHomeIconItemSelected = _.debounce(function(e) {
		onBack();
	}, Alloy.Globals.Integers.debounce, true);
}

/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * when window is opened
 */
var onOpen = function(e) {
	if(OS_IOS){
		init(e);
	}else{
		_.defer(init, e);
	}
	$.establishment_detail.onOpen(e);
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
	$.establishment_detail.onClose(e);
};

var onBuy = _.debounce(function(e) {
	$.establishment_detail.onBuyTicketTable();
}, Alloy.Globals.Integers.debounce, true);

/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.cleanup = cleanup;
