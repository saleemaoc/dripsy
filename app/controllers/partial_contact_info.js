/**
 * @author jagu
 *
 */
/** ------------------------
 Constants
 ------------------------**/
var TAG = 'controller/partial_contact_info';
/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var establishment = null;
/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {
		if (_.has(properties, 'establishment')) {
			establishment = properties.establishment;
		}
		_.extend($.partial_contact_info, _.omit(properties, 'establishment'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
	$.phone.addEventListener('click', onCallPhone);
	$.link.addEventListener('click', onOpenUrl);
};

/**
 * initialize controller
 */
var init = function() {
	showLoader();
	applyProperties(args);
	applyListeners();
	//gc
	args = null;
	_.delay(setData, Alloy.Globals.Integers.SetDataDelay);
};

/**
 *	show loader_city and hide picker_city
 */
var setData = function() {
	if (_.isObject(establishment)) {
		$.establishment.set(establishment.attributes);
		updateUI();
		//gc
		establishment = null;
	}
	_.delay(hideLoader, 1000);
};

var updateUI = function() {
	if (!$.establishment.getIsNotEmptyPhoneNumber()) {
		$.partial_contact_info.remove($.phone);
	}
	if (!$.establishment.getIsNotEmptyWebsite()) {
		$.partial_contact_info.remove($.link);
	}
};

/**
 * show loader and hide page
 */
var showLoader = function() {
	(require('TiLog')).info(TAG, ' showLoader()');
	$.loader.show();
	$.partial_contact_info.hide({
		animated : OS_IOS
	});
};
var hideLoader = function() {

	(require('TiLog')).info(TAG, ' hideLoader()');
	$.loader.hide();
	$.partial_contact_info.show({
		animated : OS_IOS
	});
};

/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * when window is opened
 */
var onOpen = function(e) {
	if (OS_IOS) {
		init(e);
	} else {
		_.defer(init, e);
	}
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
};

var onCallPhone = function(e) {
	(require('TiUtils')).callPhone($.establishment.get('phone'));
};

var onOpenUrl = function(e) {
	Ti.Platform.openURL($.establishment.get('website'));
};
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.onOpen = onOpen;
exports.onClose = onClose;
exports.cleanup = cleanup;
