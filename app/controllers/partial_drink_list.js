/**
 * @author jagu
 */

/** ------------------------
 Constants
 ------------------------**/
var TAG = 'controller/partial_drink_list';
/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
/**
 * no pagination
 */
var limit = 999;
var collection = $.local_collection;
/**
 * http library
 */
var httpCK;
/**
 * name query
 */
var name = '';
/**
 * flag to indicate pull to refresh
 */
var isPullToRefresh = false;
/**
 * flag to indicate scrolling refresh
 */
var isScrolling = false;
/**
 * time out to search
 */
var timeOutOnSearch = null;
/**
 * establishment id
 */
var establishmentID = null;
/**
 * flag to prevent auto initialization
 */
var autoInit = false;
/**
 * variable to store purchase
 *
 */

var purchaseStore = [];

var timeoutAddPurchase = null;
/** ------------------------
 Dependencies
 ------------------------**/
/**
 * get drink dao
 * @return {DrinkDao}
 */
var getDao = function() {
	return new (require('daos/DrinkDao'))();
};

/**
 * get drink service
 * @return {DrinkService}
 */
var getService = function() {
	return (require('services/DrinkService')).inject(getDao());
};

/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		if (_.has(properties, 'establishmentID')) {
			establishmentID = properties.establishmentID;
		}

		if (_.has(properties, 'autoInit')) {
			init({});
		}

		_.extend($.partial_drink_list, _.omit(properties, 'establishmentID', 'autoInit'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
		if (httpCK) {
			httpCK.abort();
		}
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
};

/**
 * initialize controller
 */
var init = function() {
	applyProperties(args);
	applyListeners();
	//gc
	args = null;
	showLoader();
	_.delay(getData, Alloy.Globals.Integers.SetDataDelay);
};

/**
 * get params to get
 */
var getParamsToGet = function() {

	return {
		establishment_id : establishmentID,
		limit : limit,
		offset : collection.models.length
	};

};

/**
 * get data
 */
var getData = function() {
	if (_.isNumber(establishmentID)) {
		getService().get(getParamsToGet(), onSuccessGet, onErrorGet, httpCK);
	}
};

/**
 * success get service
 * @param {Object} response
 */
var onSuccessGet = function(response) {
	updateData();
};

/**
 * when service display an error
 * @param {String} error
 */
var onErrorGet = function(error) {
	(require('TiLog')).error(TAG + ' onErrorGet()', error);
	showMessage(L('error_connection_drink', 'They could not get the drinks, press to try again'));
	hideLoader();
};

/**
 * update data in list
 */
var updateData = function() {
	getDao().findList(establishmentID, name, limit, collection.models.length, true, onSuccessData, onErrorData, collection);
};

/**
 * @param {Object} e
 */
var onSuccessData = function(e) {
	(require('TiLog')).info(TAG + ' onSuccessData()', JSON.stringify(collection.models));
	var section = Titanium.UI.createTableViewSection();
	collection.map(function(drink) {

		var purchase = getPurchaseByID(drink.getID());

		var drinkController = Alloy.createController('tableview_row_drink', {
			drink : drink,
			purchase : purchase,
		});

		//listeners
		drinkController.on('click:less', onClickAmountDrink);
		drinkController.on('click:more', onClickAmountDrink);
		drinkController.on('click', onClickDrink);

		section.add(drinkController.getView());

		//gc
		purchase = null;
		drinkController = null;

	});
	$.table.setData([section]);
	hideLoader();
};

/**
 * @param {Object} e
 */
var onErrorData = function(e) {
	(require('TiLog')).error(TAG + ' onErrorData()', JSON.stringify(e));
};

/**
 * Show main loader and hide list
 */
var showLoader = function() {
	//clear message
	$.message.hide({
		animated : OS_IOS
	});
	$.message.text = '';
	//show loader
	$.loader.show();
	//hide table
	$.table.hide({
		animated : OS_IOS
	});
};

/**
 * Hide main loader and show list
 */
var hideLoader = function() {
	$.loader.hide();
	$.table.show();
};

/**
 * show message
 * @param {String} message
 */
var showMessage = function(message) {
	$.message.text = message;
	$.message.show({
		animated : OS_IOS
	});
};

/**
 * notify toolbar
 * @param {Integer} length
 */
var notifyToolbar = function(length) {
	$.trigger('update:toolbar', {
		count : length
	});
};

/**
 * add purchase quantity
 * @param {Integer} id
 * @param {Integer} quantity
 */
var addPurchase = function(id, quantity, drink) {
	//find purchase
	var purchase = getPurchaseByID(id);
	if (_.isObject(purchase)) {
		//if quantity equals 0 remove purchase to store
		if (quantity == 0) {
			purchaseStore = _.without(purchaseStore, purchase);
		} else {
			//update object
			_.extend(purchase, {
				id : id,
				quantity : quantity,
				drink : drink
			});
		}
	} else {
		purchaseStore.push({
			id : id,
			quantity : quantity,
			drink : drink
		});
	}

	var length = purchaseStore.length;

	notifyToolbar(length);
};

/**
 * get purchase by id
 * @param {Integer} id
 * @return {Object}
 */
var getPurchaseByID = function(id) {
	return _.findWhere(purchaseStore, {
		id : id
	});
};

/**
 * get purchase store
 * @return {Array}
 */
var getPurchaseStore = function() {
	return purchaseStore;
};

/**
 * cancel timeout to add purchase
 */
var cancelAddPurchase = function() {
	if (!_.isNull(timeoutAddPurchase)) {
		clearTimeout(timeoutAddPurchase);
		timeoutAddPurchase = null;
	}
};
/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * when window is opened
 */
var onOpen = function(e) {
	if (autoInit) {
		if (OS_IOS) {
			init(e);
		} else {
			_.defer(init, e);
		}
	}
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
};

/**
 * on search drink
 * @param {String} value
 */
var onSearch = function(value) {
	if (timeOutOnSearch != null) {
		clearTimeout(timeOutOnSearch);
		timeOutOnSearch = null;
	}
	name = value;
	timeOutOnSearch = setTimeout(function(e) {
		collection.reset();
		updateData();
	}, Alloy.Globals.Integers.search);
};

/**
 * on click amount drink
 * @param {Object} e: {id:1, value:23}
 */
var onClickAmountDrink = function(e) {
	var id = e.id;
	var quantity = e.value;
	var drink = e.drink;

	//gc
	e = null;
	cancelAddPurchase();
	timeoutAddPurchase = _.delay(function() {
		addPurchase(id, quantity, drink);
	}, 250);
};

/**
 * on click drink
 * @param {Object} e
 */
var onClickDrink = function(e) {
	var drink = e.drink;
	Alloy.Globals.AppRoute.openDrinkDetail({
		drink : drink
	});
};
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.onOpen = onOpen;
exports.onClose = onClose;
exports.cleanup = cleanup;
exports.init = init;
exports.onSearch = onSearch;
exports.getPurchaseStore = getPurchaseStore;
