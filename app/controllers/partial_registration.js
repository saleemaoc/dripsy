/**
 * @author jagu
 *
 */

/** ------------------------
 Constants
 ------------------------**/
var TAG = 'controllers/partial_registration';
var DELAY_UPDATE_UI_FB = 250;
/** ------------------------
 Fields
 ------------------------**/
/**
 *  Arguments passed into this controller can be accessed via the `$.args` object directly or:
 */
var args = $.args;

/**
 * http library
 */
var HttpCK = null;

/**
 * loader reference
 */
var loader = null;

/**
 * flag to indicate if password is required
 */
var passwordRequired = true;

var userFacebook = null;
if (OS_IOS) {
	var parentWindow = null;
}
/** ------------------------
 Dependencies
 ------------------------**/

/**
 * get service dependency
 * @return {UserService}
 */
var getService = function() {
	return Alloy.Globals.ServiceBuilder.getUserService();
};

/** ------------------------
 Methods
 ------------------------**/

/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		if (_.has(properties, 'userFacebook')) {
			userFacebook = properties.userFacebook;
		}

		if (OS_IOS && _.has(properties, 'parentWindow')) {
			parentWindow = properties.parentWindow;
		}

		if (OS_ANDROID && _.has(properties, 'currentActivity')) {
			$.photo.applyProperties({
				activity : properties.currentActivity
			});
		}
		_.extend($.partial_registration, _.omit(properties, 'userFacebook', 'parentWindow', 'currentActivity'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
		if (HttpCK) {
			HttpCK.abort();
		}
		if (OS_IOS) {
			parentWindow = null;
		}
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
	$.registration_edition.on("changeGender", onChangeGender);
	$.v_terms_condition.addEventListener('click', onTermsCondition);
	$.registration_edition.on('hidekeyboard', hideKeyboard);
};

/**
 * initialize controller
 */
var init = function() {
	applyProperties(args);
	applyListeners();
	HttpCK = new (require('HttpCK'))();
	//gc
	args = null;
	showLoader();
	initializeViews();
	_.delay(updateUIWithFB, DELAY_UPDATE_UI_FB);

};

/**
 * initialize views
 */
var initializeViews = function() {
	$.registration_edition.setGender(-1);
	setDefaultImageByGender();
	_.delay(hideLoader, Alloy.Globals.Integers.PreventDetachedView);
};

/**
 * @param {Integer} gender
 */
var setDefaultImageByGender = function(gender) {
	var defaultImageByGender = Alloy.Globals.Images.avatar_background_men;

	if (gender == 2) {
		defaultImageByGender = Alloy.Globals.Images.avatar_background_female;
	}

	$.photo.applyProperties({
		image : {
			image : Alloy.CFG.constants.mock_url_broken_link_image,
			defaultImage : defaultImageByGender
		}
	});
};

/**
 * @params {String} firstName
 * @params {String} lastName
 * @params {String} email
 * @params {Integer} gender
 * @params {dateBirth} String
 * @params {String} password
 * @params {String} confirmPassword
 * @params {Boolean} termsConditions
 */
var validateFields = function(firstName, lastName, email, gender, dateBirth, password, confirmPassword, termsConditions) {

	if (!_.isString(firstName) || _.isEmpty(firstName.trim())) {
		(require('TiUtils')).toast(L('first_name_required', 'First Name is required'));
		return false;
	}
	if (!_.isString(lastName) || _.isEmpty(lastName.trim())) {
		(require('TiUtils')).toast(L('last_name_required', 'Last name is required'));
		return false;
	}
	if ((!require('JsUtils').validateEmail(email))) {
		(require('TiUtils')).toast(L('login_error_email', 'Email is not valid'));
		return false;
	}
// 	Hide the gender validation becaue app store rejected the app [Surinder]
	// if (gender == null || gender == -1) {
		// (require('TiUtils')).toast(L('gender_required', 'Gender is required'));
		// return false;
	// }
	if (!$.registration_edition.getDateBirthIsChange()) {
		(require('TiUtils')).toast(L('date_birth_required', 'Date of birth is required'));
		return false;
	}
	if (passwordRequired && password.trim().length < Alloy.CFG.validate.user_password_length) {
		(require('TiUtils')).toast(String.format(L('password_length', 'The password must have more than %s characters'), Alloy.CFG.validate.user_password_length + ""));
		return false;
	}
	if (passwordRequired && (!_.isString(confirmPassword) || _.isEmpty(confirmPassword.trim()))) {
		(require('TiUtils')).toast(L('confirm_password_required', 'Confirm password is required'));
		return false;
	}
	if (passwordRequired && confirmPassword != password) {
		(require('TiUtils')).toast(L('confirm_password_not_equal', "Both passwords don't match. Please try again."));
		return false;
	}
	if (!termsConditions) {
		(require('TiUtils')).toast(L('terms_conditions_accept', 'You must accept the terms and conditions to register'));
		return false;
	}
	return true;
};

/**
 * sign up user
 */
var onSignUp = _.debounce(function(e) {

	var firstName = $.registration_edition.getFirstName();
	var lastName = $.registration_edition.getLastName();
	var email = $.registration_edition.getEmail();
	var gender = $.registration_edition.getGender();
	var dateBirth = (require('FormatUtils')).formatDateToDateFormatService($.registration_edition.getDateBirth());
	var password = $.txt_password.getValue();
	var confirmPassword = $.txt_confirm_password.getValue();
	var termsConditions = $.sw_terms_condition.getValue();
	var photo = $.photo.getImagePath();

	if (validateFields(firstName, lastName, email, gender, dateBirth, password, confirmPassword, termsConditions)) {

		var params = {
			first_name : firstName,
			last_name : lastName,
			role_id : Alloy.CFG.user_role,
			email : email.trim(),
			gender : gender,
			date_birth : dateBirth,
			password : password,
			confirm_terms_condition : termsConditions,
			nickname : firstName + ' ' + lastName,
		};

		if (photo != null) {
			var file = Ti.Filesystem.getFile(photo);
			file = Ti.Utils.base64encode(file.read()).toString();
			params.image = file;
			//gc
			file = null;
		}
		if(OS_ANDROID){
			Ti.UI.Android.hideSoftKeyboard();
		}	
		showLoader();
		getService().registration(params, onSuccess, onError, HttpCK);
	}

}, Alloy.Globals.Integers.debounce, true);

var onSuccess = function(response) {
	hideLoader();
	$.trigger('success', {
		user : response
	});
};

var onError = function(error) {
	hideLoader();

	switch(error.code) {
	case 0:
		message = L('error_network_connection_no_internet', "Oops! looks like you don't have Internet connection.");
		break;
	case Alloy.CFG.Http.EmailUsed:
		message = L('exist_account_with_this_email', 'An account already exists with this email');
		break;
	case Alloy.CFG.Http.Unauthorized:
		message = L('user_unauthorized', 'User unauthorized');
		break;
	case Alloy.CFG.Http.InternalServerError:
		message = L('error_internal_server_error', 'An error occurred on the server');
		break;
	case Alloy.CFG.Http.CustomYearsRequired:
		message = String.format(L('error_years_min_required', 'Your age must be greater than %s years'), error.message);
		break;
	default:
		// message = L('error_email_exist', 'This email is already taken');
		message = L('error_occurred', 'An error occurred!');
		break;
	}

	(require('TiUtils')).toast(message);
	(require('TiUtils')).toast(error.code + " > " + error.status + " > " + error.message);
};

/**
 * Change default Avatar (Female or Male)
 */
var onChangeGender = function(e) {
	var photo = $.photo.getImagePath();
	if (photo == null || _.isEmpty(photo)) {
		setDefaultImageByGender(e.gender);
	}
};
/**
 * show loader and hide page
 */
var showLoader = function() {
	$.loader.show();
	$.page.hide({
		animated : OS_IOS
	});
};

var hideLoader = function() {
	$.loader.hide();
	$.page.show({
		animated : OS_IOS
	});
};

/**
 * update ui componentes with user facebook information
 */
var updateUIWithFB = function() {
	if (_.isObject(userFacebook)) {

		if (_.isObject(userFacebook)) {

			if (_.has(userFacebook, 'email')) {
				$.registration_edition.setEmail(userFacebook.email);
				$.registration_edition.disableEmail();
			}

			if (_.has(userFacebook, 'first_name')) {
				$.registration_edition.setFirstName(userFacebook.first_name);
			}

			if (_.has(userFacebook, 'last_name')) {
				$.registration_edition.setLastName(userFacebook.last_name);
			}

			if (_.has(userFacebook, 'gender')) {
				var genderID = userFacebook.gender == 'male' ? Alloy.CFG.constants.gender.male : Alloy.CFG.constants.gender.female;
				$.registration_edition.setGender(genderID);
				onChangeGender({
					gender : genderID
				});
			}

			if (_.has(userFacebook, 'birthday')) {
				$.registration_edition.setDateBirth(userFacebook.birthday);
			}
			//remove password
			passwordRequired = false;
			$.page.remove($.txt_confirm_password);
			$.page.remove($.txt_password);

			if (OS_IOS) {
				//remove lines
				$.page.remove($.v_line_confirm_password);
				$.page.remove($.v_line_password);
			}
		}
	}
};

var hideKeyboard = function() {
	$.registration_edition.hideKeyboard();
	if (passwordRequired) {
		$.txt_confirm_password.blur();
		$.txt_password.blur();
	}
};
/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * when window is opened
 */
var onOpen = function(e) {
	if (OS_IOS) {
		init(e);
	} else {
		_.defer(init, e);
	}
	$.photo.onOpen(e);
	$.registration_edition.onOpen(e);
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
};

var onTermsCondition = _.debounce(function(e) {
	Alloy.Globals.AppRoute.openTermsAndCondition();
}, Alloy.Globals.Integers.debounce, true);

(function configureUI() {
	$.photo.applyProperties({
		//top : Alloy.Globals.Dimens.header_avatar_edition_top(),
		height : Alloy.Globals.Dimens.profile_header_avatar_background_size()
	});
})();
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.onOpen = onOpen;
exports.onClose = onClose;
exports.cleanup = cleanup;
exports.onSignUp = onSignUp;
