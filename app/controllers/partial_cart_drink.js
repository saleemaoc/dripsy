/**
 * @author jagu
 *
 */
/** ------------------------
 Constants
 ------------------------**/
var TAG = 'controllers/partial_cart_drink';
/**
 * value to update tip amount
 *
 */
var DELAY_UPDATE_TIP_AMOUNT = 250;
/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
/**
 * http library
 */
var httpCK;
/**
 * purchase store
 */
var purchaseStore = [];
/**
 * total amount
 */
var totalAmount = parseFloat(0);
/**
 * tip amount
 */
var tipAmount = parseFloat(0);

var taxAmount = parseFloat(0);
/**
 * admin fee
 */
var adminFee = Alloy.CFG.constants.admin_fee;
/**
 * flag to indicate if run update data automatically
 */
var autoUpdateData = true;
/**
 * to send gift
 */
var gift = null;
/**
 * transaction
 */
var transaction = null;
/**
 * timeout to update tip amount
 */
var timeOutUpdateTipAmount = null;

var isStripe = false;
var paymentOnline;
/** ------------------------
 Dependencies
 ------------------------**/
/**
 * get gift service
 * @return {Service}
 */
var getGiftService = function() {
	return Alloy.Globals.ServiceBuilder.getGiftService().inject(Alloy.Globals.DaoBuilder.getGiftDao());
};

/**
 * get transaction service
 * @return {Serice}
 */
var getTransactionService = function() {
	return Alloy.Globals.ServiceBuilder.getTransactionService().inject(Alloy.Globals.DaoBuilder.getTransactionDao());
};
/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		if (_.has(properties, 'purchaseStore')) {
			purchaseStore = properties.purchaseStore;
		}

		if (_.has(properties, 'gift')) {
			gift = properties.gift;
		}

		if (_.has(properties, 'autoUpdateData')) {
			autoUpdateData = properties.autoUpdateData;
		}

		_.extend($.partial_cart_drink, _.omit(properties, 'purchaseStore', 'autoUpdateData', 'gift'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
		if (httpCK) {
			httpCK.abort();
		}
	} catch(e) {
	}
};
/**
 * apply listeners to controller
 */
var applyListeners = function() {
	$.txt_tip_amount.addEventListener('change', onChangeTipAmount);
	$.txt_tip_amount.addEventListener('singletap', onTxtFocus);
	$.txt_tip_amount.addEventListener('longpress', onTxtFocus);
	$.btn_buy.addEventListener('click', onBuy);
	$.btn_tip_15.addEventListener('click', doTip15);
	$.btn_tip_18.addEventListener('click', doTip18);
	$.btn_tip_20.addEventListener('click', doTip20);
	$.btn_tip_custom.addEventListener('click', doCustomTip);
};

/**
 * initialize controller
 */
var init = function() {
	applyProperties(args);
	//gc
	args = null;
	//create http instance
	httpCK = new (require('HttpCK'))();
	applyListeners();
	showLoader();
	if (autoUpdateData) {
		updateData();
	}
};

/**
 * update data to controller
 */
var updateData = function() {

	if (_.isArray(purchaseStore) && !_.isEmpty(purchaseStore)) {
		//reset total price
		totalAmount = parseFloat(0);
		var items = [];

		//iterate purchases
		for (var i = 0,
		    j = purchaseStore.length; i < j; i++) {

			var purchase = purchaseStore[i];
			Ti.API.info('YOLO ' + JSON.stringify(purchase));
			var quantity = parseInt(purchase.quantity);
			var price = Alloy.Globals.FormatUtils.formatFloatTwoDecimal(purchase.quantity * purchase.drink.getPrice());
			var name = purchase.drink.getName();

			totalAmount = Alloy.Globals.FormatUtils.formatFloatTwoDecimal(totalAmount + price);

			items.push({
				quantity : {
					text : quantity
				},
				name : {
					text : name
				},
				price : {
					text : Alloy.Globals.CurrentCurrencySymbol + " " + Alloy.Globals.FormatUtils.formatPrice(price)
				}
			});
		};

		//add items
		$.list.sections[0].setItems(items);
		
		// Add admin_fee to totalAmount [Surinder]
		totalAmount = totalAmount + adminFee;
		
		//update total price
		updateTotalValue();
	}

	_.delay(hideLoader, Alloy.Globals.Integers.PreventDetachedView);
};

var updateTotalValue = function() {
	if (tipAmount < 0) {
		tipAmount = 0;
	} else {
		//taxAmount = Alloy.Globals.FormatUtils.formatFloatTwoDecimal((totalAmount * .08875).toFixed(2));
		taxAmount = totalAmount * .08875;
		taxAmount = Math.round(taxAmount * 100) / 100;
		
		Ti.API.info('Before TA: ' + taxAmount);
		$.lbl_admin_amount.text = "Admin Fee    " + Alloy.Globals.CurrentCurrencySymbol + " " + Alloy.Globals.FormatUtils.formatPrice(adminFee);
		$.lbl_tax_amount.text = "Tax 8.875%    " + Alloy.Globals.CurrentCurrencySymbol + " " + Alloy.Globals.FormatUtils.formatPrice(taxAmount);
		$.lbl_total_value.text = Alloy.Globals.CurrentCurrencySymbol + " " + Alloy.Globals.FormatUtils.formatPrice(totalAmount + tipAmount + taxAmount);
		
		//taxAmount = Alloy.Globals.FormatUtils.formatPrice(taxAmount);
		//Ti.API.info('total: ' + Alloy.Globals.FormatUtils.formatPrice(totalAmount + taxAmount));
	}

};
/**
 * show loader and hide page
 */
var showLoader = function() {
	$.loader.show();
	$.page.hide({
		animated : OS_IOS
	});
};

/**
 * hide loader
 */
var hideLoader = function() {
	$.loader.hide();
	$.page.show({
		animated : OS_IOS
	});
};

/**
 * generate transaction details to service by purchase store
 * @return {Array}
 */
var generateTransactionDetailsToServiceByPurchaseStore = function() {
	var transactionDetailsToService = [];
	var transactionDetailDao = Alloy.Globals.DaoBuilder.getTransactionDetailDao();
	_.map(purchaseStore, function(purchase) {
		var transactionDetail = transactionDetailDao.getModel();
		transactionDetail.generateToDrink(purchase.quantity, purchase.drink);

		//add
		transactionDetailsToService.push(transactionDetail.toService());
		//gc
		transactionDetail = null;
	});
	//gc
	transactionDetailDao = null;

	return transactionDetailsToService;
};

/**
 * generate transaction detail to payment
 */
var generateTransactionDetailsToPayment = function() {

	var transactionDetailsToPayment = [];
	var transactionDetailDao = Alloy.Globals.DaoBuilder.getTransactionDetailDao();

	_.map(purchaseStore, function(purchase) {
		var transactionDetail = transactionDetailDao.getModel();
		transactionDetail.generateToDrink(purchase.quantity, purchase.drink);

		//add
		transactionDetailsToPayment.push(transactionDetail.toPaypal());
		//gc
		transactionDetail = null;
	});

	if (_.isObject(transaction)) {
		var invoceItemTip = transaction.generateTransactionDetailPaypalForTip();
		if (!_.isNull(invoceItemTip)) {
			transactionDetailsToPayment.push(invoceItemTip);
		}
	}

	return transactionDetailsToPayment;
};

/**
 * on success gift
 * @param {Object} response
 */
var onSuccessGift = function(response) {
	(require('TiLog')).info(TAG, 'onSuccessGift ->' + JSON.stringify(response));
	$.trigger('gift:success', response);
};

/**
 * on error gift
 * @param {Object} error
 */
var onErrorGift = function(error) {
	(require('TiLog')).error(TAG, 'onErrorGift ->' + JSON.stringify(error));

	if (isStripe) {
		var message = paymentOnline.analizeError(JSON.parse(error.message));
		showCardErrorMessage(message);
	} else {
		(require('TiUtils')).toast(L('error_process_payment', 'An error occurred while processing your purchase, please try again.'));
	}
	Ti.App.fireEvent('hide_loader');
};

var showCardErrorMessage = function(message) {
	var dialog = Ti.UI.createAlertDialog({
		message : message.concat("\n").concat(L('do_you_want_to_pay_with_another_card', 'Do you want pay with another card?')),
		buttonNames : [L('close', 'Close'), L('confirm', 'Confirm')],
		cancel : 0
	});

	dialog.addEventListener('click', function(e) {
		if (e.index == 1) {
			paymentOnline.open();
		}
	});
	dialog.show();
};

/**
 * on success transaction
 * @param {Object} response
 */
var onSuccessTransaction = function(response) {
	(require('TiLog')).info(TAG, 'onSuccessTransaction ->' + JSON.stringify(response));
	var transactionID = response.id;

	$.trigger('transaction:success', {
		transactionID : transactionID
	});

};

/**
 * on error gift
 * @param {Object} error
 */
var onErrorTransaction = function(error) {
	Ti.API.error('RAY ERROR:' + JSON.stringify(error));
	(require('TiLog')).error(TAG, 'onErrorTransaction ->' + JSON.stringify(error));
	if (isStripe) {
		var message = paymentOnline.analizeError(JSON.parse(error.message));
		showCardErrorMessage(message);
	} else {
		(require('TiUtils')).toast(L('error_process_payment', 'An error occurred while processing your purchase, please try again.'));
	}
	Ti.App.fireEvent('hide_loader');
};

var hideKeyboard = function() {
	if (OS_IOS) {
		$.txt_tip_amount.blur();
	}
};

var validateTipView = function() {
	return $.txt_tip_amount.value.indexOf("-") == -1;
};
/** ------------------------
 Listeners
 ------------------------**/

/**
 * when window is opened
 */
var onOpen = function(e) {
	if (OS_IOS) {
		init(e);
	} else {
		$.txt_tip_amount.setSoftKeyboardOnFocus(Titanium.UI.Android.SOFT_KEYBOARD_SHOW_ON_FOCUS);
		_.defer(init, e);
	}
	$.txt_tip_amount.hintText = Alloy.Globals.CurrentCurrencySymbol + " 0.00";
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
};

var doTip15 = function(e) {
	$.txt_tip_amount.blur();
	$.txt_tip_amount.value = Alloy.Globals.CurrentCurrencySymbol + " " + Alloy.Globals.FormatUtils.formatPrice(totalAmount * 0.15);
	tipAmount = formatWOSym($.txt_tip_amount.value);
	updateTotalValue();
};
var doTip18 = function(e) {
	$.txt_tip_amount.blur();
	$.txt_tip_amount.value = Alloy.Globals.CurrentCurrencySymbol + " " + Alloy.Globals.FormatUtils.formatPrice(totalAmount * 0.18);
	tipAmount = formatWOSym($.txt_tip_amount.value);
	updateTotalValue();
};
var doTip20 = function(e) {
	$.txt_tip_amount.blur();
	$.txt_tip_amount.value = Alloy.Globals.CurrentCurrencySymbol + " " + Alloy.Globals.FormatUtils.formatPrice(totalAmount * 0.2);
	tipAmount = formatWOSym($.txt_tip_amount.value);
	updateTotalValue();
};
var doCustomTip = function(e) {
	$.txt_tip_amount.value = "";
	tipAmount = Alloy.Globals.FormatUtils.formatFloatTwoDecimal(0);
	$.txt_tip_amount.focus();
	updateTotalValue();
};

var onTxtFocus = function(e) {
	$.txt_tip_amount.value = "";
};

/**
 * on change tip amount
 * @param {Object} e
 */
var onChangeTipAmount = function(e) {
	var value = e.value;
	var number = formatWOSym(value);
	if (_.isString(value) && !_.isEmpty(value) && _.isNumber(number) && number > 0) {
		tipAmount = Alloy.Globals.FormatUtils.formatFloatTwoDecimal(number);
	} else {
		tipAmount = 0;
	}
	if (timeOutUpdateTipAmount != null) {
		clearTimeout(timeOutUpdateTipAmount);
		timeOutUpdateTipAmount = null;
	}
	timeOutUpdateTipAmount = setTimeout(updateTotalValue, DELAY_UPDATE_TIP_AMOUNT);
};

/**
 * on buy drinks
 */
var onBuy = _.debounce(function(e) {

	if (validateTipView()) {
		hideKeyboard();
		if (_.isObject(gift) && (require('AppProperties')).getAlertSendGift()) {
			var dialog = Titanium.UI.createAlertDialog({
				title : L('send_gift_alert', 'Send gift'),
				message : L('alert_send_gift', "Once you send a gift, you won't be able to cancel it."),
				cancel : 0,
				buttonNames : [L('cancel', 'Cancel'), L('agreed', 'Agreed')]
			});

			dialog.addEventListener('click', function(e) {
				switch(e.index) {
				case 1:
					(require('AppProperties')).setAlertSendGift(false);
					proccessPayment();
					break;
				}
			});

			dialog.show();
		} else {
			proccessPayment();
		}
	} else {
		(require('TiUtils')).toast(L('tip_invalid', 'Tip is invalid'));
	}
}, Alloy.Globals.Integers.debounce, true);

var proccessPayment = function() {
	showLoader();
	if (_.isNull(transaction)) {
		transaction = Alloy.Globals.DaoBuilder.getTransactionDao().getModel();
	}

	var totalAmountWithTip = Alloy.Globals.FormatUtils.formatFloatTwoDecimal(totalAmount + tipAmount + taxAmount);
	Ti.API.info('After TA: ' + taxAmount);
	if (_.isObject(gift)) {
		transaction.generateTransactionGift(establishmentID, Alloy.Globals.UserID, totalAmountWithTip, tipAmount,taxAmount, adminFee);
	} else {
		transaction.generateTransaction(establishmentID, Alloy.Globals.UserID, Alloy.CFG.constants.transaction_type.drink, totalAmountWithTip, tipAmount, "", taxAmount, adminFee);
	}

	Ti.API.info('[Transaction]: ' + JSON.stringify(transaction));

	var payment = transaction.toPaypal(generateTransactionDetailsToPayment());
	Ti.API.info('[Payment]: ' + JSON.stringify(payment));

	paymentOnline = Alloy.createWidget('co.clarika.stripe', {
		payment : payment,
		transactionAmount : transaction.getCurrencyAndTotalAmount()
	});
	paymentOnline.on('success', onSuccessPayment);
	paymentOnline.on('cancel', onCancelPayment);
	paymentOnline.open();
	//gc
	payment = null;
	//gc
	selectPaymentMethodController = null;
};
/**
 * on success payment transaction
 * @param {Object} e
 */
var onSuccessPayment = _.debounce(function(e) {
	Ti.App.fireEvent('show_loader', {
		message : L('processing_purchase', 'Processing purchase...'),
		cancelable : false,
		backgroundColor : Alloy.Globals.Colors.primary
	});
	transaction.processOnlinePayment(e);
	isStripe = e.paymentMethod == Alloy.CFG.Stripe.method;
	var transactionDetailsToService = generateTransactionDetailsToServiceByPurchaseStore();
	Ti.API.info('transactionDetailsToService: ' + JSON.stringify(transactionDetailsToService));

	if (_.isObject(gift)) {
		var transactionToService = transaction.toService(transactionDetailsToService);
		getGiftService().send(gift.toService(transactionToService), onSuccessGift, onErrorGift, httpCK);
	} else {
		getTransactionService().send(transaction.toService(transactionDetailsToService), onSuccessTransaction, onErrorTransaction, httpCK);
	}
}, Alloy.Globals.Integers.debounce, true);

/**
 * on cancel payment transaction
 * @param {Object} e
 */
var onCancelPayment = _.debounce(function(e) {
	hideLoader();
}, Alloy.Globals.Integers.debounce, true);
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.onOpen = onOpen;
exports.onClose = onClose;
exports.cleanup = cleanup;
exports.updateData = updateData;

function formatWOSym(currency) {

	var formatCurr = currency.split(" ");
	if (formatCurr.length == 1) {
		return Alloy.Globals.FormatUtils.convertStringToNumber(currency);
	} else {
		return Alloy.Globals.FormatUtils.convertStringToNumber(formatCurr[1]);
	}
}