/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var subtitle = '';
/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		if (_.has(properties, 'subtitle')) {
			subtitle = properties.subtitle;
		}

		if (_.has(properties, 'gender')) {
			$.users_presence.applyProperties({
				gender : properties.gender
			});
		}

		if (_.has(properties, 'establishmentID')) {
			$.users_presence.applyProperties({
				establishmentID : properties.establishmentID
			});
		}

		_.extend($.win_users_presence, _.omit(properties, 'subtitle', 'gender'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
	if (OS_IOS) {
		$.search.addEventListener('change', onSearch);
	}
	if (OS_ANDROID) {
		$.win_users_presence.addEventListener('androidback', onBack);
	}
};

/**
 * initialize controller
 */
var init = function(e) {
	applyProperties(args);
	//gc
	args = null;
	configureToolbar(e);
	applyListeners();

};

/**
 * configure toolbar
 */
var configureToolbar = function(e) {
	if (OS_ANDROID) {
		var activity = $.win_users_presence.getActivity();
		activity.onCreateOptionsMenu = onCreateOptionsMenu;
		activity.invalidateOptionsMenu();
		activity.getActionBar().displayHomeAsUp = true;
		activity.getActionBar().onHomeIconItemSelected = onHomeIconItemSelected;
		activity.getActionBar().setSubtitle(subtitle);
		//gc
		activity = null;
	}

};

var onBack = function() {
	$.win_users_presence.close();
};

if (OS_ANDROID) {
	/**
	 * on create options menu
	 * @param {Object} e
	 */
	var onCreateOptionsMenu = function(e) {
		e.menu.clear();
		var searchView = Titanium.UI.Android.createSearchView({
			hintText : L('search', 'Search'),
		});

		searchView.addEventListener('change', onSearch);
		e.menu.add({
			title : L('search', 'Search'),
			actionView : searchView,
			icon : Alloy.Globals.Images.ic_action_search_24dp,
			showAsAction : Ti.Android.SHOW_AS_ACTION_IF_ROOM | Ti.Android.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW
		});
		//gc
		searchView = null;

	};
	/**
	 * on home icon item selected
	 */
	var onHomeIconItemSelected = _.debounce(function(e) {
		onBack();
	}, Alloy.Globals.Integers.debounce, true);

}

/** ------------------------
 Listeners
 ------------------------**/

/**
 * when window is opened
 */
var onOpen = function(e) {
	if(OS_IOS){
		init(e);
	}else{
		_.defer(init, e);
	}
	$.users_presence.onOpen(e);
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	$.users_presence.onClose(e);
	cleanup();
};

var onSearch = function(e) {
	$.users_presence.onSearch(e);
};

/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.cleanup = cleanup;
