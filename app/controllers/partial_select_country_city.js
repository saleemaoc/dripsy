/**
 * @author jagu
 *
 */
/** -----------------------------
 * Constants
 ------------------------------**/
var TAG = 'controllers/partial_select_country_city';
/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var HttpCK;
var currentCountryID = (require('AppProperties').getCurrentCountry());
var currentCityID = (require('AppProperties').getCurrentCity());

/** ------------------------
 Dependencies
 ------------------------**/

/**
 * get service dependencie
 */
var getCountryService = function() {
	return Alloy.Globals.ServiceBuilder.getCountryService().inject(getCountryDao());
};

var getCityService = function() {
	return Alloy.Globals.ServiceBuilder.getCityService().inject(getCityDao());
};

/**
 * get dao dependencie
 */
var getCountryDao = function() {
	return Alloy.Globals.DaoBuilder.getCountryDao();
};

/**
 * get dao dependencie
 */
var getCityDao = function() {
	return Alloy.Globals.DaoBuilder.getCityDao();
};
/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		_.extend($.partial_select_country_city, _.omit(properties));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
		if (HttpCK) {
			HttpCK.abort();
		}
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
	$.picker_country.on('change', onChangeCountry);
	$.picker_city.on('change', onChangeCity);
	$.btn_ok.addEventListener('click', onClick);
};

/**
 * initialize controller
 */
var init = function() {
	applyProperties(args);
	applyListeners();
	//gc
	args = null;
	//create http instance
	HttpCK = new (require('HttpCK'))();
	showLoader();
	getCountryData();

};

/**
 * validate spinners city and country
 */
var validateFields = function() {
	if (currentCountryID == -1) {
		(require('TiUtils')).toast(L('country_required', 'You must select a country'));
		return false;
	} else if (currentCityID == -1) {
		(require('TiUtils')).toast(L('city_required', 'You must select a city'));
		return false;
	} else {
		return true;
	}
};

/**
 * get country data
 */
var getCountryData = function() {
	getCountryService().get(getParamsToGetCountry(), onSuccessGetCountry, onErrorGetCountry, HttpCK);
	configureCity();
};

/**
 * get param to get country
 */
var getParamsToGetCountry = function() {
	return {
		enabled : Alloy.CFG.constants.true
	};
};

/**
 * success get country
 * @param {Object} response
 */
var onSuccessGetCountry = function(reponse) {
	(require('TiLog')).info(TAG, ' onSuccessGetCountry()');
	_.defer(configureCountry);
};

/**
 * error get country
 * @param {Object} error
 */
var onErrorGetCountry = function(error) {
	(require('TiLog')).error(TAG + ' onErrorGetCountry()', JSON.stringify(error));
	(require('TiUtils')).toast(L('error_get_countries', 'Could not update the countries'));
	_.defer(configureCountry);
};

/**
 * configure country widget
 */
var configureCountry = function() {
	$.picker_country.applyProperties({
		options : $.picker_country.formatDataToPicker(getCountryDao().findForLocation(), 'id', 'name', true, L('country', 'Country'), false, false, true),
		selectedByID : currentCountryID
	});
	hideLoader();
};
/** ------------------------
 Listeners
 ------------------------**/

/**
 * When user change country
 * @param {Object} e
 */
var onChangeCountry = function(e) {

	if (e.row.id == -1 || e.row.id != currentCountryID) {
		currentCityID = -1;
	}

	currentCountryID = e.row.id;
	if (currentCountryID != -1) {
		getCityData({
			country_id : currentCountryID,
			enabled : Alloy.CFG.constants.true
		});
	} else {
		configureCity();
	}
};

/**
 * when user change city
 * @param {Object} e
 */
var onChangeCity = function(e) {
	currentCityID = e.row.id;
};

/**
 * get City Data
 */

var getCityData = function(params) {
	showLoaderCity();
	getCityService().get(params, onSuccessGetCity, onErrorGetCity, HttpCK);
};

var onSuccessGetCity = function(reponse) {
	_.defer(configureCity);
};

var onErrorGetCity = function(error) {
	(require('TiLog')).error(TAG + ' onErrorGetCity()', JSON.stringify(error));
	(require('TiUtils')).toast(L('error_get_cities', 'Could not update the cities'));
	_.defer(configureCity);
};

/**
 * configure city
 */
var configureCity = function() {

	var label = L('city', 'City');

	$.picker_city.applyProperties({
		options : $.picker_city.formatDataToPicker(getCityDao().findForLocationByCountryID(currentCountryID), 'id', 'name', true, label, false, false, true),
		selectedByID : currentCityID
	});

	hideLoaderCity();
};

/**
 * onClick btn_ok
 */
var onClick = function() {
	if (validateFields()) {

		(require('AppProperties')).setCurrentCountry(currentCountryID);
		(require('AppProperties')).setCurrentCity(currentCityID);

		var country = getCountryDao().findByID(currentCountryID);
		(require('AppProperties')).setGlobalsByCountry(country);
		//gc
		country = null;

		var city = getCityDao().findByID(currentCityID);
		(require('AppProperties')).setGlobalsByCity(city);
		//gc
		city = null;

		Alloy.Globals.AppRoute.openHome();
		$.trigger('success');
	}
};

/**
 * show loader and hide page
 */
var showLoader = function() {
	$.loader.show();
	$.page.visible = false;
};

var hideLoader = function() {
	$.loader.hide();
	$.page.visible = true;
};

/**
 *	show loader_city and hide picker_city
 */

var showLoaderCity = function() {
	$.picker_city.showLoader();

};

var hideLoaderCity = function() {
	$.picker_city.hideLoader();
};

var beforeOpen = function() {

	var propertiesToPicker = {
		android : {
			font : {
				fontFamily : Alloy.Globals.Fonts.Bold,
				fontSize : Alloy.Globals.Dimens.font_big_text
			},
			color : Alloy.Globals.Colors.typographic_white
		}
	};

	$.picker_country.applyProperties(propertiesToPicker);
	$.picker_city.applyProperties(propertiesToPicker);

};

/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * when window is opened
 */
var onOpen = function(e) {
	if (OS_IOS) {
		init(e);
	} else {
		_.defer(init, e);
	}
	$.picker_country.onOpen(e);
	$.picker_city.onOpen(e);
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
};

beforeOpen();
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.onOpen = onOpen;
exports.onClose = onClose;
exports.cleanup = cleanup;
