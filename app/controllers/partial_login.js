/**
 * @author jagu
 *
 */
/** ------------------------
 Constants
 ------------------------**/
var TAG = 'controller/partial_login';
/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
/**
 * http library
 */
var httpCK = null;
/**
 * loader
 */
var loader = null;
/**
 * loader for facebook
 */
var loaderFB = null;
/**
 * parent view reference
 */
var parentWindow = null;

//local login
var mUsername = null;
var mPassword = null;

var isLocalLogin = true;
/**
 * flag to prevent call two time using pushwoosh
 */
var callToLogin = true;

var onLoginFacebookProcess = false;

var service = null;
/** ------------------------
 Dependencies
 ------------------------**/

/**
 * get service dependencie
 * @return {UserService}
 */
var getService = function() {
	if (_.isNull(service)) {
		service = Alloy.Globals.ServiceBuilder.getUserService();
	}
	return service;
};

/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		if (_.has(properties, 'parentWindow')) {
			parentWindow = properties.parentWindow;
			setFacebookProxy();
		}
		_.extend($.partial_login, _.omit(properties, 'parentWindow'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
		removeFacebookListener();
		if (httpCK) {
			httpCK.abort();
		}
	} catch(e) {
	}
};
/**
 * apply listeners to controller
 */
var applyListeners = function() {
	$.btn_sign_in.addEventListener('click', onSignIn);
	$.btn_facebook.addEventListener('click', onFacebookLogin);
	$.v_lbl_not_member_sing_up.addEventListener('click', onSignUp);
	$.lbl_login_forgot.addEventListener('click', onForgotPassword);
	Alloy.Globals.Facebook.addEventListener('login', onLoginFacebook);
	if (OS_IOS) {
		$.partial_login.addEventListener('click', hideKeyboard);
	}
};

/**
 * initialize controller
 */
var init = function() {
	applyProperties(args);
	applyListeners();
	httpCK = new (require('HttpCK'))();
	//gc
	args = null;

};

/**
 * validate fields
 * @param {Object} user
 * @param {String} password
 */
var validateFields = function(user, password) {
	if (_.isString(user) && !_.isEmpty(user.trim()) && _.isString(password) && !_.isEmpty(password.trim())) {
		if ((!require('JsUtils').validateEmail(user))) {
			(require('TiUtils')).toast(L('login_error_email', 'The email is already registered'));
			return false;
		}
		if (password.trim().length < Alloy.CFG.validate.user_password_length) {
			(require('TiUtils')).toast(String.format(L('login_error_email', 'The password must have more than %s caracater'), Alloy.CFG.validate.user_password_length + ""));
			return false;
		}
		return true;
	} else {
		(require('TiUtils')).toast(L('login_error_required', 'User and password are required'));
		return false;
	}
};

/**
 * set proxy for Facebook
 */
var setFacebookProxy = function() {
	if (OS_ANDROID && !_.isNull(parentWindow)) {
		parentWindow.fbProxy = Alloy.Globals.Facebook.createActivityWorker({
			lifecycleContainer : parentWindow
		});

		//gc
		parentWindow = null;
	}
};

/**
 * @param {String} username
 * @param {String} password
 */
var auth = function(username, password) {

	mUsername = username;
	mPassword = password;
	isLocalLogin = true;

	registerPushWoosh();

};

/**
 * on success login
 * @param {Object} response
 */
var onSuccessLogin = function(response) {
	(require('TiLog')).info(TAG, 'onSuccessLogin(), ' + JSON.stringify(response));
	saveTokenAndUserInfo(response);
	hideLoader();
	Alloy.Globals.AppRoute.openHome();
	$.trigger('success');
};

/**
 * on error login
 * @param {Object} error
 */
var onErrorLogin = function(error) {
	(require('TiLog')).error(TAG, 'onErrorLogin(), ' + JSON.stringify(error));
	hideLoader();
	switch(error.code) {
	case 0:
		Alloy.Globals.TiUtils.toast(L('error_network_connection_no_internet', "Oops! looks like you don't have Internet connection."));
		break;
	case Alloy.CFG.Http.InternalServerError:
		Alloy.Globals.TiUtils.toast(L('error_server', 'An error occurred on the server please contact the administrator'));
		break;
	case Alloy.CFG.Http.AccountInactive:
		showAlertAccountInactive();
		break;
	default:
		Alloy.Globals.TiUtils.toast(L('login_error_user_password', 'User and/or password is incorrect'));
		break;
	}
	Alloy.Globals.Facebook.logout();
};

var showAlertAccountInactive = function() {
	var alert = Ti.UI.createAlertDialog({
		message : L('activate_your_account_to_login_notification', 'To continue, please check your email and confirm this address.'),
		buttonNames : [L('accept', 'Accept'), L('resend_email', 'Resend email')],
		cancel : 0
	});

	alert.addEventListener('click', function(e) {
		if (e.index == 1) {
			resendEmailToActivateAccount();
		}
	});

	alert.show();

};

var resendEmailToActivateAccount = function() {
	loader = Alloy.createWidget('co.clarika.loader', {
		message : {
			text : L('resending_email', 'Resending email...'),
			color : Alloy.Globals.Colors.accent,
			font : {
				fontFamily : Alloy.Globals.Fonts.Regular,
				fontSize : Alloy.Globals.Dimens.font_subheading
			}
		},
		activityIndicator : {
			indicatorColor : Alloy.Globals.Colors.accent,
			style : Titanium.UI.ActivityIndicatorStyle.BIG
		}
	});

	loader.on(loader.events.OPEN, function(e) {
		var user = $.txt_login_mail.getValue();
		getService().resendEmailToActivateAccount(user, successResendEmailToActivateAccount, errorResendEmailToActivateAccount, httpCK);
	});

	loader.show();
};

var successResendEmailToActivateAccount = function(response) {
	hideLoader();
	Alloy.Globals.TiUtils.toast(L('activate_your_account_to_login', 'An email was sent to your address. To complete your registration, please confirm your account.'));
};

var errorResendEmailToActivateAccount = function(error) {
	hideLoader();
	switch(error.code) {
	//account activate
	case 401:
		//auto logging
		onSignIn();
		break;
	default:
		Alloy.Globals.TiUtils.toast(L('error_resend_email_to_activate_account', 'An error ocurred when try activate account, try again.'));
		break;
	}

};

/**
 * save token and user information in shared preferenced and database
 * @param {Object} response
 */
var saveTokenAndUserInfo = function(response) {
	(require('AppProperties')).setUserIsLogged(true);
	(require('AppProperties')).setToken(response.token);
	(require('AppProperties')).setUserID(response.id);
	(require('AppProperties')).checkGlobals();
	Alloy.Globals.UserLogged.mapping(response);
	//save user card
	if (!_.isNull(response.user_card)) {
		(Alloy.createWidget('co.clarika.stripe')).saveNewCard({
			cardLast4 : response.user_card.last_card,
			brand : response.user_card.brand,
			expMonth : response.user_card.exp_month,
			expYear : response.user_card.exp_year,
			token : response.user_card.token
		});
	} else {
		(Alloy.createWidget('co.clarika.stripe')).saveNewCard(null);
	}
	//gc
	userLogged = null;
	response = null;
};

/**
 * create a loader widget and opened
 * @param Function success
 */
var showLoader = function(success, email, password) {
	if (loader == null) {
		loader = Alloy.createWidget('co.clarika.loader', {

			message : {
				text : L('logging_in', 'Logging In...'),
				color : Alloy.Globals.Colors.accent,
				font : {
					fontFamily : Alloy.Globals.Fonts.Regular,
					fontSize : Alloy.Globals.Dimens.font_subheading
				}
			},
			activityIndicator : {
				indicatorColor : Alloy.Globals.Colors.accent,
				style : Titanium.UI.ActivityIndicatorStyle.BIG
			}
		});

		loader.on(loader.events.OPEN, function(e) {
			if (_.isString(email) && _.isString(password)) {
				success(email, password);
			} else {
				success(e);
			}
		});
	}
	loader.show();
};

/**
 * hide loader
 */
var hideLoader = function() {
	if (loader) {
		loader.hide();
	}
	loader = null;
};

var getFacebookInfo = function() {
	Alloy.Globals.Facebook.requestWithGraphPath('me', {
		'fields' : 'first_name,last_name,gender,birthday,email'
	}, 'GET', function(e) {
		if (e.success) {
			var jsonUserFacebook = JSON.parse(e.result);

			hideFBLoader();
			//open register with facebook information
			var controller = Alloy.Globals.AppRoute.openRegistration({
				userFacebook : jsonUserFacebook
			});

			controller.on('success', onSuccessFBRegistration);
			controller.on('cancel', onCancelFBRegistration);
			//gc
			controller = null;
		} else {
			hideFBLoader();
			(require('TiUtils')).toast(L('error_facebook_login', 'There was an error logging in with Facebook'));

		}
	});

};

/**
 * on local auth
 */
var sendLocalAuth = function() {
	var params = {
		username : mUsername,
		password : mPassword,
		device_token : Alloy.Globals.PushWooshHelper.getRegistrationId()
	};
	getService().auth(params, onSuccessLogin, onErrorLogin, httpCK);
};

/**
 * login with facebook
 */
var onSocialAuth = function() {
	showFBLoader();
	isLocalLogin = false;
	registerPushWoosh();
};

var sendSocialAuth = function() {
	var params = {
		facebook_id : Alloy.Globals.Facebook.uid,
		facebook_token : Alloy.Globals.Facebook.accessToken,
		device_token : Alloy.Globals.PushWooshHelper.getRegistrationId()
	};
	getService().socialAuth(params, onSuccessSocialAuth, onErrorSocialAuth, httpCK);
};
/**
 * on success social login
 * @param {Object} response
 */
var onSuccessSocialAuth = function(response) {
	(require('TiLog')).info(TAG, 'onSuccessSocialAuth -> ' + JSON.stringify(response));
	saveTokenAndUserInfo(response);
	hideFBLoader();
	Alloy.Globals.AppRoute.openHome();
	$.trigger('success');
};

/**
 * on error social auth
 * @param {Object} error
 */
var onErrorSocialAuth = function(error) {
	(require('TiLog')).error(TAG, 'onErrorSocialAuth -> ' + JSON.stringify(error));
	switch(error.code) {
	case Alloy.CFG.Http.EmailUsed:
		(require('TiUtils')).toast(L('exist_account_with_this_email', 'An account already exists with this email'));
		hideFBLoader();
		break;
	case Alloy.CFG.Http.SocialAuthLoginUserNotRegister:
		getFacebookInfo();
		break;
	default:
		(require('TiUtils')).toast(L('login_error_user_facebook', 'There was an error logging in with Facebook'));
		hideFBLoader();
		break;
	}
};

/**
 * show FB loader
 */
var showFBLoader = function() {
	if (!loaderFB) {
		loaderFB = Alloy.createWidget('co.clarika.loader', {
			message : {
				text : L('loggin_with_facebook', 'Logging in with Facebook...')
			}
		});
		loaderFB.on(loaderFB.events.BACK, onCancelFBRegistration);
		loaderFB.show();
	}

};

/**
 * hide loader
 */
var hideFBLoader = function() {
	onLoginFacebookProcess = false;
	if (loaderFB) {
		loaderFB.hide();
	}
	loaderFB = null;
};

/**
 * register notification
 */
var registerPushWoosh = function() {
	callToLogin = true;
	if (_.isNull(Alloy.Globals.PushWooshHelper.getRegistrationId()) || _.isEmpty(Alloy.Globals.PushWooshHelper.getRegistrationId())) {
		Alloy.Globals.PushWooshHelper.registerForPushNotifications(onSuccessPushWoosh, onErrorPushWoosh);
	} else {
		onSuccessPushWoosh();
	}

};

/**
 * on success push woosh
 * @param {Object} e
 */
var onSuccessPushWoosh = function(e) {
	//FIXME pushwoosh https://github.com/Pushwoosh/pushwoosh-appcelerator-titanium/issues/6
	setTimeout(function() {
		if (callToLogin) {
			if (_.isObject(e)) {
				(require('TiLog')).info(TAG, 'onSuccessPushWoosh, e -> ' + JSON.stringify(e));
				Alloy.Globals.PushWooshHelper.setRegistrationId(e.registrationId);
			}
			sendLogin();
		}
		callToLogin = false;
	}, 0);
};

/**
 * on error push woosh
 * @param {Object} e
 */
var onErrorPushWoosh = function(e) {
	//FIXME pushwoosh https://github.com/Pushwoosh/pushwoosh-appcelerator-titanium/issues/6
	setTimeout(function() {
		if (callToLogin) {
			(require('TiLog')).error(TAG, 'onErrorPushWoosh, e -> ' + JSON.stringify(e));
			sendLogin();
		}
		callToLogin = false;
	}, 0);
};

var sendLogin = function() {
	if (isLocalLogin) {
		sendLocalAuth();
	} else {
		sendSocialAuth();
	}
};
/** ------------------------
 Listeners
 ------------------------**/
/**
 * when window is opened
 */
var onOpen = function(e) {
	if (OS_IOS) {
		init(e);
	} else {
		_.defer(init, e);
	}
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
};

var removeFacebookListener = function() {
	Alloy.Globals.Facebook.removeEventListener('login', onLoginFacebook);
};

/**
 * sign in user
 * @param {Object} e
 */
var onSignIn = _.debounce(function(e) {
	var user = $.txt_login_mail.getValue();
	var password = $.txt_login_password.getValue();

	if (validateFields(user, password)) {
		showLoader(onAuth);
	}
}, Alloy.Globals.Integers.debounce, true);

/**
 * on auth user
 */
var onAuth = function() {
	var email = $.txt_login_mail.getValue();
	var password = $.txt_login_password.getValue();
	auth(email, password);
};

var onFacebookLogin = _.debounce(function() {
	Alloy.Globals.Facebook.authorize();
}, Alloy.Globals.Integers.debounce, true);

var onSignUp = _.debounce(function(e) {
	var controller = Alloy.Globals.AppRoute.openRegistration();
	controller.on('success', onSuccessRegistration);

	//gc
	controller = null;
}, Alloy.Globals.Integers.debounce, true);

/**
 * on success registration
 * @param {Object} e
 */
var onSuccessRegistration = _.debounce(function(e) {
	Alloy.Globals.TiUtils.alert(L('activate_your_account_to_login', 'An email was sent to your address. To complete your registration, please confirm your account.'), L('successful_registration', 'Successful registration'));
}, Alloy.Globals.Integers.debounce, true);

/**
 * on success registration
 */
var onSuccessFBRegistration = _.debounce(function(e) {
	onSocialAuth();
}, Alloy.Globals.Integers.debounce, true);

var onCancelFBRegistration = function() {
	Alloy.Globals.Facebook.logout();
	hideFBLoader();
};
/**
 * on login facebook
 * @param {Object} e
 */
var onLoginFacebook = function(e) {
	if (!onLoginFacebookProcess) {
		onLoginFacebookProcess = true;
		if (e.success) {

			onSocialAuth();
		} else if (e.error) {
			(require('TiLog')).error(TAG, 'onFacebookLogin(), ' + JSON.stringify(e.error));
			Alloy.Globals.Facebook.logout();
			hideFBLoader();
			(require('TiUtils')).toast(L('login_error_user_facebook', 'There was an error logging in with Facebook'));
		} else {
			hideFBLoader();
			onLoginFacebookProcess = false;
		}

	}

};

var onForgotPassword = _.debounce(function(e) {
	var controller = Alloy.Globals.AppRoute.openForgotPassword();
	//gc
	controller = null;
}, Alloy.Globals.Integers.debounce, true);

var hideKeyboard = function() {
	$.txt_login_mail.blur();
	$.txt_login_password.blur();
};
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.onOpen = onOpen;
exports.onClose = onClose;
exports.cleanup = cleanup;
exports.validateFields = validateFields;
