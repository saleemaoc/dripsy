/**
 * @author pflores
 *
 */
/** ------------------------
 Fields
 ------------------------**/
/**
 * Arguments passed into this controller can be accessed via the `$.args` object directly or:
 */
var args = $.args;

/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {
		_.extend($.partial_information_screen, _.omit(properties));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
	} catch(e) {
	}
};
/**
 * apply listeners to controller
 */
var applyListeners = function() {
};

/**
 * initialize controller
 */
var init = function() {
	applyProperties(args);
	applyListeners();
	setVersion();
	//gc
	args = null;
};

/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */
var setVersion = function() {
	$.lbl_number_version.setText(Titanium.App.version);
};

/**
 * when window is opened
 */
var onOpen = function(e) {
	if(OS_IOS){
		init(e);
	}else{
		_.defer(init, e);
	}
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
};

/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.onOpen = onOpen;
exports.onClose = onClose;
exports.cleanup = cleanup;
exports.setVersion = setVersion;
