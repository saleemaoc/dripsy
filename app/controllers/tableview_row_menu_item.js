/** ---------
 Fields
 -------- **/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
/** ---------
 Methods
 -------- **/
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		if (_.has(properties, 'icon')) {
			_.extend($.icon, properties.icon);
		} else {
			withoutIcon();
		}

		if (_.has(properties, 'title')) {
			_.extend($.title, properties.title);
		}

		if (_.isFunction(properties.notification)) {
			$.notification.visible = properties.notification();
		}

		_.extend($.tableview_row_menu_item, _.omit(properties, 'title', 'icon', 'notification'));
	}
};

/**
 * remove icon and apply new styles to title
 */
var withoutIcon = function() {
	$.icon.left = 0;
	$.icon.hide({
		animated : OS_IOS
	});
	$.title.left = Alloy.Globals.Dimens.padding_horizontal;
};

var init = function() {
	applyProperties(args);
	//gc
	args = null;
};

var setNotification = function(value) {
	$.notification.visible = value;
};
//initialize row
init();

/** ---------
 Public
 -------- **/
exports.applyProperties = applyProperties;
exports.setNotification = setNotification;
