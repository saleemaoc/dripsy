/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
if (OS_ANDROID) {
	var abx = require('com.alcoapps.actionbarextras');
}
var success = false;
/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		if (_.has(properties, 'userFacebook')) {
			$.registration.applyProperties({
				userFacebook : properties.userFacebook
			});
		}

		_.extend($.win_registration, _.omit(properties, 'userFacebook'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
	} catch(e) {
	}
};

var onBack = function() {
	$.win_registration.close();
	$.trigger('cancel');
};

/**
 * configure toolbar
 */
var configureToolbar = function(e) {

	if (OS_ANDROID) {
		abx.setBackgroundColor(Alloy.Globals.Colors.action_bar_transparent);
		abx.setElevation(0);
		var activity = $.win_registration.getActivity();
		activity.getActionBar().displayHomeAsUp = true;
		activity.getActionBar().onHomeIconItemSelected = onHomeIconItemSelected;
		//gc
		activity = null;
	}
};

/**
 * on home icon item selected
 */
var onHomeIconItemSelected = _.debounce(function(e) {
	onBack();
}, Alloy.Globals.Integers.debounce, true);

/**
 * apply listeners to controller
 */
var applyListeners = function() {
	$.registration.on('success', onSuccess);
	$.registration.on('error', onError);
	if (OS_ANDROID) {
		$.win_registration.addEventListener('androidback', onBack);
	}
};

var onError = function() {

};

/**
 * initialize controller
 */
var init = function(e) {
	configureToolbar(e);
	applyProperties(args);
	applyListeners();
	//gc
	args = null;
	configureActivity();
};

var configureActivity = function() {
	if (OS_ANDROID) {
		var currentActivity = $.win_registration.getActivity();
		$.registration.applyProperties({
			currentActivity : currentActivity
		});
		//gc
		currentActivity = null;
	}
};

/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * when window is opened
 */
var onOpen = function(e) {
	init(e);
	$.registration.onOpen(e);
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	$.registration.onClose(e);
	if (OS_IOS && !success) {
		$.trigger('cancel');
	}
	cleanup();
};

/**
 * press ok data
 */
var onOk = function(e) {
	$.registration.onSignUp();
};

var onSuccess = function(e) {
	$.trigger('success', e);
	success = true;
	$.win_registration.close();
};

var onBack = function(e) {
	if (OS_IOS) {
		Alloy.Globals.AppRoute.closeNavigationWindowSecondary();
	}
	$.win_registration.close();
};

/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.cleanup = cleanup;
