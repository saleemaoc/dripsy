/**
 * @author jagu
 */

/** ------------------------
 Constants
 ------------------------**/
var TAG = 'controller/partial_conversation_list';
/**
 * 20 seconds
 */
var DELAY_FIND = 20000;
/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var isInit = false;
/**
 * limit to conversation query
 */
var limit = Alloy.CFG.paggination_default || 20;
/**
 * collection
 */
var collection = $.local_collection;
/**
 * http library
 */
var httpCK;
/**
 * flag to indicate state to loader
 */
var loading = false;
/**
 * refresh flags
 */
var isPullToRefresh = false;
var isScrolling = false;
/**
 * time out to find new conversations
 */
var timeOutfind = null;
/** ------------------------
 Dependencies
 ------------------------**/

/**
 * return dependencie to service
 */
var getService = function() {
	return Alloy.Globals.ServiceBuilder.getConversationService().inject(getDao());
};

/**
 * return dependencie to dao
 */
var getDao = function() {
	return new Alloy.Globals.DaoBuilder.getConversationDao();
};

/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		_.extend($.partial_conversation_list, _.omit(properties));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		clearFindNewConversations();
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
		if (isInit && !_.isUndefined($.is)) {
			$.is.cleanup();
		}
		if (httpCK) {
			httpCK.abort();
		}

	} catch(e) {

	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
	$.list.addEventListener('itemclick', onClickItem);
};

/**
 * initialize controller
 */
var init = function() {
	applyProperties(args);
	applyListeners();
	//gc
	args = null;
	//initialize inifinite scroll
	$.is.init($.list);
	isInit = true;
	//create http instance
	httpCK = new (require('HttpCK'))();
	if (collection.models.length == 0) {
		showLoader();
	}
	_.delay(getData, Alloy.Globals.Integers.PreventDetachedView);
};

/**
 * get params to get
 */
var getParamsToGet = function() {

	var params = {
	};

	var lastUpdated = (require('AppProperties')).getChatLastUpdated();
	if (!_.isNull(lastUpdated)) {
		params.modified = (require('FormatUtils')).formatStringDBDateToDateTimeFormatService(lastUpdated);
	}
	return params;

};

var getExtraParams = function() {
	var extraParams = {

	};
	if (isPullToRefresh) {
		//if is pull to refresh ignore cache
		extraParams.ttl = false;
	}
	return extraParams;
};

/**
 * get data
 */
var getData = function() {
	getService().get(getParamsToGet(), onSuccessGet, onErrorGet, httpCK, getExtraParams());
};

/**
 * success get service
 * @param {Object} response
 */
var onSuccessGet = function(response) {
	if (response.results.length < limit) {
		_.delay(removeScrolling, Alloy.Globals.Integers.PreventDetachedView);
	}
	updateLastUpdated();
	hidePullToRefresh();
	updateData();
};

/**
 * when service display an error
 * @param {String} error
 */
var onErrorGet = function(error) {
	(require('TiLog')).error(TAG + ' onErrorGet()', error);
	hidePullToRefresh();
	updateData();
};

var getQuery = function() {
	return ' limit ' + limit + ' offset ' + collection.models.length;
};

/**
 * update data in list
 */
var updateData = function() {
	if (collection.models.length == 0) {
		hideLoader();
	}
	getDao().findByUserID(Alloy.Globals.UserID, getQuery(), true, onSuccessData, onErrorData, collection);
};

/**
 * @param {Object} e
 */
var onSuccessData = function(e) {
	(require('TiLog')).info(TAG + ' onSuccessData()', JSON.stringify(e));
	showEmptyMessage();
	hideLoaderScrolling();
	executeProcessToFindConversations();
};

/**
 * @param {Object} e
 */
var onErrorData = function(e) {
	(require('TiLog')).error(TAG + ' onErrorData()', JSON.stringify(e));
	hideLoaderScrolling();
	executeProcessToFindConversations();
};

/**
 * Show main loader and hide list
 */
var showLoader = function() {
	if (!loading) {
		loading = true;
		$.loader.show();
		$.list.hide({
			animated : OS_IOS
		});
		$.empty.hide({
			animated : OS_IOS
		});
	}
};

/**
 * Hide main loader and show list
 */
var hideLoader = function() {
	if (loading) {
		loading = false;
		$.loader.hide();
		$.list.show({
			animated : OS_IOS
		});
	}
};

/**
 * Fire pull to refresh
 */
var refresh = function() {
	$.ptr.refresh();
};

/**
 * check if need show empty message
 */
var showEmptyMessage = function() {
	if (collection.models.length > 0) {
		$.list.visible = true;
		$.empty.hide();
	} else {
		$.list.visible = false;
		$.empty.show();
	}

};
/**
 * hide pull to refresh is showing
 */
var hidePullToRefresh = function() {
	if (isPullToRefresh) {
		isPullToRefresh = false;
		$.ptr.hide();
	}
};

/**
 * detached infinite scrolling
 */
var removeScrolling = function() {
	isScrolling = false;
	$.is.state($.is.DONE);
};

/**
 * hide loader scrolling
 */
var hideLoaderScrolling = function() {
	if (isScrolling) {
		isScrolling = false;
		$.is.state($.is.SUCCESS);
	}
};

/**
 * get collection
 */
var getCollection = function() {
	return collection;
};

/**
 * abort request to get conversations
 */
var cancelGetConversations = function() {
	if (_.isObject(httpCK)) {
		httpCK.abortByTag(getService().TAGS.TAG_GET);
	}
};

/**
 * reset collection
 */
var collectionReset = function() {
	if (collection.models.length > 0) {
		collection.reset();
	}
};

/**
 * update date chat last updated only for pull to refresh and first get
 */
var updateLastUpdated = function() {
	if (!isScrolling) {
		var dateLastUpdated = (require('FormatUtils')).getStringDBToDate();
		(require('AppProperties')).setChatLastUpdated(dateLastUpdated);
	}
};

/**
 * execute process to find messages
 */
var executeProcessToFindConversations = function() {
	clearFindNewConversations();
	timeOutfind = setTimeout(refresh, DELAY_FIND);
};

/**
 * clear timeout to find new conversations
 */
var clearFindNewConversations = function() {
	if (!_.isNull(timeOutfind)) {
		clearTimeout(timeOutfind);
		timeOutfind = null;
	}
	cancelGetConversations();
};
/** ------------------------
 Listeners
 ------------------------**/

/**
 * when window is opened
 */
var onOpen = function(e) {
	if (OS_IOS) {
		init(e);
	} else {
		_.defer(init, e);
	}
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
};

/**
 * @param {Object} e
 */
var onResume = function(e) {
	if (!_.isEmpty(collection.models)) {
		refresh();
	}
};

/**
 * When user tap pull to refresh
 * @param {Object} e
 */
var onPullToRefresh = function(e) {
	if (!isPullToRefresh) {
		clearFindNewConversations();
		isPullToRefresh = true;
		cancelGetConversations();
		collectionReset();
		getData();
	}
};

/**
 * when user scrolling to end
 * @param {Object} e
 */
var onEndScrolling = function(e) {
	isScrolling = true;
	updateData();
};

/**
 * on click item
 * @param {Object} e
 */
var onClickItem = function(e) {
	var model = collection.models[e.itemIndex];

	switch(e.bindId) {
	case 'full_name':
	case 'photo_profile':
		//open user profile
		Alloy.Globals.AppRoute.openUserProfile({
			userID : model.getOtherUserID()
		});
		break;
	default:
		//open conversation
		$.trigger('itemClick', {
			conversation : model
		});

		//get title
		var title = model.transform().other_user_full_name;

		clearFindNewConversations();
		markRead(model);

		var conversationController = Alloy.Globals.AppRoute.openConversationDetail({
			conversation : model,
			title : title
		});

		//FIX focus https://jira.appcelerator.org/browse/TIMOB-16560
		if (OS_IOS) {
			conversationController.on('ios:back', iosBack);
		}
		//gc
		conversationController = null;
		break;
	}

};

var iosBack = function() {
	$.trigger('ios:back', {});
};

/**
 * mark read conversation
 * @param {Conversation} conversation
 */
var markRead = function(conversation) {
	getDao().markRead(conversation);
};
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.onOpen = onOpen;
exports.onClose = onClose;
exports.cleanup = cleanup;
exports.refresh = refresh;
exports.onResume = onResume;
