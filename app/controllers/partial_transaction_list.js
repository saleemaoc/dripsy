/**
 * @author jagu
 */

/** ------------------------
 Constants
 ------------------------**/
var TAG = 'controller/partial_transaction_list';
/** ------------------------
 Fields
 ------------------------**/
/**
 * Arguments passed into this controller can be accessed via the `$.args` object directly or:
 */
var args = $.args;
/**
 * limit to establishment query
 */
var limit = 6;
/**
 * collection
 */
var collection = $.local_collection;
/**
 * http library
 */
var httpCK;
/**
 * flag to indicate state to loader
 */
var loading = false;
/**
 * refresh flags
 */
var isPullToRefresh = false;
var isScrolling = false;

/**
 * user id to transactions
 */
var userID = null;

/**
 * widget loader window
 */
var loader = null;
/** ------------------------
 Dependencies
 ------------------------**/

/**
 * return dependencie to service
 */
var getService = function() {
	return Alloy.Globals.ServiceBuilder.getTransactionService().inject(getDao());
};

/**
 * return dependencie to dao
 */
var getDao = function() {
	return Alloy.Globals.DaoBuilder.getTransactionDao();
};

/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {
		if (_.has(properties, 'userID')) {
			userID = properties.userID;
		}
		_.extend($.partial_transaction_list, _.omit(properties, 'userID'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
		if (httpCK) {
			httpCK.abort();
		}
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
	$.table.addEventListener('click', onClickItem);
	$.table.addEventListener('longclick', onLongClickItem);
};

/**
 * initialize controller
 */
var init = function() {
	applyProperties(args);
	//gc
	args = null;
	applyListeners();
	initInfiniteScroll();
	//create http instance
	httpCK = new (require('HttpCK'))();

	if (collection.models.length == 0) {
		showLoader();
	}
	_.delay(getData, Alloy.Globals.Integers.SetDataDelay);
};

/**
 * initialize infinite scroll
 */
var initInfiniteScroll = function() {
	$.is.init($.table);
};

/**
 * get params to get
 */
var getParamsToGet = function() {

	return {
		show : Alloy.CFG.constants.true,
		limit : limit,
		offset : collection.models.length
	};

};

var getExtraParams = function() {
	var extraParams = {

	};
	if (isPullToRefresh) {
		//if is pull to refresh ignore cache
		extraParams.ttl = false;
	}
	return extraParams;
};
/**
 * get data
 */
var getData = function() {

	getService().get(getParamsToGet(), onSuccessGet, onErrorGet, httpCK, getExtraParams());
};

/**
 * success get service
 * @param {Object} response
 */
var onSuccessGet = function(response) {
	Ti.API.info('[onSuccessGet]: '+JSON.stringify(response));
	if (response.results.length < limit) {
		_.delay(removeScrolling, Alloy.Globals.Integers.PreventDetachedView);
	}
	hidePullToRefresh();
	hideLoaderScrolling();
	updateData();
};

/**
 * when service display an error
 * @param {String} error
 */
var onErrorGet = function(error) {
	(require('TiLog')).error(TAG + ' onErrorGet()', error);
	hidePullToRefresh();
	hideLoaderScrolling();
	updateData();
};

/**
 * get extra query
 */
var getQuery = function() {
	return ' limit ' + limit + ' offset ' + collection.models.length;
};

/**
 * update data in list
 */
var updateData = function() {
	getDao().findByBuyerUserID(userID, getQuery(), true, onSuccessData, onErrorData, collection);

};

/**
 * @param {Object} e
 */
var onSuccessData = function(e) {
	(require('TiLog')).info(TAG + ' onSuccessData()', JSON.stringify(e));
	showEmptyMessage();
	hideLoader();

};

/**
 * @param {Object} e
 */
var onErrorData = function(e) {
	(require('TiLog')).error(TAG + ' onErrorData()', JSON.stringify(e));
	hideLoader();
	showEmptyMessage();
};

/**
 * Show main loader and hide list
 */
var showLoader = function() {
	if (!loading) {
		loading = true;
		$.ptr.hide();
		$.loader.show();
		$.table.hide({
			animated : OS_IOS
		});
		$.empty.hide();
	}
};

/**
 * Hide main loader and show list
 */
var hideLoader = function() {
	if (loading) {
		loading = false;
		$.loader.hide();
		$.table.show({
			animated : OS_IOS
		});
	}
};

/**
 * Fire pull to refresh
 */
var refresh = function() {
	$.ptr.refresh();
};

/**
 * check if need show empty message
 */
var showEmptyMessage = function() {
	if (collection.models.length > 0) {
		$.table.visible = true;
		$.empty.hide();
	} else {
		$.table.visible = false;
		$.empty.show();
	}

};
/**
 * hide pull to refresh is showing
 */
var hidePullToRefresh = function() {
	isPullToRefresh = false;
	$.ptr.hide();
};

/**
 * detached infinite scrolling
 */
var removeScrolling = function() {
	isScrolling = false;
	$.is.state($.is.DONE, "");
};

/**
 * hide loader scrolling
 */
var hideLoaderScrolling = function() {
	isScrolling = false;
	$.is.state($.is.SUCCESS);
};
/**
 * reset collection
 */
var collectionReset = function() {
	if (collection.models.length > 0) {
		collection.reset();
	}
};

/**
 * hide transaction
 * @param {Transaction} transaction
 */
var hideTransaction = function(transaction) {
	showWindowLoader({
		message : {
			text : L('hiding_transaction', 'Hiding transaction...')
		}
	});

	getService().hideTransaction(transaction.get('id'), transaction.toHideService(), onSuccessHideTransaction, onErrorHideTransaction, httpCK);
};

/**
 * success hide transacion
 * @param {Object} response
 */
var onSuccessHideTransaction = function(response) {
	(require('TiLog')).info(TAG, 'onSuccessHideTransaction -> ' + JSON.stringify(response));
	collectionReset();
	hideWindowLoader();
	//refresh data onresume
};

/**
 * error to try hide transaction
 * @param {Object} error
 */
var onErrorHideTransaction = function(error) {
	(require('TiLog')).error(TAG, 'onErrorHideTransaction -> ' + JSON.stringify(error));
	(require('TiUtils')).toast(L('error_hide_transaction', 'An error occurred hide the transaction'));
	hideWindowLoader();
};
/**
 * show window loader
 * @param {Object} params
 */
var showWindowLoader = function(params) {
	loader = Alloy.createWidget('co.clarika.loader', params || {});
	loader.show();
};

/**
 * hide window laoder
 */
var hideWindowLoader = function() {
	if (!_.isNull(loader)) {
		loader.hide();
		loader = null;
	}
};

/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * when window is opened
 */
var onOpen = function(e) {
	if (OS_IOS) {
		init(e);
	} else {
		_.defer(init, e);
	}
};

/**
 * on resume
 * @param {Object} e
 */
var onResume = function(e) {
	if (!isPullToRefresh) {
		refresh();
	}
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
};

/**
 * when user scrolling to end
 * @param {Object} e
 */
var onEndScrolling = function(e) {
	isScrolling = true;
	getData();
};

/**
 * When user tap pull to refresh
 * @param {Object} e
 */
var onPullToRefresh = function(e) {
	if (!isPullToRefresh) {
		isPullToRefresh = true;
		getDao().destroyAll();
		collectionReset();
		showLoader();
		getData();
	}
};

/**
 * on click item
 * @param {Object} e
 */
var onClickItem = function(e) {
	var model = collection.models[e.index];

	var transactionDetailController = Alloy.Globals.AppRoute.openTransactionDetail({
		transaction : model
	});

	//FIX focus https://jira.appcelerator.org/browse/TIMOB-16560
	if (OS_IOS) {
		transactionDetailController.on('ios:back', iosBack);
	}

	//gc
	transactionDetailController = null;
	model = null;
};

/**
 * on long click item
 * @param {Object} e
 */
var onLongClickItem = function(e) {
	var model = collection.models[e.index];
	var dialog = Ti.UI.createAlertDialog({
		title : L('question_title_hide_transaction', 'Hide transaction'),
		message : L('question_message_hide_transaction', 'Once hidden the transaction can not be displayed again'),
		buttonNames : [L('cancel', 'Cancel'), L('accept', 'Accept')],
		cancel : 0
	});

	dialog.addEventListener('click', function(e) {
		switch(e.index) {
		case 1:
			hideTransaction(model);
			break;
		}
	});

	dialog.show();

	//gc
	dialog = null;
};

var iosBack = function() {
	$.trigger('ios:back', {});
	onResume();
};
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.onOpen = onOpen;
exports.onClose = onClose;
exports.cleanup = cleanup;
exports.refresh = refresh;
exports.onResume = onResume;
