/**
 * @author jagu
 *
 */
/** ------------------------
 Constants
 ------------------------**/
var TAG = 'controllers/partial_forgot_password';
/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var HttpCK;
/** ------------------------
 Dependencies
 ------------------------**/
/**
 * return user dependencie to service
 * @return {UserService}
 */
var getService = function() {
	return Alloy.Globals.ServiceBuilder.getUserService();
};

/**
 * get dao dependency
 * @return {Dao}
 */
var getDao = function() {
	return Alloy.Globals.DaoBuilder.getUserDao();
};
/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {
		_.extend($.partial_forgot_password, properties);
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
		if (HttpCK) {
			HttpCK.abort();
		}
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
	$.btn_ok.addEventListener('click', onOk);
};

/**
 * initialize controller
 */
var init = function() {
	applyProperties(args);
	applyListeners();
	//gc
	args = null;
	HttpCK = new (require('HttpCK'))();
};

/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * send email data
 */
var onOk = _.debounce(function(e) {
	var email = $.txt_mail.getValue();
	if (validateFields(email)) {
		var params = {
			email : email
		};
		showLoader();
		getService().sendEmail(params, onSuccess, onError, HttpCK);
	}

}, Alloy.Globals.Integers.debounce, true);
/**
 * @param {String} current_password
 * @param {String} new_password
 * @param {String} repeat_password
 */
var validateFields = function(email) {
	if (!_.isString(email) || _.isEmpty(email.trim())) {
		(require('TiUtils')).alert(L('email_empty', 'You must type your mail'));
		return false;
	}
	if ((!require('JsUtils').validateEmail(email))) {
		(require('TiUtils')).alert(L('error_email', 'Email is not valid'));
		return false;
	}
	return true;
};

/**
 *on success forgot password
 * @param {Object} response
 */
var onSuccess = function(response) {
	hideLoader();
	$.trigger('success', {
	});
};

/**
 *on error forgot password
 * @param {Object} error
 */
var onError = function(error) {
	(require('TiLog')).error(TAG, JSON.stringify(error));
	hideLoader();
	var message = "";
	switch(error.code) {
		case Alloy.CFG.Http.EmailUsed:
		message = L ('mail_is_a_facebook_account', 'The email is from an account associated with Facebook');
		break;
	case Alloy.CFG.Http.SocialAuthLoginUserNotRegister:
		message = L('mail_incorrect', 'Oops! Your mail doesn\'t exist. Create your account right now.');
		break;
	case Alloy.CFG.Http.Unauthorized:
		message = L('block_user', 'I\'m sorry, your user was blocked by Night2Night');
	}

	(require('TiUtils')).toast(message);
};

/**
 * show loader and hide page
 */
var showLoader = function() {
	$.loader.show();
	$.page.visible = false;
};
var hideLoader = function() {
	$.loader.hide();
	$.page.visible = true;
};

/**
 * when window is opened
 */
var onOpen = function(e) {
	if(OS_IOS){
		init(e);
	}else{
		_.defer(init, e);
	}
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
};

/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.onOpen = onOpen;
exports.onClose = onClose;
exports.cleanup = cleanup;
exports.onOk = onOk;
