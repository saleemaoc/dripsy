/** ---------
 Constants
 -------- **/
var TAG = 'controllers/tableview_row_drink';
/** ---------
 Fields
 -------- **/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var drink = null;
var purchase = null;
/** ---------
 Methods
 -------- **/
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		if (_.has(properties, 'purchase')) {
			purchase = properties.purchase;
		}

		if (_.has(properties, 'drink')) {
			drink = properties.drink;
		}

		_.extend($.tableview_row_drink, _.omit(properties, 'drink', 'purchase'));
	}
};

/**
 * refresh data
 */
var refreshData = function() {
	if (_.isObject(drink)) {
		$.name.text = drink.getName();
		$.price.text = Alloy.Globals.CurrentCurrencySymbol + " " + Alloy.Globals.FormatUtils.formatPrice(drink.getPrice());
		$.image.image = _.isNull(drink.getImageMini()) ? Alloy.CFG.constants.mock_url_broken_link_image : drink.getImageMini();

		if (_.isObject(purchase) && purchase.quantity > 0) {
			$.count.setValue(purchase.quantity);
		}
		if (drink.isRecommended()) {
			$.v_recommended.show({
				animated : false
			});
		}
	}
};

/**
 * initialize controller
 */
var init = function() {
	applyProperties(args);
	//gc
	args = null;
	refreshData();
	applyListeners();
};

var applyListeners = function() {
	$.count.on('click:less', onLess);
	$.count.on('click:more', onMore);
	$.v_info.addEventListener('click', onClick);
	$.v_content_image.addEventListener('click', onClick);
};

/** ---------
 Listeners
 -------- **/

/**
 * on less amount
 * @param {Object} e
 */
var onLess = function(e) {
	var id = drink.get('id');

	$.trigger('click:less', {
		id : id,
		value : e.value,
		drink : drink
	});
};

/**
 * on more amount
 * @param {Object} e
 */
var onMore = function(e) {
	var id = drink.get('id');

	$.trigger('click:more', {
		id : id,
		value : e.value,
		drink : drink
	});
};

/**
 * on click drink
 */
var onClick = function(e) {
	var id = drink.get('id');

	$.trigger('click', {
		id : id,
		drink : drink
	});
};

//initialize row
init(args);
/** ---------
 Public
 -------- **/
exports.applyProperties = applyProperties;
