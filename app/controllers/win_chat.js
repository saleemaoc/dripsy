/** ------------------------
 Constants
 ------------------------**/
var TAG = 'controllers/win_chat';
/** ------------------------
 Fields
 ------------------------**/
/**
 * Arguments passed into this controller can be accessed via the `$.args` object directly or:
 */
var args = $.args;

/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {
		_.extend($.win_chat, _.omit(properties));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
	if (OS_ANDROID) {
		$.win_chat.getActivity().onResume = onResume;
		$.win_chat.addEventListener('androidback', onBack);
	} else {
		$.conversations.on('ios:back', onResume);
	}
};

/**
 * initialize controller
 */
var init = function(e) {
	applyProperties(args);
	//gc
	args = null;
	applyListeners();
	configureToolbar(e);
};

/**
 * configure toolbar
 */
var configureToolbar = function(e) {
	if (OS_ANDROID) {
		var activity = $.win_chat.getActivity();
		activity.getActionBar().displayHomeAsUp = true;
		activity.getActionBar().onHomeIconItemSelected = onHomeIconItemSelected;
		//gc
		activity = null;
	}

};

var onBack = function() {
	$.win_chat.close();
};

if (OS_ANDROID) {
	/**
	 * on home icon item selected
	 */
	var onHomeIconItemSelected = _.debounce(function(e) {
		onBack();
	}, Alloy.Globals.Integers.debounce, true);
}
/** ------------------------
 Listeners
 ------------------------**/

/**
 * when window is opened
 */
var onOpen = function(e) {
	if(OS_IOS){
		init(e);
	}else{
		_.defer(init, e);
	}
	$.conversations.onOpen(e);

	//clear chat badge
	Alloy.Globals.AppProperties.setPendingChat(false);
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	$.trigger('close', {});
	$.conversations.onClose(e);
	cleanup();
};

/**
 * on resume
 */
var onResume = function(e) {
	Alloy.Globals.AppRoute.setCurrentRoute('win_chat');
	$.conversations.onResume(e);
};
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.cleanup = cleanup;
