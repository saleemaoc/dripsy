/**
 * @author jagu
 *
 */
/** ------------------------
 Constants
 ------------------------**/
var TAG = 'controllers/partial_user_profile';
/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
/**
 * user
 */
var user = null;
/**
 * flag to indicate if first time to call resume
 */
var firstTime = true;
/**
 * user presence
 */
var userPresence = null;
/**
 * user id
 */
var userID;
/**
 * loader
 */
var loader = null;
/**
 * http library
 */
var httpCK = null;
/** ------------------------
 Dependencies
 ------------------------**/
var getDao = function() {
	return Alloy.Globals.DaoBuilder.getUserDao();
};

/** ------------------------
 Methods
 ------------------------**/

/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		if (_.has(properties, 'user')) {
			user = properties.user;
		}

		if (_.has(properties, 'userPresence')) {
			userPresence = properties.userPresence;
		}

		if (_.has(properties, 'userID')) {
			userID = properties.userID;
		}

		_.extend($.partial_user_profile, _.omit(properties, 'user', 'userPresence', 'userID'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
		$.interests.cleanup();
		if (httpCK) {
			httpCK.abort();
		}
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
	$.fab_pencil.on('click', onEditProfile);
	$.fab_drink.on('click', onDrink);
	$.fab_chat.on('click', onChat);
	if (OS_IOS) {
		$.img_avatar.addEventListener('load', hideAvatarLoader);
		$.img_avatar.addEventListener('error', hideAvatarLoader);
	}
};

/**
 * initialize controller
 */
var init = function() {
	applyProperties(args);
	applyListeners();
	//gc
	args = null;
	//create http instance
	httpCK = new (require('HttpCK'))();
	_.delay(setData, Alloy.Globals.Integers.SetDataDelay);
};

var setData = function() {
	if (_.isNumber(userID)) {
		user = getDao().findByID(userID);
		userID = null;
	}
	if (_.isObject(user)) {
		$.user.set(user.attributes);
		user = null;
	}

	if (_.isNumber($.user.get('id'))) {
		showAvatarLoader();
		$.background_image.defaultImage = $.user.getBackgroundDefault();
		$.background_image.brokenLinkImage = $.user.getBackgroundDefault();
		if (OS_ANDROID) {
			$.background_image.setDefaultImage($.user.getBackgroundDefault());
			$.background_image.setBrokenLinkImage($.user.getBackgroundDefault());
		}
		var backgroundImage = _.isNull($.user.getPhoto()) || !Alloy.Globals.JsUtils.validateURL($.user.getPhoto()) ? Alloy.CFG.constants.mock_url_broken_link_image : $.user.getPhoto();
		$.background_image.image = backgroundImage;
		if (OS_ANDROID) {
			$.background_image.setImage(backgroundImage);
		}
		
		$.img_avatar.applyProperties({
			image : {
				image : $.user.getPhotoMini(),
				defaultImage : $.user.getPhotoDefault()
			}
		});
		if (OS_IOS && !Alloy.Globals.JsUtils.validateURL($.user.getPhotoMini())) {
			hideAvatarLoader();
		}
		$.img_avatar.refresh();
		initializeViews();
	}

	var state = $.user.getState();
	if (_.isString(state) && !_.isEmpty(state)) {

		var status1 = "";
		var status2 = "";

		var LENGTH_FIRST_LINE = 40;

		if (state.length > LENGTH_FIRST_LINE) {

			status1 = state.substr(0, LENGTH_FIRST_LINE);
			status2 = state.substr(LENGTH_FIRST_LINE, state.length - LENGTH_FIRST_LINE);
		} else {
			status1 = state;
		}

		$.status_1.text = status1;
		$.status_2.text = status2;
	}
};

/**
 * set visibility to view work
 */
var setVisibilityForWork = function() {
	var userWork = _.isString($.user.getWhereWork()) && !_.isEmpty($.user.getWhereWork());
	var userHasOccupation = _.isString($.user.getOccupation()) && !_.isEmpty($.user.getOccupation());

	if (OS_ANDROID) {
		if (userWork) {
			$.work.show({
				animated : OS_IOS
			});
			$.work.height = Ti.UI.SIZE;
		} else {
			$.work.hide({
				animated : OS_IOS
			});
			$.work.height = 0;
		}

		if (userHasOccupation) {
			$.occupation.width = Ti.UI.SIZE;
			$.occupation.height = Ti.UI.SIZE;
			$.occupation.show({
				animated : OS_IOS
			});
		} else {
			$.occupation.width = 0;
			$.occupation.height = 0;
			$.occupation.hide({
				animated : OS_IOS
			});
		}

		if (userWork && userHasOccupation) {
			$.at.show({
				animated : OS_IOS
			});
			$.at.width = Ti.UI.SIZE;
			$.at.height = Ti.UI.SIZE;
		} else {
			$.at.hide({
				animated : OS_IOS
			});
			$.at.width = 0;
			$.at.height = 0;
		}
	} else {
		if (userWork || userHasOccupation) {
			$.occupation_at_work.show({
				animated : OS_IOS
			});
			$.occupation_at_work.height = Ti.UI.SIZE;
		} else {
			$.occupation_at_work.hide({
				animated : OS_IOS
			});
			$.occupation_at_work.height = 0;
		}
	}
};

var setVisibilityForFavoriteDrink = function() {
	var favoriteDrink = $.user.getFavoriteDrink();
	if (!_.isString(favoriteDrink) || _.isEmpty(favoriteDrink.trim())) {
		$.lbl_favorite_drink.hide({
			animated : OS_IOS
		});
		$.lbl_favorite_drink_value.hide({
			animated : OS_IOS
		});
		$.lbl_favorite_drink.height = 0;
		$.lbl_favorite_drink_value.height = 0;
	} else {
		$.lbl_favorite_drink.show({
			animated : OS_IOS
		});
		$.lbl_favorite_drink_value.show({
			animated : OS_IOS
		});
		$.lbl_favorite_drink.height = Ti.UI.SIZE;
		$.lbl_favorite_drink_value.height = Ti.UI.SIZE;
	}
};

/**
 * set visibility to view university
 * @param {Boolean} value
 */
var setVisibilityForUniversity = function(value) {
	if (value) {
		$.university.show({
			animated : OS_IOS
		});
		$.university.height = Ti.UI.SIZE;
	} else {
		$.university.hide({
			animated : OS_IOS
		});
		$.university.height = 0;
	}
};

/**
 * initialize views
 */
var initializeViews = function() {
	configurePublicUI();
	setVisibilityForWork();
	setVisibilityForUniversity($.user.hasEducation());
	setVisibilityForFavoriteDrink();

	if (OS_IOS) {
		var userWork = _.isString($.user.getWhereWork()) && !_.isEmpty($.user.getWhereWork());
		var userHasOccupation = _.isString($.user.getOccupation()) && !_.isEmpty($.user.getOccupation());

		var ocuppationAndWork = '';
		if (userHasOccupation) {
			ocuppationAndWork = $.user.getOccupation();
		}
		if (userWork) {
			if (userHasOccupation) {
				ocuppationAndWork = ocuppationAndWork.concat(' ').concat(L('space_at_space', 'at')).concat(' ').concat($.user.getWhereWork());
			} else {
				ocuppationAndWork = $.user.getWhereWork();
			}
		}

		$.occupation_at_work.text = ocuppationAndWork;

	}
	//interest
	var tagsByUser = Alloy.Globals.DaoBuilder.getUserTagDao().findTagsByUser($.user.getID());
	$.lbl_interests.visible = !(!_.isArray(tagsByUser) || _.isEmpty(tagsByUser));
	$.interests.applyProperties({
		tags : tagsByUser,
		collection : (Alloy.Globals.DaoBuilder.getTagDao()).getCollection()
	});
	$.interests.refreshTags();
	$.partial_user_profile.show({
		animated : OS_IOS
	});
};

/**
 * configure view for other users
 */
var configurePublicUI = function() {
	//if user not is logged change view
	if ($.user.get('id') != Alloy.Globals.UserID) {
		$.v_img_avatar.applyProperties({
			left : Alloy.Globals.Dimens.padding_horizontal
		});
		$.fab_pencil.applyProperties({
			visible : false
		});

		$.v_fab_public_user.show({
			animated : OS_IOS
		});

		var drinkVisible = false;
		if (_.isObject(userPresence)) {
			var establishment = Alloy.Globals.DaoBuilder.getEstablishmentDao().findByID(userPresence.getEstablishmentID());
			drinkVisible = establishment.getShowPriceList();

		}
		$.fab_drink.applyProperties({
			visible : drinkVisible
		});

	}
};

/**
 * When window resume
 * @param {Object} e
 */
var onResume = function(e) {
	if (OS_IOS || !firstTime) {
		$.user.fetch({
			id : $.user.getID()
		});
		_.defer(setData);
	}
	firstTime = false;
};

/**
 * open dialog with question to conversation
 */
var questionOpenNewConversation = function() {
	var dialog = Titanium.UI.createAlertDialog({
		message : L('conversation_question', 'Want to send a conversation request?'),
		buttonNames : [L('no', 'No'), L('yes', 'Yes')],
		cancel : 0
	});

	dialog.addEventListener('click', function(e) {
		switch(e.index) {
		case 1:
			sendRequestToNewConversation();
			break;
		}
	});

	dialog.show();
	//gc
	dialog = null;
};

/**
 * send a request to initialize new conversation
 */
var sendRequestToNewConversation = function() {
	showLoader();
	var toUserID = $.user.getID();
	Alloy.Globals.ServiceBuilder.getConversationService().inject(Alloy.Globals.DaoBuilder.getConversationDao()).sendNewRequest(toUserID, onSuccessNewConversation, onErrorNewConversation, httpCK);
};

/**
 * on success new conversation
 * @param {Object} response
 */
var onSuccessNewConversation = function(response) {
	hideLoader();
	if (!checkIfExistConversation(true)) {
		(require('TiUtils')).toast(L('send_request_conversation_success', 'Successfully sent request!'));
	}
};

/**
 * on error new conversation
 * @param {Object} error

 */
var onErrorNewConversation = function(error) {
	(require('TiLog')).error(TAG, 'onErrorNewConversation -> ' + JSON.stringify(error));
	var message;
	switch(error.code) {
	case Alloy.CFG.Http.ExistInvitationChat:
		message = L('error_conversation_invitation_exist', 'Chat request sent but not answered');
		break;
	default:
		message = L('error_conversation_send_invitation', 'An error occurred while trying to send your invitation');
		break;
	}
	(require('TiUtils')).toast(message);
	hideLoader();
};

/**
 * show loader to send request
 */
var showLoader = function() {
	loader = Alloy.createWidget('co.clarika.loader', {
		message : {
			text : L('sending_chat_request', 'Sending chat request...')
		}
	});
	loader.show();

};

/**
 * hide loader
 */
var hideLoader = function() {
	if (loader) {
		loader.hide();
	}
	loader = null;
};
/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * when window is opened
 */
var onOpen = function(e) {
	$.fab_pencil.onOpen(e);
	$.fab_drink.onOpen(e);
	$.fab_chat.onOpen(e);
	if (OS_IOS) {
		init(e);
	} else {
		_.defer(init, e);
	}
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
};

/**
 * redirect to edit profile
 */
var onEditProfile = _.debounce(function(e) {
	var editProfileController = Alloy.Globals.AppRoute.openEditProfile({
		userID : $.user.get('id')
	});
	if (OS_IOS) {
		editProfileController.on('ios:back', iosBack);
	}
	//gc
	editProfileController = null;
}, Alloy.Globals.Integers.debounce, true);

/**
 * send drink
 */
var onDrink = _.debounce(function(e) {
	//notify to analytics
	Alloy.Globals.AppRoute.sendTracking(Alloy.CFG.constants.analytics.send_drink);

	var gift = Alloy.Globals.DaoBuilder.getGiftDao().getModel();
	var userID = $.user.getID();

	gift.generateNewGift(Alloy.Globals.UserID, userID);

	var establishmentID = userPresence.getEstablishmentID();
	var subtitle = String.format(L('send_to', 'Send to %s'), $.user.getFirstName());

	var priceListController = Alloy.Globals.AppRoute.openPriceList({
		gift : gift,
		establishmentID : establishmentID,
		subtitle : subtitle
	});

	if (OS_IOS) {
		priceListController.on('ios:back', iosBack);
	}

	priceListController.on('gift:success', function() {
		(require('TiUtils')).toast(L('gift_send_successfully', 'The gift was successfully sent'));
		_.delay(function() {
			Ti.App.fireEvent('hide_loader');
		}, 500);

	});
	//gc
	priceListController = null;

}, Alloy.Globals.Integers.debounce, true);

/**
 * on chat user
 * @param {Object} e
 */
var onChat = _.debounce(function(e) {

	//notify to analytics
	Alloy.Globals.AppRoute.sendTracking(Alloy.CFG.constants.analytics.chat_request);

	if (!checkIfExistConversation()) {
		questionOpenNewConversation();
	}

}, Alloy.Globals.Integers.debounce, true);

var checkIfExistConversation = function(onlyIsAccepted) {
	var toUserID = $.user.getID();

	var conversation = Alloy.Globals.DaoBuilder.getConversationDao().findActiveConversationToUser(toUserID);
	if (_.isObject(conversation) && (!onlyIsAccepted || conversation.isAccepted())) {
		var title = conversation.transform().other_user_full_name;
		var conversationDetailController = Alloy.Globals.AppRoute.openConversationDetail({
			conversation : conversation,
			title : title
		});
		if (OS_IOS) {
			conversationDetailController.on('ios:back', iosBack);
		}
		//gc
		conversationDetailController = null;
		return true;
	} else {
		return false;
	}
};

var iosBack = function() {
	$.trigger('ios:back', {});
};

var showAvatarLoader = function() {
	if (OS_IOS) {
		$.loader_avatar.show();
	}
};

var hideAvatarLoader = function() {
	if (OS_IOS) {
		$.loader_avatar.hide();
	}
};
(function configureUI() {

	var headerSize = Alloy.Globals.Dimens.profile_header_avatar_size();
	var borderSize = Alloy.Globals.Dimens.profile_header_avatar_border_size();
	var borderRadiusSize = Alloy.Globals.Dimens.profile_header_avatar_border_border_radius();

	var backgroundHeight = Alloy.Globals.Dimens.profile_header_avatar_background_size();
	$.background_image.height = backgroundHeight;
	$.container_background.height = backgroundHeight;
	$.v_info.top = Alloy.Globals.Dimens.profile_header_avatar_size_half();
	$.v_info_content.top = Alloy.Globals.Dimens.profile_header_avatar_size_half();
	$.img_avatar.applyProperties({
		image : {
			height : headerSize,
			width : headerSize,
			borderRadius : Alloy.Globals.Dimens.profile_header_avatar_border_radius()
		},
		border : {
			height : borderSize,
			width : borderSize,
			borderRadius : borderRadiusSize,
			borderWidth : borderRadiusSize
		}
	});
})();

/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.onOpen = onOpen;
exports.onClose = onClose;
exports.cleanup = cleanup;
exports.onResume = onResume;
