/**
 * @author jagu
 *
 */
/** ------------------------
 Constants
 ------------------------**/
var INDEX_NIGHTCLUBS = 0;
var INDEX_BARS = 1;
/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var firstTimeOpenNightclubs = true;
var firstTimeOpenBars = true;
var currentPage = 0;
/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		if (_.has(properties, 'collectionBars')) {
			$.bars.applyProperties({
				collection : properties.collectionBars
			});
		}

		if (_.has(properties, 'collectionNightclubs')) {
			$.nightclubs.applyProperties({
				collection : properties.collectionNightclubs
			});
		}

		if (_.has(properties, 'currentPage')) {
			currentPage = properties.currentPage;
		}

		_.extend($.partial_establishment_map_mode, _.omit(properties, 'collectionBars', 'collectionNightclubs'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
	} catch(e) {
	}
};
/**
 * apply listeners to controller
 */
var applyListeners = function() {
	$.scrollable_view.addEventListener('scrollend', onScrollEnd);
};

/**
 * initialize controller
 */
var init = function() {
	Ti.App.fireEvent('closekeyboard');
	applyProperties(args);
	applyListeners();
	//gc
	args = null;
	$.tabs.setScrollableView($.scrollable_view);
	$.scrollable_view.setCurrentPage(currentPage);

	if (currentPage == INDEX_NIGHTCLUBS) {
		initializeNightclubs();
	} else {
		initializeBars();
	}

};

var initializeNightclubs = function() {
	firstTimeOpenNightclubs = false;
	$.nightclubs.onOpen({});
};

var initializeBars = function() {
	firstTimeOpenBars = false;
	$.bars.onOpen({});
};

var getCollectionBars = function() {
	$.bars.getCollection();
};

var getCollectionNightclubs = function() {
	$.nightclubs.getCollection();
};

var whenChangeTab = function() {
	switch(true) {
	case (firstTimeOpenNightclubs && currentPage == INDEX_NIGHTCLUBS):
		initializeNightclubs();
		break;
	case (firstTimeOpenBars && currentPage == INDEX_BARS):
		initializeBars();
		break;

	}
	$.trigger('scrollend', {
		currentPage : currentPage
	});
};

/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * when window is opened
 */
var onOpen = function(e) {
	if(OS_IOS){
		init(e);
	}else{
		_.defer(init, e);
	}
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
};

/**
 * when user select tab
 * @param {Object} e
 */
var onSelectTab = function(e) {
	currentPage = e.tab;
	if (OS_IOS) {
		whenChangeTab();
	}
};

/**
 * Fired when the view has stopped moving completely.
 * @param {Object} e
 */
var onScrollEnd = function(e) {
	currentPage = e.currentPage;
	whenChangeTab();
};

/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.onOpen = onOpen;
exports.onClose = onClose;
exports.cleanup = cleanup;
exports.getCollectionBars = getCollectionBars;
exports.getCollectionNightclubs = getCollectionNightclubs;
