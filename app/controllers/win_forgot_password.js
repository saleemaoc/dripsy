/** ------------------------
 Constants
 ------------------------**/
var TAG = 'controllers/win_forgot_password';
/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
//optional
if (OS_ANDROID) {
	var abx = require('com.alcoapps.actionbarextras');
}
/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
	$.forgot_password.on('success', onSuccess);
	$.forgot_password.on('error', onError);
	if (OS_ANDROID) {
		$.win_forgot_password.addEventListener('androidback', onBack);
	}
};

var onSuccess = function(e) {
	(require('TiUtils')).toast(L('send_mail_successful', 'Check your email. A new password was sent'));
	$.trigger('success', e);
	if (OS_IOS) {
		onBackIOS();
	} else {
		$.win_forgot_password.close();
	}
};

var onError = function() {
};

/**
 * initialize controller
 */
var init = function(e) {
	configureToolbar(e);
	applyProperties(args);
	//gc
	args = null;
	applyListeners();
};

var onBack = function() {
	$.win_forgot_password.close();
};
/**
 * configure toolbar
 */
var configureToolbar = function(e) {
	if (OS_ANDROID) {
		abx.setBackgroundColor(Alloy.Globals.Colors.action_bar_transparent);
		abx.setElevation(0);
		var activity = $.win_forgot_password.getActivity();
		activity.getActionBar().displayHomeAsUp = true;
		activity.getActionBar().onHomeIconItemSelected = onHomeIconItemSelected;
		//gc
		activity = null;
	}

};

if (OS_ANDROID) {
	/**
	 * on home icon item selected
	 */
	var onHomeIconItemSelected = _.debounce(function(e) {
		onBack()
	}, Alloy.Globals.Integers.debounce, true);
}

/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * when window is opened
 */
var onOpen = function(e) {
	if(OS_IOS){
		init(e);
	}else{
		_.defer(init, e);
	}
	$.forgot_password.onOpen(e);
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	$.forgot_password.onClose(e);
	cleanup();
};

/**
 * press ok button
 */
var onOk = function(e) {
	$.forgot_password.onOk();
};

var onBackIOS = function() {
	Alloy.Globals.AppRoute.closeNavigationWindowSecondary();
};
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.cleanup = cleanup;
