/**
 * @author jagu
 */

/** ------------------------
 Constants
 ------------------------**/
var TAG = 'controller/partial_users_presence';
/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var limit = 18;
var httpCK;
var type = null;
var users_length = 0;
var name = '';
//Refresh flags
var isPullToRefresh = false;
/**
 * flag to indicate state to loader
 */
var loading = false;
/**
 * flag indicate scrolling
 */
var isScrolling = false;
/**
 * gender type
 */
var gender = Alloy.CFG.constants.gender.male;
/**
 * time out onsearch user
 */
var timeOutOnSearch = null;
/**
 * to filter establishment
 */
var establishmentID = null;

var female = false;
/** ------------------------
 Dependencies
 ------------------------**/
/**
 * get service
 */
var getService = function() {
	return Alloy.Globals.ServiceBuilder.getUserPresenceService().inject(getDao());
};

/**
 * get dao
 */
var getDao = function() {
	return Alloy.Globals.DaoBuilder.getUserPresenceDao();
};
/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {
		if (_.has(properties, 'gender')) {
			gender = properties.gender;
		}

		if (_.has(properties, 'establishmentID')) {
			establishmentID = properties.establishmentID;
		}
		_.extend($.partial_users_presence, _.omit(properties, 'gender', 'establishmentID'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
		//$.is.cleanup();
		if (httpCK) {
			httpCK.abort();
		}
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
};

/**
 * initialize controller
 */
var init = function() {
	applyProperties(args);
	female = (gender == Alloy.CFG.constants.gender.female) ? true : false;
	applyListeners();
	//gc
	args = null;
	//initialize inifinite scroll
	$.is.init($.table);
	//create http instance
	httpCK = new (require('HttpCK'))();
	if (users_length == 0) {
		showLoader();
	}
	//get data
	_.delay(getData, Alloy.Globals.Integers.SetDataDelay);
};

/**
 * get params to get
 */
var getParamsToGet = function() {
	return {
		user__gender : gender,
		limit : limit,
		offset : users_length,
		show : Alloy.CFG.constants.true,
		establishment_id : establishmentID,
		name : name
	};
};

/**
 * get data
 */
var getData = function() {
	httpCK.abort();
	getService().get(getParamsToGet(), onSuccessGet, onErrorGet, httpCK);
};

/**
 * success get service
 * @param {Object} response
 */
var onSuccessGet = function(response) {
	(require('TiLog')).info(TAG, ' onSuccessGet ->' + JSON.stringify(response));
	hidePullToRefresh();
	addRows(response.results);
	showEmptyMessage();

};

/**
 * when service display an error
 * @param {String} error
 */
var onErrorGet = function(error) {
	(require('TiLog')).error(TAG + ' onErrorGet()', error);
	hidePullToRefresh();
	hideLoader();
	showEmptyMessage();
};

var getQuery = function() {
	return ' limit ' + limit + ' offset ' + users_length;
};

/**
 * update data in list
 */
var addRows = function(rowsData) {
	if (_.isArray(rowsData) && !_.isEmpty(rowsData)) {
		users_length += rowsData.length;
		var section = Titanium.UI.createTableViewSection();
		for (var i = 0,
		    j = rowsData.length; i < j; i = i + 3) {
			//params for user 1
			var user_1 = {
				visible : true,
			};
			//add params for user 1
			_.extend(user_1, rowsData[i]);
			//params for user 2
			var user_2 = {
			};
			//params for user 3
			var user_3 = {
			};

			var indexUser2 = i + 1;

			//iterate users
			if (indexUser2 < rowsData.length) {
				_.extend(user_2, rowsData[indexUser2]);
				user_2.visible = true;
				var indexUser3 = indexUser2 + 1;

				if (indexUser3 < rowsData.length) {
					_.extend(user_3, rowsData[indexUser3]);
					user_2.visible = true;
				}
			}
			//pass users to row
			section.add(Alloy.createController('tableview_row_user_presence', {
				user_1 : user_1,
				user_2 : user_2,
				user_3 : user_3,
				female : female
			}).getView());
		};
		//add section in table
		$.table.appendSection(section);
	}

	hideLoaderScrolling();
	hideLoader();
};

/**
 * show loader and hide list
 */
var showLoader = function() {
	if (!loading) {
		loading = true;
		$.ptr.hide();
		$.loader.show();
		$.table.hide({
			animated : OS_IOS
		});
		$.empty.hide();
	}
};

var hideLoader = function() {
	if (loading) {
		loading = false;
		$.loader.hide();
		$.table.show({
			animated : OS_IOS
		});
	}
};

var refresh = function() {
	$.ptr.refresh();
};

/**
 * hide pull to refresh is showing
 */
var hidePullToRefresh = function() {
	if (isPullToRefresh) {
		isPullToRefresh = false;
		$.ptr.hide();
	}
};
/**
 * detached infinite scrolling
 */
var removeScrolling = function() {
	isScrolling = false;
	$.is.state($.is.DONE, "");
};

var hideLoaderScrolling = function() {
	if (isScrolling) {
		isScrolling = false;
		$.is.state($.is.SUCCESS);
	}
};

var getCollection = function() {
	return collection;
};

/**
 * check if need show empty message
 */
var showEmptyMessage = function() {
	if (users_length > 0) {
		$.table.show();
		$.empty.hide();
	} else {
		$.table.hide();
		$.empty.show();
	}

};

var clearList = function() {
	$.table.setData([]);
	users_length = 0;
};
/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * when window is opened
 */
var onOpen = function(e) {
	if (OS_IOS) {
		init(e);
	} else {
		_.defer(init, e);
	}
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
};

/**
 * When user tap pull to refresh
 * @param {Object} e
 */
var onPullToRefresh = function(e) {

	if (!isPullToRefresh) {
		isPullToRefresh = true;
		clearList();
		showLoader();
		getData();
	}
};

/**
 * when user scrolling to end
 * @param {Object} e
 */
var onEndScrolling = function(e) {
	(require('TiLog')).info(TAG + ' onEndScrolling()', JSON.stringify(e));
	isScrolling = true;
	getData();
};

/**
 * @param {Object} e
 */
var onSearch = function(e) {
	if (name != e.source.value) {
		name = e.source.value;

		if (timeOutOnSearch != null) {
			clearTimeout(timeOutOnSearch);
			timeOutOnSearch = null;
		}
		showLoader();
		clearList();
		timeOutOnSearch = _.delay(getData, 200);
	}

};

/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.onOpen = onOpen;
exports.onClose = onClose;
exports.cleanup = cleanup;
exports.refresh = refresh;
exports.onSearch = onSearch;
