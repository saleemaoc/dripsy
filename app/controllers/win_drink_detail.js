/** ------------------------
 Constants
 ------------------------**/
var TAG = 'controllers/win_drink_detail';
/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
//optional

if (OS_ANDROID) {
	var abx = require('com.alcoapps.actionbarextras');
}

var drink = null;
/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {
		if (_.has(properties, 'drink')) {
			drink = properties.drink;
		}
		_.extend($.win_drink_detail, _.omit(properties, 'drink'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
	if (OS_ANDROID) {
		$.win_drink_detail.addEventListener('androidback', onBack);
	}
};

/**
 * initialize controller
 */
var init = function(e) {
	applyProperties(args);
	//gc
	args = null;
	applyListeners();
	configureToolbar(e);
	_.delay(setData, Alloy.Globals.Integers.SetDataDelay);
};

/**
 * configure toolbar
 */
var configureToolbar = function(e) {
	if (OS_ANDROID) {
		abx.setBackgroundColor(Alloy.Globals.Colors.action_bar_transparent);
		abx.setElevation(0);
		var activity = $.win_drink_detail.getActivity();
		activity.getActionBar().displayHomeAsUp = true;
		activity.getActionBar().onHomeIconItemSelected = onHomeIconItemSelected;
		//gc
		activity = null;
	}

};

var onBack = function() {
	$.win_drink_detail.close();
};

if (OS_ANDROID) {
	/**
	 * on home icon item selected
	 */
	var onHomeIconItemSelected = _.debounce(function(e) {
		onBack();
	}, Alloy.Globals.Integers.debounce, true);
}

var setData = function() {
	if (_.isObject(drink)) {
		$.drink.set(drink.attributes);
		drink = null;

		var imageDrink = $.drink.getImageMini();

		$.background_image.image = $.drink.getImage();
		$.background_image.defaultImage = Alloy.Globals.Images.big_default_drink;
		$.background_image.brokenLinkImage = Alloy.Globals.Images.big_default_drink;
		if (OS_ANDROID) {
			$.background_image.setImage($.drink.getImage());
			$.background_image.setDefaultImage(Alloy.Globals.Images.big_default_drink);
			$.background_image.setBrokenLinkImage(Alloy.Globals.Images.big_default_drink);
		}
		
		$.img_drink.setDefaultImage(Alloy.Globals.Images.big_default_drink);
		$.img_drink.setImage(imageDrink);

		$.lbl_description.setText($.drink.getDescription());
		$.lbl_name.setText($.drink.getName());
		$.lbl_price.setText($.drink.getCurrencyPrice());

		$.scroll.visible = true;
	}
};

(function configureUI() {
	var backgroundHeight = Alloy.Globals.Dimens.drink_detail_background_image_size();
	$.container_background.height = backgroundHeight;
	$.background_image.height = backgroundHeight;
})();
/** ------------------------
 Listeners
 ------------------------**/

/**
 * when window is opened
 */
var onOpen = function(e) {
	if (OS_IOS) {
		init(e);
	} else {
		_.defer(init, e);
	}
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
};

/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.cleanup = cleanup;
