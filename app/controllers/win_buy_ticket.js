/** ------------------------
 Constants
 ------------------------**/
var TAG = 'controllers/win_buy_ticket';
/** ------------------------
 Fields
 ------------------------**/
/**
 * Arguments passed into this controller can be accessed via the `$.args` object directly or:
 */
var args = $.args;
/**
 * subtitle for toolbar
 */
var subtitle = '';
/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {

	if (_.isObject(properties)) {

		if (_.has(properties, 'subtitle')) {
			subtitle = properties.subtitle;
		}

		if (_.has(properties, 'establishment')) {
			$.buy_ticket.applyProperties({
				establishment : properties.establishment
			});
		}

		if (_.has(properties, 'establishmentID')) {
			$.buy_ticket.applyProperties({
				establishmentID : properties.establishmentID
			});
		}

		_.extend($.win_buy_ticket, _.omit(properties, 'establishmentID', 'establishment', 'subtitle'));
	}

};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
	$.buy_ticket.on('buy:success', onSuccessBuy);
	if (OS_ANDROID) {
		$.win_buy_ticket.addEventListener('androidback', onBack);
	}
};

/**
 * initialize controller
 */
var init = function(e) {
	applyProperties(args);
	//gc
	args = null;
	applyListeners();
	configureToolbar(e);
};

/**
 * configure toolbar
 */
var configureToolbar = function(e) {
	if (OS_ANDROID) {
		var activity = $.win_buy_ticket.getActivity();
		activity.getActionBar().displayHomeAsUp = true;
		activity.getActionBar().onHomeIconItemSelected = onHomeIconItemSelected;
		activity.getActionBar().setSubtitle(subtitle);
		//gc
		activity = null;
	}

};

var onBack = function() {
	$.win_buy_ticket.close();
};

if (OS_ANDROID) {
	/**
	 * on home icon item selected
	 */
	var onHomeIconItemSelected = _.debounce(function(e) {
		onBack();
	}, Alloy.Globals.Integers.debounce, true);
}
/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * when window is opened
 */
var onOpen = function(e) {
	if(OS_IOS){
		init(e);
	}else{
		_.defer(init, e);
	}
	$.buy_ticket.onOpen(e);
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
	$.buy_ticket.onClose(e);

};

/**
 * success buy ticket
 * @param {Object} e
 */
var onSuccessBuy = function(e) {
	
	$.win_buy_ticket.close();
	
	Alloy.Globals.AppRoute.openTransactionDetail({
		transactionID : e.transactionID
	});

};
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.cleanup = cleanup;
