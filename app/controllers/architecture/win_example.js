/** ------------------------
 Constants
 ------------------------**/
var TAG = 'controllers/win_example';
/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
//optional
/*
* if(OS_ANDROID){
* 	var abx = require('com.alcoapps.actionbarextras');
* }
*
*/
/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {
		/*
		 * e.g
		 */

		/*
		 *
		 * if(_.has(properties, 'window')){
		 * 	_.extend($.window, properties.window)
		 * }
		 *
		 *
		 *
		 *
		 */
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	// let Alloy clean up listeners to global collections for data-binding
	// always call it since it'll just be empty if there are none
	$.destroy();
	// remove all event listeners on the controller
	$.off();
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
};

/**
 * initialize controller
 */
var init = function(e) {
	applyProperties(args);
	//gc
	args = null;
	applyListeners();
	//configureToolbar(e);
};

/**
 * configure toolbar
 */
var configureToolbar = function(e) {
	if (OS_ANDROID) {
		/*abx.setBackgroundColor(Alloy.Globals.Colors.action_bar_transparent);
		 abx.setElevation(0);
		 var activity = $.win_establishment_detail.getActivity();
		 activity.getActionBar().displayHomeAsUp = true;
		 activity.getActionBar().onHomeIconItemSelected = onHomeIconItemSelected;
		 //gc
		 activity = null;*/
	}

};

if (OS_ANDROID) {
	/**
	 * on home icon item selected
	 */
	var onHomeIconItemSelected = _.debounce(function(e) {
		$.win_drinks.close();
	}, Alloy.Globals.Integers.debounce, true);
}
/** ------------------------
 Listeners
 ------------------------**/

/**
 * when window is opened
 */
var onOpen = function(e) {
	init(e);
	$.examples.onOpen(e);
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	$.examples.onClose(e);
	cleanup();
};

/**
 * add example
 */
var onAdd = function(e) {

};

/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.cleanup = cleanup;
