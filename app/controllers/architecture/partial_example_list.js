/**
 * @author jagu
 */

/** ------------------------
 Constants
 ------------------------**/
var TAG = 'controller/architecture/partial_example_list';
/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var limit = Alloy.CFG.paggination_default || 20;
var collection = Alloy.Collections.example;
var httpCK;
//Refresh flags
var isPullToRefresh = false;
var isScrolling = false;

/** ------------------------
 Dependencies
 ------------------------**/

/**
 * return dependencie to service
 */
var getService = function() {
	return (require('services/ExampleService'));
};

/**
 * return dependencie to dao
 */
var getDao = function() {
	return new (require('daos/ExampleDao'))();
};

/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {
		/*
		 * e.g
		 */

		/*
		 *
		 * if(_.has(properties, 'view')){
		 * 	_.extend($.view, properties.view)
		 * }
		 *
		 *
		 *
		 *
		 */
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	// let Alloy clean up listeners to global collections for data-binding
	// always call it since it'll just be empty if there are none
	$.destroy();
	// remove all event listeners on the controller
	$.off();
	$.is.cleanup();
	if (httpCK) {
		httpCK.abort();
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
};

/**
 * initialize controller
 */
var init = function() {
	applyProperties(args);
	applyListeners();
	//gc
	args = null;
	//initialize inifinite scroll
	$.is.init($.list);
	//create http instance
	httpCK = new (require('HttpCK'))();
	//get data
	_.defer(getData);
};

/**
 * get params to get
 */
var getParamsToGet = function() {
	return {};
};

/**
 * get data
 */
var getData = function() {
	(require('TiLog')).info(TAG, ' getData()');
	if (collection.models.length == 0) {
		showLoader();
	}

	getService().get(getParamsToGet(), onSuccessGet, onErrorGet, httpCK);
};

/**
 * success get service
 * @param {Object} response
 */
var onSuccessGet = function(response) {
	(require('TiLog')).info(TAG, ' onSuccessGet()');

	hidePullToRefresh();
	if (collection.models.length == 0) {
		hideLoader();
	}
	updateData();
};

/**
 * when service display an error
 * @param {String} error
 */
var onErrorGet = function(error) {
	(require('TiLog')).error(TAG + ' onErrorGet()', error);
	hidePullToRefresh();

};

/**
 * get query to fetch
 */
var getQuery = function() {
	return 'limit ' + limit + ' offset ' + collection.models.length;
};

/**
 * update data in list
 */
var updateData = function() {
	(require('TiLog')).info(TAG, ' updateData()');
	getDao().find(getQuery(), true, onSuccessData, onErrorData, collection);
};

/**
 * @param {Object} e
 */
var onSuccessData = function(e) {
	(require('TiLog')).info(TAG + ' onSuccessData()', JSON.stringify(e));
	hideLoaderScrolling();
};

/**
 * @param {Object} e
 */
var onErrorData = function(e) {
	(require('TiLog')).error(TAG + ' onErrorData()', JSON.stringify(e));
	hideLoaderScrolling();
};

/**
 * show loader and hide list
 */
var showLoader = function() {
	$.loader.show();
	$.list.visible = false;
};

var hideLoader = function() {
	$.loader.hide();
	$.list.show();
};

var refresh = function() {
	$.ptr.refresh();
};

/**
 * hide pull to refresh is showing
 */
var hidePullToRefresh = function() {
	if (isPullToRefresh) {
		isPullToRefresh = false;
		$.ptr.hide();
	}
};

var hideLoaderScrolling = function() {
	if (isScrolling) {
		isScrolling = false;
		$.is.state($.is.SUCCESS);
	}
};
/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * when window is opened
 */
var onOpen = function(e) {
	_.defer(init);
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
};

/**
 * When user tap pull to refresh
 * @param {Object} e
 */
var onPullToRefresh = function(e) {
	(require('TiLog')).info(TAG + ' pullToRefresh()', JSON.stringify(e));
	isPullToRefresh = true;
	getData();
};

/**
 * when user scrolling to end
 * @param {Object} e
 */
var onEndScrolling = function(e) {
	(require('TiLog')).info(TAG + ' onEndScrolling()', JSON.stringify(e));
	isScrolling = true;
	updateData();
};

/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.onOpen = onOpen;
exports.onClose = onClose;
exports.cleanup = cleanup;
exports.refresh = refresh;
