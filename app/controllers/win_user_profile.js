/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
if (OS_ANDROID) {
	var abx = require('com.alcoapps.actionbarextras');
}
/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {

	if (_.isObject(properties)) {

		if (_.has(properties, 'user')) {
			//pass user
			$.profile.applyProperties({
				user : properties.user
			});
		}

		if (_.has(properties, 'userID')) {
			//pass user
			$.profile.applyProperties({
				userID : properties.userID
			});
		}

		if (_.has(properties, 'userPresence')) {
			//pass user presence
			$.profile.applyProperties({
				userPresence : properties.userPresence
			});
		}

		_.extend($.win_user_profile, _.omit(properties, 'user', 'userPresence', 'userID'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
	if (OS_ANDROID) {
		$.win_user_profile.addEventListener('androidback', onBack);
	}
};

var beforeOpen = function() {
	if (OS_ANDROID) {
		$.win_user_profile.getActivity().onResume = onResume;
	} else {
		$.profile.on('ios:back', onResume);
	}
};

/**
 * initialize controller
 */
var init = function(e) {
	configureToolbar(e);
	applyProperties(args);
	applyListeners();
	//gc
	args = null;
};

var onBack = function() {
	$.win_user_profile.close();
};
/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * when window is opened
 */
var onOpen = function(e) {
	if(OS_IOS){
		init(e);
	}else{
		_.defer(init, e);
	}
	$.profile.onOpen(e);
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	$.profile.onClose(e);
	if (OS_IOS) {
		$.trigger('ios:back', {});
	}
	cleanup();
};

/**
 * configure toolbar
 */
var configureToolbar = function(e) {
	if (OS_ANDROID) {
		abx.setBackgroundColor(Alloy.Globals.Colors.action_bar_transparent);
		abx.setElevation(0);
		var activity = $.win_user_profile.getActivity();
		activity.getActionBar().displayHomeAsUp = true;
		activity.getActionBar().onHomeIconItemSelected = onHomeIconItemSelected;
		//gc
		activity = null;
	}

};

if (OS_ANDROID) {
	/**
	 * on home icon item selected
	 */
	var onHomeIconItemSelected = _.debounce(function(e) {
		onBack();
	}, Alloy.Globals.Integers.debounce, true);
}

/**
 * on resume window
 * @param {Object} e
 */
var onResume = function(e) {
	$.profile.onResume(e);
};

beforeOpen();
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.cleanup = cleanup;
