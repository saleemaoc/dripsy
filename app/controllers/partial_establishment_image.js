/**
 * @author jagu
 * databinding
 */

/** ------------------------
 Fields
 ------------------------**/

/**
 * Arguments passed into this controller can be accessed via the `$.args` object directly or:
 */
var args = $.args;

var showLoader = function() {
	$.loader.show();
	$.img.visible = false;
};

var hideLoader = function() {
	$.loader.hide();
	$.v_error.visible = false;
	$.img.show();
};

var showError = function() {
	hideLoader();
	$.v_error.visible = true;
};

var tryAgain = function() {
	showLoader();
	if (_.isString($.img.image)) {
		var date = new Date();
		var newUrl = $.img.image.concat('?time').concat(date.getTime());
		$.img.image = newUrl;
	}

};

(function applyListeners() {
	$.v_error.addEventListener('click', tryAgain);
})();
if (Ti.Network.online) {
	showLoader();
} else {
	showError();
}
