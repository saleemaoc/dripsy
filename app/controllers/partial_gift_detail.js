/**
 * @author jagu
 *
 */
/** ------------------------
 Constants
 ------------------------**/
var TAG = 'controllers/partial_gift_detail';
/** ------------------------
 Fields
 ------------------------**/
/**
 * Arguments passed into this controller can be accessed via the `$.args` object directly or:
 */
var args = $.args;
/**
 * gift to show
 */
var gift = null;

var giftID = null;
/**
 * http library
 */
var httpCK = null;

/** ------------------------
 Dependencies
 ------------------------**/

/**
 * get service dependency
 * @return {Service}
 */
var getService = function() {
	return Alloy.Globals.ServiceBuilder.getGiftService().inject(getDao());
};
/**
 * get dao dependency
 * @return {Dao}
 */
var getDao = function() {
	return Alloy.Globals.DaoBuilder.getGiftDao();
};

/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		if (_.has(properties, 'gift')) {
			gift = properties.gift;

			$.transaction_detail_list.applyProperties({
				transactionID : gift.getTransactionID()
			});
		}

		if (_.has(properties, 'giftID')) {
			giftID = properties.giftID;
		}

		_.extend($.partial_gift_detail, _.omit(properties, 'gift', 'giftID'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
		if (httpCK) {
			httpCK.abort();
		}
	} catch(e) {
	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {
	$.v_from_user_detail.addEventListener('click', onInfo);
	$.btn_accept.addEventListener('click', onAccept);
	$.btn_reject.addEventListener('click', onReject);
	$.fab_chat.on('click', onChat);
};

/**
 * initialize controller
 */
var init = function() {
	applyProperties(args);
	//gc
	args = null;
	applyListeners();
	//create http instance
	httpCK = new (require('HttpCK'))();
	showLoader();
	//get data
	updateData();
};

/**
 * find data if transaction is null
 */
var updateData = function() {
	if (_.isObject(gift)) {
		updateUI();
	} else {
		getGiftById();
	}

};

/**
 * update UI components
 */
var updateUI = function() {
	if (_.isObject(gift)) {
		$.gift.set(gift.attributes);
		gift = null;
	}
	$.image.applyProperties({
		image : {
			image : $.gift.getFromUserPhoto(),
			defaultImage : $.gift.getFromUserDefaultPhoto()
		}
	});
	$.image.refresh();

	if ($.gift.isAccept()) {
		showQR();
	} else if ($.gift.isRejected() || $.gift.isDefeated()) {
		removeButtons();
		hideCode();
	}
	hideLoader();
};

var getGiftById = function() {
	getService().getById(giftID, onSuccessGiftById, onErrorGiftById, httpCK);
};

var onSuccessGiftById = function(response) {
	gift = response;
	updateUI();
};

var onErrorGiftById = function(error) {
	Alloy.Globals.TiUtils.toast(L('error_get_gift_by_id', 'No se encontro el regalo.'));
	$.trigger('error:gift');
};
/**
 * show loader and hide page
 * @param {String} message
 */
var showLoader = function(message) {
	if (_.isString(message)) {
		$.loader.setMessage(message);
	}
	$.loader.show();
	$.page.hide();
};

/**
 * hide loader and show page
 */
var hideLoader = function() {
	//clean message
	$.loader.setMessage('');
	$.loader.hide();
	$.page.show();
};

/**
 * success accept gift
 * @param {Object} response
 */
var onSuccessAccept = function(response) {
	(require('TiLog')).info(TAG, 'onSuccessAccept -> ' + JSON.stringify(response));
	$.trigger('gift:accept_success', {});
	showQR();
	hideLoader();
};
/**
 * error accept gift
 * @param {Object} error
 */
var onErrorAccept = function(error) {
	(require('TiLog')).error(TAG, 'onErrorAccept -> ' + JSON.stringify(error));
	(require('TiUtils')).toast(L('error_accepting_gift', 'An error occurred while accepting the gift, try again'));
	hideLoader();
};

/**
 * success reject gift
 * @param {Object} response
 */
var onSuccessReject = function(response) {
	(require('TiLog')).info(TAG, 'onSuccessReject -> ' + JSON.stringify(response));
	hideLoader();
	removeButtons();
	hideCode();
};

/**
 * error reject gift
 * @param {Object} error
 */
var onErrorReject = function(error) {
	(require('TiLog')).error(TAG, 'onErrorReject -> ' + JSON.stringify(error));
	(require('TiUtils')).toast(L('error_rejecting_gift', 'An error occurred while rejecting the gift, try again'));
	hideLoader();
};

/**
 * show QR code
 */
var showQR = function() {
	removeButtons();
	var transaction = $.gift.getTransaction();
	if (transaction.isPending(true)) {
		//show qr
		$.v_qr.height = Alloy.Globals.Dimens.transaction_qr_content_size;
		$.v_qr.top = Alloy.Globals.Dimens.padding_top_double;
		$.v_qr.add((require('JsUtils')).createQRCodeImageView(transaction.getTransactionCode(), Alloy.Globals.Dimens.transaction_qr_generator, onErrorGenerateQR));
		$.v_qr.show({
			animated : OS_IOS
		});
		showCode();
	} else {
		hideCode();
	}
	//gc
	transaction = null;
};

var showCode = function() {
	$.v_transaction_code.show({
		animated : OS_IOS
	});
	$.lbl_code_qr_help.show();
	$.v_transaction_code.height = '24dp';
};
var hideCode = function() {

	$.v_transaction_code.hide({
		animated : OS_IOS
	});
	$.v_transaction_code.height = 0;
};
/**
 *  on error generate QR
 * @param {Object} e
 */
var onErrorGenerateQR = function(e) {

};

/**
 * remove buttons accept and reject
 */
var removeButtons = function() {
	$.page.remove($.v_buttons);
};
/** ------------------------
 Listeners
 ------------------------**/

/**
 * when window is opened
 * @param {Object} e
 */
var onOpen = function(e) {
	if (OS_IOS) {
		init(e);
	} else {
		_.defer(init, e);
	}
	$.fab_chat.onOpen(e);
	$.transaction_detail_list.onOpen(e);
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
	$.transaction_detail_list.onClose(e);
};

/**
 * on information
 * @param {Object} e
 */
var onInfo = _.debounce(function(e) {

	var fromUserID = $.gift.getFromUserID();
	Alloy.Globals.AppRoute.openUserProfile({
		userID : fromUserID
	});

}, Alloy.Globals.Integers.debounce, true);

/**
 * When user accept gift
 * @param {Object} e
 */
var onAccept = _.debounce(function(e) {

	showLoader(L('accepting_gift', 'Accepting gift...'));

	var giftID = $.gift.getID();
	//change status
	$.gift.setStatus(Alloy.CFG.constants.gift_status.accepted);
	//get params to send
	var params = $.gift.toService(null, true);
	//send request
	getService().update(giftID, params, onSuccessAccept, onErrorAccept, httpCK);

}, Alloy.Globals.Integers.debounce, true);

/**
 * When user reject gift
 * @param {Object} e
 */
var onReject = _.debounce(function(e) {

	showLoader(L('rejecting_gift', 'Rejecting gift...'));
	var giftID = $.gift.getID();
	//change status
	$.gift.setStatus(Alloy.CFG.constants.gift_status.rejected);
	//get params to send
	var params = $.gift.toService(null, true);
	//send request
	getService().update(giftID, params, onSuccessReject, onErrorReject, httpCK);

}, Alloy.Globals.Integers.debounce, true);

/**
 * on chat user
 * @param {Object} e
 */
var onChat = _.debounce(function(e) {

	//notify to analytics
	Alloy.Globals.AppRoute.sendTracking(Alloy.CFG.constants.analytics.chat_request);

	if (!checkIfExistConversation()) {
		questionOpenNewConversation();
	}

}, Alloy.Globals.Integers.debounce, true);

var checkIfExistConversation = function(onlyIsAccepted) {
	var toUserID = $.gift.getFromUserID();

	var conversation = Alloy.Globals.DaoBuilder.getConversationDao().findActiveConversationToUser(toUserID);

	if (_.isObject(conversation) && (!onlyIsAccepted || conversation.isAccepted())) {
		var title = conversation.transform().other_user_full_name;
		Alloy.Globals.AppRoute.openConversationDetail({
			conversation : conversation,
			title : title
		});
		return true;
	} else {
		return false;
	}
};

/**
 * open dialog with question to conversation
 */
var questionOpenNewConversation = function() {
	var dialog = Titanium.UI.createAlertDialog({
		message : L('conversation_question', 'Want to send a conversation request?'),
		buttonNames : [L('no', 'No'), L('yes', 'Yes')],
		cancel : 0
	});

	dialog.addEventListener('click', function(e) {
		switch(e.index) {
		case 1:
			sendRequestToNewConversation();
			break;
		}
	});

	dialog.show();
	//gc
	dialog = null;
};

/**
 * send a request to initialize new conversation
 */
var sendRequestToNewConversation = function() {
	showLoader(L('sending_invitation', 'Sending invitation...'));
	var toUserID = $.gift.getFromUserID();
	Alloy.Globals.ServiceBuilder.getConversationService().inject(Alloy.Globals.DaoBuilder.getConversationDao()).sendNewRequest(toUserID, onSuccessNewConversation, onErrorNewConversation, httpCK);
};

/**
 * on success new conversation
 * @param {Object} response
 */
var onSuccessNewConversation = function(response) {
	hideLoader();

	if (!checkIfExistConversation(true)) {
		(require('TiUtils')).toast(L('send_request_conversation_success', 'Successfully sent request!'));
	}
};

/**
 * on error new conversation
 * @param {Object} error

 */
var onErrorNewConversation = function(error) {
	(require('TiLog')).error(TAG, 'onErrorNewConversation -> ' + JSON.stringify(error));
	var message;
	switch(error.code) {
	case Alloy.CFG.Http.ExistInvitationChat:
		message = L('error_conversation_invitation_exist', 'Chat request sent but not answered');
		break;
	default:
		message = L('error_conversation_send_invitation', 'An error occurred while trying to send your invitation');
		break;
	}
	(require('TiUtils')).toast(message);
	hideLoader();
};
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.onOpen = onOpen;
exports.onClose = onClose;
exports.cleanup = cleanup;
