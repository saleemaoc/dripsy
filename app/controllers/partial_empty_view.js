/**
 * @author jagu
 *
 */

/** ------------------------
 Fields
 ------------------------**/

/**
 * Arguments passed into this controller can be accessed via the `$.args` object directly or:
 */
var args = $.args;

/** ------------------------
 Methods
 ------------------------**/

/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {

	if (_.isObject(properties)) {

		//extend properties to image
		if (_.has(properties, 'content')) {
			_.extend($.v_content, properties.content);
		}

		//extend properties to label
		if (_.has(properties, 'label')) {
			_.extend($.lbl_empty, properties.label);
		}

		//extend properties to label
		if (_.has(properties, 'description')) {
			_.extend($.lbl_description, properties.description);
		}

		//extend properties to image
		if (_.has(properties, 'image')) {
			_.extend($.img_empty, properties.image);
		}

		//extend properties to main view
		_.extend($.partial_empty_view, _.omit(properties, 'label', 'image', 'content', 'description'));
	}
};

/**
 * show empty view
 */
var show = function() {
	$.partial_empty_view.show({
		animated : OS_IOS
	});
};

/**
 * hide empty view
 */
var hide = function() {
	$.partial_empty_view.hide({
		animated : OS_IOS
	});
};

/**
 * initialize controller
 */
var init = function() {
	applyProperties(args);
	//gc
	args = null;
};

//auto initialize
init({});
/** ------------------------
 Public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.show = show;
exports.hide = hide;
