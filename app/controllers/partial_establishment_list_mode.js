/**
 * @author jagu
 *
 */
/** ------------------------
 Constants
 ------------------------**/
var INDEX_NIGHTCLUBS = 0;
var INDEX_BARS = 1;
/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var firstTimeOpenNightclubs = true;
var firstTimeOpenBars = true;
var currentPage = 0;
var name = '';
var needRefresh = false;
/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {

	if (_.isObject(properties)) {
		if (_.has(properties, 'collectionBars')) {
			$.bars.applyProperties({
				collection : properties.collectionBars,
				name : name
			});
		}

		if (_.has(properties, 'collectionNightclubs')) {
			$.nightclubs.applyProperties({
				collection : properties.collectionNightclubs,
				name : name
			});
		}

		if (_.has(properties, 'currentPage')) {
			currentPage = properties.currentPage;
		}
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
	} catch(e) {
	}
};
/**
 * apply listeners to controller
 */
var applyListeners = function() {
	$.scrollable_view.addEventListener('scrollend', onScrollEnd);
};

/**
 * initialize controller
 */
var init = function() {
	Ti.App.fireEvent('closekeyboard');
	applyProperties(args);
	applyListeners();
	//gc
	args = null;
	$.tabs.setScrollableView($.scrollable_view);
	$.scrollable_view.setCurrentPage(currentPage);
	if (currentPage == INDEX_NIGHTCLUBS) {
		initializeNightclubs();
	} else {
		initializeBars();
	}
};

var initializeNightclubs = function() {
	firstTimeOpenNightclubs = false;
	$.nightclubs.onOpen({
		autoSearch : false
	});
};

var initializeBars = function() {
	firstTimeOpenBars = false;
	$.bars.onOpen({
		autoSearch : false
	});
};

var whenChangeTab = function() {
	switch(true) {
	case (firstTimeOpenNightclubs && currentPage == INDEX_NIGHTCLUBS):
		initializeNightclubs();
		break;
	case (firstTimeOpenBars && currentPage == INDEX_BARS):
		initializeBars();
		break;
	default:
		checkIfQueryChangeAndNotify();
		break;

	}
	$.trigger('scrollend', {
		currentPage : currentPage
	});
};

/** ------------------------
 Listeners
 ------------------------**/

/**
 * when window is opened
 */
var onOpen = function(e) {
	if(OS_IOS){
		init(e);
	}else{
		_.defer(init, e);
	}
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
};

/**
 * when user select tab
 * @param {Object} e
 */
var onSelectTab = function(e) {
	currentPage = e.tab;
	if (OS_IOS) {
		whenChangeTab();
	}

};

/**
 * Fired when the view has stopped moving completely.
 * @param {Object} e
 */
var onScrollEnd = function(e) {
	currentPage = e.currentPage;
	whenChangeTab();
};

var getCollectionBars = function() {
	return $.bars.getCollection();
};

var getCollectionNightclubs = function() {
	return $.nightclubs.getCollection();
};

/**
 * on search
 * @param {Object} e
 */
var onSearch = function(e) {
	if (name != e.source.value) {
		needRefresh = true;
		name = e.source.value;
	}

	switch(currentPage) {
	case INDEX_NIGHTCLUBS:
		$.nightclubs.onSearch(e);
		break;
	case INDEX_BARS:
		$.bars.onSearch(e);
		break;
	}
};

/**
 * check if user write new query in other tab and notify the current tab
 */
var checkIfQueryChangeAndNotify = function() {

	if (needRefresh) {
		switch(currentPage) {
		case INDEX_NIGHTCLUBS:
			$.nightclubs.fireSearch(name);
			break;
		case INDEX_BARS:
			$.bars.fireSearch(name);
			break;
		}
	}
	needRefresh = false;
};

/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.onOpen = onOpen;
exports.onClose = onClose;
exports.cleanup = cleanup;
exports.getCollectionBars = getCollectionBars;
exports.getCollectionNightclubs = getCollectionNightclubs;
exports.onSearch = onSearch;
