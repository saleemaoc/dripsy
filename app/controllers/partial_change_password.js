/**
 * @author jagu
 *
 */
/** -----------------------------
 * Constants
 ------------------------------**/
var TAG = 'controllers/partial_change_password';
/** ------------------------
 Fields
 ------------------------**/
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var HttpCK;
/** ------------------------
 Dependencies
 ------------------------**/
/**
 * return user dependencie to service
 * @return {UserService}
 */
var getService = function() {
	return Alloy.Globals.ServiceBuilder.getUserService();
};
/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {
		_.extend($.partial_change_password, _.omit(properties));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();
		if (HttpCK) {
			HttpCK.abort();
		}
	} catch(e) {
	}
};
/**
 * apply listeners to controller
 */
var applyListeners = function() {
	$.btn_ok.addEventListener('click', onChangePassword);
	$.v_change_password_forgot.addEventListener('click', onForgotPassword);
};

/**
 * initialize controller
 */
var init = function() {
	applyProperties(args);
	applyListeners();
	//gc
	args = null;
	HttpCK = new (require('HttpCK'))();
};

/** ------------------------
 Listeners
 ------------------------**/
/**
 *
 */

/**
 * chnage password data
 */
var onChangePassword = _.debounce(function(e) {

	var current_password = $.txt_change_password_current.getValue();
	var new_password = $.txt_change_password_new.getValue();
	var repeat_password = $.txt_change_password_new_again.getValue();

	if (validateFields(current_password, new_password, repeat_password)) {
		var params = {
			current_password : current_password,
			new_password : new_password,
			repeat_password : repeat_password
		};
		showLoader();
		getService().changePassword(params, onSuccess, onError, HttpCK);
	}

}, Alloy.Globals.Integers.debounce, true);
/**
 * @param {String} current_password
 * @param {String} new_password
 * @param {String} repeat_password
 */
var validateFields = function(current_password, new_password, repeat_password) {
	if (!_.isString(current_password) || _.isEmpty(current_password.trim())) {
		(require('TiUtils')).toast(L('current_password_empty', 'You must type your current password'));
		return false;
	}
	if (!_.isString(new_password) || _.isEmpty(new_password.trim())) {
		(require('TiUtils')).toast(L('confim_password_required', 'Confirm password is required'));
		return false;
	}
	if (new_password.trim().length < Alloy.CFG.validate.user_password_length) {
		(require('TiUtils')).toast(String.format(L('password_lenght_new', 'The new password must have more than %s character'), Alloy.CFG.validate.user_password_length + ""));
		return false;
	}
	if (current_password == new_password) {
		(require('TiUtils')).toast(L('current_password_equal_new_password', 'The new password must be different from the current password'));
		return false;
	}
	if (repeat_password != new_password) {
		(require('TiUtils')).toast(L('confirm_password_not_equal', "Both passwords don't match. Please try again."));
		return false;
	}
	return true;
};
/**
 *
 */
var onSuccess = function(response) {
	hideLoader();
	$.trigger('success', {});
};

/**
 *
 */
var onError = function(error) {
	hideLoader();
	var message = "";
	switch(error.code) {
	case Alloy.CFG.Http.BadRequest:
		message = L('current_password_incorrect', 'The current password is incorrect');
		break;
	}

	(require('TiUtils')).toast(message);
};

/**
 * show loader and hide page
 */
var showLoader = function() {
	$.loader.show();
};

/**
 * hide loader and showpage
 */
var hideLoader = function() {
	$.loader.hide();
};

/**
 * when window is opened
 */
var onOpen = function(e) {
	if(OS_IOS){
		init(e);
	}else{
		_.defer(init, e);
	}
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
};

/**
 * on forgot password
 * @param {Object} e
 */
var onForgotPassword = _.debounce(function(e) {

	Alloy.Globals.AppRoute.openForgotPassword({});

}, Alloy.Globals.Integers.debounce, true);

/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.onOpen = onOpen;
exports.onClose = onClose;
exports.cleanup = cleanup;
exports.onChangePassword = onChangePassword;
