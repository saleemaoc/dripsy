/**
 * @author jagu
 *
 */
/** ------------------------
 Constants
 ------------------------**/
var TAG = 'controllers/partial_buy_ticket';
/**
 * value to update quantity
 *
 */
var DELAY_UPDATE_QUANTITY = 250;
/**
 * value to update tip amount
 *
 */
var DELAY_UPDATE_TIP_AMOUNT = 250;

/** ------------------------
 Fields
 ------------------------**/
/**
 * Arguments passed into this controller can be accessed via the `$.args` object directly or:
 */
var args = $.args;
/**
 * http library
 */
var httpCK;

/**
 * to filter concepts
 */
var date = new Date();
/**
 * to filter establishment
 */
var establishmentID = null;
/**
 * concept collection
 */
var collection = null;
/**
 * store current concept
 */
var concept = null;
/**
 * current quantity
 */
var currentQuantity = 0;

var transaction = null;
/**
 * transaction detail to store buyer concept
 */
var transactionDetail = null;
/**
 * tip amount & tax amount
 */
var tipAmount = 0;
var taxAmount = 0;
/**
 * timeout to update quantity
 */
var timeOutUpdateQuantity = null;
/**
 * timeout to update tip amount
 */
var timeOutUpdateTipAmount = null;

var isStripe = false;
var paymentOnline;

var maxDateEvent = new Date();

/** ------------------------
 Dependencies
 ------------------------**/
/**
 * get service dependency
 * @return {Service}
 */
var getService = function() {
	return Alloy.Globals.ServiceBuilder.getConceptService().inject(getDao());
};

/**
 * get dao dependency
 * @return {Service}
 */
var getDao = function() {
	return Alloy.Globals.DaoBuilder.getConceptDao();
};

/**
 * get transaction service
 * @return {Service}
 */
var getTransactionService = function() {
	return Alloy.Globals.ServiceBuilder.getTransactionService().inject(Alloy.Globals.DaoBuilder.getTransactionDao());
};
/**
 * get transaction detail dao
 * @return {Dao}
 */
var getTransactionDetailDao = function() {
	return Alloy.Globals.DaoBuilder.getTransactionDetailDao();
};
/** ------------------------
 Methods
 ------------------------**/
/**
 * apply properties to controller
 * @param {Object} properties
 */
var applyProperties = function(properties) {
	if (_.isObject(properties)) {

		if (_.has(properties, 'establishmentID')) {
			establishmentID = properties.establishmentID;
		}
		_.extend($.partial_buy_ticket, _.omit(properties, 'establishmentID'));
	}
};

/**
 * http://www.tidev.io/2014/09/18/cleaning-up-alloy-controllers/
 */
var cleanup = function() {
	try {
		// let Alloy clean up listeners to global collections for data-binding
		// always call it since it'll just be empty if there are none
		$.destroy();
		// remove all event listeners on the controller
		$.off();

	} catch(e) {

	}
};

/**
 * apply listeners to controller
 */
var applyListeners = function() {

	$.picker_ticket.on('change', onChangePickerTicketTable);
	$.picker_ticket.on('click', hideKeyboard);
	$.datepicker_date.on('accept', onChangeDate);
	$.datepicker_date.on('click', hideKeyboard);
	$.txt_quantity.addEventListener('change', onChangeQuantity);
	$.txt_tip_amount.addEventListener('change', onChangeTipAmount);
	$.btn_buy.addEventListener('click', onBuy);

};

/**
 * initialize controller
 */
var init = function() {
	applyProperties(args);
	//gc
	args = null;
	applyListeners();
	showLoader();
	_.delay(getData, Alloy.Globals.Integers.SetDataDelay);
	initUI();
	initializeTransactionDetail();
};

/**
 * initialize UI components
 */
var initUI = function() {
	configureMaxDate();
	$.datepicker_date.applyProperties({
		picker : {
			minDate : new Date(),
			maxDate : maxDateEvent
		},
		valueWithoutRefreshLabel : new Date()
	});
};

var configureMaxDate = function() {
	maxDateEvent.setFullYear(date.getFullYear() + 1);
};
/**
 * get params to concepts
 * @return {Object}
 */
var getParams = function() {
	return {
		date : (require('FormatUtils')).formatDateToDateFormatService(date),
		establishment_id : establishmentID
	};
};

/**
 * get concepts
 */
var getData = function() {
	getService().get(getParams(), onSuccessGetConcept, onErrorGetConcept, httpCK);
};

/**
 * success get concept
 * @param {Object} response
 */
var onSuccessGetConcept = function(response) {
	(require('TiLog')).info(TAG, 'onSuccessGetConcept -> ' + JSON.stringify(response));
	updateData();
};

/**
 * error get concept
 * @param {Object} error
 */
var onErrorGetConcept = function(error) {
	(require('TiLog')).error(TAG, 'onErrorGetConcept -> ' + JSON.stringify(error));
	hideLoader();
	resetUI();
};

/**
 * update data to collection
 */
var updateData = function() {
	collection = getDao().find();
	if (_.isEmpty(collection.models)) {

		(require('TiUtils')).toast((L('no_events_available_for_the_selected_date', 'No events available for the selected date')));
	}
	configureUI(collection);
};

/**
 * configure UI
 *@param {Object} concepts
 */
var configureUI = function(concepts) {
	$.picker_ticket.applyProperties({
		options : $.picker_ticket.formatDataToPicker(concepts, 'id', 'description_price', true, L('choose_an_option', 'Choose an option...')),
		selected : -1
	});

	hideLoader();
};

/**
 * show loader and hide page
 */
var showLoader = function() {
	$.loader.show();
	$.page.hide({
		animated : OS_IOS
	});
};

/**
 * hide loader and show page
 */
var hideLoader = function() {
	$.loader.hide();
	$.page.show({
		animated : OS_IOS
	});
};

/**
 * initialize new transaction detail
 */
var initializeTransactionDetail = function() {
	transactionDetail = getTransactionDetailDao().getModel();
	transactionDetail.generate();
};

/**
 * update quantity to transaction detail
 */
var updateQuantity = function() {
	transactionDetail.setQuantity(currentQuantity);
	updateButtonUI();
};
/**
 * set style button
 */
var updateButtonUI = function() {
	var quantity = transactionDetail.getQuantity();
	var titleButton = L('buy', 'Buy');

	if ((quantity > 0 && _.isObject(concept)) || tipAmount > 0) {
		var subtotal = tipAmount;
		if (_.isObject(transactionDetail) && transactionDetail.getSubtotal() > 0) {
			var _totalAmount = parseFloat(0);
			_totalAmount = transactionDetail.getSubtotal();

			// apply tax information
			if (concept.getDescription().toLowerCase() == "table") {
				taxAmount = _totalAmount * .08875;
				taxAmount = Math.round(taxAmount * 100) / 100;
				Ti.API.info('Before TA: ' + taxAmount);
			} else {
				taxAmount = 0;
			}
			subtotal = Alloy.Globals.FormatUtils.formatFloatTwoDecimal(subtotal + _totalAmount + taxAmount);
		}
		$.btn_buy.title = titleButton.concat(' ').concat(Alloy.Globals.CurrentCurrencySymbol + " " + Alloy.Globals.FormatUtils.formatPrice(subtotal));
	} else {
		$.btn_buy.title = titleButton;
	}
};

/**
 * reset UI componentes
 */
var resetUI = function() {
	$.picker_ticket.applyProperties({
		options : $.picker_ticket.formatDataToPicker([], 'id', 'description', true, L('ticket_or_table', 'Ticket | Table')),
		selected : -1
	});
	resetQuantity();
	$.txt_quantity.hintText = (L('quantity', 'Quantity'));
};

/**
 * reset quantity
 */
var resetQuantity = function() {
	$.txt_quantity.setValue("");
	if (OS_IOS) {
		onChangeQuantity({
			value : ""
		});
	}
};

/**
 * calculate new value
 */
var recalculate = function() {
	$.txt_quantity.setValue($.txt_quantity.getValue());
	if (OS_IOS) {
		onChangeQuantity({
			value : $.txt_quantity.getValue()
		});
	}
};
var validateQuantityView = function() {
	return $.txt_quantity.value.indexOf(".") != -1 || $.txt_quantity.value.indexOf("-") != -1;
};

var validateTipView = function() {
	return $.txt_tip_amount.value.indexOf("-") == -1;
};
/**
 * validate transaction detail
 *
 */
var validateTransactionDetail = function() {
	if (transactionDetail.validateRequired() && validateTipView()) {

		if (validateQuantityView()) {
			(require('TiUtils')).toast(L('amount_invalid', 'Invalid amount'));
			return false;
		} else if (transactionDetail.validateQuantity()) {
			return true;
		} else {
			(require('TiUtils')).toast(String.format(L('the_amount_available_is', 'The amount available is %s'), concept.getQuantity() + ""));
			return false;
		}

	} else if (transactionDetail.getQuantity() < 0) {
		(require('TiUtils')).toast(L('concept_amount_major_zero', 'The amount must be greater than zero'));
	} else if (!transactionDetail.validateConcept()) {
		(require('TiUtils')).toast(L('ticket_or_table_required', 'Please select an option from the concept menu and try again'));
		return false;
	} else if (!_.isNumber(transactionDetail.getQuantity()) || transactionDetail.getQuantity() == 0) {
		(require('TiUtils')).toast(L('quantity_required', 'Quantity is required'));
		return false;
	} else if (!validateTipView()) {
		(require('TiUtils')).toast(L('tip_invalid', 'Tip is invalid'));
		return false;
	} else {
		return false;
	}
};

/**
 * send transaction to service
 */
var sendTransaction = function() {
	showLoader();
	//generate transaction

	transaction = Alloy.Globals.DaoBuilder.getTransactionDao().getModel();

	var totalAmount = Alloy.Globals.FormatUtils.formatFloatTwoDecimal(transactionDetail.getSubtotal() + tipAmount + taxAmount);
	transaction.generateTransaction(establishmentID, Alloy.Globals.UserID, concept.getTransactionTypeID(), totalAmount, tipAmount, date, taxAmount);

	getTransactionService().send(transaction.toService([transactionDetail.toService()], true), onSuccessSendTransaction, onErrorSendTransaction, httpCK);
};

var onSuccessSendTransaction = function(response) {
	transaction.mapping(response);
	pay();
};

/**
 * on error send transaction
 * @param {Object} error
 */
var onErrorSendTransaction = function(error) {

	transaction = null;

	(require('TiLog')).error('onErrorSendTransaction', JSON.stringify(error));
	hideLoader();
	var message = L('error_buy_concept_try_again', 'An error occurred while buying the ticket or table try again');
	switch(error.code) {
	case Alloy.CFG.Http.NotAvailableConcept:
		if (parseInt(error.message) == 0) {
			message = L('ticket_or_table_sould_out', 'Ticket | Table sould out!');
			$.txt_quantity.hintText = L('sould_out', 'Sould out!');
		} else {
			message = String.format(L('you_can_only_buy_tickets', 'You can only buy %s tickets'), error.message);
		}
		break;
	}
	(require('TiUtils')).toast(message);
};

var pay = function() {
	var invoiceItems = [transactionDetail.toPaypal()];
	var invoiceItemTip = transaction.generateTransactionDetailPaypalForTip();
	if (!_.isNull(invoiceItemTip)) {
		invoiceItems.push(invoiceItemTip);
	}
	var payment = transaction.toPaypal(invoiceItems);

	hideKeyboard();

	paymentOnline = Alloy.createWidget('co.clarika.stripe', {
		payment : payment,
		transactionAmount : transaction.getCurrencyAndTotalAmount()
	});
	paymentOnline.on('success', onSuccessPayment);
	paymentOnline.on('cancel', onCancelPayment);
	paymentOnline.open();

	//gc
	payment = null;
};

/**
 * on success update payment
 * @param {Object} response
 */
var onSuccessUpdatePayment = function(response) {
	(require('TiLog')).info(TAG, 'onSuccessTransaction -> ' + JSON.stringify(response));

	var transactionID = response.id;
	$.trigger('buy:success', {
		transactionID : transactionID
	});

};

/**
 * on error update payment
 * @param {Object} error
 */
var onErrorUpdatePayment = function(error) {
	(require('TiLog')).error(TAG, 'onErrorTransaction -> ' + JSON.stringify(error));
	if (isStripe) {
		var message = paymentOnline.analizeError(JSON.parse(error.message));
		showCardErrorMessage(message);

	} else {
		(require('TiUtils')).toast(L('error_process_payment', 'An error occurred while processing your purchase, please try again.'));
	}

	Ti.API.fireEvent('hide_loader');
};

var showCardErrorMessage = function(message) {
	var dialog = Ti.UI.createAlertDialog({
		message : message.concat("\n").concat(L('do_you_want_to_pay_with_another_card', 'Do you want pay with another card?')),
		buttonNames : [L('close', 'Close'), L('confirm', 'Confirm')],
		cancel : 0
	});

	dialog.addEventListener('click', function(e) {
		if (e.index == 1) {
			paymentOnline.open();
		}
	});
	dialog.show();
};
/**
 * set quantity textfield by concept
 */
var setQuantityHintTextByConcept = function() {
	if (!_.isNull(concept)) {
		$.txt_quantity.hintText = String.format(L('available_number', 'Available: %s'), (concept.getQuantity() + ""));
	} else {
		$.txt_quantity.hintText = L('quantity', 'Quantity');
	}
};
/** ------------------------
 Listeners
 ------------------------**/

/**
 * when window is opened
 */
var onOpen = function(e) {
	if (OS_IOS) {
		init(e);
	} else {
		_.defer(init, e);
	}
	$.picker_ticket.onOpen(e);
};

/**
 * when window is closed
 * @param {Object} e
 */
var onClose = function(e) {
	cleanup();
	$.picker_ticket.onClose(e);
};

/**
 * on change picker tablet
 * @param {Object} e
 */
var onChangePickerTicketTable = function(e) {
	(require('TiLog')).info(TAG, 'onChangePickerTicketTable ->' + JSON.stringify(e.rowIndex));
	Ti.API.info('[onChangePickerTicketTable]: ' + JSON.stringify(e));

	if (e.rowIndex > 0) {
		var realIndex = e.rowIndex - 1;
		concept = collection.models[realIndex];
	} else {
		//Clear concept
		concept = null;
	}

	Ti.API.info('[concept]: ' + JSON.stringify(concept));

	transactionDetail.setConcept(concept);
	setQuantityHintTextByConcept();
	recalculate();
};

/**
 * on change date
 * @param {Object} e
 */
var onChangeDate = function(e) {
	(require('TiLog')).info(TAG, 'onChangeDate ->' + JSON.stringify(e));
	if (date != e.value) {

		date = new Date(e.value);
		showLoader();
		initializeTransactionDetail();
		resetUI();
		getData();
	}
};

/**
 * on change quantity
 * @param {Object} e
 */
var onChangeQuantity = function(e) {
	Ti.API.info('[change]:');
	var value = e.value;
	if (_.isString(value) && !_.isEmpty(value)) {
		currentQuantity = parseInt(value);
	} else {
		currentQuantity = value;
	}
	if (timeOutUpdateQuantity != null) {
		clearTimeout(timeOutUpdateQuantity);
		timeOutUpdateQuantity = null;
	}
	timeOutUpdateQuantity = setTimeout(updateQuantity, DELAY_UPDATE_QUANTITY);
};

/**
 * on change tip amount
 * @param {Object} e
 */
var onChangeTipAmount = function(e) {
	var value = e.value;
	var number = Alloy.Globals.FormatUtils.convertStringToNumber(value);
	if (_.isString(value) && !_.isEmpty(value) && _.isNumber(number) && number > 0) {
		tipAmount = Alloy.Globals.FormatUtils.formatFloatTwoDecimal(number);
	} else {
		tipAmount = 0;
	}
	if (timeOutUpdateTipAmount != null) {
		clearTimeout(timeOutUpdateTipAmount);
		timeOutUpdateTipAmount = null;
	}
	timeOutUpdateTipAmount = setTimeout(updateButtonUI, DELAY_UPDATE_TIP_AMOUNT);
};

/**
 * on buy ticket
 * @param {Object} e
 */
var onBuy = _.debounce(function(e) {
	if (validateTransactionDetail()) {
		hideKeyboard();
		sendTransaction();
	}

}, Alloy.Globals.Integers.debounce, true);

/**
 * on success payment transaction
 * @param {Object} e
 */
var onSuccessPayment = _.debounce(function(e) {

	Ti.App.fireEvent('show_loader', {
		message : L('processing_purchase', 'Processing purchase...'),
		cancelable : false,
		backgroundColor : Alloy.Globals.Colors.primary
	});

	transaction.processOnlinePayment(e);

	isStripe = e.paymentMethod == Alloy.CFG.Stripe.method;
	var paramsToUpdatePayment = transaction.getParamsToUpdatePayment();
	//send transaction
	getTransactionService().updatePayment(transaction.getID(), paramsToUpdatePayment, onSuccessUpdatePayment, onErrorUpdatePayment, httpCK);
}, Alloy.Globals.Integers.debounce, true);

/**
 * on error payment transaction
 * @param {Object} error
 */
var onErrorPayment = _.debounce(function(error) {
	(require('TiLog')).error(TAG, 'onErrorPayment -> ' + JSON.stringify(error));
	(require('TiUtils')).toast(L('error_process_payment', 'An error occurred while processing your purchase, please try again.'));
}, Alloy.Globals.Integers.debounce, true);

/**
 * on cancel payment transaction
 * @param {Object} e
 */
var onCancelPayment = _.debounce(function(e) {
	hideLoader();
}, Alloy.Globals.Integers.debounce, true);

var hideKeyboard = function() {
	if (OS_IOS) {
		$.txt_quantity.blur();
		$.txt_tip_amount.blur();
	} else {
		Ti.UI.Android.hideSoftKeyboard();
	}
};

if (OS_IOS) {
	var onNextToTip = _.debounce(function(e) {
		$.txt_quantity.blur();
		$.txt_tip_amount.focus();
	}, Alloy.Globals.Integers.debounce, true);
}
/** ------------------------
 public
 ------------------------**/
exports.applyProperties = applyProperties;
exports.onOpen = onOpen;
exports.onClose = onClose;
exports.cleanup = cleanup;
