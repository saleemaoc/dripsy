/**
 * @author jagu
 */

/**
 *	log info
 * @param {Object} TAG
 * @param {Object} message
 */
exports.info = function(TAG, message) {
	if (Alloy.CFG.debug) {
		Ti.API.info(TAG, message);
	}
};

/**
 *	log error
 * @param {Object} TAG
 * @param {Object} error
 */
exports.error = function(TAG, error) {
	Ti.API.error(TAG, error);
};

/**
 *	log debug
 * @param {Object} TAG
 * @param {Object} message
 */
exports.debug = function(TAG, message) {
	if (Alloy.CFG.debug) {
		Ti.API.debug(TAG, error);
	}
};
