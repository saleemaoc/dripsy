module.exports = {
	/** ------------------------
	 Theme
	 ------------------------**/
	primary : '#25252e',
	primary_90 : '#E625252e',
	primaryDark : '#1f1e24',
	accent : '#049DA9',
	facebook_button : '#3949AB',
	secondary : '#EA6553',
	secondary_50 : '#80EA6553',
	windowBackground : '#1f1e24',
	windowBackgroundLight : '#DEDEE2',
	separator : '#4DFFFFFF',
	background_gradient : '#A6000000',
	red : '#EB5455',
	green : '#4CAA4E',
	accent_pressed : '#038184',
	accent_focused : '#038184',
	accent_disabled : '#25252F',
	secondary_pressed : '#E25344',
	secondary_focused : '#E25344',
	secondary_disabled : '#4CAA4E',
	facebook : '#3949AB',
	facebook_pressed : '#2036A5',
	accent_50 : '#80049DA9',
	accent_dark : '#00848E',
	accent_dark_90 : '#E6048F9A',
	separator_accent : '#03686B',
	separator_dark : '#1e1e24',
	separator_primary : '#E625252e',
	separator_white : '#FFFFFF',
	button_transparent : '#CCFFFFFF',
	button_transparent_focused : '#E25344',
	button_transparent_pressed : '#E25344',
	accent_optional : '#735AB7',
	type_presence_going : '#8d9193',
	type_presence_here : '#71b552',
	action_bar_transparent : '#0025252e',
	action_bar_transparent_without_alpha : '25252e',
	separator_black : '#19181d',
	chat_background_me : '#80049DA9',
	modal_background : '#80000000',
	/** ------------------------
	 Palette
	 ------------------------**/
	black : '#000000',
	white : '#FFFFFF',
	/** ------------------------
	 Typographic Light
	 ------------------------**/
	typographic_black : '#000000',
	typographic_black_40 : '#66000000',
	typographic_black_54 : '#8A000000',
	typographic_black_87 : '#DE000000',
	typographic_black_60 : '#99000000',
	typographic_black_80 : '#CC000000',
	/** ------------------------
	 Typographic Dark
	 ------------------------**/
	typographic_white : '#FFFFFF',
	typographic_white_40 : '#66FFFFFF',
	typographic_white_54 : '#8AFFFFFF',
	typographic_white_87 : '#DEFFFFFF',
	typographic_white_60 : '#99FFFFFF',
	typographic_white_80 : '#CCFFFFFF',

	/** ------------------------
	 Grey Scale
	 ----------------------**/

	/** ------------------------
	 Opacities
	 ----------------------**/
	opacity_white_to_gray : 0.6
};
