var loader = Alloy.createWidget('co.clarika.loader', {
	message : {
		color : Alloy.Globals.Colors.accent,
		font : {
			fontFamily : Alloy.Globals.Fonts.Regular,
			fontSize : Alloy.Globals.Dimens.font_subheading
		}
	},
	activityIndicator : {
		indicatorColor : Alloy.Globals.Colors.accent,
		style : Titanium.UI.ActivityIndicatorStyle.BIG
	}
});
;

var showLoader = function(e) {
	
	var properties = {
		message : {
			text : ''
		},
		backgroundColor : Alloy.Globals.Colors.modal_background,
		cancelable : true
	};

	if (_.isObject(e)) {
		if (_.has(e, 'message')) {
			properties.message.text = e.message;
		}

		if (_.has(e, 'backgroundColor')) {
			properties.backgroundColor = e.backgroundColor;
		}

		if (_.has(e, 'cancelable')) {
			properties.cancelable = e.cancelable;
		}

	}

	loader.applyProperties(properties);
	loader.show();

};

var hideLoader = function() {
	loader.hide();
};

exports.register = function() {
	Ti.App.addEventListener('show_loader', showLoader);
	Ti.App.addEventListener('hide_loader', hideLoader);
};
