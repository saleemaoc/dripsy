/** --------
 Object
 -------- **/
function GiftDao() {
	this.MODEL_NAME = 'gift';
};

//inheritance
GiftDao.prototype = new (require('daos/GenericDao'))();
GiftDao.prototype.constructor = GiftDao;

/** --------
 Constants
 -------- **/
var TAG = 'GiftDao';

/** --------
 FILTERS
 -------- **/

var FILTER_VISIBLE = " show = 1 ";
var AND_FILTER_VISIBLE = " and" + FILTER_VISIBLE;
var ORDER_BY_DATE_ACCEPTANCE = " ORDER BY datetime(transaction_transaction_date) DESC";

/** --------
 Methods
 -------- **/

/**
 * @param {Integer} toUserID
 * @param {String} query
 * @param {Boolean} async
 * @param {Function} success
 * @param {Function} error
 * @param {Alloy.Collection} collection
 */
GiftDao.prototype.findByToUserID = function(toUserID, query, async, success, error, collection) {
	var defaultQuery = 'WHERE to_user_id = ' + toUserID + AND_FILTER_VISIBLE + ORDER_BY_DATE_ACCEPTANCE;
	return this.find(defaultQuery.concat(query), async, success, error, collection);
};

/**
 * delete all records
 */
GiftDao.prototype.destroyAll = function() {
	(Alloy.createCollection(this.MODEL_NAME)).deleteAll();
};
/** --------
 Public
 -------- **/
module.exports = GiftDao;
