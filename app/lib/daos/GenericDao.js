/** --------
 Object
 -------- **/
function GenericDao() {

};

/** --------
 Constants
 -------- **/
var TAG = 'GenericDao';

/** --------
 Methods
 -------- **/

/**
 * get new model
 * @return {AlloyModel}
 */
GenericDao.prototype.getModel = function() {
	return Alloy.createModel(this.MODEL_NAME);
};

/**
 * find model by ID
 * @param {Integer} ID
 * @param {Boolean} async
 * @param {Function} success
 * @param {Function} error
 */
GenericDao.prototype.findByID = function(ID, async, success, error) {

	var model = this.getModel();

	if (async) {
		model.fetch({
			id : ID,
			success : success,
			error : error
		});
	} else {
		model.fetch({
			id : ID
		});
		return model;
	}
};

/**
 * @param {String} query
 * @param {Boolean} async
 * @param {Function} success
 * @param {function} error
 * @param {AlloyCollection} collection
 */
GenericDao.prototype.find = function(query, async, success, error, collection) {
	(require('TiLog')).info(TAG, query);
	if (!collection) {
		collection = Alloy.createCollection(this.MODEL_NAME);
	}
	var paramsToFetch = {
		update : true
	};

	if (async) {
		paramsToFetch.success = success;
		paramsToFetch.error = error;
	}

	if (_.isString(query)) {
		paramsToFetch.query = 'SELECT * FROM ' + this.MODEL_NAME + ' ' + query;
	}

	collection.fetch(paramsToFetch);

	if (!async) {
		return collection;
	}
};

/**
 *
 * @param {Array} || {Object} data
 * @return {Alloy.Model} only if params is object
 */
GenericDao.prototype.updateData = function(data) {
	if (_.isArray(data)) {
		this.updateCollection(data);
	} else if (_.isObject(data)) {
		return this.updateModel(data);
	} else {
		(require('TiLog')).error('updateData', 'not updateData');
	}
};

/**
 *update in local database models to service
 * @param {Array} modelsService
 */
GenericDao.prototype.updateCollection = function(modelsService) {
	var _this = this;
	_.map(modelsService, function(modelService) {
		_this.updateModel(modelService);
	});
	//gc
	_this = null;
};

/**
 * update in local database model to service
 * @param {Object} modelService
 * @return {Alloy.Model}
 */
GenericDao.prototype.updateModel = function(modelService) {

	var model = this.findByID(modelService.id);

	if (_.isFunction(model.mapping)) {
		model.mapping(modelService);
	} else {
		Ti.API.error(TAG, MODEL_NAME + ' must implement the mapping function');
	}

	return model;
};

/**
 * delete by id
 * @param {Integer} id
 */
GenericDao.prototype.deleteByID = function(id) {
	var model = this.findByID(id);
	if (!_.isNull(model)) {
		model.destroy();
	}
};

/**
 * create new collection
 */
GenericDao.prototype.getCollection = function() {
	return Alloy.createCollection(this.MODEL_NAME);
};

/** --------
 Public
 -------- **/

module.exports = GenericDao;
