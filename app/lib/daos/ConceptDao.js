/** --------
 Object
 -------- **/
function ConceptDao() {
	this.MODEL_NAME = 'concept';
}

//inheritance
ConceptDao.prototype = new (require('daos/GenericDao'))();
ConceptDao.prototype.constructor = ConceptDao;

/** --------
 Constants
 -------- **/
var TAG = 'ConceptDao';

ConceptDao.prototype.destroyAll = function() {
	(Alloy.createCollection(this.MODEL_NAME)).deleteAll();
};
/** --------
 Public
 -------- **/
module.exports = ConceptDao;
