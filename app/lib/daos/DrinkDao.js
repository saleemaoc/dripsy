/** --------
 Object
 -------- **/
function DrinkDao() {
	this.MODEL_NAME = 'drink';
}

//inheritance
DrinkDao.prototype = new (require('daos/GenericDao'))();
DrinkDao.prototype.constructor = DrinkDao;

/** --------
 Constants
 -------- **/
var TAG = 'DrinkDao';
var ORDER_BY = ' ORDER BY recommended DESC, name ASC ';

/**
 * find drink list by establishment
 */
DrinkDao.prototype.findList = function(establishmentID, name, limit, offset, async, success, error, collection) {
	var query = 'WHERE establishment_id = ' + establishmentID + ' and name LIKE "%' + name + '%" ' + ORDER_BY + 'limit ' + limit + ' offset ' + offset;
	return this.find(query, async, success, error, collection);
};
/**
 *
 * @param {String} name
 * @param {Boolean} async
 * @param {Function} success
 * @param {Function} error
 * @param {AlloyCollection} collection
 */
DrinkDao.prototype.findByName = function(name, async, success, error, collection) {
	var query = ' name LIKE "%' + name + '%"';
	return this.find(query, async, success, error, collection);
};

DrinkDao.prototype.destroyAll = function() {
	(Alloy.createCollection(this.MODEL_NAME)).deleteAll();
};
/** --------
 Public
 -------- **/
module.exports = DrinkDao;
