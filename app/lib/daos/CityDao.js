/** --------
 Object
 -------- **/
function CityDao() {
	this.MODEL_NAME = 'city';
}

//inheritance
CityDao.prototype = new (require('daos/GenericDao'))();
CityDao.prototype.constructor = CityDao;

var ORDER_BY = " ORDER BY name";
/** --------
 Constants
 -------- **/
var TAG = 'CityDao';

/**
 *
 * @param {String} name
 * @param {Boolean} async
 * @param {Function} success
 * @param {Function} error
 * @param {AlloyCollection} collection
 */
CityDao.prototype.findByName = function(name, async, success, error, collection) {
	var query = ' name LIKE "%' + name + '%"' + ORDER_BY;
	return this.find(query, async, success, error, collection);
};

/**
 *
 * @param {Integer} countryID
 * @param {Boolean} async
 * @param {Function} success
 * @param {Function} error
 * @param {AlloyCollection} collection
 */
CityDao.prototype.findByCountryID = function(countryID, async, success, error, collection) {
	(require('TiLog')).info(TAG + countryID);
	if (countryID > 0) {
		var query = 'WHERE country_id = ' + countryID + ORDER_BY;

		return this.find(query, async, success, error, collection);
	}
};

/**
 * find cities for location by country
 * @param {Integer} countryID
 * @param {Boolean} async
 * @param {Function} success
 * @param {Function} error
 * @param {AlloyCollection} collection
 */
CityDao.prototype.findForLocationByCountryID = function(countryID, async, success, error, collection) {
	(require('TiLog')).info(TAG + countryID);
	if (countryID > 0) {
		var query = 'WHERE enabled = 1 and country_id = ' + countryID + ORDER_BY;
		return this.find(query, async, success, error, collection);
	}
};

/** --------
 Public
 -------- **/
module.exports = CityDao;
