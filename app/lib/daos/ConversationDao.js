/** --------
 Object
 -------- **/
function ConversationDao() {
	this.MODEL_NAME = 'conversation';
}

//inheritance
ConversationDao.prototype = new (require('daos/GenericDao'))();
ConversationDao.prototype.constructor = ConversationDao;

/** --------
 Constants
 -------- **/
var TAG = 'ConversationDao';
/** --------
 FILTERS
 -------- **/
var ORDER_BY_UPDATED_DATE = ' ORDER BY datetime(updated_date) DESC';
var STATE_ACCEPTED_OR_PENDING = ' (state = ' + Alloy.CFG.constants.conversation_state.accepted + ' or state = ' + Alloy.CFG.constants.conversation_state.pending + ' )';
var AND_STATE_ACCEPTED_OR_PENDING = ' and ' + STATE_ACCEPTED_OR_PENDING;

/**
 * @param {Integer} userID
 * @param {String} query
 * @param {Boolean} async
 * @param {Function} success
 * @param {Function} error
 * @param {Alloy.Collection} collection
 */
ConversationDao.prototype.findByUserID = function(userID, query, async, success, error, collection) {

	var defaultQuery = ' WHERE (from_user_id = ' + userID + ' or to_user_id = ' + userID + ') and deleted = 0' + AND_STATE_ACCEPTED_OR_PENDING + ORDER_BY_UPDATED_DATE;

	if (_.isString(query)) {
		defaultQuery = defaultQuery.concat(query);
	}

	return this.find(defaultQuery, async, success, error, collection);
};

/**
 * @param {Integer} toUserID
 * @param {String} query
 * @param {Boolean} async
 * @param {Function} success
 * @param {Function} error
 * @param {Alloy.Collection} collection
 */
ConversationDao.prototype.findActiveConversationToUser = function(toUserID, query, async, success, error, collection) {

	var defaultQuery = ' WHERE ((to_user_id = ' + toUserID + ' and from_user_id = ' + Alloy.Globals.UserID + ') or ( from_user_id = ' + toUserID + ' and to_user_id = ' + Alloy.Globals.UserID + ')) and deleted = 0 ' + AND_STATE_ACCEPTED_OR_PENDING;

	if (_.isString(query)) {
		defaultQuery = defaultQuery.concat(query);
	}

	collection = this.find(defaultQuery.concat(ORDER_BY_UPDATED_DATE), async, success, error, collection);
	if (!_.isEmpty(collection.models)) {
		return collection.models[0];
	} else {
		return null;
	}
};

ConversationDao.prototype.markRead = function(conversation) {
	conversation.setUnread(false);
	conversation.save();
};
/** --------
 Public
 -------- **/
module.exports = ConversationDao;
