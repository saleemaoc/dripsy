/** --------
 Object
 -------- **/
function ExampleDao() {
	this.MODEL_NAME = 'example';
}

//inheritance
ExampleDao.prototype = new (require('daos/GenericDao'))();
ExampleDao.prototype.constructor = ExampleDao;

/** --------
 Constants
 -------- **/
var TAG = 'ExampleDao';

/**
 *
 * @param {String} name
 * @param {Boolean} async
 * @param {Function} success
 * @param {Function} error
 * @param {AlloyCollection} collection
 */
ExampleDao.prototype.findByName = function(name, async, success, error, collection) {
	var query = ' name LIKE "%' + name + '%"';
	return this.find(query, async, success, error, collection);
};

/** --------
 Public
 -------- **/
module.exports = ExampleDao;