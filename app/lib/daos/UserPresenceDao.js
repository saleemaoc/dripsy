/** --------
 Object
 -------- **/
function UserPresenceDao() {
	this.MODEL_NAME = 'userpresence';
}

//inheritance
UserPresenceDao.prototype = new (require('daos/GenericDao'))();
UserPresenceDao.prototype.constructor = UserPresenceDao;

/** --------
 Constants
 -------- **/
var TAG = 'UserPresenceDao';
/** --------
 Public
 -------- **/
module.exports = UserPresenceDao;

