/** --------
 Object
 -------- **/
function TransactionDao() {
	this.MODEL_NAME = 'n2n_transaction';
}

//inheritance
TransactionDao.prototype = new (require('daos/GenericDao'))();
TransactionDao.prototype.constructor = TransactionDao;

/** --------
 Constants
 -------- **/
var TAG = 'TransactionDao';
/** --------
 FILTERS
 -------- **/
var FILTER_NOT_PENDING_PAYMENT = " status != " + Alloy.CFG.constants.transaction_status.pending_payment;
var AND_FILTER_NOT_PENDING_PAYMENT = " and " + FILTER_NOT_PENDING_PAYMENT;
var FILTER_VISIBLE = " show = 1 ";
var AND_FILTER_VISIBLE = " and" + FILTER_VISIBLE;
var ORDER_BY_TRANSACTION_DATE = " ORDER BY datetime(transaction_date) DESC";
/** --------
 Methods
 -------- **/

/**
 * @param {Integer} userID
 * @param {String} query
 * @param {Boolean} async
 * @param {Function} success
 * @param {Function} error
 * @param {Alloy.Collection} collection
 */
TransactionDao.prototype.findByBuyerUserID = function(userID, query, async, success, error, collection) {
	var defaultQuery = 'WHERE buyer_user_id = ' + userID + AND_FILTER_VISIBLE + AND_FILTER_NOT_PENDING_PAYMENT + ORDER_BY_TRANSACTION_DATE;
	return this.find(defaultQuery.concat(query), async, success, error, collection);
};

/**
 * delete all records
 */
TransactionDao.prototype.destroyAll = function() {
	(Alloy.createCollection(this.MODEL_NAME)).deleteAll();
};
/** --------
 Public
 -------- **/
module.exports = TransactionDao;
