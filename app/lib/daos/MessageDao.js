/** --------
 Object
 -------- **/
function MessageDao() {
	this.MODEL_NAME = 'message';
}

//inheritance
MessageDao.prototype = new (require('daos/GenericDao'))();
MessageDao.prototype.constructor = MessageDao;

/** --------
 Constants
 -------- **/
var TAG = 'MessageDao';
/** --------
 FILTERS
 -------- **/
var ORDER_BY_DATE = " ORDER BY datetime(date) DESC";
var STATE_PENDING_TO_SENT = " state = " + Alloy.CFG.constants.message_state.pending_to_sent;
var AND_STATE_PENDING_TO_SENT = " and " + STATE_PENDING_TO_SENT;
/** --------
 Public
 -------- **/

/**
 *find messages by conversation id
 * @param {Object} conversationID
 * @param {Object} query
 * @param {Object} async
 * @param {Object} success
 * @param {Object} error
 * @param {Object} collection
 */
MessageDao.prototype.findMessagesByConversationID = function(conversationID, query, async, success, error, collection) {
	var defaultQuery = 'WHERE conversation_id = ' + conversationID + ' ' + ORDER_BY_DATE;
	if (_.isString(query)) {
		defaultQuery = defaultQuery.concat(query);
	}
	return this.find(defaultQuery, async, success, error, collection);
};

/**
 * insert new message
 * @param {Integer} conversationID
 * @param {Integer} fromUserID
 * @param {Integer} toUserID
 * @param {String} fromUserPhoto
 * @param {String} message
 * @return {Alloy.Model}
 */
MessageDao.prototype.insert = function(conversationID, fromUserID, toUserID, fromUserPhoto, message) {

	var model = this.getModel();

	model.setConversationID(conversationID);
	model.setFromUserID(fromUserID);
	model.setFromUserPhoto(fromUserPhoto);
	model.setToUserID(toUserID);
	model.setText(message);
	model.setState(Alloy.CFG.constants.message_state.pending_to_sent);
	model.setDate((require('FormatUtils')).getStringDBToDate());

	model.save();
	return model;
};

/**
 *
 * @param {Integer} conversationID
 * @param {String} query
 * @param {Boolean} async
 * @param {Function} success
 * @param {Function} error
 * @param {Alloy.Collection} collection
 */
MessageDao.prototype.findMessagesToSentByConversationID = function(conversationID, query, async, success, error, collection) {
	var defaultQuery = 'WHERE conversation_id = ' + conversationID + AND_STATE_PENDING_TO_SENT;
	if (_.isString(query)) {
		defaultQuery = defaultQuery.concat(query);
	}
	return this.find(defaultQuery, async, success, error, collection);
};

/**
 *
 * @param {Integer} conversationID
 * @param {Integer} lastMessageID
 * @param {String} query
 * @param {Boolean} async
 * @param {Function} success
 * @param {Function} error
 * @param {Alloy.Collection} collection
 */
MessageDao.prototype.findNewMessagesToOtherUserByConversationIDAndLastMessageID = function(conversationID, lastMessageID, query, async, success, error, collection) {

	var defaultQuery = 'WHERE conversation_id = ' + conversationID;
	if (_.isNumber(lastMessageID)) {
		defaultQuery = defaultQuery.concat(' and id > ' + lastMessageID);
	}

	defaultQuery = defaultQuery + ' ' + ORDER_BY_DATE;
	if (_.isString(query)) {
		defaultQuery = defaultQuery.concat(query);
	}
	return this.find(defaultQuery, async, success, error, collection);
};

/**
 * Override
 * update in local database model to service
 * @param {Object} modelService
 * @return {Alloy.Model}
 */
MessageDao.prototype.updateModel = function(modelService) {
	
	var model = null;
	if (modelService.from_user_id == Alloy.Globals.UserID) {
		var model = this.findByID(modelService.from_user_message_temp_id);
	} else {
		var model = this.findByServerID(modelService.id);
	}

	if (_.isFunction(model.mapping)) {
		model.mapping(modelService);
	} else {
		Ti.API.error(TAG, MODEL_NAME + ' must implement the mapping function');
	}

	return model;
};

/**
 * find model by ServerID
 * @param {Integer} serverID
 * @param {Boolean} async
 * @param {Function} success
 * @param {Function} error
 */
MessageDao.prototype.findByServerID = function(serverID) {

	var collection = this.getCollection();

	collection.fetch({
		query : 'SELECT * FROM ' + this.MODEL_NAME + ' WHERE server_id = ' + serverID,
	});

	if (!_.isEmpty(collection.models)) {
		return collection.first();
		;
	} else {
		return this.getModel();
	}
};

module.exports = MessageDao;
