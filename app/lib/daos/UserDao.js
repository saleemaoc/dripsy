/** --------
 Object
 -------- **/
function UserDao() {
	this.MODEL_NAME = 'user';
}

//inheritance
UserDao.prototype = new (require('daos/GenericDao'))();
UserDao.prototype.constructor = UserDao;

/** --------
 Constants
 -------- **/
var TAG = 'UserDao';

/**
 *
 * @param {String} name
 * @param {Boolean} async
 * @param {Function} success
 * @param {Function} error
 * @param {AlloyCollection} collection
 */
UserDao.prototype.findByName = function(name, async, success, error, collection) {
	var query = ' name LIKE "%' + name + '%"';
	return this.find(query, async, success, error, collection);
};


/** --------
 Public
 -------- **/
module.exports = UserDao;