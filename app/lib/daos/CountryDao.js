/** --------
 Object
 -------- **/
function CountryDao() {
	this.MODEL_NAME = 'country';
}

//inheritance
CountryDao.prototype = new (require('daos/GenericDao'))();
CountryDao.prototype.constructor = CountryDao;

var ORDER_BY = " ORDER BY name ASC";
/** --------
 Constants
 -------- **/
var TAG = 'CountryDao';

/**
 *
 * @param {String} name
 * @param {Boolean} async
 * @param {Function} success
 * @param {Function} error
 * @param {AlloyCollection} collection
 */
CountryDao.prototype.findByName = function(name, async, success, error, collection) {
	var query = ' name LIKE "%' + name + '%"' + ORDER_BY;
	return this.find(query, async, success, error, collection);
};

/**
 *find country for location
 * @param {Boolean} async
 * @param {Function} success
 * @param {Function} error
 * @param {AlloyCollection} collection
 */
CountryDao.prototype.findForLocation = function(async, success, error, collection) {
	var query = 'WHERE enabled = 1' + ORDER_BY;
	return this.find(query, async, success, error, collection);
};

/**
 *find all countries
 * @param {Boolean} async
 * @param {Function} success
 * @param {Function} error
 * @param {AlloyCollection} collection
 */
CountryDao.prototype.findWithOrder = function(async, success, error, collection) {
	var query = ORDER_BY;
	return this.find(query, async, success, error, collection);
};
/** --------
 Public
 -------- **/
module.exports = CountryDao;
