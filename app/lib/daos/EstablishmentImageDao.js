/** --------
 Object
 -------- **/
function EstablishmentImageDao() {
	this.MODEL_NAME = 'establishment_image';
};

//inheritance
EstablishmentImageDao.prototype = new (require('daos/GenericDao'))();
EstablishmentImageDao.prototype.constructor = EstablishmentImageDao;

/** --------
 Constants
 -------- **/
var TAG = 'EstablishmentImageDao';

/** --------
 Methods
 -------- **/
/**
 * @param {Integer} establishment_id
 * @param {Boolean} sync
 * @param {Function} success
 * @param {Function} error
 * @param {Alloy.Collection} collection
 */
EstablishmentImageDao.prototype.findByEstablishmentID = function(establishmentID, async, success, error, collection) {
	var query = 'WHERE establishment_id = ' + establishmentID + ' ORDER BY is_primary DESC';
	return this.find(query, async, success, error, collection);
};

EstablishmentImageDao.prototype.destroyAll = function() {
	(Alloy.createCollection(this.MODEL_NAME)).deleteAll();
};
/** --------
 Public
 -------- **/
module.exports = EstablishmentImageDao;
