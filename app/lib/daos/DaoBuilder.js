/**
 * Dao Builder
 * @author jagu
 */

/** --------
 Constants
 -------- **/
var TAG = 'lib/DaoBuilder';
/** --------
 Methods
 -------- **/
/**
 * build dao by name
 * @param {String} dao
 * @return {Dao}
 */
var build = function(dao) {
	return new (require(('daos/'+dao)))();
};
/**
 * get new user dao
 * @return {UserDao}
 */
exports.getUserDao = function() {
	return build('UserDao');
};

/**
 * get new establishment dao
 * @return {EstablishmentDao}
 */
exports.getEstablishmentDao = function() {
	return build('EstablishmentDao');
};

/**
 * get new country dao
 * @return {CountryDao}
 */
exports.getCountryDao = function() {
	return build('CountryDao');
};

/**
 * get new city dao
 * @return {CityDao}
 */
exports.getCityDao = function() {
	return build('CityDao');
};

/**
 * get new userpresence dao
 * @return {UserPresenceDao}
 */
exports.getUserPresenceDao = function() {
	return build('UserPresenceDao');
};

/**
 * get new tag dao
 * @return {TagDao}
 */
exports.getTagDao = function() {
	return build('TagDao');
};

/**
 * get new drink dao
 * @return {DrinkDao}
 */
exports.getDrinkDao = function() {
	return build('DrinkDao');
};

/**
 * get new establishment image dao
 * @return {EstablishmentImageDao}
 */
exports.getEstablishmentImageDao = function() {
	return build('EstablishmentImageDao');
};

/**
 * get new transaction dao
 * @return {TransactionDao}
 */
exports.getTransactionDao = function() {
	return build('TransactionDao');
};

/**
 * get new transaction detail dao
 * @return {TransactionDetailDao}
 */
exports.getTransactionDetailDao = function() {
	return build('TransactionDetailDao');
};

/**
 * get new concept dao
 * @return {ConceptDao}
 */
exports.getConceptDao = function() {
	return build('ConceptDao');
};

/**
 * get new gift dao
 * @return {GiftDao}
 */
exports.getGiftDao = function() {
	return build('GiftDao');
};

/**
 * get new conversation dao
 * @return {ConversationDao}
 */
exports.getConversationDao = function() {
	return build('ConversationDao');
};

/**
 * get new message dao
 * @return {MessageDao}
 */
exports.getMessageDao = function() {
	return build('MessageDao');
};

/**
 * get user tag dao
 * @return {UserTagDao}
 */
exports.getUserTagDao = function() {
	return build('UserTagDao');
};
