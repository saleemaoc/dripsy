/** --------
 Object
 -------- **/
function UserTagDao() {
	this.MODEL_NAME = 'user_tag';
}

//inheritance
UserTagDao.prototype = new (require('daos/GenericDao'))();
UserTagDao.prototype.constructor = UserTagDao;

/** --------
 Constants
 -------- **/
var TAG = 'UserTagDao';

/**
 * find by tag id and user id
 * @param {Integer} tagID
 * @param {Integer} userID
 */
UserTagDao.prototype.findByIDAndUserID = function(tagID, userID) {

	var result = this.find('INNER JOIN tag "T" on tag_id = T.id WHERE tag_id = ' + tagID + ' and user_id = ' + userID, false);

	if (result.models.length > 0) {
		return result.models[0];
	} else {
		return null;
	}
};

/**
 * add tag to user
 * @param {Object} tag
 * @param {Integer} userID
 * @return {Alloy.Model}
 */
UserTagDao.prototype.addTagToUser = function(tag, userID) {
	var userTag = this.findByIDAndUserID(tag.getID(), userID);
	if (_.isNull(userTag)) {
		var model = this.getModel();
		model.set('tag_id', tag.get('id'));
		model.set('user_id', userID);
		model.save();
		return model;
	} else {
		return null;
	}
};
/**
 * find tags by user
 * @param {Integer} userID
 * @return {Array}
 */
UserTagDao.prototype.findTagsByUser = function(userID) {
	var tags = [];

	var collectionUserTag = this.find('WHERE user_id = ' + userID);
	var tagDao = Alloy.Globals.DaoBuilder.getTagDao();
	collectionUserTag.map(function(userTag) {
		tags.push(tagDao.findByID(userTag.get('tag_id')));
	});

	//gc
	collectionUserTag = null;
	tagDao = null;
	return tags;
};

/**
 * remove specific tag to user
 * @param {Integer} tagID
 * @param {Integer} userID
 */
UserTagDao.prototype.removeTagToUser = function(tagID, userID) {

	var userTags = this.find('WHERE user_id = ' + userID + ' and tag_id = ' + tagID);

	var userTag = null;
	if (_.isArray(userTags.models) && !_.isEmpty(userTags.models)) {
		userTag = userTags.models[0];
	}

	if (!_.isNull(userTag)) {
		userTag.destroy();
	}
};

/** --------
 Public
 -------- **/
module.exports = UserTagDao;
