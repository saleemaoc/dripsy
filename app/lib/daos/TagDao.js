/** --------
 Object
 -------- **/
function TagDao() {
	this.MODEL_NAME = 'tag';
}

//inheritance
TagDao.prototype = new (require('daos/GenericDao'))();
TagDao.prototype.constructor = TagDao;

/** --------
 Constants
 -------- **/
var TAG = 'TagDao';

/**
 *
 * @param {String} name
 * @param {Boolean} async
 * @param {Function} success
 * @param {Function} error
 * @param {AlloyCollection} collection
 */
TagDao.prototype.findByName = function(name, async, success, error, collection) {
	var query = ' WHERE name LIKE "%' + name + '%"';
	return this.find(query, async, success, error, collection);
};

/**
 * @param {Integer} serverID
 * @return {Alloy.model}
 */
TagDao.prototype.findByServerID = function(serverID) {
	var result = this.find(' WHERE server_id = ' + serverID, false);
	if (result.models.length > 0) {
		return result.models[0];
	} else {
		return this.getModel();
	}
};

/**
 * @param {Integer} serverID
 * @return {Alloy.model}
 */
TagDao.prototype.findByServerIDOrName = function(serverID, name) {
	var result = this.find(' WHERE server_id = ' + serverID + ' or name LIKE "' + name + '"', false);
	if (result.models.length > 0) {
		return result.models[0];
	} else {
		return this.getModel();
	}
};

/**
 * find by exact name
 * @param {String} name
 * @return {Alloy.Model}
 */
TagDao.prototype.findByExactName = function(name) {
	var result = this.find(' WHERE name LIKE "' + name + '"', false);
	if (result.models.length > 0) {
		return result.models[0];
	} else {
		return null;
	}
};

/**
 * find by exact name and user id
 * @param {String} name
 * @param {Integer} userID
 */
TagDao.prototype.findByExactNameAndUserID = function(name, userID) {

};

/**
 * @param {String} name
 * @param {Integer} userID
 * @return {Tag}
 */
TagDao.prototype.createTagToUser = function(name, userID) {
	var result = this.find(' WHERE name LIKE "' + name + '"', false);
	var tag;
	if (result.models.length == 0) {
		tag = this.create(name);
	} else {
		tag = result.models[0];
	}
	var userTag = Alloy.Globals.DaoBuilder.getUserTagDao().addTagToUser(tag, userID);
	if (_.isObject(userTag)) {
		return tag;
	} else {
		return null;
	}

};

TagDao.prototype.create = function(name) {
	var model = Alloy.createModel(this.MODEL_NAME);
	model.set('name', name);
	model.save();
	return model;
};

/**
 *
 * @param {String} name
 * @param {Boolean} async
 * @param {Function} success
 * @param {Function} error
 * @param {AlloyCollection} collection
 */
TagDao.prototype.findPopular = function(async, success, error, collection) {
	var query = ' WHERE popular = 1 ORDER BY count DESC';
	return this.find(query, async, success, error, collection);
};

TagDao.prototype.getNewInterests = function(async, success, error, collection) {
	var query = ' WHERE server_id = -1';
	return this.find(query, async, success, error, collection);
};

/**
 * update in local database model to service
 * @param {Object} modelService
 * @return {Alloy.Model}
 */
TagDao.prototype.updateModel = function(modelService) {

	var model = this.findByServerIDOrName(modelService.id, modelService.name);

	if (_.isFunction(model.mapping)) {
		model.mapping(modelService);
	} else {
		Ti.API.error(TAG, MODEL_NAME + ' must implement the mapping function');
	}

	return model;
};
/** --------
 Public
 -------- **/
module.exports = TagDao;
