/** --------
 Object
 -------- **/
function TransactionDetailDao() {
	this.MODEL_NAME = 'n2n_transaction_detail';
}

//inheritance
TransactionDetailDao.prototype = new (require('daos/GenericDao'))();
TransactionDetailDao.prototype.constructor = TransactionDetailDao;

/** --------
 Methods
 -------- **/
/**
 * @param {Integer} transactionID
 * @param {String} query
 * @param {Boolean} async
 * @param {Function} success
 * @param {Function} error
 * @param {Alloy.Collection} collection
 * @return {Alloy.Collection} @Nullable
 */
TransactionDetailDao.prototype.findByTransactionID = function(transactionID, query, async, success, error, collection) {
	var defaultQuery = ' WHERE transaction_id = ' + transactionID;
	return this.find(defaultQuery.concat(query), async, success, error, collection);
};
/** --------
 Constants
 -------- **/
var TAG = 'TransactionDetailDao';

/** --------
 Public
 -------- **/
module.exports = TransactionDetailDao;
