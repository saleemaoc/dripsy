/** --------
 Object
 -------- **/
function EstablishmentDao() {
	this.MODEL_NAME = 'establishment';
};

//inheritance
EstablishmentDao.prototype = new (require('daos/GenericDao'))();
EstablishmentDao.prototype.constructor = EstablishmentDao;

/** --------
 Constants
 -------- **/
var TAG = 'EstablishmentDao';

/**
 * find only bars
 * @param {String} query
 * @param {Boolean} async
 * @param {Function} success
 * @param {Function} error
 * @param {AlloyCollection} collection
 */
EstablishmentDao.prototype.findBars = function(query, async, success, error, collection) {

	var defaultQuery = 'WHERE type = ' + Alloy.CFG.constants.establishment_type.bar;
	return this.find(defaultQuery.concat(query), async, success, error, collection);
};

/**
 *find only nightclubs
 * @param {String} query
 * @param {Boolean} async
 * @param {Function} success
 * @param {Function} error
 * @param {AlloyCollection} collection
 */
EstablishmentDao.prototype.findNightclubs = function(query, async, success, error, collection) {
	var defaultQuery = 'WHERE type = ' + Alloy.CFG.constants.establishment_type.nightclub;
	return this.find(defaultQuery.concat(query), async, success, error, collection);
};

/**
 *find only nightclubs by name
 * @param {String} name
 * @param {Boolean} async
 * @param {Function} success
 * @param {Function} error
 * @param {AlloyCollection} collection
 */
EstablishmentDao.prototype.findNightclubsByName = function(name, query, async, success, error, collection) {
	var defaultQuery = ' and name LIKE "%' + name + '%"';
	return this.findNightclubs(defaultQuery.concat(query), async, success, error, collection);
};

/**
 *find only bars by name
 * @param {String} name
 * @param {Boolean} async
 * @param {Function} success
 * @param {Function} error
 * @param {AlloyCollection} collection
 */
EstablishmentDao.prototype.findBarsByName = function(name, query, async, success, error, collection) {
	var defaultQuery = ' and name LIKE "%' + name + '%"';
	return this.findBars(defaultQuery.concat(query), async, success, error, collection);
};

/**
 * delete all records
 * @param {Integer} type
 */
EstablishmentDao.prototype.destroyAllByType = function(type) {
	(Alloy.createCollection(this.MODEL_NAME)).deleteAll(type);
};

/** --------
 Public
 -------- **/
module.exports = EstablishmentDao;
