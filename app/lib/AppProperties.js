/**
 * @author jagu
 * @description library to manage shared preferences
 *
 */

/** ------------------------
 Constants
 ------------------------**/
var IS_LOGGED = "user_is_logged";
var TOKEN = "token";
var REFRESH_TOKEN = "refresh_token";
var USER_ID = "user_id";
var CURRENT_COUNTRY = "current_country";
var CURRENT_CITY = "current_city";
var GENERATOR_DB = "generator_db";
var CHAT_LAST_UPDATED = "chat_last_updated";
var ALERT_SEND_GIFT = "alert_send_drink";
var PENDING_TRANSACTION = "pending_transaction";
var PENDING_CHAT = "pending_chat";
var NOTIFICATION_CHECK_DATE = "notification_check_date";
/** ------------------------
 Methods
 ------------------------**/

/**
 * return is user logged
 * @return boolean
 */
var getUserIsLogged = function() {
	return Ti.App.Properties.getBool(IS_LOGGED, false);
};

exports.getUserIsLogged = getUserIsLogged;
/**
 * set user is logged
 * @param boolean value
 */
var setUserIsLogged = function(value) {
	Ti.App.Properties.setBool(IS_LOGGED, value);
};

exports.setUserIsLogged = setUserIsLogged;

/**
 * set token to auth
 * @param String token
 */
var setToken = function(token) {
	Ti.App.Properties.setString(TOKEN, token);
	Alloy.Globals.Token = token;
};

exports.setToken = setToken;

/**
 * get token to auth
 * @return String
 */
exports.getToken = function() {
	return Ti.App.Properties.getString(TOKEN, null);
};

/**
 * set refreshToken to auth
 * @param String refreshToken
 */
var setRefreshToken = function(token) {
	Ti.App.Properties.setString(REFRESH_TOKEN, token);
};
exports.setRefreshToken = setRefreshToken;

/**
 * get refreshtToken to auth
 * @return String
 */
exports.getRefreshToken = function() {
	return Ti.App.Properties.getString(REFRESH_TOKEN);
};

/**
 * set user id
 * @param {Object} userID
 */
var setUserID = function(userID) {
	Alloy.Globals.UserID = userID;
	Ti.App.Properties.setInt(USER_ID, userID);
};

exports.setUserID = setUserID;

var getUserID = function() {
	return Ti.App.Properties.getInt(USER_ID, -1);
};

exports.getUserID = getUserID;

/**
 * set Country
 */
var setCurrentCountry = function(current_country) {
	Alloy.Globals.CurrentCountry = current_country != -1 ? current_country : null;
	Ti.App.Properties.setInt(CURRENT_COUNTRY, current_country);
};

exports.setCurrentCountry = setCurrentCountry;

var getCurrentCountry = function() {
	return Ti.App.Properties.getInt(CURRENT_COUNTRY, -1);
};

exports.getCurrentCountry = getCurrentCountry;

/**
 * set City
 */
var setCurrentCity = function(current_city) {
	Alloy.Globals.CurrentCity = current_city != -1 ? current_city : null;
	Ti.App.Properties.setInt(CURRENT_CITY, current_city);
};

exports.setCurrentCity = setCurrentCity;

var getCurrentCity = function() {
	return Ti.App.Properties.getInt(CURRENT_CITY, -1);
};

exports.getCurrentCity = getCurrentCity;

/**
 *@param {Boolean} value
 */
exports.setGeneratorDB = function(value) {
	Ti.App.Properties.setBool(GENERATOR_DB, value);
};
exports.getGeneratorDB = function() {
	return Ti.App.Properties.getBool(GENERATOR_DB, false);
};

/**
 * setter for chat last updated
 *@param {String} value
 */
exports.setChatLastUpdated = function(value) {
	Ti.App.Properties.setString(CHAT_LAST_UPDATED, value);
};

/**
 * getter for chat last updated
 * @return {String}
 */
exports.getChatLastUpdated = function() {
	return Ti.App.Properties.getString(CHAT_LAST_UPDATED, null);
};

/**
 * check and public user id
 */
exports.checkGlobals = function() {
	Alloy.Globals.FirstTimeFindNotification = true;
	var userID = getUserID();
	if (userID != -1) {
		Alloy.Globals.UserID = userID;
		Alloy.Globals.UserLogged = Alloy.Globals.DaoBuilder.getUserDao().findByID(userID);
	}

	var currentCountry = getCurrentCountry();
	if (currentCountry != -1) {
		Alloy.Globals.CurrentCountry = currentCountry;
		var country = Alloy.Globals.DaoBuilder.getCountryDao().findByID(currentCountry);
		setGlobalsByCountry(country);
		//gc
		country = null;

		var currentCity = getCurrentCity();
		if (currentCity != -1) {
			var city = Alloy.Globals.DaoBuilder.getCityDao().findByID(currentCity);
			setGlobalsByCity(city);
			//gc
			city = null;
			Alloy.Globals.CurrentCity = currentCity;
		}
	}
};

/**
 * clean properties for logout app
 */
exports.logout = function() {
	if (Ti.Network.online) {
		try {
			setCurrentCity(-1);
			Alloy.Globals.CurrentTaxiServices = null;
			setCurrentCountry(-1);
			setUserID(-1);
			Alloy.Globals.UserLogged = null;
			setToken(null);
			setUserIsLogged(false);
			setAlertSendGift(true);
			clearNotificationCheckDate();
			//clear card
			(Alloy.createWidget('co.clarika.stripe')).saveNewCard(null);
			//clear network cache
			(new (require('XHR'))()).purge();

			//logout pushwoosh
			Alloy.Globals.PushWooshHelper.unregister();
			if (Alloy.Globals.FacebookHelper.loggedIn()) {
				Alloy.Globals.FacebookHelper.logout();
			} else {
				Alloy.Globals.AppRoute.openLogin();
			}

		} catch(e) {

		}
	} else {
		Alloy.Globals.TiUtils.toast(L('error_network_connection_no_internet', "Oops! looks like you don't have internet connection."));
	}
};

/**
 * set globals variables for country
 * @param {Object} country
 */
var setGlobalsByCountry = function(country) {
	Alloy.Globals.CurrentCurrencySymbol = country.getCurrencySymbol();
	Alloy.Globals.CurrentCurrencyCode = country.getCurrencyCode();
	Alloy.Globals.CurrentCountryDistanceCode = country.getDistanceCode();
	Alloy.Globals.CurrentCountryDistanceSymbol = country.getDistanceSymbol();
	Alloy.Globals.CurrentCountryPaymentMethods = country.getPaymentMethods();
};

exports.setGlobalsByCountry = setGlobalsByCountry;

/**
 * set globals variables for city
 * @param {Object} country
 */
var setGlobalsByCity = function(city) {
	Alloy.Globals.CurrentTaxiServices = JSON.parse(city.get('taxi_services'));
};

exports.setGlobalsByCity = setGlobalsByCity;

/**
 * set facebook access token
 * @param {String} accessToken
 */
var setFacebookAccessToken = function(accessToken) {
	Ti.App.Properties.setString(FACEBOOK_ACCESS_TOKEN, accessToken);
};

exports.setFacebookAccessToken = setFacebookAccessToken;
/**
 * get facebook access token
 * @return {String}
 */
exports.getFacebookAccessToken = function() {
	return Ti.App.Properties.getString(FACEBOOK_ACCESS_TOKEN, null);
};

/**
 * set alert send gift
 * @param {String} accessToken
 */
var setAlertSendGift = function(alertSendGift) {
	Ti.App.Properties.setString(ALERT_SEND_GIFT, alertSendGift);
};

exports.setAlertSendGift = setAlertSendGift;
/**
 * get alert send gift
 * @return {String}
 */
exports.getAlertSendGift = function() {
	return Ti.App.Properties.getBool(ALERT_SEND_GIFT, true);
};

exports.setPendingChat = function(value) {
	Ti.App.Properties.setBool(PENDING_CHAT, value);
	Ti.App.fireEvent('pending_chat', {
		value : value
	});

};

exports.getPendingChat = function() {
	return Ti.App.Properties.getBool(PENDING_CHAT, false);
};

exports.setPendingTransaction = function(value) {

	Ti.App.Properties.setBool(PENDING_TRANSACTION, value);
	Ti.App.fireEvent('pending_transaction', {
		value : value
	});
};

exports.getPendingTransaction = function() {
	return Ti.App.Properties.getBool(PENDING_TRANSACTION, false);
};

var setNotificationCheckDate = function() {
	var dateSplit = Alloy.Globals.moment.utc(new Date()).format(Alloy.CFG.default.formatDBDate).split(' ');
	var dateToSave = "";
	if (dateSplit.length == 2) {
		dateToSave = dateSplit[0] + 'T' + dateSplit[1] + 'Z';
	} else {
		dateToSave = date.toString();
	}
	Ti.App.Properties.setString(NOTIFICATION_CHECK_DATE, dateToSave);
	return dateToSave;
};

exports.setNotificationCheckDate = setNotificationCheckDate;

var clearNotificationCheckDate = function() {
	Ti.App.Properties.setString(NOTIFICATION_CHECK_DATE, null);
};

exports.updateNotificationCheckDate = function() {
	if (getUserIsLogged()) {
		setNotificationCheckDate();
	}
};
exports.getNotificationCheckDate = function() {
	var notificationCheckDate = Ti.App.Properties.getString(NOTIFICATION_CHECK_DATE, null);
	//first time
	if (notificationCheckDate == null) {
		return setNotificationCheckDate();
	} else {
		return notificationCheckDate;
	}

};
