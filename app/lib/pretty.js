/*
* JavaScript Pretty Date
* Copyright (c) 2011 John Resig (ejohn.org)
* Licensed under the MIT and GPL licenses.
*/

// Takes an ISO time and returns a string representing how
// long ago the date represents.
exports.prettyDate = function(time, withoutShortFormat) {
	if (_.isString(time)) {
		var date = new Date((time || "").replace(/-/g, "/").replace(/[TZ]/g, " "));
	} else if (_.isDate(time)) {
		//time is {Date}
		date = time;
	} else {
		date = new Date();
	}
	var stringDate = Alloy.Globals.FormatUtils.getStringDBToDate();
	var diffDate = Alloy.Globals.FormatUtils.getDateToString(stringDate);
	var diff = (diffDate - date.getTime()) / 1000;
	var day_diff = Math.floor(diff / 86400);
	if (_.isNaN(day_diff) || !_.isNumber(day_diff) || day_diff < 0) {
		return L('just_now', 'Just now');
	} else {
		var pretty = '';

		diff = parseInt(diff);
		if (withoutShortFormat || day_diff == 0) {

			if (!withoutShortFormat) {
				switch(true) {
				case diff < 60:
					pretty = L('just_now', 'Just now');
					break;
				case diff < 120:
					pretty = L('one_min_ag', '1 minute ago');
					break;
				case diff < 3600:
					pretty = String.format(L('minutes_ago', '%s minutes ago'), Math.floor(diff / 60) + '');
					break;
				case diff < 7200:
					pretty = L('one_hour_ago', '1 hour ago');
					break;
				case diff < 86400:
					pretty = String.format(L('hours_ago', '%s hours ago'), Math.floor(diff / 3600) + '');
					break;
				}
			}else{
				pretty =  (require('FormatUtils')).getStringTimeToDate(date);
			}
		} else if (day_diff == 1) {
			pretty = L('yesterday', 'Yesterday');
		} else {
			pretty = (require('FormatUtils')).getAbStringToDate(date);
		}
		return pretty;

	}
	return '';
};

