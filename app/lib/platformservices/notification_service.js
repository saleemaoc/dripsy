
var unregister = function() {
	if (OS_IOS) {
		Ti.App.currentService.unregister();
	} else {
		Ti.Android.currentService.stop();
	}
};

var notificationService = require('services/NotificationService');

var _Alloy = Alloy;
notificationService.get(function() {
	_Alloy.Globals.FirstTimeFindNotification = false;
	unregister();
	_Alloy = null;
}, function() {

	unregister();

}, _Alloy);

