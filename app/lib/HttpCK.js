/**
 * @author jagu
 * @description Libreria para manejar servicios api.
 */
/** ------------------------
 Static fields
 ------------------------**/

/** ------------------------
 Object
 ------------------------**/
var HttpCK = function() {
	/** ------------------------
	 Fields
	 ------------------------**/
	this.MapRequest = [];
	this.TAGS = {};
	this.XHR = new (require('XHR'))();
};

/**
 * @param {String} url
 * @param {Object} data
 * @param {Function} success,
 * @param {Function} error,
 * @param {Object} options
 * @return HttpClient
 */
HttpCK.prototype.post = function(url, data, success, error, options) {
	var defaultOptions = {
		shouldAuthenticate : true,
		oAuthToken : (require('AppProperties')).getToken()
	};
	if (options) {
		_.extend(defaultOptions, options);
	}
	return this.XHR.post(url, data, success, error, defaultOptions);
};

/**
 * @param {String} tag
 * @param {String} url
 * @param {Object} data
 * @param {Function} success,
 * @param {Function} error,
 * @param {Object} options
 * @return HttpClient
 */
HttpCK.prototype.postByTag = function(tag, url, data, success, error, options) {
	var _this = this;
	var client = this.post(url, data, function(response) {
		_this.findInMapRequestAndRemoveByTag(tag);
		success(response);
	}, function(response) {
		_this.findInMapRequestAndRemoveByTag(tag);
		error(response);
	}, options);
	this.findInMapRequestAndRemoveByTag(tag);
	//Push on map request
	this.MapRequest.push({
		client : client,
		tag : tag
	});

	return client;
};

/**
 * @param {String} url
 * @param {Object} data
 * @param {Function} success,
 * @param {Function} error,
 * @param {Object} options
 * @return HttpClient
 */
HttpCK.prototype.put = function(url, data, success, error, options) {
	var defaultOptions = {
		shouldAuthenticate : true,
		oAuthToken : (require('AppProperties')).getToken()
	};
	if (options) {
		_.extend(defaultOptions, options);
	}
	return this.XHR.put(url, data, success, error, defaultOptions);
};

/**
 * @param {String} tag
 * @param {String} url
 * @param {Object} data
 * @param {Function} success,
 * @param {Function} error,
 * @param {Object} options
 * @return HttpClient
 */
HttpCK.prototype.putByTag = function(tag, url, data, success, error, options) {
	var _this = this;
	var client = this.put(url, data, function(response) {
		_this.findInMapRequestAndRemoveByTag(tag);
		success(response);
	}, function(response) {
		_this.findInMapRequestAndRemoveByTag(tag);
		error(response);
	}, options);
	this.findInMapRequestAndRemoveByTag(tag);
	//Push on map request
	this.MapRequest.push({
		client : client,
		tag : tag
	});

	return client;
};
/**
 * @param {String} url
 * @param {Object} params
 * @param {Function} success
 * @param {Function} error
 * @param {Object} options
 */
HttpCK.prototype.get = function(url, params, success, error, options) {
	(require('TiLog')).info('Token', (require('AppProperties')).getToken());
	var defaultOptions = {
		shouldAuthenticate : true,
		oAuthToken : (require('AppProperties')).getToken()
	};

	if (options) {
		_.extend(defaultOptions, options);
	}
	return this.XHR.get(_.isObject(params) ? url + (require('JsUtils')).serialize(params) : url, success, error, defaultOptions);
};

/**
 * @param {String} tag
 * @param {String} url
 * @param {Object} params
 * @param {Function} success
 * @param {Function} error
 * @param {Object} options
 */
HttpCK.prototype.getByTag = function(tag, url, params, success, error, options) {

	var _this = this;
	var client = this.get(url, params, function(response) {
		_this.findInMapRequestAndRemoveByTag(tag);
		success(response);
	}, function(response) {
		_this.findInMapRequestAndRemoveByTag(tag);
		error(response);
	}, options);
	this.findInMapRequestAndRemoveByTag(tag);
	//Push on map request
	this.MapRequest.push({
		client : client,
		tag : tag
	});

	return client;
};

/**
 * @param {String} url
 * @param {Function} success
 * @param {Function} error
 * @param {Object} options
 */
HttpCK.prototype.destroy = function(url, success, error, options) {

	var defaultOptions = {
		shouldAuthenticate : true,
		oAuthToken : (require('AppProperties')).getToken()
	};

	if (options) {
		_.extend(defaultOptions, options);
	}
	return this.XHR.destroy(url, success, error, defaultOptions);
};

/**
 * @param {String} tag
 * @param {String} url
 * @param {Function} success
 * @param {Function} error
 * @param {Object} options
 */
HttpCK.prototype.destroyByTag = function(tag, url, success, error, options) {
	var _this = this;
	var client = this.destroy(url, function(response) {
		_this.findInMapRequestAndRemoveByTag(tag);
		success(response);
	}, function(response) {
		_this.findInMapRequestAndRemoveByTag(tag);
		error(response);
	}, options);
	this.findInMapRequestAndRemoveByTag(tag);
	//Push on map request
	this.MapRequest.push({
		client : client,
		tag : tag
	});

	return client;
};

/**
 * abort all request
 */
HttpCK.prototype.abort = function() {
	for (var i = 0,
	    j = this.MapRequest.length; i < j; i++) {
		var request = this.MapRequest[i];
		this.abortRequest(request);
	};
	this.resetMapRequest();
};

/**
 * abort request
 * @param {Object} request
 */
HttpCK.prototype.abortRequest = function(request) {
	if (_.isObject(request) && _.isObject(request.client) && _.isFunction(request.client.abort)) {
		request.client.abort();
	};
};
/**
 * abort request by tag
 * @param String tag
 */
HttpCK.prototype.abortByTag = function(tag) {
	var request = this.findByTag(tag);

	this.abortRequest(request);
	this.removeRequestInMapRequest(request);

};

/**
 * find a request by tag in map request and remove
 * @param {String} tag
 */
HttpCK.prototype.findInMapRequestAndRemoveByTag = function(tag) {
	var request = this.findByTag(tag);
	if (!_.isNull(request)) {
		this.removeRequestInMapRequest(request);
	}
};

/**
 * remove request in map request
 * @param {Object} request
 */
HttpCK.prototype.removeRequestInMapRequest = function(request) {
	this.MapRequest = _.without(this.MapRequest, request);
};

/**
 * reset map request
 */
HttpCK.prototype.resetMapRequest = function() {
	this.MapRequest = [];
};

/**
 * find request by tag
 * @param {String} tag
 * @return {Object} request
 */
HttpCK.prototype.findByTag = function(tag) {
	var result = _.where(this.MapRequest, {
		tag : tag
	});
	if (_.isArray(result) && !_.isEmpty(result)) {
		return _.first(result);
	}
	return null;
};

/** ------------------------
 Services
 ------------------------**/

/**
 *EXAMPLE LOGIN
 * @param {Object} user
 * @param {Object} password
 * @param {Object} onSuccess
 * @param {Object} onError
 * @return HttpClient

 HttpCK.prototype.auth = function(user, password, onSuccess, onError) {

 return this.XHR.post(Alloy.CFG.url + Alloy.CFG.api.auth, {
 username : user,
 password : password
 }, onSuccess, onError, {
 shouldAuthenticate : false
 });

 };
 */

module.exports = HttpCK;
