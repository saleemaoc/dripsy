/**
 * @author Juan Ignacio Agu
 */

//Object
var NetworkManager = function() {
	this.showingErrorConnection = false;
	this.pendingRequest = null;
	this.pendingData = null;
	this.pendingError = null;
};

NetworkManager.prototype.showErrorConnection = function(pendingRequest, pendingData, pendingError) {
	if (!this.showingErrorConnection) {
		this.showingErrorConnection = true;
		this.pendingRequest = pendingRequest;
		this.pendingData = pendingData;
		this.pendingError = pendingError;
		var dialog = Ti.UI.createAlertDialog({
			message : L('check_connection', 'Oops! check your connection please'),
			canceledOnTouchOutside : false,
			persistent : OS_IOS,
			cancel : 0,
			buttonNames : [L('cancel', 'Cancel'), L('retry', 'Retry')]
		});

		var _this = this;
		dialog.addEventListener('click', function(e) {
			if (e.index == 1) {
				if (_.isObject(_this.pendingRequest)) {
					if (_.isObject(_this.pendingData)) {
						_this.pendingRequest.send(_this.pendingData);
					} else {
						_this.pendingRequest.send();
					}
				}

			} else {
				if (_.isFunction(_this.pendingError)) {
					_this.pendingError({
						code : 0
					});
				}
			}
			dialog.hide();
			_this.showingErrorConnection = false;
			_this.pendingRequest = null;
			_this.pendingData = null;
			_this.pendingError = null;
			//gc
			_this = null;
			dialog = null;
		});

		dialog.show();
		//gc
	}
};

NetworkManager.prototype.checkConnection = function(pendingRequest, pendingData, pendingError) {
	if (Titanium.Network.networkType === Titanium.Network.NETWORK_NONE) {
		this.showErrorConnection(pendingRequest, pendingData, pendingError);
		return false;
	} else {
		return true;
	}
};

module.exports = NetworkManager;
