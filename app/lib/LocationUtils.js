/**
 * @author jagu
 * utils for location
 */

/**
 * set current coords
 */
exports.setCoords = function(coords) {
	Alloy.Globals.CoordLatitude = coords.latitude;
	Alloy.Globals.CoordLongitude = coords.longitude;
};

/**
 * return if exist location
 * @return {Boolean}
 */
exports.hasLocation = function() {
	return (!_.isNull(Alloy.Globals.CoordLatitude) && !_.isUndefined(Alloy.Globals.CoordLatitude)) && (!_.isNull(Alloy.Globals.CoordLongitude) && !_.isUndefined(Alloy.Globals.CoordLongitude));
};

exports.questionOpenSettings = function() {
	var dialog = Titanium.UI.createAlertDialog({
		title : L('configure_gps', 'Configure GPS'),
		message : L('configure_gps_message', 'GPS is OFF, enable it in Settings'),
		buttonNames : [L('cancel', 'Cancel'), L('open_settings', 'Open settings')],
		cancel : 0
	});

	dialog.addEventListener('click', function(e) {
		switch(e.index) {
		case 1:
			openSettings();
			break;
		}
	});

	dialog.show();
};
/**
 * open settings to active GPS
 */
var openSettings = function() {
	var settingsIntent = Titanium.Android.createIntent({
		action : 'android.settings.LOCATION_SOURCE_SETTINGS'
	});
	Ti.Android.currentActivity.startActivity(settingsIntent);
};

exports.openSettings = openSettings;
