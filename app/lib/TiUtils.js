/**
 * @author jagu
 */

/**
 * create simple alert
 * @param String message
 */
exports.alert = function(message, title, onSuccess) {
	var dialog = Titanium.UI.createAlertDialog({
		message : message,
		buttonNames : [L('accept', 'Accept')]
	});

	if (_.isString(title)) {
		dialog.title = title;
	}
	if (_.isFunction(onSuccess)) {
		dialog.destructive = false;
		dialog.addEventListener('click', onSuccess);
	}
	dialog.show();
};

/**
 * create simple alert
 * @param String message
 */
exports.questionAlert = function(message, title, onSuccess) {
	var dialog = Titanium.UI.createAlertDialog({
		message : message,
		cancel : 0,
		buttonNames : [L('cancel', 'Cancel'), L('accept', 'Accept')]
	});

	if (_.isString(title)) {
		dialog.title = title;
	}
	if (_.isFunction(onSuccess)) {
		dialog.destructive = false;
		dialog.addEventListener('click', function(e) {
			if (e.index == 1) {
				onSuccess();
			}
		});
	}
	dialog.show();
};
/**
 * show a toast
 * @param {Object} message
 */
var toast = function(message) {
	if (OS_IOS) {
		Titanium.UI.createAlertDialog({
			title : '',
			message : message
		}).show();
	} else {
		var toast = Ti.UI.createNotification({
			message : message,
			duration : Ti.UI.NOTIFICATION_DURATION_SHORT
		});
		toast.show();
	}
};

exports.toast = toast;
exports.getGeoLocation = function() {
	if (Titanium.Geolocation.locationServicesEnabled) {
		var locationCallback = function(e) {
			if (e.success) {
				var longitude = e.coords.longitude;
				var latitude = e.coords.latitude;
				var accuracy = e.coords.accuracy;

			}
		};
		Titanium.Geolocation.addEventListener('location', locationCallback);
	} else {
		alert('Your device has geo turned off - turn it on');
	}

};

exports.setDefaultValuesToGeoLocation = function() {
	Titanium.Geolocation.accuracy = Titanium.Geolocation.ACCURACY_BEST;

};

/**
 *
 * @param {Object} string
 * @param {Object} length
 */
exports.parseStringLimitLength = function(string, length) {
	if (string.length > length) {
		return (string.substr(0, (length - 1))) + '...';
	}
	return string;
};

/**
 * open a url
 * @param {String} url
 */
exports.openUrl = function(url) {
	Ti.Platform.openURL(url);
};

/**
 * call phone
 * @param {Object} number
 */
exports.callPhone = function(number) {
	if (_.isEmpty(number)) {
		number = number.trim();
		var call = 'tel:' + number;
		if (OS_IOS) {
			Titanium.Platform.openURL(call);
		} else {

			var intent = Ti.Android.createIntent({
				action : Ti.Android.ACTION_CALL,
				data : call
			});
			Ti.Android.currentActivity.startActivity(intent);
		}
	}
};

// convert dp to pixel.
var dpToPixel = function(dp) {
	return (parseInt(dp) * (Titanium.Platform.displayCaps.dpi / 160));
};

exports.dpToPixel = dpToPixel;

// convert pixel to dp.
var pixelToDp = function(px) {
	return (parseInt(px) / (Titanium.Platform.displayCaps.dpi / 160)) + 'dp';

};

exports.pixelToDp = pixelToDp;
/**
 * Calculate background transparency when user scrolling
 * @param {Object} e
 * @return {String} color
 */
exports.calculateActionBarBackgroundColor = function(e, height) {
	if (_.isNull(height) || _.isUndefined(height)) {
		//set default height
		height = 500;
	} else if (_.isString(height)) {
		//if height param has dp
		var hasDp = height.indexOf('dp');
		if (hasDp != -1) {
			height = dpToPixel(height.replace('dp', ''));
		}
	}
	var alpha = "";

	// I want to apply the fading effect to the first {height} pixels of scrolling
	// So, I'm only setting the alpha channel for 0 <= y < 500
	if (e.y < height && e.y >= 0) {

		//calculate the opacity factor and convert it to hexadecimal
		alpha = (Math.round(e.y / height * 255).toString(16));

		// add leading zero if needed
		(alpha.length === 1) && ( alpha = '0' + alpha);
	}
	return '#' + alpha + Alloy.Globals.Colors.action_bar_transparent_without_alpha;
};

/**
 * get blob by path
 * @param {String} path
 * @return {Ti.Blob}
 */
exports.getBlobToPath = function(path) {

	var f = Ti.Filesystem.getFile(path);
	var blob = f.read();

	return blob;
};
