exports.get = function(params) {

	var men = {
		results : [{
			establishment : 1,
			user : {
				id : 1,
				first_name : 'Pablo',
				last_name : 'Flores',
				gender : 1,
				work : 'Clarika',
				occupation : 'Mobile developer',
				photo : 'https://s-media-cache-ak0.pinimg.com/564x/02/25/46/022546c30df06f84f2426ea9192bca8b.jpg'
			},
			type_presence : 1,
		}, {
			establishment : 1,
			user : {
				id : 2,
				first_name : 'Gerardo',
				last_name : 'Boiero',
				gender : 1,
				work: 'Clarika',
				occupation : 'Technical leader',
				photo : 'https://ae01.alicdn.com/kf/HTB1_P2LKpXXXXceXpXXq6xXFXXXA/2016-patchwork-font-b-polo-b-font-men-blue-grey-black-classic-casual-fashion-style-slim.jpg'
				
			},
			type_presence : 2
		}, {
			establishment : 1,
			user : {
				id : 3,
				first_name : 'Ricardo',
				last_name : 'Matuk',
				gender : 1,
				work: 'Clarika',
				occupation : 'CEO',
				photo : 'http://www.czcfct.com/image/cache/data/product_pic/201605/1008/ebee5ff0d0b5c0c3f5a464ceb71f776e-600x600.jpg'
			},
			type_presence : 1,
		}, {
			establishment : 1,
			user : {
				id : 4,
				first_name : 'Andres',
				last_name : 'Gallardo',
				gender : 1,
				work: 'Clarika',
				occupation : 'Developer backend',
				photo : 'http://www.abeer-iq.com/ups/uploads/144719477428376.jpg'
			},
			type_presence : 2,
		}, {
			establishment : 1,
			user : {
				id : 5,
				first_name : 'Juan',
				last_name : 'Agu',
				work: 'Clarika',
				occupation : 'Mobile Developer',
				gender : 1
			},
			type_presence : 2,
		}]
	};

	var women = {
		results : [{
			establishment : 1,
			user : {
				id : 6,
				first_name : 'Carolina',
				last_name : 'Lozada',
				gender : 2,
				work: 'Clarika',
				occupation : 'CEO',
				photo : ''
			},
			type_presence : 1,
		}, {
			establishment : 1,
			user : {
				id : 7,
				first_name : 'Sofia',
				last_name : 'Maspero',
				gender : 2,
				work: 'Clarika',
				occupation : 'RRHH',
				photo : 'https://pbs.twimg.com/media/CkrnDXKVAAAU8V3.jpg'
			},
			type_presence : 2
		}, {
			establishment : 1,
			user : {
				id : 8,
				first_name : 'Jesica',
				last_name : 'Prado',
				gender : 2,
				work: 'Clarika',
				occupation : 'QA',
				photo : 'https://s-media-cache-ak0.pinimg.com/564x/61/cf/18/61cf1887a1aaf72e3734c57bfcf3e5c5.jpg'
			},
			type_presence : 2,
		}, {
			establishment : 1,
			user : {
				id : 9,
				first_name : 'Lucia',
				last_name : 'Garbarino',
				gender : 2,
				work: 'Clarika',
				occupation : 'UX-UI',
				photo : 'https://s-media-cache-ak0.pinimg.com/564x/65/57/26/6557264bc28d85e41132ccb974404cb3.jpg'
			},
			type_presence : 1,
		}, {
			establishment : 1,
			user : {
				id : 10,
				first_name : 'Alejandra',
				last_name : 'Aliaga',
				gender : 2,
				work: 'Clarika',
				occupation : 'UX-UI',
				photo : 'http://2.bp.blogspot.com/-ZiReHplfIVw/VDAthH60vXI/AAAAAAAAAWU/HqmAs1skD5U/s1600/Cool-And-Stylish-Girls-Dp-For-Facebook-ownfunmaza103.jpg'
			},
			type_presence : 1,
		}]
	};
	if (params.gender == Alloy.CFG.constants.gender.male) {
		return men;
	} else {
		return women;
	}
};