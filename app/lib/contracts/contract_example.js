/**
 * @author jagu
 * contract for example model
 */

/**
 * simulate get response
 */
exports.get = function() {
	var example = {
		name : 'Example ',
		description : 'Description to example'
	};
	var result = [];
	for (var i = 1,
	    j = 100; i < j; i++) {
		result.push({
			id : i,
			name : example.name + i,
			description : example.description + i
		});
	};

	return result;
};
