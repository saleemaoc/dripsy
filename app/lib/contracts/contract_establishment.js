/**
 * @author jagu
 * contract for example model
 */

/**
 * simulate get response
 */
exports.get = function() {

	return {

		results : [{
			id : 1,
			name : 'Marquee New York',
			description : 'Highest ranked New York nightclub',
			address : '289 10th Ave',
			city_id : 1,
			latitude : '40.750041',
			longitude : '-74.002763',
			image : 'http://www.stopbreathebump.com/wp-content/uploads/2013/04/733820_165264006966415_1480498674_n.jpg',
			state : 1,
			website : 'http://marqueeny.com/',
			phone_number : '646-473-0202',
			ranking : 4,
			hour_wednesday_from : '23:00:00Z',
			hour_wednesday_to : '04:00:00Z',
			hour_friday_from : '23:00:00Z',
			hour_friday_to : '04:00:00Z',
			hour_saturday_from : '23:00:00Z',
			hour_saturday_to : '04:00:00Z',
			type : 2,
		}, {
			id : 2,
			name : 'Stage 48',
			description : 'Stage 48 is a multi-use venue that hosts Concerts, Night Life, and Private/Corporate',
			address : '605 W 48th St, New York, NY 10036, Estados Unidos',
			city_id : 1,

			latitude : '40.7650548',

			longitude : '-74.0660259',

			image : 'http://www.stage48events.com/wp-content/uploads/2014/04/STAGE48_281.jpg',

			state : 1,

			website : 'http://stage48.com/',

			phone_number : '+1 212-957-1800',

			ranking : 3,

			hour_monday_from : '00:00:00Z',

			hour_monday_to : '00:00:00Z',

			hour_tuesday_from : '00:00:00Z',

			hour_tuesday_to : '00:00:00Z',

			hour_wednesday_from : '00:00:00Z',

			hour_wednesday_to : '00:00:00Z',

			hour_thursday_from : '00:00:00Z',

			hour_thursday_to : '00:00:00Z',

			hour_friday_from : '00:00:00Z',

			hour_friday_to : '00:00:00Z',

			hour_saturday_from : '00:00:00Z',

			hour_saturday_to : '00:00:00Z',

			hour_sunday_from : '00:00:00Z',

			hour_sunday_to : '00:00:00Z',

			type : 2,

		}, {

			id : 3,

			name : 'Cielo',

			description : 'Dance Clubs & Discos, Nightlife',

			address : " 18 Little W 12th St ",

			city_id : 1,

			latitude : ' 40.742574 ',

			longitude : ' -74.007367 ',

			image : 'https://www.nitetables.com/master_admin/uploads/large/1389818006_121_1313771858_cielo_photo_5.jpeg',

			state : 1,

			website : 'http://www.cieloclub.com/',

			phone_number : '+1 646-543-8556',

			ranking : 4,

			hour_monday_from : '22:00:00Z',

			hour_monday_to : '04:00:00Z',

			hour_tuesday_from : '22:00:00Z',

			hour_tuesday_to : '04:00:00Z',

			hour_wednesday_from : '22:00:00Z',

			hour_wednesday_to : '04:00:00Z',

			hour_thursday_from : '22:00:00Z',

			hour_thursday_to : '04:00:00Z',

			hour_friday_from : '22:00:00Z',

			hour_friday_to : '04:00:00Z',

			hour_saturday_from : '22:00:00Z',

			hour_saturday_to : '04:00:00Z',

			hour_sunday_from : '22:00:00Z',

			hour_sunday_to : '04:00:00Z',

			type : 2,

		}, {

			id : 4,

			name : "Rudy's",

			description : 'Seating, Television, Wheelchair Accessible, Serves Alcohol, Full Bar, Free Wifi',

			address : ' 627 9th Ave, New York, NY 10036, Estados Unidos',

			city_id : 1,

			latitude : '40.7600077',

			longitude : '-74.0618061',

			image : 'https://static.wixstatic.com/media/f5a6d5_1eb805f0690645e5842347261c2e8b7f.jpg/v1/fill/w_979,h_743,al_c,q_85,usm_0.66_1.00_0.01/f5a6d5_1eb805f0690645e5842347261c2e8b7f.jpg',

			state : 1,

			website : 'http://www.rudysbarnyc.com/',

			phone_number : '646-707-0890',

			ranking : 4,

			hour_monday_from : '20:00:00Z',

			hour_monday_to : '04:00:00Z',

			hour_tuesday_from : '20:00:00Z',

			hour_tuesday_to : '04:00:00Z',

			hour_wednesday_from : '20:00:00Z',

			hour_wednesday_to : '04:00:00Z',

			hour_thursday_from : '20:00:00Z',

			hour_thursday_to : '04:00:00Z',

			hour_friday_from : '20:00:00Z',

			hour_friday_to : '04:00:00Z',

			hour_saturday_from : '20:00:00Z',

			hour_saturday_to : '04:00:00Z',

			hour_sunday_from : '00:00:00Z',

			hour_sunday_to : '04:00:00Z',

			type : 1,

		}, {

			id : 5,

			name : "Foley's NY",

			description : 'Seating, Waitstaff, Television, Wheelchair Accessible, Serves Alcohol, Full Bar, Reservations ',

			address : '18 West 33rd Street, New York, NY 10001, Estados Unidos',

			city_id : 1,

			latitude : '40.7480694',

			longitude : '-74.0563487',

			image : 'https://static.wixstatic.com/media/d2a66b_da72f87e8819c5fffd47081cb8feafc7.jpg/v1/fill/w_784,h_521,al_c,q_90,usm_0.66_1.00_0.01/d2a66b_da72f87e8819c5fffd47081cb8feafc7.jpg',

			state : 1,

			website : 'http://www.foleysny.com/',

			phone_number : '+1 212-290-0080',

			ranking : 4,

			hour_monday_from : '22:00:00Z',

			hour_monday_to : '02:00:00Z',

			hour_tuesday_from : '22:00:00Z',

			hour_tuesday_to : '02:00:00Z',

			hour_wednesday_from : '22:00:00Z',

			hour_wednesday_to : '02:00:00Z',

			hour_thursday_from : '22:00:00Z',

			hour_thursday_to : '02:00:00Z',

			hour_friday_from : '22:00:00Z',

			hour_friday_to : '03:00:00Z',

			hour_saturday_from : '22:00:00Z',

			hour_saturday_to : '03:00:00Z',

			hour_sunday_from : '22:00:00Z',

			hour_sunday_to : '02:00:00Z',

			type : 1,

		}, {

			id : 6,

			name : 'Ayza Wine & Chocolate Bar',

			description : 'Takeout, Seating, Waitstaff, Wheelchair Accessible, Serves Alcohol, Full Bar, Reservations ',

			address : '11 W 31st St, New York, NY 10001, Estados Unidos',

			city_id : 1,

			latitude : '40.7470188',

			longitude : '-74.0568989',

			image : 'http://ayzanyc.com/wp-content/uploads/sites/2/2015/01/pic-07.jpg',

			state : 1,

			website : 'http://ayzanyc.com/',

			phone_number : '+1 212-714-2992',

			ranking : 4,

			hour_monday_from : '12:00:00Z',

			hour_monday_to : '23:00:00Z',

			hour_tuesday_from : '12:00:00Z',

			hour_tuesday_to : '02:00:00Z',

			hour_wednesday_from : '12:00:00Z',

			hour_wednesday_to : '02:00:00Z',

			hour_thursday_from : '12:00:00Z',

			hour_thursday_to : '02:00:00Z',

			hour_friday_from : '12:00:00Z',

			hour_friday_to : '02:00:00Z',

			hour_saturday_from : '12:00:00Z',

			hour_saturday_to : '02:00:00Z',

			hour_sunday_from : '14:30:00Z',

			hour_sunday_to : '23:00:00Z',

			type : 1,

		}, {

			id : 7,

			name : 'Bar Americain',

			description : 'Reservations, Seating, Waitstaff, Wheelchair Accessible, Serves Alcohol, Full Bar, Private Dining',

			address : '152 W 52nd St, New York, NY 10019, Estados Unidos',

			city_id : 1,

			latitude : '40.7617609',

			longitude : '-74.0517644',

			image : 'http://mgandcompany.com/wp-content/gallery/bar-american/bar-american2.jpg',

			state : 1,

			website : 'http://baramericain.com/nyc/',

			phone_number : '+1 212-265-9700',

			ranking : 4,

			hour_monday_from : '17:00:00Z',

			hour_monday_to : '23:00:00Z',

			hour_tuesday_from : '17:00:00Z',

			hour_tuesday_to : '23:00:00Z',

			hour_wednesday_from : '17:00:00Z',

			hour_wednesday_to : '23:00:00Z',

			hour_thursday_from : '17:00:00Z',

			hour_thursday_to : '23:00:00Z',

			hour_friday_from : '17:00:00Z',

			hour_friday_to : '23:30:00Z',

			hour_saturday_from : '17:00:00Z',

			hour_saturday_to : '23:30:00Z',

			hour_sunday_from : '17:00:00Z',

			hour_sunday_to : '23:00:00Z',

			type : 1,

		}, {

			id : 8,

			name : "Murray's Cheese Bar",

			description : 'Free Wifi, Takeout, Reservations, Seating, Waitstaff, Serves Alcohol, Wine and Beer, Wheelchair Accessible ',

			address : '264 Bleecker St, New York, NY 10014, Estados Unidos',

			city_id : 1,

			latitude : '40.7311938',

			longitude : '-74.0732039',

			image : 'http://www.flyingfourchette.com/wp-content/uploads/2015/01/Murrays-Cheese-Bar-2.jpg',

			state : 1,

			website : 'http://www.murrayscheesebar.com/',

			phone_number : '646-476-8882',

			ranking : 4,

			hour_monday_from : '17:00:00Z',

			hour_monday_to : '22:00:00Z',

			hour_tuesday_from : '12:00:00Z',

			hour_tuesday_to : '22:00:00Z',

			hour_wednesday_from : '12:00:00Z',

			hour_wednesday_to : '00:00:00Z',

			hour_thursday_from : '12:00:00Z',

			hour_thursday_to : '00:00:00Z',

			hour_friday_from : '12:00:00Z',

			hour_friday_to : '00:00:00Z',

			hour_saturday_from : '11:00:00Z',

			hour_saturday_to : '22:00:00Z',

			hour_sunday_from : '11:00:00Z',

			hour_sunday_to : '22:00:00Z',

			type : 1,

		}, {

			id : 9,

			name : 'Webster hall',

			description : "is one of North America's most prestigious and historic entertainment venues operating continuously as a performance hall and nightclub since 1886",

			address : '10038,, 125 E 11th St, New York, NY 10003, Estados Unidos',

			city_id : 1,

			latitude : '40.7316963',

			longitude : '-74.0593432',

			image : 'http://whizbiz.net/uploads/gallery/Webster_Hall.jpg',

			state : 1,

			website : 'http://www.websterhall.com/',

			phone_number : '(212) 353.1600',

			ranking : 3,

			hour_monday_from : '00:00:00Z',

			hour_monday_to : '00:00:00Z',

			hour_tuesday_from : '00:00:00Z',

			hour_tuesday_to : '00:00:00Z',

			hour_wednesday_from : '00:00:00Z',

			hour_wednesday_to : '00:00:00Z',

			hour_thursday_from : '00:00:00Z',

			hour_thursday_to : '00:00:00Z',

			hour_friday_from : '00:00:00Z',

			hour_friday_to : '00:00:00Z',

			hour_saturday_from : '00:00:00Z',

			hour_saturday_to : '00:00:00Z',

			hour_sunday_from : '00:00:00Z',

			hour_sunday_to : '00:00:00Z',

			type : 1,

		}, {

			id : 10,

			name : 'Terminal 5',

			description : 'Terminal 5 is a music venue. It has a multi-level event site with five distinct room environments. Alcoholic beverages are served during events along with light snacks. On most nights, a smoking section and bar are open on the roof deck.',

			address : '610 W 56th St, New York, NY 10019, Estados Unidos',

			city_id : 1,

			latitude : '40.7696889',

			longitude : '-74.0627546',

			image : 'http://www.boweryevents.com/wp/wp-content/uploads/2013/02/Photo12.jpg',

			state : 1,

			website : 'http://www.terminal5nyc.com/',

			phone_number : '+1 212-582-6600',

			ranking : 3,

			hour_monday_from : '00:00:00Z',

			hour_monday_to : '00:00:00Z',

			hour_tuesday_from : '00:00:00Z',

			hour_tuesday_to : '00:00:00Z',

			hour_wednesday_from : '00:00:00Z',

			hour_wednesday_to : '00:00:00Z',

			hour_thursday_from : '00:00:00Z',

			hour_thursday_to : '00:00:00Z',

			hour_friday_from : '00:00:00Z',

			hour_friday_to : '00:00:00Z',

			hour_saturday_from : '00:00:00Z',

			hour_saturday_to : '00:00:00Z',

			hour_sunday_from : '00:00:00Z',

			hour_sunday_to : '00:00:00Z',

			type : 1,

		}, {

			id : 11,

			name : 'The Copacabana Times Square',

			description : 'Club nocturno que ofrece cócteles legendarios, música en vivo y una pista de baile en la azotea en un ambiente animado. Frank Sinatra, Sammy Davis Jr., The Supremes, Sam Cooke y Robin Williams han realizado en la Copa.',

			address : '268 W 47th St, New York, NY 10036, Estados Unidos',

			city_id : 1,

			latitude : '40.7601257',

			longitude : '-74.0572735',

			image : 'https://media-cdn.tripadvisor.com/media/photo-s/09/fa/41/cf/external-look.jpg',

			state : 1,

			website : 'http://copacabanany.com/',

			phone_number : '+1 212-221-2672',

			ranking : 3,

			hour_tuesday_from : '18:00:00Z',

			hour_tuesday_to : '00:00:00Z',

			hour_friday_from : '22:00:00Z',

			hour_friday_to : '04:00:00Z',

			hour_saturday_from : '22:00:00Z',

			hour_saturday_to : '04:00:00Z',

			type : 2,

		}, {

			id : 12,

			name : 'Express Cocina Bar',

			description : 'Especialistas en Pizza a la Piedra - Calzone Napolitano - Cerveza Tirada - Chopp - Balón - Tanque Wafles - Tortas',

			address : 'Rivadavia 170, 4220 Termas de Río Hondo, Santiago del Estero',

			city_id : 3,

			latitude : '-27.4982495',

			longitude : '-64.9296543',

			image : 'http://www.termasderiohondo.com/Express/fotos/Bar-Express-las-termas-de-rio-hondo-santiago-del-estero-37.jpg',

			state : 1,

			website : 'http://lastermasderiohondo.com/Express/',

			phone_number : '03858 42-3323',

			ranking : 3,

			hour_monday_from : '08:30:00Z',

			hour_monday_to : '02:00:00Z',

			hour_tuesday_from : '08:30:00Z',

			hour_tuesday_to : '02:00:00Z',

			hour_wednesday_from : '08:30:00Z',

			hour_wednesday_to : '02:00:00Z',

			hour_thursday_from : '08:30:00Z',

			hour_thursday_to : '02:00:00Z',

			hour_friday_from : '08:30:00Z',

			hour_friday_to : '02:00:00Z',

			hour_saturday_from : '08:30:00Z',

			hour_saturday_to : '02:00:00Z',

			hour_sunday_from : '08:30:00Z',

			hour_sunday_to : '02:00:00Z',

			type : 2,

		}, {

			id : 13,

			name : 'Flop Disco',

			description : 'Club nocturno está dirigido hacia los adultos, hacia personas mayores de 21 años.',

			address : 'Independencia y Marconi 4200 Santiago del Estero',

			city_id : 3,

			latitude : '-27.8108739',

			longitude : '-64.2402932',

			image : 'https://fbcdn-sphotos-g-a.akamaihd.net/hphotos-ak-xtp1/v/t1.0-9/13322125_1083381268367838_1472216390907805505_n.jpg?oh=8d5aa0832412e68d915245a156cdbfc6&oe=58816E7E&__gda__=1480764760_9ff8d067cd1f30d696ff31c932197db9',

			state : 1,

			website : 'http://www.flopdisco.com.ar/',

			phone_number : '0385 487-3068',

			ranking : 3,

			hour_monday_from : '00:00:00Z',

			hour_monday_to : '00:00:00Z',

			hour_tuesday_from : '00:00:00Z',

			hour_tuesday_to : '00:00:00Z',

			hour_wednesday_from : '00:00:00Z',

			hour_wednesday_to : '00:00:00Z',

			hour_thursday_from : '00:00:00Z',

			hour_thursday_to : '00:00:00Z',

			hour_friday_from : '00:00:00Z',

			hour_friday_to : '00:00:00Z',

			hour_saturday_from : '00:00:00Z',

			hour_saturday_to : '00:00:00Z',

			hour_sunday_from : '00:00:00Z',

			hour_sunday_to : '00:00:00Z',

			type : 1,

		}, {

			id : 14,

			name : 'Johnny B Good',

			description : 'Restaurant Temático Musical.',

			address : 'Avenida Rafael Nuñez 4791',

			city_id : 2,

			latitude : '-31.360357',

			longitude : '-64.3087684',

			image : 'http://cdn.resermap.com/restaurant/johnny-b-good-bo-cerro-de-las-rosas-541c88df95414_original.jpeg',

			state : 1,

			website : 'http://www.jbgood.com/wp/',

			phone_number : '0351 482-9410',

			ranking : 3,

			hour_monday_from : '07:00:00Z',

			hour_monday_to : '02:00:00Z',

			hour_tuesday_from : '07:00:00Z',

			hour_tuesday_to : '02:00:00Z',

			hour_wednesday_from : '07:00:00Z',

			hour_wednesday_to : '02:00:00Z',

			hour_thursday_from : '07:00:00Z',

			hour_thursday_to : '04:00:00Z',

			hour_friday_from : '07:00:00Z',

			hour_friday_to : '04:00:00Z',

			hour_saturday_from : '10:00:00Z',

			hour_saturday_to : '05:00:00Z',

			hour_sunday_from : '10:00:00Z',

			hour_sunday_to : '02:00:00Z',

			type : 2,

		}, {

			id : 15,

			name : 'Unplugged - Live Music Bar',

			description : 'UNPlugged, un espacio diferente y pensado para disfrutar entre amigos... Un Club con resto y cocteleria premium... Formato de Bandas, eventos temáticos y Djs para vivir noches a pura fiesta hasta 05 am',

			address : 'Av. Rafael Nuñez 4812 - 5000 Córdoba',

			city_id : 2,

			latitude : '-31.3601227',

			longitude : '-64.2405191',

			image : 'https://fbcdn-sphotos-a-a.akamaihd.net/hphotos-ak-xfp1/v/t1.0-9/13600112_1100206416682659_5676662031778448741_n.jpg?oh=f0bc5dc21c3337a7d4857f32166f3e0c&oe=586F6243&__gda__=1483723795_48341d0bf2535d1defe62166664f1634',
			state : 1,
			website : '',
			phone_number : '0351 609-2055',
			ranking : 3,
			hour_thursday_from : '22:00:00Z',
			hour_thursday_to : '05:00:00Z',
			hour_friday_from : '22:00:00Z',
			hour_friday_to : '05:00:00Z',
			hour_saturday_from : '22:00:00Z',
			hour_saturday_to : '05:00:00Z',
			type : 1,
		}]
	};
};
