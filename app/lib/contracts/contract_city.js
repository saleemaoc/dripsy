/**
 * simulate get country response
 */
var results = [{
	id : 1,
	name : "New York",
	active : true,
	country_id : 2,
	country : {
		id : 2,
		name : "United States",
		active : true
	}
}, {
	id : 2,
	name : "Cordoba",
	active : true,
	country_id : 1,
	country : {
		id : 1,
		name : "Argentina",
		active : true
	}
}, {
	id : 3,
	name : "Santiago del Estero",
	active : true,
	country_id : 1,
	country : {
		id : 1,
		name : "Argentina",
		active : true
	}
}];

exports.getCity = function(params) {
	return {
		results : _.where(results, params)
	};
};
