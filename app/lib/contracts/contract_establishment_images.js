/**
 * @author jagu
 * contract for example model
 */

/**
 * simulate get response
 */
exports.get = function() {

	return {
		results : [{
			id : 1,
			photo : 'Barto',
			establishment_id : 1,
		}]
	};
};
