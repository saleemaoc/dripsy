/**
 * simulate get country response 
 */
exports.getCountry = function(){
	return {
		results:[{
			id:1,
			name: "Argentina",
			active:true
		},
		{
			id:2,
			name: "United States",
			active: true
		}]
	};
};