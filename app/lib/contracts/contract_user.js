/**
 * @author jagu
 * contract user
 */

/*
 * simulate success response
 */
exports.postRegistration = function() {
	return user;
};

exports.login = function(params, success, error) {
	if (params.username == user.mail && params.password == user.password) {
		success(response_login);
	} else {
		error({
			code : 401
		});
	}
};

var user = {
	first_name : 'Usuario',
	last_name : 'Demo',
	mail : 'clarika@clarika.com',
	password : 'passpass',
	token : '1',
	photo : 'https://www.prlog.org/11538132-avatar-color-dresses.jpg',
	nickname : 'Usuario demo',
	work : 'Clarika S.A',
	occupation : 'Developer',
	active : true,
	gender : 1,
	id : 1
};

exports.changePassword = function(params, succes, error){
	if (params.current_password == user.password && params.new_password == params.repeat_password) {
		succes(response);
	} else {
		error({
			code : 401
		});
	}
};

exports.sendMail = function(params, succes, error){
	if (params.email == user.mail) {
		succes(response);
	} else {
		error({
			code : 404
		});
	}
};

var response_login = user;
var response = user;