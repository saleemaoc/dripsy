/**
 * @author jagu
 * contract for example model
 */

/**
 * simulate get response
 */
exports.get = function() {

	return {

		results : [{
			id : 1,
			file : "",
			establishment_id : 1,
			primary : false
		}, {
			id : 2,
			file : "",
			establishment_id : 1,
			primary : false
		}, {
			id : 3,
			file : "",
			establishment_id : 1,
			primary : false
		}, {
			id : 4,
			file : "",
			establishment_id : 1,
			primary : false
		}]
	};
};
