var PATH = '/images/';

module.exports = {
	nn_iso : PATH + 'nn_iso.png',
	nn_clarika : PATH + 'nn_clarika.png',
	nn_background_color : PATH + 'nn_background_color.jpg',
	ic_facebook : PATH + 'ic_facebook.png',
	ic_money_24dp : PATH + 'ic_money_28dp.png',
	ic_action_chat_24dp : PATH + 'ic_action_chat_24dp.png',
	ic_action_pin_24dp : PATH + 'ic_action_pin_24dp.png',
	ic_clock_16dp : PATH + 'ic_clock_16dp.png',
	ic_clock_24dp : PATH + 'ic_clock_24dp.png',
	ic_star_16dp : PATH + 'ic_star_16dp.png',
	ic_star_24dp : PATH + 'ic_star_24dp.png',
	ic_star_normal_24dp : PATH + 'ic_star_normal_24dp.png',
	ic_action_search_24dp : PATH + 'ic_action_search_24dp.png',
	ic_ok_24dp : PATH + 'ic_ok_24dp.png',
	ic_photo_24dp : PATH + 'ic_photo_24dp.png',
	avatar_background_men : PATH + 'avatar_background_men.png',
	avatar_background_female : PATH + 'avatar_background_female.png',
	avatar_men : PATH + 'avatar_profile_men.jpg',
	avatar_woman : PATH + 'avatar_profile_woman.jpg',
	map_pin_annotation : PATH + 'map_pin.png',
	map_pin_annotation_selected : PATH + 'map_pin_selected.png',
	ic_link_24dp : PATH + 'ic_link_24dp.png',
	ic_action_cart : PATH + 'ic_action_cart_32dp.png',
	//badges for chart
	ic_action_cart_1 : PATH + 'ic_action_cart_1_32dp.png',
	ic_action_cart_2 : PATH + 'ic_action_cart_2_32dp.png',
	ic_action_cart_3 : PATH + 'ic_action_cart_3_32dp.png',
	ic_action_cart_4 : PATH + 'ic_action_cart_4_32dp.png',
	ic_action_cart_5 : PATH + 'ic_action_cart_5_32dp.png',
	ic_action_cart_6 : PATH + 'ic_action_cart_6_32dp.png',
	ic_action_cart_7 : PATH + 'ic_action_cart_7_32dp.png',
	ic_action_cart_8 : PATH + 'ic_action_cart_8_32dp.png',
	ic_action_cart_9 : PATH + 'ic_action_cart_9_32dp.png',
	ic_action_cart_more : PATH + 'ic_action_cart_more_32dp.png',
	//
	ic_action_menu_24dp : PATH + 'ic_action_menu_24dp.png',
	ic_arrow_right : PATH + 'ic_arrow_right.png',
	ic_car_28dp : PATH + 'ic_car_28dp.png',
	ic_money_28dp : PATH + 'ic_money_28dp.png',
	ic_clock : PATH + 'ic_clock.png',
	ic_link : PATH + 'ic_link.png',
	ic_phone_green : PATH + 'ic_phone_green.png',
	ic_pin : PATH + 'ic_pin.png',
	ic_phone_white_28dp : PATH + 'ic_phone_white_28dp.png',
	nn_background_white_black : PATH + 'nn_background_white_black.jpg',
	ic_action_list_24dp : PATH + 'ic_action_list_24dp.png',
	ic_action_edit_24dp : PATH + 'ic_action_edit_24dp.png',
	ic_pin_accent : PATH + 'ic_pin_accent.png',
	ic_pin_accent : PATH + 'ic_pin_accent.png',
	ic_action_drink_24dp : PATH + 'ic_action_drink_24dp.png',
	ic_add_white_24dp : PATH + 'ic_add_24dp.png',
	ic_less_accent_24dp : PATH + 'ic_less_24dp.png',
	ic_more_accent_24dp : PATH + 'ic_more_24dp.png',
	ic_info_24dp : PATH + 'ic_info_24dp.png',
	ic_money_120dp : PATH + 'ic_money_120dp.png',
	ic_ticket_24dp : PATH + 'ic_ticket_24dp.png',
	ic_table_24dp : PATH + 'ic_table_24dp.png',
	ic_drink_24dp : PATH + 'ic_drink_24dp.png',
	ic_chat_24dp : PATH + 'ic_chat_24dp.png',
	ic_chat_120dp : PATH + 'ic_chat_120dp.png',
	ic_location_24dp : PATH + 'ic_location_24dp.png',
	ic_location_16dp : PATH + 'ic_location_16dp.png',
	ic_drink_120dp : PATH + 'ic_drink_120dp.png',
	ic_send_24dp : PATH + 'ic_send_24dp.png',
	ic_action_delete_24dp : PATH + 'ic_action_delete_24dp.png',
	ic_back : PATH + 'ic_back.png',
	ic_card : PATH + 'ic_card.png',
	ic_card_visa : PATH + 'ic_visa.png',
	ic_card_amex : PATH + 'ic_amex.png',
	ic_card_master_card : PATH + 'ic_master.png',
	ic_close : PATH + 'ic_close.png',
	default_drink : PATH + 'default_drink.jpg',
	big_default_drink : PATH + 'big_avatar_drink.jpg',
	/**
	 * create map ping programmatically
	 */
	getMapPinAnnotation : function() {
		var mainView = Ti.UI.createView({
			height : Alloy.Globals.Dimens.icon_size,
			width : Alloy.Globals.Dimens.icon_size,
			backgroundColor : 'transparent'
		});

		var transparentBorder = Ti.UI.createView({
			height : Alloy.Globals.Dimens.icon_size,
			width : Alloy.Globals.Dimens.icon_size,
			borderRadius : Alloy.Globals.Dimens.icon_size_half,
			borderWidth : Alloy.Globals.Dimens.icon_size_half,
			borderColor : Alloy.Globals.Colors.secondary_50,
			backgroundColor : Alloy.Globals.Colors.secondary_50
		});

		var circle = Ti.UI.createView({
			height : Alloy.Globals.Dimens.icon_small_size,
			width : Alloy.Globals.Dimens.icon_small_size,
			borderRadius : Alloy.Globals.Dimens.icon_small_size_half,
			borderWidth : Alloy.Globals.Dimens.icon_small_size_half,
			borderColor : Alloy.Globals.Colors.secondary,
			backgroundColor : Alloy.Globals.Colors.secondary
		});

		mainView.add(transparentBorder);
		mainView.add(circle);

		return mainView;
	}
};
