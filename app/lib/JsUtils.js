/**
 * functions utils
 */

/**
 * get guid
 * @param {String} _separator
 */
exports.getGUID = function(_separator) {
	var separator = '-';
	if (_.isString(_separator)) {
		separator = _separator;
	}
	function s4() {
		return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
	}

	return s4() + s4() + separator + s4() + separator + s4() + separator + s4() + separator + s4() + s4() + s4();
};

/*
 * get distance beetween two coords
 * @param {Object} coordsSource
 * @param {Object} coordsTarget
 * @param {String} unit
 */
exports.getDistanceToCoords = function(coordsSource, coordsTarget, unit) {

	//latitude
	var lat1 = coordsSource.latitude;
	var lat2 = coordsTarget.latitude;

	//longitude
	var lon1 = coordsSource.longitude;
	var lon2 = coordsTarget.longitude;

	var d = distance(lat1, lon1, lat2, lon2, unit);
	return d.toFixed(1);

};

//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//:::                                                                         :::
//:::  This routine calculates the distance between two points (given the     :::
//:::  latitude/longitude of those points). It is being used to calculate     :::
//:::  the distance between two locations using GeoDataSource (TM) prodducts  :::
//:::                                                                         :::
//:::  Definitions:                                                           :::
//:::    South latitudes are negative, east longitudes are positive           :::
//:::                                                                         :::
//:::  Passed to function:                                                    :::
//:::    lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)  :::
//:::    lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)  :::
//:::    unit = the unit you desire for results                               :::
//:::           where: 'M' is statute miles (default)                         :::
//:::                  'K' is kilometers                                      :::
//:::                  'N' is nautical miles                                  :::
//:::                                                                         :::
//:::  Worldwide cities and other features databases with latitude longitude  :::
//:::  are available at http://www.geodatasource.com                          :::
//:::                                                                         :::
//:::  For enquiries, please contact sales@geodatasource.com                  :::
//:::                                                                         :::
//:::  Official Web site: http://www.geodatasource.com                        :::
//:::                                                                         :::
//:::               GeoDataSource.com (C) All Rights Reserved 2015            :::
//:::                                                                         :::
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

var distance = function(lat1, lon1, lat2, lon2, unit) {
	var radlat1 = Math.PI * lat1 / 180;
	var radlat2 = Math.PI * lat2 / 180;
	var theta = lon1 - lon2;
	var radtheta = Math.PI * theta / 180;
	var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
	dist = Math.acos(dist);
	dist = dist * 180 / Math.PI;
	dist = dist * 60 * 1.1515;
	if (unit == "K") {
		dist = dist * 1.609344;
	}
	if (unit == "N") {
		dist = dist * 0.8684;
	}
	return dist;
};

exports.distance = distance;

/**
 * get day string to date
 * @param {Object} date
 * @return {String}
 */
exports.getDayStringToDate = function(date) {
	if (!_.isDate(date)) {
		date = new Date();
	}

	var day = date.getDay();

	var stringDay = '';
	switch(day) {
	case 0:
		stringDay = 'sunday';
		break;
	case 1:
		stringDay = 'monday';
		break;
	case 2:
		stringDay = 'tuesday';
		break;
	case 3:
		stringDay = 'wednesday';
		break;
	case 4:
		stringDay = 'thursday';
		break;
	case 5:
		stringDay = 'friday';
		break;
	case 6:
		stringDay = 'saturday';
		break;
	};

	return stringDay;
};

/*
 * Validate email
 */
exports.validateEmail = function(email) {
	var regex = /\b[A-Z0-9._%+-]+@(?:[A-Z0-9-]+\.)+[A-Z]{2,20}\b/gi;
	return (regex.test(email.trim()));
};

/**
 * calculate age by string date
 * @param {String} stringDateBirth
 */
exports.calculateAgeToStringDateBirth = function(stringDateBirth) {
	var today = new Date();
	var birthDate = new Date(stringDateBirth);
	var age = today.getFullYear() - birthDate.getFullYear();
	var m = today.getMonth() - birthDate.getMonth();
	if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
		age--;
	}
	return age;
};

/**
 *
 * @param {Object} s
 */
exports.hasWhiteSpace = function(s) {
	return s.indexOf(' ') >= 0;
};

/**
 * Creates an image view using Google's QR Code generator.
 * @param text The text to be encoded in the QR code
 * @param size The size of the QR code; Possible Sizes: 100x100, 150x150, 200x200, 250x250, 300x300, 350x350, 400x400, 500x500
 * @param {Function} error
 */
exports.createQRCodeImageView = function(text, size, error) {
	var url = 'http://chart.apis.google.com/chart?cht=qr&chs=' + size + '&chl=' + encodeURI(text) + '&chld=H|0';
	var width = size.split('x')[0],
	    height = size.split('x')[1];
	if (Ti.Android) {
		width += 'dp';
		height += 'dp';
	}
	var imageView = Ti.UI.createImageView({
		image : encodeURI(url),
		defaultImage : 'none',
		width : width,
		height : height
	});
	if (_.isFunction(error)) {
		imageView.addEventListener('error', error);
	}

	return imageView;

};

/**
 * serialize object
 * @param {Object} obj
 * @param {Object} prefix
 */
var serialize = function(obj, prefix) {
	var str = [];
	for (var p in obj) {
		if (obj.hasOwnProperty(p)) {
			var k = prefix ? prefix + "[" + p + "]" : p,
			    v = obj[p];
			str.push( typeof v == "object" ? serialize(v, k) : encodeURIComponent(k) + "=" + encodeURIComponent(v));
		}
	}
	return !_.isEmpty(str) ? '?' + str.join("&") : '';
};

exports.serialize = serialize;
exports.validateURL = function(url) {
	if (_.isString(url)) {
		var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
		'((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|' + // domain name
		'((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
		'(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
		'(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
		'(\\#[-a-z\\d_]*)?$', 'i');
		// fragment locator
		return pattern.test(url);
	} else {
		return false;
	}
};

/**
 * compare two dates without time and return true if the dates are the same
 * @param {Date} date1
 * @param {Date} date2
 * @return {boolean}
 */
exports.compareTwoDatesWithoutTime = function(date1, date2) {
	//Remove time
	date1.setHours(0, 0, 0, 0);
	date2.setHours(0, 0, 0, 0);

	return date1.getTime() == date2.getTime();
};

exports.convertPositiveToNegativeNumber = function(number) {
	return Math.abs(number) * -1;
};

exports.getYesterdayDate = function() {
	var now = new Date();
	now.setDate(now.getDate() - 1);
	return now;
};

exports.createURLToCDN = function(url, size) {

	if (Alloy.CFG.cdn) {
		url = Alloy.CFG.url_images.concat(size).concat("/").concat(url);
	} else {
		url = Alloy.CFG.url_images.concat("/").concat(url);
	}
	return url;
};
