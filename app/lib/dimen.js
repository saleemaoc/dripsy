//Util
// convert dp to pixel.
var dpToPixel = function(dp) {
	var density = OS_IOS ? 1 : Ti.Platform.displayCaps.logicalDensityFactor;
	return parseInt(parseInt(dp) * density);
};
// convert pixel to dp.
var pixelToDp = function(px) {
	var density = OS_IOS ? 1 : Ti.Platform.displayCaps.logicalDensityFactor;
	return parseInt((parseInt(px) / density )) + 'dp';
};

//Dimens
var dimens = {

	/**
	 * Device
	 */
	ios_status_bar : '10dp',

	/**
	 * Fonts
	 */

	font_big_text : '30sp',
	font_big_text_2 : '28sp',
	font_big_text_3 : '25sp',
	font_big_title : '24sp',
	font_title : '20sp',
	font_subheading : '14sp',
	font_subheading_2 : '18sp',
	font_body : '16sp',
	font_body_4 : '14sp',
	font_body_3 : '18sp',
	font_body_2 : '19sp',
	font_button : '16sp',
	font_caption : '12sp',
	font_label : '13sp',
	font_label_small : '10sp',
	font_tab : '14sp',
	font_button_small : '12sp',

	/**
	 * Components
	 */

	button_height : '45dp',
	button_image_size : '36dp',
	drawer_left_width : Ti.UI.FILL,
	tab_indicator_height : '3dp',
	fab_mini_touch_size : '42dp',
	fab_mini_touch_border_radius : '21dp',
	fab_mini_circle_size : '40dp',
	fab_mini_circle_border_radius : '20dp',
	fab_mini_icon_size : '22dp',
	component_width_two_columns : '48%',
	component_form_height_default : '48dp',
	search_bar_height : '44dp',
	text_area_height : '60dp',

	/**
	 * General
	 */

	padding_horizontal : '16dp',
	padding_horizontal_double : '32dp',
	padding_horizontal_half : '8dp',
	padding_vertical : '8dp',
	padding_vertical_double : '16dp',
	padding_top_32dp : '32dp',
	padding_vertical_half : '4dp',
	padding_top_24dp : '24dp',
	icon_size : '24dp',
	icon_size_half : '12dp',
	icon_small_size : '16dp',
	icon_small_size_half : '8dp',
	padding_fix_big_text : '5dp',

	padding_horizontal_form : '16dp',
	padding_horizontal_form_double : '32dp',
	padding_horizontal_form_half : '8dp',
	padding_vertical_form : '8dp',
	padding_vertical_form_double : '16dp',
	padding_left_to_small_icon : '24dp',

	height_px : function() {
		return Ti.Platform.displayCaps.platformHeight;
	},
	padding_top_half_height : function() {
		return pixelToDp(Alloy.Globals.Dimens.height_px() / 2);
	},
	/**
	 * Login
	 */

	login_logo_size : '80dp',
	login_components_top : "33%",
	/**
	 * Registration
	 */

	registration_photo_height : '250dp',
	registration_terms_agree_padding_left : '52dp',

	/**
	 * Menu
	 */

	menu_profile_header_height : '95dp',
	menu_profile_header_avatar_background_height : '300dp',
	menu_profile_header_avatar_size : '60dp',
	menu_profile_header_avatar_border_radius : '30dp',
	menu_profile_header_avatar_border_size : '68dp',
	menu_profile_header_avatar_border_border_radius : '34dp',
	menu_profile_header_info_left : '100dp',
	menu_section_height : '40dp',
	menu_section_separator_height : '1dp',
	menu_item_height : '40dp',
	menu_item_title_left : '52dp',

	/**
	 * Profile
	 */
	profile_header_avatar_background_size_in_px : function() {
		return Ti.Platform.displayCaps.platformWidth;
	},
	profile_header_avatar_size_in_px : function() {
		return Alloy.Globals.Dimens.profile_header_avatar_background_size_in_px() / 3;
	},
	profile_header_avatar_size : function() {
		return pixelToDp(Alloy.Globals.Dimens.profile_header_avatar_size_in_px());
	},
	profile_header_avatar_background_size : function() {
		var pixel = Alloy.Globals.Dimens.profile_header_avatar_background_size_in_px();
		return pixelToDp(pixel);
	},
	profile_header_avatar_border_radius : function() {
		return pixelToDp(Alloy.Globals.Dimens.profile_header_avatar_size_in_px() / 2);
	},
	profile_header_avatar_border_size_in_px : function() {
		return Alloy.Globals.Dimens.profile_header_avatar_size_in_px() + ( OS_ANDROID ? 20 : 12);
	},
	profile_header_avatar_border_size : function() {
		return pixelToDp(Alloy.Globals.Dimens.profile_header_avatar_border_size_in_px());
	},
	profile_header_avatar_border_border_radius : function() {
		return pixelToDp(Alloy.Globals.Dimens.profile_header_avatar_border_size_in_px() / 2);
	},
	profile_header_info_left : '100dp',
	profile_header_info_top : OS_ANDROID ? '60dp' : '40dp',
	profile_two_column_width : '48%',
	profile_header_avatar_size_half : function() {
		return pixelToDp(Alloy.Globals.Dimens.profile_header_avatar_border_size_in_px() / 2);
	},
	header_avatar_edition_top : function() {
		var top = Alloy.Globals.Dimens.profile_header_avatar_size_in_px();
		if (OS_ANDROID) {
			var density = OS_IOS ? 1 : Ti.Platform.displayCaps.logicalDensityFactor;
			top -= (72 * density);
		}
		return Alloy.Globals.JsUtils.convertPositiveToNegativeNumber(top);
	},

	/**
	 * Establishment
	 */

	establishment_list_item_template_height : '160dp',
	establishment_list_item_template_gradient_height : '80dp',
	establishment_detail_buttons_width : function() {
		return pixelToDp((Ti.Platform.displayCaps.platformWidth * 0.30));
	},

	/**
	 * Ranking
	 */

	ranking_start_size : '16dp',
	ranking_start_padding_horizontal : '1dp',

	/**
	 * Establishment map
	 */

	establishment_detail_map_height : '150dp',
	establishment_detail_label_info_left : '32dp',
	establishment_detail_description_height : '40dp',
	/**
	 * Map annotation user
	 */

	map_annotation_user_border_transparent_size : '64dp',
	map_annotation_user_border_transparent_border : '32dp',
	map_annotation_user_border_border : '27dp',
	map_annotation_user_border_size : '54dp',
	map_annotation_user_image_border : '25dp',
	map_annotation_user_image_size : '50dp',

	/**
	 * Establishment detail
	 */

	establishment_detail_header_height : '250dp',
	establishment_detail_money_icon_size : '30dp',
	establishment_detail_car_icon_size : '34dp',
	establishment_detail_photo_shadow_height : '50dp',

	/**
	 * User presence
	 */

	user_presence_item_height : '155dp',
	user_presence_image_size : '90dp',
	user_presence_image_border_radius : '45dp',
	user_presence_image_border_size : '98dp',
	user_presence_image_border_border_radius : '49dp',
	user_presence_state_circle_size : '10dp',
	user_presence_state_circle_border_radius : '5dp',
	user_presence_state_border_size : '18dp',
	user_presence_state_border_border_radius : '9dp',
	user_presence_item_width : '33%',
	user_presence_name_height : '42dp',
	/**
	 * Drink
	 */

	drink_row_height : '80dp',
	drink_row_info_padding_left : '80dp',
	drink_row_info_padding_right : '126dp',
	drink_row_image_border_size : '60dp',
	drink_row_image_border_border_radius : '30dp',
	drink_row_image_size : '52dp',

	drink_row_image_border_radius : '26dp',
	drink_row_recommended_size : '18dp',
	drink_row_recommended_size_half : '9dp',
	drink_row_recommended_image_size : '10dp',
	/**
	 * Empty view
	 */

	empty_image_size : '60dp',

	/**
	 * Separator
	 */

	separator_fix_padding_horizontal : '5dp',

	/**
	 * Cart drink
	 */

	cart_drink_item_padding_left : '40dp',
	cart_drink_item_padding_right : '80dp',
	cart_drink_item_height : '40dp',
	cart_drink_item_total_label_padding_top : '16dp',

	/**
	 * List empty
	 */
	list_empty_icon_size : '120dp',
	list_empty_label_padding_top : '40dp',
	list_empty_label_width : '180dp',

	/**
	 * Transaction
	 */
	tableview_row_transaction_height : '115dp',
	tableview_row_info_padding_left : '98dp',
	tableview_row_info_padding_right : '50dp',
	tableview_row_transaction_title_height : '22dp',
	tableview_row_transaction_description_height : '18dp',
	/**
	 * Gift
	 */
	tableview_row_gift_title_height : '22dp',
	tableview_row_gift_description_height : '18dp',
	/**
	 * Transaction detail
	 */

	transaction_detail_establishment_detail_height : '110dp',
	tableview_row_transaction_detail_height : '60dp',
	transaction_detail_qr_size : '210dp',
	transaction_detail_title_name_padding_left : '108dp',
	transaction_gift_detail_title_name_padding_left : '78dp',
	transaction_gift_detail_title_name_padding_right_to_fab:'56dp',
	transaction_detail_title_name_padding_right : '100dp',
	transaction_detail_item_padding_left : '60dp',
	transaction_detail_item_padding_right : '128dp',
	transaction_detail_item_currency_amount_width : '100dp',
	transaction_detail_item_height : '55dp',
	transaction_qr_generator : '150x150',
	transaction_qr_content_size : '200dp',
	transaction_total_label_padding_top : '16dp',

	/**
	 * Information
	 */
	information_logo_height : '100dp',
	information_version_padding_top : '65%',
	information_all_right_reserved_padding_top : '50dp',

	/**
	 * Forgot password
	 *
	 */
	forgot_password_forgot_first_label_padding_top : '100dp',

	/**
	 * conversation
	 */

	conversation_list_item_template_height : '120dp',
	conversation_list_item_photo_profile_size : '80dp',
	conversation_list_item_photo_profile_border_radius : '40dp',
	conversation_list_item_photo_profile_border_size : '86dp',
	conversation_list_item_photo_profile_border_border_radius : '43dp',
	conversation_list_item_info_padding_right : '44dp',
	conversation_list_item_info_padding_left : '116dp',
	conversation_list_item_state_size : '8dp',
	conversation_list_item_state_border_radius : '4dp',
	conversation_list_item_full_name_padding_horizontal : /*conversation_list_item_state_size+2*/'10dp',
	/**
	 * Message
	 */
	message_content_write_message_height : '68dp',
	message_content_read_message_padding : '45dp',
	message_content_read_message_padding_top : '20dp',
	message_user_photo_size : '70dp',
	message_user_photo_border_radius : '35dp',
	message_padding_to_user_photo : '30dp',
	message_txt_send_message_padding_right : '70dp',
	message_height : '30dp',
	message_send_text_height : '40dp',
	message_send_text_bottom : '2dp',
	/**
	 * Drink detail
	 */

	drink_detail_background_image_size : function() {
		return pixelToDp(Ti.Platform.displayCaps.platformWidth);
	},
	drink_detail_image_padding_top : '140dp',
	drink_detail_image_size : '180dp',
	drink_detail_image_size_border_radius : '90dp',
	drink_detail_image_size_border_size : '190dp',
	drink_detail_image_size_border_border_radius : '95dp',
	drink_detail_image_size_half : '95dp',
	drink_detail_name_padding_top : OS_ANDROID ? '65dp' : '50dp',

	/**
	 * Stripe
	 */
	stripe_card_number_field_width : OS_ANDROID ? '24%' : '22%',
	stripe_card_number_field_left : OS_ANDROID ? '1%' : '2%'

};

var platformHeight = Ti.Platform.displayCaps.platformHeight / Ti.Platform.displayCaps.logicalDensityFactor;

if (platformHeight <= 300) {
	_.extend(dimens, {
		establishment_detail_header_height : '120dp'
	});
} else if (platformHeight <= 450) {
	_.extend(dimens, {
		establishment_detail_header_height : '170dp'
	});
} else if (platformHeight <= 600) {
	_.extend(dimens, {
		establishment_detail_header_height : '190dp'
	});
} else if (platformHeight <= 720) {
	_.extend(dimens, {
		establishment_detail_header_height : '200dp'
	});
}
/**
 * specific resolutions
 */

if (Ti.Platform.displayCaps.platformHeight <= 568) {

	//iphone 4, iphone 5 and small android devices
	_.extend(dimens, {

		/**
		 * Fonts
		 */

		font_big_text : '26sp',
		font_big_text_2 : '24sp',
		font_big_text_3 : '22sp',
		font_big_title : '21sp',
		font_title : '18sp',

		/**
		 * Components
		 */

		fab_mini_touch_size : '38dp',
		fab_mini_touch_border_radius : '19dp',
		fab_mini_circle_size : '36dp',
		fab_mini_circle_border_radius : '18dp',
		fab_mini_icon_size : '14dp',

		/**
		 * Registration
		 */

		registration_photo_height : '200dp',

		/**
		 * Profile
		 */

		profile_header_info_top : OS_ANDROID ? '45dp' : '30dp',
		profile_fab_top : '205dp',

		/**
		 * User presence
		 */

		user_presence_image_size : '84dp',

		user_presence_image_border_radius : '42dp',
		user_presence_image_border_size : '90dp',
		user_presence_image_border_border_radius : '45dp',

	});
	//small devices
	if (OS_ANDROID) {
		_.extend(dimens, {
			/**
			 * Components
			 */
			button_height : '36dp',
			/**
			 * General
			 */
			padding_vertical : '4dp',
			padding_vertical_double : '8dp',
			padding_top_32dp : '16dp',
			padding_vertical_form : '4dp',
			padding_vertical_form_double : '8dp',

			/**
			 * Login
			 */
			login_components_top : "27%",
		});
	}
} else if (Ti.Platform.displayCaps.platformHeight >= 667 && Ti.Platform.displayCaps.platformHeight < 736) {

	if (OS_IOS) {
		//iphone 6
		_.extend(dimens, {
			establishment_detail_header_height : '220dp',
			/**
			 * List empty
			 */
			list_empty_label_width : '180dp',

		});
	}
	//small devices
	if (OS_ANDROID) {
		_.extend(dimens, {
			/**
			 * Components
			 */
			button_height : '42dp',
			/**
			 * General
			 */
			padding_vertical : '6dp',
			padding_vertical_double : '12dp',
			padding_top_32dp : '24dp',
			padding_vertical_form : '6dp',
			padding_vertical_form_double : '12dp',

			/**
			 * Login
			 */
			login_components_top : "27%",

		});
	}
} else if (Ti.Platform.displayCaps.platformHeight >= 736) {
	if (OS_IOS) {
		//iphone 6s plus or more
		_.extend(dimens, {

			padding_horizontal_form : '32dp',
			padding_horizontal_form_double : '64dp',
			padding_horizontal_form_half : '16dp',
			padding_vertical_form : '16dp',
			padding_vertical_form_double : '32dp',

			establishment_detail_header_height : '250dp',

			/**
			 * List empty
			 */
			list_empty_label_width : '180dp',

			/**
			 * Stripe
			 */
			stripe_card_number_field_width : '22%',

		});
	}

}

/**
 * CDN
 */
var densityFactor = Ti.Platform.displayCaps.logicalDensityFactor;
if (densityFactor <= 1) {
	_.extend(dimens, {
		/**
		 * Establishment
		 */
		establishment_image_cdn_size : '200x100',
		establishment_image_mini_cdn_size : '90x90',
		/**
		 * User
		 */
		user_photo_cdn_size : '180x180',
		user_photo_mini_cdn_size : '90x90',
		/**
		 * Drink
		 */
		drink_image_cdn_size : '120x120',
		drink_image_mini_cdn_size : '60x60'
	});
} else if (densityFactor <= 2) {
	_.extend(dimens, {
		/**
		 * Establishment
		 */
		establishment_image_cdn_size : '400x200',
		establishment_image_mini_cdn_size : '180x180',
		/**
		 * User
		 */
		user_photo_cdn_size : '360x360',
		user_photo_mini_cdn_size : '180x180',
		/**
		 * Drink
		 */
		drink_image_cdn_size : '240x240',
		drink_image_mini_cdn_size : '120x120'
	});

} else if (densityFactor <= 3) {
	_.extend(dimens, {
		/**
		 * Establishment
		 */
		establishment_image_cdn_size : '600x300',
		establishment_image_mini_cdn_size : '270x270',
		/**
		 * User
		 */
		user_photo_cdn_size : '540x540',
		user_photo_mini_cdn_size : '270x270',
		/**
		 * Drink
		 */
		drink_image_cdn_size : '360x360',
		drink_image_mini_cdn_size : '180x180'
	});
} else {
	_.extend(dimens, {
		/**
		 * Establishment
		 */
		establishment_image_cdn_size : '800x400',
		establishment_image_mini_cdn_size : '360x360',
		/**
		 * User
		 */
		user_photo_cdn_size : '720x720',
		user_photo_mini_cdn_size : '360x360',
		/**
		 * Drink
		 */
		drink_image_cdn_size : '480x480',
		drink_image_mini_cdn_size : '240x240'
	});
}

module.exports = dimens;
