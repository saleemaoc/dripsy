//public pushwoosh
var pushwoosh = require('com.pushwoosh.module');
var isInit = false;
var REGISTRATION_ID = "pushwoosh_registration_id";
var configurePushReceived = false;
var configurePushOpened = false;
/* --------
 Utils
 -------- */
var initialize = function() {
	if (Ti.Network.online && !isInit) {
		pushwoosh.initialize({
			"application" : Alloy.CFG.Pushwoosh.app_id,
			"gcm_project" : Alloy.CFG.Pushwoosh.gcm_project
		});
		isInit = true;
	}
};

var registerPushOpened = function(e) {
	if (!configurePushOpened) {
		pushwoosh.onPushOpened(function(e) {
			Ti.API.info('pushopened');
			var handlePush = e;
			_.defer(function() {
				Alloy.Globals.AppRoute.openByNotification(handlePush, true);
			});
		});
		configurePushOpened = true;
	}
};

var registerPushReceived = function() {
	if (!configurePushReceived) {
		pushwoosh.onPushReceived(function(e) {
			_.defer(function() {
				var handlePush = e;
				Alloy.Globals.AppRoute.openByNotification(handlePush);
			});

		});
		configurePushReceived = true;
	}
};

var registerForPushNotifications = function(success, error) {
	if (Ti.Network.online) {
		initialize();
		pushwoosh.registerForPushNotifications(success, error);
	} else {
		error({});
	}
};

var unregister = function() {
	pushwoosh.unregister();
	Ti.App.Properties.setString(REGISTRATION_ID, "");
};

var setRegistrationId = function(registrationId) {
	if (_.isString(registrationId) && !_.isEmpty(registrationId)) {
		Ti.App.Properties.setString(REGISTRATION_ID, registrationId);
	}
};

var getRegistrationId = function() {
	return Ti.App.Properties.getString(REGISTRATION_ID, "");
};

var configure = function() {
	registerPushReceived();
	registerPushOpened();
};

var clearNotifications = function() {
	if (OS_ANDROID) {
		Ti.Android.NotificationManager.cancelAll();
	} else {
		Ti.App.iOS.cancelAllLocalNotifications();
		Ti.UI.iOS.setAppBadge(1);
		Ti.UI.iOS.setAppBadge(0);
	}
};

exports.Pushwoosh = pushwoosh;
exports.initialize = initialize;
exports.registerPushOpened = registerPushOpened;
exports.registerPushReceived = registerPushReceived;
exports.registerForPushNotifications = registerForPushNotifications;
exports.unregister = unregister;
exports.setRegistrationId = setRegistrationId;
exports.getRegistrationId = getRegistrationId;
exports.configure = configure;
exports.clearNotifications = clearNotifications;
