/**
 *get string to date
 * @param Date date
 * @return string
 */
exports.getStringToDate = function(date) {
	if (!date) {
		date = new Date();
	}
	return Alloy.Globals.moment(date).format("DD/MM/YYYY");

};

/**
 *get string to datetime
 * @param Date date
 * @return string
 */
exports.getStringToDateTime = function(date) {
	if (!date) {
		date = new Date();
	}
	var stillUtc = Alloy.Globals.moment.utc(date).toDate();
	return Alloy.Globals.moment(stillUtc).local().format("DD/MM/YYYY hh:mm:ss A");
};

/**
 *get string to datetime without seconds
 * @param Date date
 * @return string
 */
exports.getStringToDateTimeWithoutSeconds = function(date) {
	if (!date) {
		date = new Date();
	}
	var stillUtc = Alloy.Globals.moment.utc(date).toDate();
	return Alloy.Globals.moment(stillUtc).local().format(Ti.Locale.currentLanguage == 'es' ? "DD/MM/YYYY hh:mm A" : "MM/DD/YYYY hh:mm A");
};

exports.getStringTimeToDate = function(date) {
	if (!date) {
		date = new Date();
	}
	var stillUtc = Alloy.Globals.moment.utc(date).toDate();
	return Alloy.Globals.moment(stillUtc).local().format("hh:mm A");
};

/**
 * convert string to date
 * @param String string
 * @return date
 */
var getDateToString = function(string) {
	var date = Alloy.Globals.moment(string);
	return date.toDate();
};

exports.getDateToString = getDateToString;

/**
 * convert string to date
 * @param String string
 * @return date
 */
var getDateByStringDB = function(stringDB) {

	var splitStringDB = stringDB.split(" ");
	if (splitStringDB.length == 2) {
		stringDB = splitStringDB[0] + 'T' + splitStringDB[1] + 'Z';
	}
	var date = Alloy.Globals.moment(stringDB);
	return date.toDate();
};

exports.getDateByStringDB = getDateByStringDB;

/**
 * get ab string
 * @param Date date
 * @return string
 */
var getAbStringToDate = function(date) {
	if (!date) {
		date = new Date();
	}

	return Alloy.Globals.moment(date).locale(Ti.Locale.currentLanguage).format(Ti.Locale.currentLanguage == 'es' ? Alloy.CFG.default.formatABDate : Alloy.CFG.default.formatABDateEn);
};

exports.getAbStringToDate = getAbStringToDate;

/**
 * @param {Date} date
 * @return {String}
 */
var getStringDBToDate = function(date) {

	if (!date) {
		date = new Date();
	}
	return Alloy.Globals.moment(date).format(Alloy.CFG.default.formatDBDate);
};

exports.getStringDBToDate = getStringDBToDate;

/**
 * get ab string date to string date
 * @param String string
 * @return String
 */
exports.getAbStringDateToString = function(string) {
	return getAbStringToDate(getDateToString(string));
};

/**
 * get full year to date
 * @param {Object} date
 * @return {String}
 */
exports.getFullYearToDate = function(date) {
	return Alloy.Globals.moment(date).format("YYYY");
};

/**
 *
 * @param {String} stringDate
 * @return date with TZ
 */
exports.formatStringDBDateToDateTimeFormatService = function(stringDate) {
	var dateSplit = stringDate.split(' ');
	if (dateSplit.length == 2) {
		return dateSplit[0] + 'T' + dateSplit[1] + 'Z';
	} else {
		return stringDate;
	}
};

/**
 *
 * @param {Date} date
 * @return datetime with TZ
 */
exports.formatDateToDateTimeFormatService = function(date) {
	if (!date) {
		date = new Date();
	}
	var dateSplit = getStringDBToDate(date).split(' ');
	if (dateSplit.length == 2) {
		return dateSplit[0] + 'T' + dateSplit[1] + 'Z';
	} else {
		return date.toString();
	}
};

/**
 *
 * @param {Date} date
 * @return date
 */
exports.formatDateToDateFormatService = function(date) {
	var dateSplit = getStringDBToDate(date).split(' ');
	return dateSplit[0];
};

/**
 *
 * @param {Date} date
 */
exports.getHourFormatToDate = function(date) {

	if (!_.isDate(date)) {
		date = new Date();
	}
	var minutes = date.getMinutes();

	minutes = minutes < 10 ? "0" + minutes : minutes;

	return date.getHours() + ':' + minutes + " hs";

};

exports.getHourFormatToDateWithMeridian = function(date) {

	if (!_.isDate(date)) {
		date = new Date();
	}
	var hours = date.getHours();
	
	var minutes = date.getMinutes();

	minutes = minutes < 10 ? "0" + minutes : minutes;
	
	if (hours == 12) {
		return hours + ':' + minutes + " pm";
	} else {
		if (hours > 12) {
			return (hours - 12) + ':' + minutes + " pm";
		} else {
			return hours + ':' + minutes + " am";
		}	
	}
};

var formatPrice = function(price) {
	return parseFloat(price).toFixed(2);
};

exports.formatPrice = formatPrice;

exports.formatFloatTwoDecimal = function(value) {
	return parseFloat(formatPrice(value));
};

exports.convertStringToNumber = function(str) {
	try {
		return parseFloat(str);
	} catch(e) {
		return 0;
	}
};

