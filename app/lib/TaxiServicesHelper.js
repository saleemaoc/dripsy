var UBER = 'Uber';

/**
 * verifica si existe uber para la ciudad en la que se encuentra el usuario
 * @return {Boolean}
 */
exports.isUberEnabled = function() {
	if (_.isArray(Alloy.Globals.CurrentTaxiServices)) {
		return !_.isUndefined(_.findWhere(Alloy.Globals.CurrentTaxiServices, {
			name : UBER
		}));
	} else {
		return false;
	}
};
