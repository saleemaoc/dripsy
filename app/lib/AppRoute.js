/**
 * @author jagu
 * @description contiene todas las rutas posibles de la aplicación
 */

/** ------------------------
 Static fields
 ------------------------**/

var ROUTES = {
	HOME : 'win_home',
	LOGIN : 'win_login',
	REGISTRATION : 'win_registration',
	SELECT_COUNTRY_CITY : 'win_select_country_city',
	WEBVIEW : 'win_web_view',
	ESTABLISHMENT_DETAIL : 'win_establishment_detail',
	USERS_PRESENCE : 'win_users_presence',
	CONTACT_INFO : 'win_contact_info',
	PROFILE : 'win_user_profile',
	PRICE_LIST : 'win_drinks',
	CART_DRINK : 'win_cart_drink',
	USER_EDIT_PROFILE : 'win_user_edit_profile',
	INFORMATION : 'win_information_screen',
	MY_TRANSACTIONS : 'win_my_transactions',
	TRANSACTION_DETAIL : 'win_transaction_detail',
	CHANGE_PASSWORD : 'win_change_password',
	BUY_TICKET : 'win_buy_ticket',
	GIFT_DETAIL : 'win_gift_detail',
	FORGOT_PASSWORD : 'win_forgot_password',
	CHAT : 'win_chat',
	CONVERSATION_DETAIL : 'win_conversation_detail',
	DRINK_DETAIL : 'win_drink_detail',
	SELECT_PAYMENT_METHOD : 'win_select_payment_method'
};

var NOTIFICATION_TYPE = {
	CONVERSATION_INVITATION : 'conversation',
	CONVERSATION_MESSAGE : 'messageconversation',
	GIFT_DRINK : 'giftdrink'
};

/** ------------------------
 Object
 ------------------------**/
var AppRoute = function() {

};

/** ------------------------
 Fields
 ------------------------**/

var ga = require('ti.ga');
ga.setDispatchInterval(Alloy.CFG.GoogleAnalytics.interval);
var tracker = ga.createTracker({
	trackingId : Alloy.CFG.GoogleAnalytics.trackingID,
	useSecure : Alloy.CFG.GoogleAnalytics.useSecure,
	debug : Alloy.CFG.GoogleAnalytics.debug,
	enableAdvertisingIdCollection : true
});

ga = null;

if (OS_IOS) {
	var navigationWindow = Titanium.UI.iOS.createNavigationWindow();
	var navigationWindowSecondary = null;
}
var firstTime = true;
var currentRoute = null;
var currentConversationID = -1;
var forceOpenNavigationWindowWithoutWindow = false;

/** ------------------------
 Methods
 ------------------------**/

/**
 * open controller
 * @param String controller
 * @param {Object} params
 * @param {boolean} withoutNavWindow
 * @param {boolean} otherNavigationWindow
 */
AppRoute.prototype.open = function(controller, params, withoutNavWindow, otherNavigationWindow, withSecondaryNavigation) {
	this.setCurrentRoute(controller);
	var c = Alloy.createController(controller, params);

	this.openWindow(c.getView(), withoutNavWindow, otherNavigationWindow, withSecondaryNavigation);
	this.tracking(controller);
	return c;
};

/**
 * open window
 *
 */
AppRoute.prototype.openWindow = function(win, withoutNavWindow, otherNavigationWindow, withSecondaryNavigation) {

	var paramsToWindow = {
		animated : true
	};
	if (OS_IOS && otherNavigationWindow) {
		if (!withSecondaryNavigation) {
			navigationWindowSecondary = Titanium.UI.iOS.createNavigationWindow({
				window : win
			});
			navigationWindowSecondary.open(paramsToWindow);
		} else {
			if (navigationWindowSecondary) {
				navigationWindowSecondary.openWindow(win, paramsToWindow);
			}
		}
	} else if (withoutNavWindow || OS_ANDROID) {
		win.open();
	} else if (OS_IOS) {
		if (firstTime) {
			firstTime = false;
			if (!forceOpenNavigationWindowWithoutWindow) {
				navigationWindow.setWindow(win);
				navigationWindow.open(paramsToWindow);
			} else {
				win.open(paramsToWindow);
			}

		} else {
			navigationWindow.openWindow(win, paramsToWindow);
		}

	}
};
/**
 * open home controller
 * @param {Object} params
 * @return {TiController}
 */
AppRoute.prototype.openHome = function(params) {
	(require('AppProperties')).checkGlobals();
	if (OS_IOS) {
		this.closeAndClearNavigationWindow();
	}
	if (Alloy.Globals.CurrentCountry && Alloy.Globals.CurrentCity) {
		var homeController = this.open(ROUTES.HOME, params || {}, false);
		if (OS_ANDROID) {
			Alloy.Globals.PushWooshHelper.configure();
		}
		return homeController;
	} else {
		this.openSelectCountryCity(_.extend(params || {}, {
			theme : 'AppTheme.NoActionBar',
			navBarHidden : true
		}));
	}
};

/**
 * open login controller
 * @param {Object} auth
 * @return {TiController}
 */
AppRoute.prototype.openLogin = function(params) {

	if (OS_IOS) {
		this.closeAndClearNavigationWindow();
	}
	var loginController = this.open(ROUTES.LOGIN, params || {});
	return loginController;
};

/**
 * open select_country_city controller
 * @param {Object}
 * @return {TiController}
 */
AppRoute.prototype.openSelectCountryCity = function(params, withoutNavWindow) {
	return this.open(ROUTES.SELECT_COUNTRY_CITY, params || {}, withoutNavWindow);
};

/**
 * open registration controller
 * @param {Object} auth
 * @return {TiController}
 */
AppRoute.prototype.openRegistration = function(params) {
	return this.open(ROUTES.REGISTRATION, params || {});
};

/**
 * open user profile controller
 * @param {Object} params
 * @return {TiController}
 */
AppRoute.prototype.openUserProfile = function(params) {
	return this.open(ROUTES.PROFILE, params || {});
};

/**
 * open web_view controller
 * @param {Object}
 * @return {TiController}
 */
AppRoute.prototype.openTermsAndCondition = function(params) {
	return this.open(ROUTES.WEBVIEW, params || {
		url : Alloy.CFG.web.url + (Ti.Locale.currentLanguage == 'es' ? Alloy.CFG.web.terms_conditions_es : Alloy.CFG.web.terms_conditions_en),
		title : L('terms_and_conditions', 'Terms and conditions')
	});
};

/**
 * open politics and privacy
 * @param {Object} params
 * @return {TiController}
 */
AppRoute.prototype.openPoliticsAndPrivacy = function(params) {
	return this.open(ROUTES.WEBVIEW, params || {
		url : Alloy.CFG.web.url + (Ti.Locale.currentLanguage == 'es' ? Alloy.CFG.web.policies_and_privacy_es : Alloy.CFG.web.policies_and_privacy_en),
		title : L('policies_and_privacy', 'Privacy Policy')
	});
};

/**
 * open establishment_detail
 * @param {Object} auth
 * @return {TiController}
 */
AppRoute.prototype.openEstablishmentDetail = function(params) {
	return this.open(ROUTES.ESTABLISHMENT_DETAIL, params || {});
};

/**
 * open userpresence
 * @param {Object} auth
 * @return {TiController}
 */
AppRoute.prototype.openUsersPresence = function(params) {
	return this.open(ROUTES.USERS_PRESENCE, params || {});
};

/**
 * open contact_info
 * @param {Object} params
 * @return {TiController}
 */
AppRoute.prototype.openContactInfo = function(params) {
	return this.open(ROUTES.CONTACT_INFO, params || {});
};

/**
 * open edit profile
 * @param {Object} params
 * @return {TiController}
 */
AppRoute.prototype.openEditProfile = function(params) {
	return this.open(ROUTES.USER_EDIT_PROFILE, params || {
		user : Alloy.Globals.DaoBuilder.getUserDao().findByID(Alloy.Globals.UserID)
	});
};

/**
 * open price_list
 * @param {Object} params
 * @return {TiController}
 */
AppRoute.prototype.openPriceList = function(params) {
	return this.open(ROUTES.PRICE_LIST, params || {});
};

/**
 * open cart drink
 * @param {Object} params
 * @return {TiController}
 */
AppRoute.prototype.openCartDrink = function(params) {
	return this.open(ROUTES.CART_DRINK, params || {});
};

/**
 * open change_password
 * @param {Object}
 * @return {TiController}
 */
AppRoute.prototype.openChangePassword = function(params) {
	return this.open(ROUTES.CHANGE_PASSWORD, params || {});
};

/**
 * open user logged transactions
 * @param {Object} params
 * @return {TiController}
 */
AppRoute.prototype.openMyTransactions = function(params) {
	Alloy.Globals.PushWooshHelper.clearNotifications();
	return this.open(ROUTES.MY_TRANSACTIONS, params || {
		userID : Alloy.Globals.UserID
	});
};

/**
 * open transaction detail
 * @param {Object} params
 * @return {TiController}
 */
AppRoute.prototype.openTransactionDetail = function(params) {
	return this.open(ROUTES.TRANSACTION_DETAIL, params || {});
};

/**
 * open user information screen controller
 * @param {Object} params
 * @return {TiController}
 */
AppRoute.prototype.openInformationScreen = function(params) {
	return this.open(ROUTES.INFORMATION, params || {});
};

/**
 * open buy ticket
 * @param {Object} params
 * @return {TiController}
 */
AppRoute.prototype.openBuyTicket = function(params) {
	return this.open(ROUTES.BUY_TICKET, params || {});
};

/**
 * open gift detail
 * @param {Object} params
 * @return {TiController}
 */
AppRoute.prototype.openGiftDetail = function(params) {
	Alloy.Globals.PushWooshHelper.clearNotifications();
	return this.open(ROUTES.GIFT_DETAIL, params || {});
};

AppRoute.prototype.openGiftDetailByNotification = function(params) {
	if (currentRoute == ROUTES.CONVERSATION_DETAIL) {
		Ti.App.fireEvent('closegift');

	} else if (currentRoute != ROUTES.MY_TRANSACTIONS) {
		//select tab gift
		var myTransactionsController = this.openMyTransactions({
			currentPage : 1
		});

		myTransactionsController.on('close', function() {
			Alloy.Globals.AppRoute.setCurrentRoute('fix_open_by_notification');
		});

		//gc
		myTransactionsController = null;
	}

};
/**
 * open user information screen controller
 * @param {Object} params
 * @return {TiController}
 */
AppRoute.prototype.openForgotPassword = function(params) {
	return this.open(ROUTES.FORGOT_PASSWORD, params || {});
};

/**
 * open chat controller
 * @param {Object} params
 * @return {TiController}
 */
AppRoute.prototype.openChat = function(params) {
	//clear current conversationID
	currentConversationID = -1;

	var chatController = this.open(ROUTES.CHAT, params || {});

	chatController.on('close', function() {
		Alloy.Globals.AppRoute.setCurrentRoute('fix_open_by_notification');
		//clear
		currentConversationID = -1;
	});
	Alloy.Globals.PushWooshHelper.clearNotifications();
	return chatController;
};

AppRoute.prototype.openChatByNotification = function(params) {
	if (currentRoute != ROUTES.CHAT) {
		this.openChat();
	}
};
/**
 * open conversation detail controller
 * @param {Object} params
 * @return {TiController}
 */
AppRoute.prototype.openConversationDetail = function(params) {

	if (_.isObject(params) && _.isObject(params.conversation)) {
		currentConversationID = params.conversation.getID();
	} else {
		currentConversationID = -1;
	}

	var conversationDetailController = this.open(ROUTES.CONVERSATION_DETAIL, params || {});

	conversationDetailController.on('close', function() {
		Alloy.Globals.AppRoute.setCurrentRoute('fix_open_by_notification');
		//clear
		currentConversationID = -1;
	});

	return conversationDetailController;
};

/**
 * open conversation detail by notification
 */
AppRoute.prototype.openConversationDetailByNotification = function(params) {

	var notificationOpenController = false;
	if (currentRoute == ROUTES.CONVERSATION_DETAIL) {

		if (currentConversationID != params.data.conversation.id) {
			Ti.App.fireEvent('closeconversation');
			notificationOpenController = true;
		}
	} else {
		notificationOpenController = true;
	}

	if (notificationOpenController) {

		var conversation = Alloy.Globals.DaoBuilder.getConversationDao().updateModel(params.data.conversation);
		var title = conversation.transform().other_user_full_name;

		var openConversationDetailController = this.openConversationDetail({
			conversation : conversation,
			title : title
		});
		openConversationDetailController.on('close', function() {
			Alloy.Globals.AppRoute.setCurrentRoute('fix_open_by_notification');
		});
		//gc
		openConversationDetailController = null;
	}

};
/**
 * open drink detail controller
 * @param {Object} params
 * @return {TiController}
 */
AppRoute.prototype.openDrinkDetail = function(params) {
	return this.open(ROUTES.DRINK_DETAIL, params || {});
};

/**
 * open select payment method controller
 * @param {Object} params
 * @return {TiController}
 */
AppRoute.prototype.openSelectPaymentMethod = function(params) {
	return this.open(ROUTES.SELECT_PAYMENT_METHOD, params || {});
};

/**
 * open controller by notification
 */
AppRoute.prototype.openByNotification = function(params, isOpen) {
	if (_.isObject(params) && _.has(params, 'extras')) {
		var extras = params.extras;

		if (_.has(extras, 'type')) {
			var notificationType = extras.type;
			switch(notificationType) {
			case NOTIFICATION_TYPE.CONVERSATION_MESSAGE:
				if (isOpen) {
					Alloy.Globals.AppRoute.openConversationDetailByNotification(extras);
					Alloy.Globals.AppProperties.setPendingChat(false);
				} else {
					Alloy.Globals.AppProperties.setPendingChat(true);
				}

				break;
			case NOTIFICATION_TYPE.CONVERSATION_INVITATION:
				if (isOpen) {
					Alloy.Globals.AppRoute.openChatByNotification(extras);
					Alloy.Globals.AppProperties.setPendingChat(false);
				} else {
					Alloy.Globals.AppProperties.setPendingChat(true);
				}

				break;
			case NOTIFICATION_TYPE.GIFT_DRINK:
				if (isOpen) {
					Alloy.Globals.AppRoute.openGiftDetailByNotification(extras);
					Alloy.Globals.AppProperties.setPendingChat(false);
				} else {
					Alloy.Globals.AppProperties.setPendingTransaction(true);
				}

				break;
			}
		}

	}
};

/**
 * generate controller tracking name
 * @param {String} controller
 * @return {String}
 */
var generateTrackingName = function(controller) {

	var trackingName = '';
	switch(controller) {
	case 'win_drinks':
		trackingName = '141793231';
		break;
	case 'win_cart_drink':
		trackingName = '141783565';
		break;
	/*case 'win_home':
	 trackingName = 'Home';
	 break;
	 case 'win_login':
	 trackingName = 'Login';
	 break;
	 case 'win_registration':
	 trackingName = 'Registration';
	 break;
	 case 'win_select_country_city':
	 trackingName = 'Select Location';
	 break;
	 case 'win_establishment_detail':
	 trackingName = 'Establishment detail';
	 break;
	 case 'win_users_presence':
	 trackingName = 'Establishment users presence';
	 break;
	 case 'win_contact_info':
	 trackingName = 'Establishment contact info';
	 break;
	 case 'win_user_profile':
	 trackingName = 'User profile';
	 break;*/
	/*case 'win_user_edit_profile':
	 trackingName = 'User edit profile';
	 break;
	 case 'win_information_screen':
	 trackingName = 'App information';
	 break;
	 case 'win_my_transactions':
	 trackingName = 'Transactions';
	 break;
	 case 'win_change_password':
	 trackingName = 'Change password';
	 break;
	 case 'win_buy_ticket':
	 trackingName = 'Establishment buy ticket';
	 break;
	 case 'win_gift_detail':
	 trackingName = 'Gift detail';
	 break;
	 case 'win_forgot_password':
	 trackingName = 'Forgot password';
	 break;
	 case 'win_chat':
	 trackingName = 'Chat';
	 break;
	 case 'win_conversation_detail':
	 trackingName = 'Conversation';
	 break;
	 case 'win_drink_detail':
	 trackingName = 'Drink detail';
	 break;*/
	}
	return trackingName;
};

/**
 * send tracking
 * @param {String} trackingName
 */
AppRoute.prototype.sendTracking = function(trackingName) {
	if (Ti.Network.online) {
		tracker.addScreenView(trackingName);
	}
};

/**
 * tracking page
 * @param {Object} controllerName
 */
AppRoute.prototype.tracking = function(controllerName) {
	var trackingName = generateTrackingName(controllerName);
	if (!_.isEmpty(trackingName)) {
		tracker.addScreenView(trackingName);
	}
};

if (OS_IOS) {
	AppRoute.prototype.getNavigationWindow = function() {
		return navigationWindow;
	};

	AppRoute.prototype.closeAndClearNavigationWindow = function() {
		if (!_.isNull(navigationWindow)) {
			navigationWindow.close();
			//create a new navigation window
			navigationWindow = Titanium.UI.iOS.createNavigationWindow();
			firstTime = true;
			forceOpenNavigationWindowWithoutWindow = false;
		}
	};

	AppRoute.prototype.closeNavigationWindowSecondary = function() {
		if (!_.isNull(navigationWindowSecondary)) {
			navigationWindowSecondary.close();
			navigationWindowSecondary = null;
		}
	};

	AppRoute.prototype.setNavigationWindow = function(navWindow) {
		forceOpenNavigationWindowWithoutWindow = true;
		navigationWindow = navWindow;
	};

}

/**
 *
 * @param {Object} route
 */
AppRoute.prototype.setCurrentRoute = function(route) {
	currentRoute = route;
};

module.exports = AppRoute;
