module.exports = {
	Regular : OS_ANDROID ? 'Roboto-Regular' : 'SF-UI-Text-Regular',
	Medium : OS_ANDROID ? 'Roboto-Medium' : 'SF-UI-Text-Medium',
	Light : OS_ANDROID ? 'Roboto-Light' : 'SF-UI-Text-Light',
	Black : OS_ANDROID ? 'Roboto-Black' : 'SF-UI-Display-Black',
	Bold : OS_ANDROID ? 'Roboto-Bold' : 'SF-UI-Display-Bold',
};
