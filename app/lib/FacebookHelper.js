if (_.isUndefined(Alloy.Globals.Facebook)) {
	Alloy.Globals.Facebook = require('facebook');
	Alloy.Globals.Facebook.permissions = Alloy.CFG.Facebook.permissions;
	Alloy.Globals.Facebook.initialize();
}

var loader = Alloy.createWidget('co.clarika.loader', {

	message : {
		text : L('logging_out', 'Logging out...'),
		color : Alloy.Globals.Colors.accent,
		font : {
			fontFamily : Alloy.Globals.Fonts.Regular,
			fontSize : Alloy.Globals.Dimens.font_subheading
		}
	},
	activityIndicator : {
		indicatorColor : Alloy.Globals.Colors.accent,
		style : Titanium.UI.ActivityIndicatorStyle.BIG
	}
});

var logout = function() {
	loader.hide();
	Alloy.Globals.AppRoute.openLogin();
};

exports.logout = function() {
	loader.show();
	Alloy.Globals.Facebook.logout();
	_.delay(logout, 1500);
};

exports.loggedIn = function() {
	return Alloy.Globals.Facebook.loggedIn;
};

