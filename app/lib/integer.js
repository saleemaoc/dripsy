module.exports = {
	elevation : 50,
	/**
	 * Timeouts and delays
	 */
	debounce : 750,
	search : 300,
	dataSet : 100,
	PreventDetachedView : 500,
	SetDataDelay : 250,
	HideLoaderDelay : 2000,
	/**
	 * User presence
	 */

	user_presence_item_name_max_lines : 2,

	/**
	 * Form limit
	 */
	first_name_limit : 20,
	last_name_limit : 20,
	password_limit : 25,
	bio_limit : 70,
	phone_limit : 20,
	favorite_drink_limit : 30,
	university_limit : 40,
	occupation_limit : 30,
	work_limit : 30

};
