/** --------
 Constants
 -------- **/
var TAG = 'lib/services/CountryService';

var TAGS = {
	TAG_ADD : 'CountryService_Add',
	TAG_GET : 'CountryService_Get'
};
/** --------
 Variables
 -------- **/

/**
 * end point
 */
var url = Alloy.CFG.url + Alloy.CFG.api.countries;

/**
 * contract
 */
var use_contract = Alloy.CFG.api.use_contract_country;
var contract;
if (use_contract) {
	contract = require('contracts/contract_country');
}
var dao;
/** --------
 Inject
 -------- **/
/**
 * inject dependencies
 * @param {Object} daoImpl
 */
exports.inject = function(daoImpl) {
	dao = daoImpl;
	return this;
};

/** --------
 Methods
 -------- **/

/**
 * persist data in database
 * @param {Object} response
 */
var persistData = function(response) {
	if (_.isObject(dao)) {
		if (_.isFunction(dao.updateData)) {
			dao.updateData(response.results);
		} else {
			Ti.API.error(TAG, 'dao must implement "updateData" function');
		}
	}
};

/**
 * get all examples
 * @param {Object} params
 * @param {Function} success
 * @param {Function} error
 * @param {HttpCK} http
 * @param {Object} options
 *
 * @return
 */
exports.get = function(params, success, error, http, options) {

	//generate options
	var defaultOptions = {
		ttl : Alloy.CFG.api.cache_time
	};

	if (_.isObject(options)) {
		_.extend(defaultOptions, options);
	}

	//check http library
	if (!http) {
		http = new (require('HttpCK'))();
	}
	
	if (use_contract) {
		//simulate response
		persistData(contract.getCountry());
		_.delay(success, Alloy.CFG.api.contract_delay, {});
		return http;
	} else {
		return http.getByTag(TAGS.TAG_GET, url, params, function(response) {
			response = JSON.parse(response.data);
			persistData(response);
			success(response);
		}, error, defaultOptions);
	}

};

/** -------
 public
 -------- **/
exports.TAGS = TAGS;
