/**
 * Service builder
 * @author jagu
 */

/**
 * build service by name
 * @param {String} service
 * @return {Service}
 */
var build = function(service) {
	return require('services/' + service);
};

/**
 * build user service
 * @return {UserService}
 */
exports.getUserService = function() {
	return build('UserService');
};

/**
 * build establishment service
 * @return {EstablishmentService}
 */
exports.getEstablishmentService = function() {
	return build('EstablishmentService');
};

/**
 * build drink service
 * @return {DrinkService}
 */
exports.getDrinkService = function() {
	return build('DrinkService');
};

/**
 * build country service
 * @return {CountryService}
 */
exports.getCountryService = function() {
	return build('CountryService');
};

/**
 * build city service
 * @return {CityService}
 */
exports.getCityService = function() {
	return build('CityService');
};

/**
 * build tag service
 * @return {TagService}
 */
exports.getTagService = function() {
	return build('TagService');
};

/**
 * build user presence service
 * @return {UserPresenceService}
 */
exports.getUserPresenceService = function() {
	return build('UserPresenceService');
};

/**
 * build establishment image service
 * @return {EstablishmentImageService}
 */
exports.getEstablishmentImageService = function() {
	return build('EstablishmentImageService');
};

/**
 * build transaction service
 * @return {TransactionService}
 */
exports.getTransactionService = function() {
	return build('TransactionService');
};

/**
 * build transaction service
 * @return {TransactionService}
 */
exports.getConceptService = function() {
	return build('ConceptService');
};

/**
 * build gift service
 * @return {GiftService}
 */
exports.getGiftService = function() {
	return build('GiftService');
};

/**
 * build conversation service
 * @return {ConversationService}
 */
exports.getConversationService = function() {
	return build('ConversationService');
};

/**
 * build message service
 * @return {MessageService}
 */
exports.getMessageService = function() {
	return build('MessageService');
};

/**
 * build message service
 * @return {CardService}
 */
exports.getCardService = function() {
	return build('CardService');
};

exports.getNotificationService = function() {
	return build('NotificationService');
};
