/** --------
 Constants
 -------- **/
var TAG = 'lib/services/NotificationService';

var TAGS = {
	TAG_GET : 'NotificationService_Get',
};

/** --------
 Variables
 -------- **/
var url = Alloy.CFG.url + Alloy.CFG.api.notifications;
/**
 * End point
 */

var appProperties = require('AppProperties');
/** --------
 Methods
 -------- **/

exports.get = function(success, error) {

	var http = new (require('HttpCK'))();

	var params = {
		date : appProperties.getNotificationCheckDate()
	};

	var options = {

	};
	return http.getByTag(TAGS.TAG_GET, url, params, function(response) {
		response = JSON.parse(response.data);
		if (response.chats > 0) {
			appProperties.setPendingChat(true);
		}
		if (response.gifts > 0) {
			appProperties.setPendingTransaction(true);
		}
		appProperties.setNotificationCheckDate();
		success();
	}, function(e) {
		error(e);
	}, options);

};

/** -------
 public
 -------- **/
exports.TAGS = TAGS;
