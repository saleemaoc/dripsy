/** --------
 Constants
 -------- **/
var TAG = 'lib/services/MessageService';

var TAGS = {
	TAG_GET : 'MessageService_Get',
	TAG_SEND : 'MessageService_Send'
};

/** --------
 Variables
 -------- **/

/**
 * contract
 */
var useContract = Alloy.CFG.api.use_contract_message;
var contract;

if (useContract) {
	contract = require('contracts/contract_message');
}

/**
 * End point
 */
var url = Alloy.CFG.url + Alloy.CFG.api.message;

var dao;

/** --------
 Inject
 -------- **/
/**
 * inject dependencies
 * @param {Object} daoImpl
 */
exports.inject = function(daoImpl) {
	dao = daoImpl;
	return this;
};

/** --------
 Methods
 -------- **/

/**
 * persist data in database
 * @param {Object} response
 */
var persistData = function(response) {
	if (_.isObject(dao)) {
		if (_.isFunction(dao.updateData)) {
			dao.updateData(response.results);
		} else {
			Ti.API.error(TAG, 'dao must implement "updateData" function');
		}
	}
};

/**
 * get messages
 * @param {Object} params
 * @param {Function} success
 * @param {Function} error
 * @param {HttpCK} http
 * @param {Object} options
 *
 * @return
 */
exports.get = function(params, success, error, http, options) {

	//generate options
	var defaultOptions = {
		ttl : false
	};

	if (_.isObject(options)) {
		_.extend(defaultOptions, options);
	}

	//check http library
	if (!http) {
		http = new (require('HttpCK'))();
	}

	if (useContract) {
		//simulate response
		persistData(contract.get());
		_.delay(success, Alloy.CFG.api.contract_delay, {});
		return http;
	} else {
		return http.getByTag(TAGS.TAG_GET, url, params, function(response) {
			response = JSON.parse(response.data);
			persistData(response);
			success(response.results);
		}, error, defaultOptions);
	}

};

/**
 * send new message
 * @param {Array} params
 * @param {Function} success
 * @param {Function} error
 * @param {HttpCK} http
 * @param {Object} options
 */
exports.send = function(messages, success, error, http, options) {

	var messagesToService = [];
	if (_.isArray(messages)) {
		_.map(messages, function(message) {
			messagesToService.push(message.toService());
		});
	}
	//check http library
	if (!http) {
		http = new (require('HttpCK'))();
	}

	/**
	 *
	 */
	return http.postByTag(TAGS.TAG_SEND, url, messagesToService, function(response) {
		response = {
			results : JSON.parse(response.data)
		};
		persistData(response);
		success(response.results);
	}, error, options);
};
/** -------
 public
 -------- **/
exports.TAGS = TAGS;
