/** --------
 Constants
 -------- **/
var TAG = 'lib/services/ConceptService';

var TAGS = {
	TAG_GET : 'ConceptService_Get'
};
/** --------
 Variables
 -------- **/

/**
 * contract
 */
var useContract = Alloy.CFG.api.use_contract_concept;
var contract;
if (useContract) {
	contract = require('contracts/contract_concept');
}

/**
 * End point
 */
var url = Alloy.CFG.url + Alloy.CFG.api.concept;

var dao;

/** --------
 Inject
 -------- **/
/**
 * inject dependencies
 * @param {Object} daoImpl
 */
exports.inject = function(daoImpl) {
	dao = daoImpl;
	return this;
};

/** --------
 Methods
 -------- **/

/**
 * persist data in database
 * @param {Object} response
 */
var persistData = function(response) {
	if (_.isObject(dao)) {
		if (_.isFunction(dao.destroyAll)) {
			dao.destroyAll();
		}
		if (_.isFunction(dao.updateData)) {
			dao.updateData(response.results);
		} else {
			Ti.API.error(TAG, 'dao must implement "updateData" function');
		}
	}
};

/**
 * get  drinks
 * @param {Object} params
 * @param {Function} success
 * @param {Function} error
 * @param {HttpCK} http
 * @param {Object} options
 *
 * @return
 */
exports.get = function(params, success, error, http, options) {

	//generate options
	var defaultOptions = {
	};

	if (_.isObject(options)) {
		_.extend(defaultOptions, options);
	}

	//check http library
	if (!http) {
		http = new (require('HttpCK'))();
	}

	if (useContract) {
		//simulate response
		persistData(contract.get());
		_.delay(success, Alloy.CFG.api.contract_delay, {});
		return http;
	} else {
		return http.getByTag(TAGS.TAG_GET, url, params, function(response) {
			response = JSON.parse(response.data);
			persistData(response);
			success(response.results);
		}, error, defaultOptions);
	}

};

/** -------
 public
 -------- **/
exports.TAGS = TAGS;
