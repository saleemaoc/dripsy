/** --------
 Constants
 -------- **/
var TAG = 'lib/services/UserService';

var TAGS = {
	TAG_AUTH : 'UserService_Auth',
	TAG_SOCIAL_AUTH : 'Userservice_Social_Auth',
	TAG_REGISTER : 'UserService_Register',
	TAG_GET : 'UserService_Get',
	TAG_EDIT : 'UserService_Edit',
	TAG_CHANGE_PASSWORD : 'UserService_Change_Password',
	TAG_SEND_EMAIL : 'UserService_Send_Email',
	TAG_RESEND_EMAIL : 'UserService_Resend_Email'
};

/** --------
 Variables
 -------- **/

/**
 * contract
 */
var useContract = Alloy.CFG.api.use_contract_user;
var contract;
if (useContract) {
	contract = require('contracts/contract_user');
}

/**
 * End point
 */
var url = Alloy.CFG.url + Alloy.CFG.api.users;

var dao;

/** --------
 Inject
 -------- **/
/**
 * inject dependencies
 * @param {Object} daoImpl
 */
exports.inject = function(daoImpl) {
	dao = daoImpl;
	return this;
};

/** --------
 Methods
 -------- **/

/**
 * persist data in database
 * @param {Object} response
 */
var persistData = function(response) {
	if (_.isObject(dao)) {
		if (_.isFunction(dao.updateData)) {
			dao.updateData(response.results);
		} else {
			Ti.API.error(TAG, 'dao must implement "updateData" function');
		}
	}
};

/**
 * LOGIN
 * @param {Object} user
 * @param {Object} password
 * @param {Object} onSuccess
 * @param {Object} onError
 * @return HttpClient
 */
exports.auth = function(params, success, error, http, options) {
	var url = Alloy.CFG.url + Alloy.CFG.api.auth;
	var defaultOptions = {
		shouldAuthenticate : false
	};

	if (options) {
		_.extend(defaultOptions, options);
	}

	if (!http) {
		http = new (require('HttpCK'))();
	}
	if (useContract) {
		//simulate response
		contract.login(params, success, error);
		return http;
	} else {
		return http.postByTag(TAGS.TAG_AUTH, url, params, function(response) {
			response = JSON.parse(response.data);
			success(response);
		}, error, defaultOptions);
	}

};

/**
 * LOGIN
 * @param {Object} user
 * @param {Object} password
 * @param {Object} onSuccess
 * @param {Object} onError
 * @return HttpClient
 */
exports.socialAuth = function(params, success, error, http, options) {
	var url = Alloy.CFG.url + Alloy.CFG.api.social_auth;
	var defaultOptions = {
		shouldAuthenticate : false
	};

	if (options) {
		_.extend(defaultOptions, options);
	}

	if (!http) {
		http = new (require('HttpCK'))();
	}
	if (useContract) {
		//simulate response
		contract.login(params, success, error);
		return http;
	} else {
		return http.postByTag(TAGS.TAG_SOCIAL_AUTH, url, params, function(response) {
			success(JSON.parse(response.data));
		}, error, defaultOptions);
	}

};

/**
 * registration
 * @param {Object} params
 * @param {Function} success
 * @param {Function} error
 * @param {HttpCK} http
 * @param {Object} options
 *
 * @return
 */
exports.registration = function(params, success, error, http, options) {

	var defaultOptions = {
		shouldAuthenticate : false,
	};

	if (options) {
		_.extend(defaultOptions, options);
	}

	if (!http) {
		http = new (require('HttpCK'))();
	}
	//check if need add Facebook information
	if (Alloy.Globals.Facebook.loggedIn) {
		params.facebook_id = Alloy.Globals.Facebook.uid;
		params.access_token = Alloy.Globals.Facebook.accessToken;
	}

	if (useContract) {
		//simulate response
		_.delay(success, Alloy.CFG.api.contract_delay, _.extend(contract.postRegistration(), params));
		return http;
	} else {
		return http.postByTag(TAGS.TAG_REGISTER, url, params, function(response) {
			success(JSON.parse(response.data));
		}, error, defaultOptions);
	}

};

/**
 * edit user
 * @param {Integer} id
 * @param {Object} params
 * @param {Function} success
 * @param {Function} error
 * @param {HttpCK} http
 * @param {Object} options
 */
exports.edit = function(id, params, success, error, http, options) {
	var defaultOptions = {
	};

	if (options) {
		_.extend(defaultOptions, options);
	}

	if (!http) {
		http = new (require('HttpCK'))();
	}
	
	if (useContract) {
		//simulate response
		_.delay(success, Alloy.CFG.api.contract_delay, _.extend(contract.postRegistration(), params));
		return http;
	} else {
		var SLASH = '/';
		console.log('TAGS.TAG_EDIT: ', TAGS.TAG_EDIT);
		console.log('URL: ', url.concat(id).concat(SLASH));
		return http.putByTag(TAGS.TAG_EDIT, url.concat(id).concat(SLASH), params, function(response) {
			response = JSON.parse(response.data);
			success(response);
		}, error, defaultOptions);
	}
};

/**
 * change password to user
 * @param {Object} params
 * @param {Function} success
 * @param {Function} error
 * @param {HttpCK} http
 * @param {Object} options
 */
exports.changePassword = function(params, success, error, http, options) {

	var url = Alloy.CFG.url + Alloy.CFG.api.change_password;

	var defaultOptions = {
	};

	if (options) {
		_.extend(defaultOptions, options);
	}

	if (!http) {
		http = new (require('HttpCK'))();
	}

	return http.postByTag(TAGS.TAG_CHANGE_PASSWORD, url, params, function(response) {
		success({});
	}, error, defaultOptions);
};
/**
 * send user mail to reset password
 * @param {Object} params
 * @param {Function} success
 * @param {Function} error
 * @param {HttpCK} http
 * @param {Object} options
 */
exports.sendEmail = function(params, success, error, http, options) {

	var url = Alloy.CFG.url + Alloy.CFG.api.send_mail;

	var defaultOptions = {
		shouldAuthenticate : false,
	};

	if (options) {
		_.extend(defaultOptions, options);
	}

	if (!http) {
		http = new (require('HttpCK'))();
	}

	if (Alloy.CFG.api.use_contract_send_mail) {
		//simulate response
		contract.sendMail(params, success, error);
		return http;
	} else {
		return http.postByTag(TAGS.TAG_SEND_EMAIL, url, params, function(response) {
			success(response);
		}, error, defaultOptions);
	}
};

/**
 *
 */
exports.resendEmailToActivateAccount = function(email, success, error, http, options) {
	var url = Alloy.CFG.url + Alloy.CFG.api.send_email;

	var defaultOptions = {
		shouldAuthenticate : false,
	};

	if (options) {
		_.extend(defaultOptions, options);
	}

	if (!http) {
		http = new (require('HttpCK'))();
	}

	var params = {
		email : email
	};

	return http.postByTag(TAGS.TAG_RESEND_EMAIL, url, params, function(response) {
		success(response);
	}, error, defaultOptions);
};

/** -------
 public
 -------- **/
exports.TAGS = TAGS;
