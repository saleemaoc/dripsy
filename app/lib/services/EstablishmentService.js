/** --------
 Constants
 -------- **/
var TAG = 'lib/services/EstablishmentService';

var TAGS = {
	TAG_GET : 'EstablishmentService_Get',
	TAG_SEND_RANKING : 'EstablishmentService_SendRanking'
};
/** --------
 Variables
 -------- **/

/**
 * contract
 */
var useContract = Alloy.CFG.api.use_contract_establishment;
var contract;
if (useContract) {
	contract = require('contracts/contract_establishment');
}

/**
 * End point
 */
var url = Alloy.CFG.url + Alloy.CFG.api.establishments;

var dao;

/** --------
 Inject
 -------- **/
/**
 * inject dependencies
 * @param {Object} daoImpl
 */
exports.inject = function(daoImpl) {
	dao = daoImpl;
	return this;
};

/** --------
 Methods
 -------- **/

/**
 * persist data in database
 * @param {Object} response
 */
var persistData = function(response) {
	if (_.isObject(dao)) {
		if (_.isFunction(dao.updateData)) {
			if (_.isArray(response.results)) {
				dao.updateData(response.results);
			} else if (_.isObject(response)) {
				dao.updateData(response);
			}

		} else {
			Ti.API.error(TAG, 'dao must implement "updateData" function');
		}
	}
};

/**
 * get  establishmnets
 * @param {Object} params
 * @param {Function} success
 * @param {Function} error
 * @param {HttpCK} http
 * @param {Object} options
 *
 * @return
 */
exports.get = function(params, success, error, http, options) {
	//generate options
	var defaultOptions = {
		ttl : Alloy.CFG.api.cache_time_establishment
	};

	if (_.isObject(options)) {
		_.extend(defaultOptions, options);
	}

	//check http library
	if (!http) {
		http = new (require('HttpCK'))();
	}

	if (useContract) {
		//simulate response
		persistData(contract.get());
		_.delay(success, Alloy.CFG.api.contract_delay, {});
		return http;
	} else {
		return http.getByTag(TAGS.TAG_GET, url, params, function(response) {
			response = JSON.parse(response.data);
			persistData(response);
			success(response);
		}, error, defaultOptions);
	}

};
/**
 * send user ranking to establishment
 * @param {Integer} id: -1 for new register
 * @param {Object} params
 * @param {Function} success
 * @param {Function} error
 * @param {HttpCk} http
 * @param {Object} options
 *
 * @return
 */
exports.sendUserRanking = function(id, params, success, error, http, options) {

	//custom url
	var url_user_ranking = Alloy.CFG.url + Alloy.CFG.api.user_establishment_ranking;
	//check http library
	if (!http) {
		http = new (require('HttpCK'))();
	}

	if (useContract) {
		//simulate response
		persistData(contract.sendUserRanking());
		_.delay(success, Alloy.CFG.api.contract_delay, {});
		return http;
	} else {
		if (id != -1) {
			var SEPARATOR = '/';
			//put request
			return http.putByTag(TAGS.TAG_SEND_RANKING, url_user_ranking.concat(id + SEPARATOR), params, function(response) {
				response = JSON.parse(response.data);
				success(response);
			}, error, options);

		} else {
			//new send ranking post request
			return http.postByTag(TAGS.TAG_SEND_RANKING, url_user_ranking, params, function(response) {
				response = JSON.parse(response.data);
				success(response);

			}, error, options);
		}
	}
};
/** -------
 public
 -------- **/
exports.TAGS = TAGS;
