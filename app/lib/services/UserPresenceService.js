/** --------
 Constants
 -------- **/
var TAG = 'lib/services/UserPresenceService';

var TAGS = {
	TAG_GET : 'UserPresence_Get',
	TAG_GET_ASSISTANT : 'UserPresence_Get_Assistant',
	TAG_SEND_ASSIST : 'UserPresence_Send_Assistant',
	TAG_DELETE_ASSIST : 'UserPresence_Delete_Assistant',
};

/** --------
 Variables
 -------- **/
/**
 * contract
 */
var useContract = Alloy.CFG.api.use_contract_user_presence;
var contract;
if (useContract) {
	contract = require('contracts/contract_user_presence');
}

/**
 * End point
 */
var url = Alloy.CFG.url + Alloy.CFG.api.userpresence;

var dao;

/** --------
 Inject
 -------- **/
/**
 * inject dependencies
 * @param {Object} daoImpl
 */
exports.inject = function(daoImpl) {
	dao = daoImpl;
	return this;
};
/** --------
 Methods
 -------- **/

/**
 * get  establishmnets
 * @param {Object} params
 * @param {Function} success
 * @param {Function} error
 * @param {HttpCK} http
 * @param {Object} options
 *
 * @return
 */
exports.get = function(params, success, error, http, options) {

	if (!http) {
		http = new (require('HttpCK'))();
	}

	if (useContract) {
		//simulate response
		_.delay(success, Alloy.CFG.api.contract_delay, contract.get(params));
		return http;
	} else {
		return http.getByTag(TAGS.TAG_GET, url, params, function(response) {
			response = JSON.parse(response.data);
			success(response);
		}, error, options);
	}

};

/**
 *  indicate if user assist to establishment
 * @param {Object} params
 * @param {Function} success
 * @param {Function} error
 * @param {HttpCK} http
 * @param {Object} options
 *
 * @return
 */
exports.getAssistant = function(params, success, error, http, options) {
	var url_assistant = Alloy.CFG.url + Alloy.CFG.api.assistant;
	if (useContract) {
		//simulate response
		_.delay(success, Alloy.CFG.api.contract_delay, contract.getAssistant(params));
		return http;
	} else {
		return http.getByTag(TAGS.TAG_GET_ASSISTANT, url_assistant, params, function(response) {
			response = JSON.parse(response.data);
			if (_.isArray(response.results) && !_.isEmpty(response.results)) {
				//return unique value
				success(response.results[0]);
			} else {
				//if empty results fire error
				error({
					message : 'Empty value'
				});
			}
		}, error, options);
	}
};

/**
 * add assist to establishment
 * @param {Integer} id (only for update assist)
 * @param {Object} params
 * @param {Function} success
 * @param {Function} error
 * @param {HttpCK} http
 * @param {Object} options
 */
exports.add = function(id, params, success, error, http, options) {

	if (!http) {
		http = new (require('HttpCK'))();
	}

	if (_.isNumber(id)) {
		var SLASH = '/';
		return http.putByTag(TAGS.TAG_SEND_ASSIST, url.concat(id).concat(SLASH), params, function(response) {
			response = JSON.parse(response.data);
			success(response);
		}, error, options);
	} else {
		return http.postByTag(TAGS.TAG_SEND_ASSIST, url, params, function(response) {
			response = JSON.parse(response.data);
			success(response);
		}, error, options);
	}
};

/**
 * force assist to establishment
 * @param {Integer} id (only for update assist)
 * @param {Object} params
 * @param {Function} success
 * @param {Function} error
 * @param {HttpCK} http
 * @param {Object} options
 */
exports.force = function(id, params, success, error, http, options) {

	var url = Alloy.CFG.url + Alloy.CFG.api.userspresenceschange;
	
	if (!http) {
		http = new (require('HttpCK'))();
	}

	if (_.isNumber(id)) {
		var SLASH = '/';
		return http.putByTag(TAGS.TAG_FORCE_ASSIST, url.concat(id).concat(SLASH), params, function(response) {
			response = JSON.parse(response.data);
			success(response);
		}, error, options);
	} else {
		return http.postByTag(TAGS.TAG_FORCE_ASSIST, url, params, function(response) {
			response = JSON.parse(response.data);
			success(response);
		}, error, options);
	}
};

/**
 * delete assist to establishment
 * @param {Integer} id
 * @param {Function} success
 * @param {Function} error
 * @param {HttpCK} http
 * @param {Object} options
 */
exports.destroy = function(id, success, error, http, options) {
	if (!http) {
		http = new (require('HttpCK'))();
	}
	var SLASH = '/';
	return http.destroyByTag(TAG.TAG_DELETE_ASSIST, url.concat(id).concat(SLASH), function(response) {
		if (dao && _.isFunction(dao.deleteByID)) {
			dao.deleteByID(id);
		}
		success({});
	}, error, options);

};
/** -------
 public
 -------- **/
exports.TAGS = TAGS;
