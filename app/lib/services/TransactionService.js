/** --------
 Constants
 -------- **/
var TAG = 'lib/services/TransactionService';

var TAGS = {
	TAG_GET : 'TransactionService_Get',
	TAG_HIDE_TRANSACTION : 'TransactionService_Hide',
	TAG_SEND_TRANSACTION : 'TransactionService_Send',
	TAG_UPDATE_PAYMENT_CODE : 'TransactionService_Update_Payment'
};
/** --------
 Variables
 -------- **/

/**
 * contract
 */
var useContract = Alloy.CFG.api.use_contract_transaction;
var contract;
if (useContract) {
	//contract = require('contracts/');
}

/**
 * End point
 */
var url = Alloy.CFG.url + Alloy.CFG.api.transaction;

/** --------
 Inject
 -------- **/
/**
 * inject dependencies
 * @param {Object} daoImpl
 */
exports.inject = function(daoImpl) {
	dao = daoImpl;
	return this;
};

/** --------
 Methods
 -------- **/

/**
 * persist data in database
 * @param {Object} response
 */
var persistData = function(response) {
	if (_.isObject(dao)) {
		if (_.isFunction(dao.updateData)) {
			if (_.isArray(response.results)) {
				dao.updateData(response.results);
			} else {
				dao.updateData(response);
			}
		} else {
			Ti.API.error(TAG, 'dao must implement "updateData" function');
		}
	}
};

/**
 * get transactions
 * @param {Object} params
 * @param {Function} success
 * @param {Function} error
 * @param {HttpCK} http
 * @param {Object} options
 *
 * @return
 */
exports.get = function(params, success, error, http, options) {

	//generate options
	var defaultOptions = {
	};

	if (_.isObject(options)) {
		_.extend(defaultOptions, options);
	}

	//check http library
	if (!http) {
		http = new (require('HttpCK'))();
	}

	if (useContract) {
		//simulate response
		persistData(contract.get(params));
		_.delay(success, Alloy.CFG.api.contract_delay, {});
		return http;
	} else {
		return http.getByTag(TAGS.TAG_GET, url, params, function(response) {
			response = JSON.parse(response.data);
			persistData(response);
			success(response);
		}, error, defaultOptions);
	}

};

/**
 * hide transaction
 * @param {Integer} id
 * @param {Object} params
 * @param {Function} success
 * @param {Function} error
 * @param {HttpCK} http
 * @param {Object} options
 */
exports.hideTransaction = function(id, params, success, error, http, options) {
	if (!http) {
		http = new (require('HttpCK'))();
	}

	if (useContract) {
		//simulate response
		contract.hideTransaction(id, success, error);
		return http;
	} else {
		var SEPARATOR = '/';
		//put request
		return http.putByTag(TAGS.TAG_HIDE_TRANSACTION, url.concat(id + SEPARATOR), params, function(response) {
			response = JSON.parse(response.data);
			persistData(response);
			success(response);
		}, error, options);
	}
};

/**
 * send new transaction
 * @param {Object} params
 * @param {Function} success
 *  @param {Function} error
 * @param {HttpCK}
 * @param {Object} options
 */
exports.send = function(params, success, error, http, options) {
	if (!http) {
		http = new (require('HttpCK'))();
	}
	return http.postByTag(TAGS.TAG_SEND_TRANSACTION, url, params, function(response) {
		response = JSON.parse(response.data);
		persistData(response);
		success(response);
	}, error, options);
};

/**
 * update payment
 * @param {Integer} id
 * @param {Object} params
 * @param {Function} success
 * @param {Function} error
 * @param {HttpCK} http
 * @param {Object} options
 */
exports.updatePayment = function(id, params, success, error, http, options) {
	if (!http) {
		http = new (require('HttpCK'))();
	}

	var SEPARATOR = '/';
	//put request
	return http.putByTag(TAGS.TAG_UPDATE_PAYMENT, url.concat(id + SEPARATOR), params, function(response) {
		response = JSON.parse(response.data);
		persistData(response);
		success(response);
	}, error, options);
};
/** -------
 public
 -------- **/
exports.TAGS = TAGS;
