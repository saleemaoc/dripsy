/** --------
 Constants
 -------- **/
var TAG = 'lib/services/TagService';

var TAGS = {
	TAG_GET : 'TagService_Get',
	TAG_ADD_TO_USER : 'TagService_AddTagToUser',
	TAG_SEND : 'TagService_Send'
};
/** --------
 Variables
 -------- **/

/**
 * contract
 */
var useContract = Alloy.CFG.api.use_contract_tag;
var contract;
if (useContract) {
	//contract = require('contracts/');
}

/**
 * End point
 */
var url = Alloy.CFG.url + Alloy.CFG.api.tag;
var dao;
/** --------
 Inject
 -------- **/
/**
 * inject dependencies
 * @param {Object} daoImpl
 */
exports.inject = function(daoImpl) {
	dao = daoImpl;
	return this;
};

/** --------
 Methods
 -------- **/

/**
 * persist data in database
 * @param {Object} response
 */
var persistData = function(response) {
	if (_.isObject(dao)) {
		if (_.isFunction(dao.updateData)) {
			if (_.isArray(response.results)) {
				dao.updateData(response.results);
			} else {
				dao.updateData(response);
			}
		} else {
			Ti.API.error(TAG, 'dao must implement "updateData" function');
		}
	}
};

/**
 * get tags
 * @param {Object} params
 * @param {Function} success
 * @param {Function} error
 * @param {HttpCK} http
 * @param {Object} options
 *
 * @return
 */
exports.get = function(params, success, error, http, options) {

	//generate options
	var defaultOptions = {
		ttl : Alloy.CFG.api.cache_time_tag
	};

	if (_.isObject(options)) {
		_.extend(defaultOptions, options);
	}

	//check http library
	if (!http) {
		http = new (require('HttpCK'))();
	}

	if (useContract) {
		//simulate response
		persistData(contract.get(params));
		_.delay(success, Alloy.CFG.api.contract_delay, {});
		return http;
	} else {
		return http.getByTag(TAGS.TAG_GET, url, params, function(response) {
			response = JSON.parse(response.data);
			persistData(response);
			success(response);
		}, error, defaultOptions);
	}

};

/**
 * send new interests to service
 * @param {Object} params
 * @param {Function} success
 * @param {Function} error
 * @param {HttpCK} http
 * @param {Object} options
 */
exports.send = function(params, success, error, http, options) {
	//check http library
	if (!http) {
		http = new (require('HttpCK'))();
	}

	http.postByTag(TAGS.TAG_SEND, url, params, function(response) {
		response = JSON.parse(response.data);
		persistData(response);
		success(response);
	}, error, options);

};
/** -------
 public
 -------- **/
exports.TAGS = TAGS;
