/** --------
 Constants
 -------- **/
var TAG = 'lib/services/GiftService';

var TAGS = {
	TAG_GET : 'GiftService_Get',
	TAG_SEND : 'GiftService_Send',
	TAG_UPDATE : 'GiftService_Update',
	TAG_GET_BY_ID : 'GiftService_Get_By_Id'
};
/** --------
 Variables
 -------- **/

/**
 * contract
 */
var useContract = Alloy.CFG.api.use_contract;
var contract;
if (useContract) {
	contract = require('contracts/contract_gift');
}

/**
 * End point
 */
var url = Alloy.CFG.url + Alloy.CFG.api.gift;

/** --------
 Inject
 -------- **/
/**
 * inject dependencies
 * @param {Object} daoImpl
 */
exports.inject = function(daoImpl) {
	dao = daoImpl;
	return this;
};

/** --------
 Methods
 -------- **/

/**
 * persist data in database
 * @param {Object} response
 */
var persistData = function(response) {
	if (_.isObject(dao)) {
		if (_.isFunction(dao.updateData)) {
			if (_.isArray(response.results)) {
				dao.updateData(response.results);
			} else {
				dao.updateData(response);
			}
		} else {
			Ti.API.error(TAG, 'dao must implement "updateData" function');
		}
	}
};

/**
 * get gifts
 * @param {Object} params
 * @param {Function} success
 * @param {Function} error
 * @param {HttpCK} http
 * @param {Object} options
 *
 * @return
 */
exports.get = function(params, success, error, http, options) {

	//generate options
	var defaultOptions = {
	};

	if (_.isObject(options)) {
		_.extend(defaultOptions, options);
	}

	//check http library
	if (!http) {
		http = new (require('HttpCK'))();
	}

	if (useContract) {
		//simulate response
		persistData(contract.getCity(params));
		_.delay(success, Alloy.CFG.api.contract_delay, {});
		return http;
	} else {
		return http.getByTag(TAGS.TAG_GET, url, params, function(response) {
			response = JSON.parse(response.data);
			persistData(response);
			success(response);
		}, error, defaultOptions);
	}

};

/**
 * send gift
 * @param {Object} params
 * @param {Function} success
 * @param {Function} error
 * @param {HttpCK} http
 * @param {Object} options
 *
 * @return
 */
exports.send = function(params, success, error, http, options) {

	//check http library
	if (!http) {
		http = new (require('HttpCK'))();
	}

	return http.postByTag(TAGS.TAG_SEND, url, params, function(response) {
		response = JSON.parse(response.data);
		persistData(response);
		success(response);
	}, error, options);
};

/**
 * accept gift
 * @param {Integer} giftID
 * @param {Object} params
 * @param {Function} success
 * @param {Function} error
 * @param {HttpCK} http
 * @param {Object} options
 *
 * @return
 */
exports.update = function(giftID, params, success, error, http, options) {

	//check http library
	if (!http) {
		http = new (require('HttpCK'))();
	}
	var SLASH = '/';

	return http.putByTag(TAGS.TAG_UPDATE, url.concat(giftID).concat(SLASH), params, function(response) {
		response = JSON.parse(response.data);
		persistData(response);
		success(response);
	}, error, options);
};

exports.getById = function(id, success, error, http, options) {
	//check http library
	if (!http) {
		http = new (require('HttpCK'))();
	}
	var SLASH = '/';

	return http.putByTag(TAGS.TAG_GET_BY_ID, url.concat(id).concat(SLASH), params, function(response) {
		response = JSON.parse(response.data);
		persistData(response);
		success(response);
	}, error, options);
};

/** -------
 public
 -------- **/
exports.TAGS = TAGS;
