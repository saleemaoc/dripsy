/** --------
 Constants
 -------- **/
var TAG = 'lib/services/ConversationService';

var TAGS = {
	TAG_GET : 'ConversationService_Get',
	TAG_GET_BY_ID : 'ConversationService_GetById',
	TAG_POST : 'ConversationService_Post',
	TAG_PUT : 'ConversationService_Put',
	TAG_DELETE : 'ConversationService_Delete'
};
/** --------
 Variables
 -------- **/

/**
 * contract
 */
var useContract = Alloy.CFG.api.use_contract_conversation;
var contract;
if (useContract) {
	contract = require('contracts/contract_conversation');
}

/**
 * End point
 */
var url = Alloy.CFG.url + Alloy.CFG.api.conversation;

var dao;

/** --------
 Inject
 -------- **/
/**
 * inject dependencies
 * @param {Object} daoImpl
 */
exports.inject = function(daoImpl) {
	dao = daoImpl;
	return this;
};

/** --------
 Methods
 -------- **/

/**
 * persist data in database
 * @param {Object} response
 */
var persistData = function(response) {
	if (_.isObject(dao)) {
		if (_.isFunction(dao.destroyAll)) {
			dao.destroyAll();
		}
		if (_.isFunction(dao.updateData)) {
			if (_.isArray(response.results)) {
				dao.updateData(response.results);
			} else if (_.isObject(response)) {
				dao.updateData(response);
			}
		} else {
			Ti.API.error(TAG, 'dao must implement "updateData" function');
		}
	}
};

/**
 * get  conversations by user
 * @param {Object} params
 * @param {Function} success
 * @param {Function} error
 * @param {HttpCK} http
 * @param {Object} options
 *
 * @return
 */
exports.get = function(params, success, error, http, options) {

	//generate options
	var defaultOptions = {
		ttl : false
	};

	if (_.isObject(options)) {
		_.extend(defaultOptions, options);
	}

	//check http library
	if (!http) {
		http = new (require('HttpCK'))();
	}

	if (useContract) {
		//simulate response
		persistData(contract.get());
		_.delay(success, Alloy.CFG.api.contract_delay, {});
		return http;
	} else {
		return http.getByTag(TAGS.TAG_GET, url, params, function(response) {
			response = JSON.parse(response.data);
			persistData(response);
			success(response);
		}, error, defaultOptions);
	}

};

/**
 * get  by id
 * @param {Integer} conversationID
 * @param {Function} success
 * @param {Function} error
 * @param {HttpCK} http
 * @param {Object} options
 *
 * @return
 */
exports.getByID = function(conversationID, success, error, http, options) {

	//generate options
	var defaultOptions = {
		ttl : false
	};

	if (_.isObject(options)) {
		_.extend(defaultOptions, options);
	}

	//check http library
	if (!http) {
		http = new (require('HttpCK'))();
	}

	return http.getByTag(TAGS.TAG_GET_BY_ID, url + conversationID + "/", {}, function(response) {
		response = JSON.parse(response.data);
		persistData(response);
		success(response);
	}, error, defaultOptions);

};

/**
 * send new request
 * @param {Integer} toUserID
 * @param {Function} success
 * @param {Function} error
 * @param {HttpCK} http
 * @param {Object} options
 */
exports.sendNewRequest = function(toUserID, success, error, http, options) {

	var fromUserID = Alloy.Globals.UserID;

	var params = {
		to_user_id : toUserID,
		from_user_id : fromUserID
	};

	//check http library
	if (!http) {
		http = new (require('HttpCK'))();
	}

	return http.postByTag(TAGS.TAG_POST, url, params, function(response) {
		Ti.API.info('sendNewRequest', JSON.stringify(response));
		response = JSON.parse(response.data);
		persistData(response);
		success(response);
	}, error, options);

};

/**
 * send accept conversation
 * @param {Integer} conversationID
 * @param {Function} success
 * @param {Function} error
 * @param {HttpCK} http
 * @param {Object} options
 */
exports.accept = function(conversationID, success, error, http, options) {

	var params = {
		state : Alloy.CFG.constants.conversation_state.accepted,
	};

	var SLASH = '/';

	//check http library
	if (!http) {
		http = new (require('HttpCK'))();
	}
	return http.putByTag(TAGS.TAG_PUT, url.concat(conversationID).concat(SLASH), params, function(response) {
		response = JSON.parse(response.data);
		persistData(response);
		success(response);
	}, error, options);

};

/**
 * send reject conversation
 * @param {Integer} conversationID
 * @param {Function} success
 * @param {Function} error
 * @param {HttpCK} http
 * @param {Object} options
 */
exports.reject = function(conversationID, success, error, http, options) {

	var params = {
		state : Alloy.CFG.constants.conversation_state.rejected,
	};

	var SLASH = '/';

	//check http library
	if (!http) {
		http = new (require('HttpCK'))();
	}
	return http.putByTag(TAGS.TAG_PUT, url.concat(conversationID).concat(SLASH), params, function(response) {
		response = JSON.parse(response.data);
		persistData(response);
		success(response);
	}, error, options);

};

/**
 * delete conversation
 * @param {Integer} id
 * @param {Function} success
 * @param {Function} error
 * @param {HttpCK} http
 * @param {Object} options
 */
exports.destroy = function(conversationID, success, error, http, options) {
	if (!http) {
		http = new (require('HttpCK'))();
	}

	var params = {
		deleted : true
	};
	var SLASH = "/";
	return http.putByTag(TAG.TAG_DELETE, url.concat(conversationID).concat(SLASH), params, function(response) {
		if (dao && _.isFunction(dao.deleteByID)) {
			dao.deleteByID(conversationID);
		}
		success({});
	}, function(e) {
		//not found
		if (e.code == 404) {
			if (dao && _.isFunction(dao.deleteByID)) {
				dao.deleteByID(conversationID);
			}
			success({});
		} else {
			error(e);
		}
	}, options);

};
/** -------
 public
 -------- **/
exports.TAGS = TAGS;
