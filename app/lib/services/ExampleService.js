/** --------
 Constants
 -------- **/
var TAG = 'lib/services/ExampleService';
var MODEL = 'example';

var TAGS = {
	TAG_ADD : 'ExampleService_Add',
	TAG_GET : 'ExampleService_Get'
};
/** --------
 Variables
 -------- **/
var contract;
if (Alloy.CFG.api.use_contract) {
	contract = require('contracts/contract_example');
}

var dao;

/** --------
 Inject
 -------- **/
/**
 * inject dependencies
 * @param {Object} daoImpl
 */
exports.inject = function(daoImpl) {
	dao = daoImpl;
	return this;
};

/** --------
 Methods
 -------- **/

/**
 * persist data in database
 * @param {Object} response
 */
var persistData = function(response) {
	if (_.isObject(dao)) {
		if (_.isFunction(dao.updateData)) {
			dao.updateData(response.results);
		} else {
			Ti.API.error(TAG, 'dao must implement "updateData" function');
		}
	}
};

/**
 * get all examples
 * @param {Object} params
 * @param {Function} success
 * @param {Function} error
 * @param {HttpCK} http
 * @param {Object} options
 *
 * @return
 */
exports.get = function(params, success, error, http, options) {

	var url = Alloy.CFG.api.url + Alloy.CFG.api.url.example;

	if (!http) {
		http = new (require('HttpCK'))();
	}

	if (Alloy.CFG.api.use_contract) {
		//simulate response
		persistData(contract.get());
		_.delay(success, Alloy.CFG.api.contract_delay, {});
		return http;
	} else {
		return http.getByTag(TAGS.TAG_GET, url, params, function(response) {
			success(response);
		}, error, options);
	}

};

/**
 * @param {Object} data
 * @param {Function} success
 * @param {Function} error
 * @param {HttpCK} http
 * @param {Object} options
 */
exports.add = function(data, success, error, http, options) {

	var url = Alloy.CFG.api.url + Alloy.CFG.api.url.example;

	if (!http) {
		http = new (require('HttpCK'))();
	}
	if (data.id) {
		return http.postByTag(TAGS.TAG_ADD, url, data, function(response) {
			success(response);
		}, error, options);
	} else {
		return http.putByTag(TAGS.TAG_ADD, url, data, function(response) {
			success(response);
		}, error, options);
	}
};

/** -------
 public
 -------- **/
exports.TAGS = TAGS;
