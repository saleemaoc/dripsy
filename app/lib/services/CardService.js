/** --------
 Constants
 -------- **/
var TAG = 'lib/services/CardService';

var TAGS = {
	TAG_GET : 'CardService_Get'
};
/** --------
 Variables
 -------- **/

/**
 * End point
 */
var url = Alloy.CFG.url + Alloy.CFG.api.usercard;

/** --------
 Methods
 -------- **/

/**
 * add card
 * @param {Object} params
 * @param {Function} success
 * @param {Function} error
 * @param {HttpCK} http
 * @param {Object} options
 *
 * @return
 */
exports.addCard = function(params, success, error, http, options) {

	//generate options
	var defaultOptions = {

	};

	if (_.isObject(options)) {
		_.extend(defaultOptions, options);
	}

	//check http library
	if (!http) {
		http = new (require('HttpCK'))();
	}

	return http.postByTag(TAGS.TAG_POST, url, params, function(response) {
		response = JSON.parse(response.data);
		success(response);
	}, error, options);
};

/** -------
 public
 -------- **/
exports.TAGS = TAGS;
