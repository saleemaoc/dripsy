exports.run = function() {
	if (Ti.App.deployType !== 'production' && Alloy.CFG.test.unit_test) {
		var behave = require('behave');
		require('spec/controllers');
		require('spec/architecture');
		require('spec/dao');
		require('spec/login_controller');
		behave.run();
	}
};
