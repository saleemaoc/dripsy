/**
 * @author jagu
 * @description Español: Integración con Uber
 *
 */

/** --------
 Constants
 -------- **/
var TAG = 'lib/uber';

var UBER = {
	CLIENTE_ID : null/*Alloy.CFG.UBER_CLIENT_ID*/,
	URL : "uber://",
	UNIVERSAL_URL : 'https://m.uber.com/ul/',
	/**
	 * android package
	 */
	PACKAGE : "com.ubercab",

};

/** --------
 Methods
 -------- **/

/**
 * generate pickup url params
 * @param {Object} pickup
 * @param {Object} dropoff
 */
var generatePickupUrlParams = function(pickup, dropoff) {

	//Pickup
	var action = 'action=setPickup';
	var pickupLatitude = 'pickup[latitude]=' + pickup.latitude;
	var pickupLongitude = 'pickup[longitude]=' + pickup.longitude;
	var pickupNickName = 'pickup[nickname]=' + pickup.nickname;
	//Dropoff
	var dropoffLatitude = 'dropoff[latitude]=' + dropoff.latitude;
	var dropoffLongitude = 'dropoff[longitude]=' + dropoff.longitude;
	var dropoffNickName = 'dropoff[nickname]=' + dropoff.nickname;
	var dropoffFormattedAddress = 'dropoff[formated_address]=' + dropoff.formated_address;

	var params = [action, pickupLatitude, pickupLongitude, pickupNickName, dropoffLatitude, dropoffLongitude, dropoffNickName, dropoffFormattedAddress];

	var paramsUrl = "?";
	var stringAppend = "";

	if (_.isString(UBER.CLIENT_ID)) {
		paramsUrl = paramsUrl.concat("client_id=" + UBER.CLIENT_ID);
		stringAppend = "&";
	}
	_.map(params, function(param) {
		paramsUrl = paramsUrl.concat(stringAppend).concat(param);
		stringAppend = "&";
	});

	
	return paramsUrl;
};
/**
 * action set pickup
 * @documentation https://developer.uber.com/docs/ride-requests/tutorials/deep-links/pickup
 * @param {Object} pickup fields required: latitude, longitude, nickname
 * @param {Object} dropoff fields required: latitude, longitude, nickname, formatted_address
 */
exports.setPickup = function(pickup, dropoff) {
	var generateUrl = generatePickupUrlParams(pickup, dropoff);
	Ti.Platform.openURL(UBER.UNIVERSAL_URL.concat(generateUrl));
};

