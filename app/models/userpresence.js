exports.definition = {
	config : {
		columns : {
			"id" : "INTEGER PRIMARY KEY",
			"establishment_id" : "INTEGER",
			"user_id" : "INTEGER",
			"type_presence" : "INTEGER",
			"user_json" : "TEXT"
		},
		adapter : {
			type : "sql",
			collection_name : "userpresence",
			idAttribute : "id"
		}
	},
	extendModel : function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
			mapping : function(modelService) {
				this.set('id', modelService.id);
				this.set('establishment_id', modelService.establishment_id);
				this.set('user_id', modelService.user_id);
				this.set('type_presence', modelService.type_presence);
				if (_.isObject(modelService.user)) {
					var user = Alloy.Globals.DaoBuilder.getUserDao().getModel();
					user.mapping(modelService.user);
					this.set('user_json', user.toJSON());
				}
			},
			/**
			 * get user
			 * @return {User}
			 */
			getUser : function() {
				var user = Alloy.Globals.DaoBuilder.getUserDao().getModel();
				user.set(this.get('user_json'));
				return user;
			},
			/**
			 * get user id
			 * @return {Integer}
			 */
			getUserID : function() {
				return this.get('user_id');
			},
			/**
			 * setter for establishment id
			 */
			setEstablishmentID : function(establishmentID) {
				this.set('establishment_id', establishmentID);
			},
			/**
			 * getter for establishment id
			 * @return {Integer}
			 */
			getEstablishmentID : function() {
				return this.get('establishment_id');
			}
		});

		return Model;
	},
	extendCollection : function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here

			// For Backbone v1.1.2, uncomment the following to override the
			// fetch method to account for a breaking change in Backbone.
			/*
			 fetch: function(options) {
			 options = options ? _.clone(options) : {};
			 options.reset = true;
			 return Backbone.Collection.prototype.fetch.call(this, options);
			 }
			 */
		});

		return Collection;
	}
};
