exports.definition = {
	config : {
		columns : {
			"id" : "INTEGER PRIMARY KEY AUTOINCREMENT",
			"name" : "TEXT",
			"count" : "INTEGER",
			"active" : "INTEGER",
			"popular" : "INTEGER",
			"server_id" : "INTEGER"
		},
		defaults : {
			"active" : 1,
			"popular" : 0,
			"server_id" : -1
		},
		adapter : {
			type : "sql",
			collection_name : "tag",
			idAttribute : "id"

		}
	},
	extendModel : function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
			mapping : function(modelService) {
				this.set('server_id', modelService.id);
				this.set('name', modelService.name);
				this.set('popular', (modelService.popular) ? 1 : 0);
				this.set('count', modelService.count);
				this.save();
			},
			transform : function() {
				var transformed = this.toJSON();
				return transformed;
			},
			toService : function() {
				var json = {};
				if (this.get('server_id') != -1) {
					json.id = this.get('server_id');
				}
				json.name = this.get('name');
				return json;
			},
			/**
			 * getter for id property
			 * @return {Integer}
			 */
			getID : function() {
				return this.get('id');
			},
			/**
			 * getter for property name
			 * @return {String}
			 */
			getName : function() {
				return this.get('name');
			},
			/**
			 * getter for server id
			 * @return {Integer}
			 */
			getServerID : function() {
				return this.get('server_id');
			}
		});

		return Model;
	},
	extendCollection : function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here

			// For Backbone v1.1.2, uncomment the following to override the
			// fetch method to account for a breaking change in Backbone.
			/*
			 fetch: function(options) {
			 options = options ? _.clone(options) : {};
			 options.reset = true;
			 return Backbone.Collection.prototype.fetch.call(this, options);
			 }
			 */
		});

		return Collection;
	}
};
