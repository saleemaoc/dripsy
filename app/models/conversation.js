exports.definition = {
	config : {
		columns : {
			"id" : "INTEGER PRIMARY KEY",
			"from_user_id" : "INTEGER",
			"to_user_id" : "INTEGER",
			"created_date" : "TEXT",
			"updated_date" : "TEXT",
			"last_message_id" : "INTEGER",
			"state" : "INTEGER",
			/**
			 * performance fields
			 */
			"last_message_text" : "TEXT",
			"last_message_date" : "TEXT",
			"from_user_full_name" : "TEXT",
			"from_user_photo" : "TEXT",
			"from_user_gender" : "INTEGER",
			"to_user_full_name" : "TEXT",
			"to_user_photo" : "TEXT",
			"to_user_gender" : "INTEGER",
			"unread" : "INTEGER",
			"deleted" : "INTEGER"
		},
		default : {
			"deleted" : 0,
			"unread" : 1
		},
		adapter : {
			type : "sql",
			collection_name : "conversation",
			idAttribute : "id"
		}
	},
	extendModel : function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
			/**
			 * mapping local model with model service
			 * @param {Object} modelService
			 */
			mapping : function(modelService) {

				if (this.getID() != modelService.id && modelService.from_user_id != Alloy.Globals.UserID) {
					//new conversation
					this.setUnread(true);
				}
				this.setID(modelService.id);
				this.setState(modelService.state);
				//dates
				this.setCreatedDate((require('FormatUtils')).getStringDBToDate(modelService.date));
				this.setUpdatedDate((require('FormatUtils')).getStringDBToDate(modelService.modified));
				//users
				var userDao = Alloy.Globals.DaoBuilder.getUserDao();
				this.setFromUserID(modelService.from_user_id);
				this.setToUserID(modelService.to_user_id);
				//save user information
				if (_.isObject(modelService.from_user)) {
					var fromUser = userDao.findByID(modelService.from_user_id);
					fromUser.mapping(modelService.from_user);
					this.setFromUser(fromUser);
					fromUser = null;
				}
				if (_.isObject(modelService.to_user)) {
					var toUser = userDao.findByID(modelService.to_user_id);
					toUser.mapping(modelService.to_user);
					this.setToUser(toUser);
					toUser = null;
				}

				//gc
				userDao = null;

				//last message
				if (_.isObject(modelService.last_message)) {
					if (this.getLastMessageID() != modelService.last_message_id && modelService.last_message.from_user_id != Alloy.Globals.UserID) {
						this.setUnread(true);
					} else {
						this.setUnread(false);
					}
					this.setLastMessageID(modelService.last_message_id);
					this.setLastMessageText(modelService.last_message.text);
					this.setLastMessageDate((require('FormatUtils')).getStringDBToDate(modelService.last_message.date));
				}

				this.setDeleted(modelService.deleted ? 1 : 0);
				this.save();
			},
			/**
			 * setter for id
			 * @param {Integer} id
			 */
			setID : function(id) {
				this.set('id', id);
			},
			/**
			 * getter for id
			 * @return {Integer}
			 */
			getID : function() {
				return this.get('id');
			},
			/**
			 * setter for from user id
			 * @param {Integer} fromUserID
			 */
			setFromUserID : function(fromUserID) {
				this.set('from_user_id', fromUserID);
			},
			/**
			 * getter for from user id
			 * @return {Integer}
			 */
			getFromUserID : function() {
				return this.get('from_user_id');
			},
			/**
			 * setter for to user id
			 * @param {Integer} toUserID
			 */
			setToUserID : function(toUserID) {
				this.set('to_user_id', toUserID);
			},
			/**
			 * getter for to user id
			 * @return {Integer}
			 */
			getToUserID : function() {
				return this.get('to_user_id');
			},
			/**
			 * setter for last message id
			 * @param {Integer} lastMessageID
			 */
			setLastMessageID : function(lastMessageID) {
				this.set('last_message_id', lastMessageID);
			},
			/**
			 * getter for last message id
			 * @return {Integer}
			 */
			getLastMessageID : function() {
				return this.get('last_message_id');
			},
			/**
			 * setter for last message text
			 * @param {String} lastMessageText
			 */
			setLastMessageText : function(lastMessageText) {
				this.set('last_message_text', lastMessageText);
			},
			/**
			 * getter for last message text
			 * @return {String}
			 */
			getLastMessageText : function() {
				return this.get('last_message_text');
			},
			/**
			 * setter for last message date
			 * @param {String} lastMessageDate
			 */
			setLastMessageDate : function(lastMessageDate) {
				this.set('last_message_date', lastMessageDate);
			},
			/**
			 * getter for last message date
			 * @return {String}
			 */
			getLastMessageDate : function() {
				return this.get('last_message_date');
			},
			/**
			 * setter for from user full name
			 * @param {String} fromUserFullName
			 */
			setFromUserFullName : function(fromUserFullName) {
				this.set('from_user_full_name', fromUserFullName);
			},
			/**
			 * getter for from user full name
			 * @return {String}
			 */
			getFromUserFullName : function() {
				return this.get('from_user_full_name');
			},
			/**
			 * setter for to user full name
			 * @param {String} toUserFullName
			 */
			setToUserFullName : function(toUserFullName) {
				this.set('to_user_full_name', toUserFullName);
			},
			/**
			 * getter for to user full name
			 * @return {String}
			 */
			getToUserFullName : function() {
				return this.get('to_user_full_name');
			},
			/**
			 * setter for from user photo
			 * @param {String} fromUserPhoto
			 */
			setFromUserPhoto : function(fromUserPhoto) {
				this.set('from_user_photo', fromUserPhoto);
			},
			/**
			 * getter for from user photo
			 * @return {String}
			 */
			getFromUserPhoto : function() {
				return this.get('from_user_photo');
			},
			/**
			 * setter for from user gender
			 * @param {Integer} fromUserGender
			 */
			setFromUserGender : function(fromUserGender) {
				this.set('from_user_gender', fromUserGender);
			},
			/**
			 * getter for from user gender
			 * @return {Integer}
			 */
			getFromUserGender : function() {
				return this.get('from_user_gender');
			},
			/**
			 * setter for to user photo
			 * @param {String} toUserPhoto
			 */
			setToUserPhoto : function(toUserPhoto) {
				this.set('to_user_photo', toUserPhoto);
			},
			/**
			 * getter for to user photo
			 * @return {String}
			 */
			getToUserPhoto : function() {
				return this.get('to_user_photo');
			},
			/**
			 * setter for to user gender
			 * @param {Integer} toUserGender
			 */
			setToUserGender : function(toUserGender) {
				this.set('to_user_gender', toUserGender);
			},
			/**
			 * getter for to user gender
			 * @return {Integer}
			 */
			getToUserGender : function() {
				return this.get('to_user_gender');
			},
			/**
			 * setter for to updated date
			 * @param {String} updatedDate
			 */
			setUpdatedDate : function(updatedDate) {
				this.set('updated_date', updatedDate);
			},
			/**
			 * getter for to updated date
			 * @return {String}
			 */
			getUpdatedDate : function() {
				return this.get('updated_date');
			},
			/**
			 * setter for to created date
			 * @param {String} createdDate
			 */
			setCreatedDate : function(createdDate) {
				this.set('created_date', createdDate);
			},
			/**
			 * getter for to created date
			 * @return {String}
			 */
			getCreatedDate : function() {
				return this.get('created_date');
			},
			/**
			 * setter for unread
			 * @param {Boolean} unread
			 */
			setUnread : function(unread) {
				this.set('unread', unread ? 1 : 0);
			},
			/**
			 * getter for unread
			 * @return {Boolean}
			 */
			getUnread : function() {
				return (this.get('unread') == 1);
			},
			/**
			 * setter for from user
			 * @param {Object} fromUser
			 */
			setFromUser : function(fromUser) {
				this.setFromUserFullName(fromUser.getFullName());
				this.setFromUserPhoto(fromUser.getPhotoMiniOrDefault());
				this.setFromUserGender(fromUser.getGender());
			},
			/**
			 * setter for to user
			 * @param {Object} toUser
			 */
			setToUser : function(toUser) {
				this.setToUserFullName(toUser.getFullName());
				this.setToUserPhoto(toUser.getPhotoMiniOrDefault());
				this.setToUserGender(toUser.getGender());
			},
			/**
			 * setter for state
			 * @param {Integer} state
			 */
			setState : function(state) {
				this.set('state', state);
			},
			/**
			 * getter for state
			 * @return {Integer}
			 */
			getState : function() {
				return this.get('state');
			},
			/**
			 * return if state is pending
			 */
			isPending : function() {
				return this.getState() == Alloy.CFG.constants.conversation_state.pending;
			},
			isAccepted : function() {
				return this.getState() == Alloy.CFG.constants.conversation_state.accepted;
			},
			/**
			 * setter for deleted
			 * @param {Integer} deleted
			 */
			setDeleted : function(deleted) {
				this.set('deleted', deleted);
			},
			/**
			 * getter for deleted
			 * @return {Integer}
			 */
			getDeleted : function() {
				return this.get('deleted');
			},
			/**
			 * get pretty transaction date
			 * @return {String}
			 */
			getPrettyUpdatedDate : function() {
				var date = (require('FormatUtils')).getDateToString(this.getUpdatedDate());
				if (!_.isDate(date)) {
					date = new Date();
				}
				return (require('pretty')).prettyDate(date);
			},
			/**
			 * getter for other user id
			 */
			getOtherUserID : function() {
				if (this.getFromUserID() == Alloy.Globals.UserID) {
					return this.getToUserID();
				} else {
					return this.getFromUserID();
				}
			},
			getLastMessageTextShort : function() {
				var lastMessageText = this.getLastMessageText();
				if (_.isString(lastMessageText) && !_.isEmpty(lastMessageText) && lastMessageText.length > 38) {
					lastMessageText = lastMessageText.substring(0, 37);
					if (OS_IOS) {
						lastMessageText = lastMessageText.concat('...');
					}
				}
				return lastMessageText;
			},
			/**
			 * transform data to databinding in componentes
			 */
			transform : function() {

				var transformed = this.toJSON();
				var gender = null;
				
				if (this.getFromUserID() == Alloy.Globals.UserID) {
					transformed.other_user_full_name = this.getToUserFullName();
					transformed.other_user_photo = this.getToUserPhoto();
					gender = this.getToUserGender();
				} else {
					transformed.other_user_full_name = this.getFromUserFullName();
					transformed.other_user_photo = this.getFromUserPhoto();
					gender = this.getFromUserGender();
				}

				transformed.other_user_photo_default = (gender == Alloy.CFG.constants.gender.male) ? Alloy.Globals.Images.avatar_men : Alloy.Globals.Images.avatar_female;

				transformed.unread = this.getUnread();

				//If the status is pending it is notified in the conversation that it is an invitation
				if (this.isPending()) {
					transformed.last_message_text = L('chat_invitation', 'Chat invitation');
				}

				transformed.updated_date_pretty_date = this.getPrettyUpdatedDate();
				return transformed;
			}
		});

		return Model;
	},
	extendCollection : function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here

			// For Backbone v1.1.2, uncomment the following to override the
			// fetch method to account for a breaking change in Backbone.
			/*
			 fetch: function(options) {
			 options = options ? _.clone(options) : {};
			 options.reset = true;
			 return Backbone.Collection.prototype.fetch.call(this, options);
			 }
			 */
		});

		return Collection;
	}
};

