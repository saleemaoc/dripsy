exports.definition = {
	config : {
		columns : {
			"id" : "INTEGER PRIMARY KEY",
			"name" : "TEXT",
			"country_id" : "INTEGER",
			"enabled" : "INTEGER",
			"taxi_services" : "TEXT"
		},
		adapter : {
			type : "sql",
			collection_name : "city",
			idAttribute : "id"
		}
	},
	extendModel : function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
			mapping : function(modelService) {
				this.set('id', modelService.id);
				this.set('name', modelService.name);
				this.set('country_id', modelService.country_id);
				this.set('enabled', modelService.enabled ? 1 : 0);
				this.set('taxi_services', JSON.stringify(modelService.taxi_services));
				this.save();
			}
		});

		return Model;
	},
	extendCollection : function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here

			// For Backbone v1.1.2, uncomment the following to override the
			// fetch method to account for a breaking change in Backbone.
			/*
			 fetch: function(options) {
			 options = options ? _.clone(options) : {};
			 options.reset = true;
			 return Backbone.Collection.prototype.fetch.call(this, options);
			 }
			 */
		});

		return Collection;
	}
};
