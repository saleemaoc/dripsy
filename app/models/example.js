exports.definition = {
	config : {
		columns : {
			"id" : "INTEGER PRIMARY KEY",
			"name" : "TEXT",
			"description" : "TEXT"
		},
		adapter : {
			type : "sql",
			collection_name : "example",
			idAttribute : "id"
		}
	},
	extendModel : function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
			mapping : function(modelService) {

				this.set('id', modelService.id);

				this.set('name', modelService.name);
				this.set('description', modelService.description);

				this.save();
			},
			transform : function() {
				var transformed = this.toJSON();

				return transformed;
			}
		});

		return Model;
	},
	extendCollection : function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here

			// For Backbone v1.1.2, uncomment the following to override the
			// fetch method to account for a breaking change in Backbone.
			/*
			 fetch: function(options) {
			 options = options ? _.clone(options) : {};
			 options.reset = true;
			 return Backbone.Collection.prototype.fetch.call(this, options);
			 }
			 */
		});

		return Collection;
	}
};
