exports.definition = {
	config : {
		columns : {
			/**
			 * Information
			 */
			"id" : "INTEGER PRIMARY KEY",
			"name" : "TEXT",
			"description" : "TEXT",
			"address" : "TEXT",
			"city_id" : "INTEGER",
			"longitude" : "TEXT",
			"latitude" : "TEXT",
			"state" : "INTEGER",
			"image" : "TEXT",
			"image_mini" : "TEXT",
			"type" : "INTEGER",
			"ranking" : "INTEGER",
			"website" : "TEXT",
			"phone_number" : "TEXT",
			"allow_sales_ticket" : "INTEGER",
			"show_price_list" : "INTEGER",
			"time_zone" : "TEXT",
			"stripe_account_id" : "TEXT",
			/**
			 * Establishment hour
			 */
			"hour_monday_from" : "TEXT",
			"hour_monday_to" : "TEXT",
			"hour_monday_tomorrow" : "INTEGER",
			"hour_tuesday_from" : "TEXT",
			"hour_tuesday_to" : "TEXT",
			"hour_tuesday_tomorrow" : "INTEGER",
			"hour_wednesday_from" : "TEXT",
			"hour_wednesday_to" : "TEXT",
			"hour_wednesday_tomorrow" : "INTEGER",
			"hour_thursday_from" : "TEXT",
			"hour_thursday_to" : "TEXT",
			"hour_thursday_tomorrow" : "INTEGER",
			"hour_friday_from" : "TEXT",
			"hour_friday_to" : "TEXT",
			"hour_friday_tomorrow" : "INTEGER",
			"hour_saturday_from" : "TEXT",
			"hour_saturday_to" : "TEXT",
			"hour_saturday_tomorrow" : "INTEGER",
			"hour_sunday_from" : "TEXT",
			"hour_sunday_to" : "TEXT",
			"hour_sunday_tomorrow" : "INTEGER",
			/**
			 * Establishment assistants
			 */
			"female_count" : "INTEGER",
			"male_count" : "INTEGER",
			/**
			 * User ranking to establishment
			 */
			"user_ranking" : "INTEGER",
			"user_ranking_id" : "INTEGER",
			/**
			 * prevent update old cache data
			 */
			"modified" : "TEXT"
		},
		defaults : {
			"female_count" : 0,
			"male_count" : 0,
			"user_ranking" : 0,
			"user_ranking_id" : -1,
			"ranking" : 0,
			"modified" : ''
		},
		adapter : {
			type : "sql",
			collection_name : "establishment",
			idAttribute : "id"
		}
	},
	extendModel : function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
			//@override
			save : function(attrs, options) {
				var date = (require('FormatUtils')).getStringDBToDate();
				this.set('modified', date);
				// Proxy the call to the original save function
				Backbone.Model.prototype.save.call(this, attrs, options);
			},
			/**
			 * Mapping local model with model service and save local model
			 * @param {Object} modelService
			 */
			mapping : function(modelService) {

				//check if need update
				var needUpdated = true;

				if (!_.isEmpty(this.get('modified'))) {
					var formatUtils = require('FormatUtils');
					var currentModified = formatUtils.getDateByStringDB(this.get('modified'));
					var serverModified = formatUtils.getDateToString(modelService.modified);
					formatUtils = null;
					needUpdated = (serverModified.valueOf() >= currentModified.valueOf());
				}
				if (needUpdated) {

					this.set('id', modelService.id);
					this.set('name', modelService.name);
					this.set('description', modelService.description);
					if (_.isString(modelService.image_name)) {
						this.set('image', Alloy.Globals.JsUtils.createURLToCDN(modelService.image_name, Alloy.Globals.Dimens.establishment_image_cdn_size));
						this.set('image_mini', Alloy.Globals.JsUtils.createURLToCDN(modelService.image_name, Alloy.Globals.Dimens.establishment_image_mini_cdn_size));
					}
					this.set('state', modelService.state);
					this.set('city_id', modelService.city_id);
					this.set('address', modelService.address);
					this.set('show_price_list', modelService.show_price_list ? 1 : 0);
					this.set('allow_sales_ticket', modelService.allow_sales_ticket ? 1 : 0);
					this.set('stripe_account_id', modelService.stripe_account_id);

					this.set('longitude', modelService.longitude);
					this.set('latitude', modelService.latitude);

					this.set('type', modelService.type);

					this.set('website', modelService.website);
					this.set('phone_number', modelService.phone_number);
					this.set('ranking', modelService.ranking);

					this.set('time_zone', modelService.time_zone);

					this.set('hour_monday_from', modelService.hour_monday_from);
					this.set('hour_monday_to', modelService.hour_monday_to);
					this.set('hour_monday_tomorrow', modelService.hour_monday_more_24_hours ? 1 : 0);

					this.set('hour_tuesday_from', modelService.hour_tuesday_from);
					this.set('hour_tuesday_to', modelService.hour_tuesday_to);
					this.set('hour_tuesday_tomorrow', modelService.hour_tuesday_more_24_hours ? 1 : 0);

					this.set('hour_wednesday_from', modelService.hour_wednesday_from);
					this.set('hour_wednesday_to', modelService.hour_wednesday_to);
					this.set('hour_wednesday_tomorrow', modelService.hour_wednesday_more_24_hours ? 1 : 0);

					this.set('hour_thursday_from', modelService.hour_thursday_from);
					this.set('hour_thursday_to', modelService.hour_thursday_to);
					this.set('hour_thursday_tomorrow', modelService.hour_thursday_more_24_hours ? 1 : 0);

					this.set('hour_friday_from', modelService.hour_friday_from);
					this.set('hour_friday_to', modelService.hour_friday_to);
					this.set('hour_friday_tomorrow', modelService.hour_friday_more_24_hours ? 1 : 0);

					this.set('hour_saturday_from', modelService.hour_saturday_from);
					this.set('hour_saturday_to', modelService.hour_saturday_to);
					this.set('hour_saturday_tomorrow', modelService.hour_saturday_more_24_hours ? 1 : 0);

					this.set('hour_sunday_from', modelService.hour_sunday_from);
					this.set('hour_sunday_to', modelService.hour_sunday_to);
					this.set('hour_sunday_tomorrow', modelService.hour_sunday_more_24_hours ? 1 : 0);

					this.set('male_count', modelService.male_count);
					this.set('female_count', modelService.female_count);

					//user ranked
					if (_.isObject(modelService.user_ranking)) {
						this.set('user_ranking_id', modelService.user_ranking.id);
						this.set('user_ranking', modelService.user_ranking.ranking);
					}
					this.save();
				}

			},
			/**
			 * getter for name
			 * @return {String}
			 */
			getName : function() {
				return this.get('name');
			},
			/**
			 * getter for latitude property
			 */
			getLatitude : function() {
				return this.get('latitude');
			},
			/**
			 * getter for ranking
			 * @return {Integer}
			 */
			getRanking : function() {
				return this.get('ranking');
			},
			/**
			 * getter for show price list
			 */
			getShowPriceList : function() {
				return (this.get('show_price_list') == 1);
			},
			/**
			 * getter for allow sales ticket
			 */
			getAllowSalesTicket : function() {
				return (this.get('allow_sales_ticket') == 1);
			},
			/**
			 * get stripe account id
			 */
			getStripeAccountId : function(){
				return this.get('stripe_account_id');	
			},
			/**
			 * get longitude property
			 */
			getLongitude : function() {
				return this.get('longitude');
			},
			/**
			 * get full address
			 */
			getFullAddress : function() {
				return this.getAddress() + ' ' + this.getCity();
			},
			/**
			 * getter for website
			 * @return {String}
			 */
			getWebsite : function() {
				return this.get('website');
			},
			/**
			 * getter for address
			 * @return {String}
			 */
			getAddress : function() {
				return this.get('address');
			},
			/**
			 * getter for image
			 * @return {String}
			 */
			getImage : function() {
				return this.get('image');
			},
			getImageMini : function() {
				return this.get('image_mini');
			},
			getTimeZone : function() {
				return this.get('time_zone');
			},
			/**
			 * get hout opened to day
			 * @param {String} day
			 * @return {Array}
			 */
			getHourOpenedToDay : function(day) {

				if (!day) {
					day = (require('JsUtils')).getDayStringToDate();
				}

				return {
					from : this.get('hour_'.concat(day).concat('_from')),
					to : this.get('hour_'.concat(day).concat('_to')),
				};

			},
			/**
			 * get date from and to open by hours
			 * @param {String} hourFrom
			 * @param {String} hourTo
			 * @return {Array}
			 */
			getDateFromToOpenByHours : function(hourFrom, hourTo, timeZone, tomorrow, restADay) {
				var fromDate,
				    toDate;
				if (_.isString(hourFrom) && _.isString(hourTo) && _.isString(timeZone)) {
					//HACK if timezone equal to UTC or GMT don't set TZ
					if (timeZone == 'UTC' || timeZone == 'GMT') {
						fromDate = Alloy.Globals.moment.utc(hourFrom, 'HH:mm:ss').toDate();
						toDate = Alloy.Globals.moment.utc(hourTo, 'HH:mm:ss').toDate();
					} else {
						fromDate = Alloy.Globals.momentTZ.tz(hourFrom, 'HH:mm:ss', timeZone).toDate();
						toDate = Alloy.Globals.momentTZ.tz(hourTo, 'HH:mm:ss', timeZone).toDate();
					}
					if (tomorrow) {
						toDate.setDate(toDate.getDate() + 1);
					}

					if (restADay) {
						fromDate.setDate(fromDate.getDate() - 1);
						toDate.setDate(toDate.getDate() - 1);
					}
				}
				return {
					from : fromDate,
					to : toDate
				};
			},
			/*
			 * get phone number is not empty
			 */
			getIsNotEmptyPhoneNumber : function() {
				return _.isString(this.get('phone_number')) && !_.isEmpty(this.get('phone_number'));
			},
			/*
			 * get website is not empty
			 */
			getIsNotEmptyWebsite : function() {
				return _.isString(this.get('website')) && !_.isEmpty(this.get('website'));
			},
			getTomorrowByDay : function(day) {
				var field = 'hour_' + day + '_tomorrow';
				return this.get(field) == 1;
			},
			/**
			 * get label is open
			 */
			getIsOpen : function(currentDay, currentDate, timeZone) {
				return {
					message : "",
					color : Alloy.Globals.Colors.green,
					value : true,
					openToday : true
				};
				
				var hourFromTo = this.getHourOpenedToDay(currentDay);
				var result = this.checkIsOpen(currentDay, currentDate, timeZone, false);
				if (!result.isOpen) {
					var yesterdayDay = Alloy.Globals.JsUtils.getDayStringToDate(Alloy.Globals.JsUtils.getYesterdayDate());
					var resultYesterday = this.checkIsOpen(yesterdayDay, currentDate, true);

					if (resultYesterday.isOpen) {
						result.isOpen = true;
					}
				}

				var openToday = (_.isObject(result.dateFromTo) && _.isDate(result.dateFromTo.to) && currentDate.getTime() < result.dateFromTo.to.getTime());
				//Para pasar a formato 24hs en vez de 12hs, quitar el "WithMeridian" de "getHourFormatToDateWithMeridian" de la sentencia debajo.
				return {
					message : result.isOpen ? (L('open', 'Open')).toUpperCase() : openToday ? String.format(L('today_opens_at', 'Today opens at %s'), (require('FormatUtils')).getHourFormatToDateWithMeridian(result.dateFromTo.from)) : (L('closed', 'Closed')).toUpperCase(),
					color : result.isOpen ? Alloy.Globals.Colors.green : Alloy.Globals.Colors.red,
					value : result.isOpen,
					openToday : result.isOpen ? result.isOpen : openToday
				};
			},
			checkIsOpen : function(currentDay, currentDate, timeZone, restADay) {

				var result = {
					isOpen : false
				};
				var hourFromTo = this.getHourOpenedToDay(currentDay);

				if (!_.isEmpty(hourFromTo.from)) {
					var dateFromTo = this.getDateFromToOpenByHours(hourFromTo.from, hourFromTo.to, timeZone, this.getTomorrowByDay(currentDay), restADay);
					result.isOpen = Alloy.Globals.moment(currentDate).isBetween(dateFromTo.from, dateFromTo.to);
					result.dateFromTo = dateFromTo;
				}

				return result;

			},
			/**
			 * get distance  to current location
			 */
			getDistance : function() {
				if (Alloy.Globals.CoordLatitude && Alloy.Globals.CoordLongitude) {
					return (require('JsUtils')).getDistanceToCoords({
						latitude : Alloy.Globals.CoordLatitude,
						longitude : Alloy.Globals.CoordLongitude
					}, {
						latitude : parseFloat(this.getLatitude()),
						longitude : parseFloat(this.getLongitude())
					}, Alloy.Globals.CurrentCountryUnitDistance);
				}
				return null;
			},
			/**
			 * get annotation information to map
			 */
			getAnnotationMap : function() {

				var annotationMap = {
					latitude : parseFloat(this.get('latitude')),
					longitude : parseFloat(this.get('longitude')),
					title : this.get('name'),
					establishment_id : this.get('id'),
					//image : Alloy.Globals.Images.map_pin_annotation
					customView : Alloy.Globals.Images.getMapPinAnnotation()
				};
				return annotationMap;
			},
			/**
			 * get city name
			 */
			getCity : function() {
				var city = Alloy.createModel('city');
				city.fetch({
					id : this.get('city_id')
				});
				return city.get('name');
			},
			/**
			 * update user ranking
			 * @param {Integer} rankingID
			 * @param {Integer} userRanking
			 */
			updateUserRanking : function(rankingID, userRanking) {
				this.set('user_ranking_id', rankingID);
				this.set('user_ranking', userRanking);
				this.save();
			},
			/**
			 * update assistant
			 * @param {Integer} maleCount
			 * @param {Integer} femaleCount
			 */
			updateAssistant : function(maleCount, femaleCount) {
				this.set('male_count', maleCount);
				this.set('female_count', femaleCount);
				this.save();
			},
			/**
			 * getter is open today
			 * @return {Boolean}
			 */
			isOpenToday : function() {
				return this.getIsOpen(Alloy.Globals.CurrentDay(), Alloy.Globals.CurrentDate(), this.getTimeZone()).value;
			},
			/**
			 *  getter to check is establishment's stripe account connected
			 *  @return {Boolean}
			 */
			isStripeConnected : function() {
				return !_.isEmpty(this.get('stripe_account_id'));
			},
			/**
			 *
			 */
			notOpenToday : function() {
				return !this.getIsOpen(Alloy.Globals.CurrentDay(), Alloy.Globals.CurrentDate(), this.getTimeZone()).openToday;
			},
			/**
			 * transform data to databinding in componentes
			 */
			transform : function() {
				var transformed = this.toJSON();
				transformed.name = this.get('name').toUpperCase();

				//calculate ranking
				var ranking = this.get('ranking');
				var star_enabled = 1;
				var star_disabled = 0.5;

				transformed.rank_1 = ranking >= 1 ? star_enabled : star_disabled;
				transformed.rank_2 = ranking >= 2 ? star_enabled : star_disabled;
				transformed.rank_3 = ranking >= 3 ? star_enabled : star_disabled;
				transformed.rank_4 = ranking >= 4 ? star_enabled : star_disabled;
				transformed.rank_5 = ranking >= 5 ? star_enabled : star_disabled;

				//calculate is open
				var isOpen = this.getIsOpen(Alloy.Globals.CurrentDay(), Alloy.Globals.CurrentDate(), this.getTimeZone());
				transformed.open = isOpen.message;
				transformed.open_color = isOpen.color;

				//calculate distance
				var distance = this.getDistance();
				transformed.distance_visible = !_.isNull(distance);
				transformed.distance = distance ? distance + ' ' + Alloy.Globals.CurrentCountryDistanceSymbol : L('distance_default_label', '-');

				//image ios
				if (OS_IOS && _.isString(transformed.image)) {
					transformed.image = encodeURI(transformed.image);
				}
				//gc
				dateFromTo = null;
				transformed.state_color = Alloy.Globals.Colors.green;
				return transformed;
			},
		});

		return Model;
	},
	extendCollection : function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here

			// For Backbone v1.1.2, uncomment the following to override the
			// fetch method to account for a breaking change in Backbone.
			/*
			 fetch: function(options) {
			 options = options ? _.clone(options) : {};
			 options.reset = true;
			 return Backbone.Collection.prototype.fetch.call(this, options);
			 }
			 */
			deleteAll : function(type) {

				var collection = this;

				var sql = "DELETE FROM " + collection.config.adapter.collection_name + ' WHERE type = ' + type;
				db = Ti.Database.open(collection.config.adapter.db_name);
				db.execute(sql);
				db.close();

				collection.trigger('sync');

			}
		});

		return Collection;
	}
};
