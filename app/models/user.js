exports.definition = {
	config : {
		columns : {
			"id" : "INTEGER PRIMARY KEY",
			"first_name" : "TEXT",
			"last_name" : "TEXT",
			"mail" : "TEXT",
			"gender" : "INTEGER",
			"date_birth" : "TEXT",
			"password" : "TEXT",
			"confirm_terms_condition" : "INTEGER",
			"photo" : "TEXT",
			"photo_mini" : "TEXT",
			"nickname" : "TEXT",
			"state" : "TEXT",
			"phone" : "TEXT",
			"university" : "TEXT",
			"occupation" : "TEXT",
			"where_work" : "TEXT",
			"country_id" : "INTEGER",
			"city_id" : "INTEGER",
			"facebook_id" : "INTEGER",
			"favorite_drink" : "TEXT"
		},
		adapter : {
			type : "sql",
			collection_name : "user",
			idAttribute : "id"
		}
	},
	extendModel : function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
			/**
			 * mapping modelService with model local
			 * @param {Object} modelService
			 * @param {Boolean} withoutSave
			 */
			mapping : function(modelService, withoutSave) {
				this.set('id', modelService.id);
				this.set('facebook_id', modelService.facebook_id);
				this.set('first_name', modelService.first_name);
				this.set('last_name', modelService.last_name);
				this.set('mail', modelService.email);
				this.set('password', modelService.password);
				this.set('gender', modelService.gender);
				this.set('date_birth', (require('FormatUtils')).getStringDBToDate(modelService.date_birth));
				this.set('nickname', modelService.nickname);
				this.set('date_birth', modelService.date_birth);
				this.set('phone', modelService.phone);
				this.set('gender', modelService.gender);
				if (_.isString(modelService.image_name)) {
					this.set('photo', Alloy.Globals.JsUtils.createURLToCDN(modelService.image_name, Alloy.Globals.Dimens.user_photo_cdn_size));
					this.set('photo_mini', Alloy.Globals.JsUtils.createURLToCDN(modelService.image_name, Alloy.Globals.Dimens.user_photo_mini_cdn_size));
				}
				this.set('state', modelService.state);

				this.set('country_id', modelService.country_id);
				this.set('city_id', modelService.city_id);

				this.set('university', modelService.university);
				this.set('where_work', modelService.where_work);
				this.set('occupation', modelService.occupation);

				this.setFavoriteDrink(modelService.favorite_drink);
				if (_.isArray(modelService.interests) && !_.isEmpty(modelService.interests)) {
					this.saveTags(modelService.interests);
				}

				if (!withoutSave) {
					this.save();
				}
			},
			/**
			 * save tags
			 * @param {Array} tags
			 */
			saveTags : function(tags) {

				var _this = this;
				_.map(tags, function(tagService) {
					if (_.isObject(tagService)) {
						var tag = Alloy.Globals.DaoBuilder.getTagDao().updateModel(tagService);
						Alloy.Globals.DaoBuilder.getUserTagDao().addTagToUser(tag, _this.getID());
					}
				});
				//gc
				_this = null;
			},
			/**
			 * check if has work
			 * @return boolean
			 */
			hasWork : function() {
				var whereWork = this.get('where_work');
				return (_.isString(whereWork) && !_.isEmpty(whereWork));
			},
			/**
			 * check if has education
			 * @return boolean
			 */
			hasEducation : function() {
				var university = this.get('university');
				return _.isString(university) && !_.isEmpty(university);
			},
			/**
			 * getter for id
			 * @return {Integer}
			 */
			getID : function() {
				return this.get('id');
			},
			/**
			 * getter for photo
			 * @return {String}
			 */
			getPhoto : function() {
				return this.get('photo');
			},
			/**
			 * get nickname property
			 * @return {String}
			 */
			getNickName : function() {
				return this.get('nickname');
			},
			/**
			 * get full name
			 * @return {String}
			 */
			getFullName : function() {
				return this.getFirstName() + ' ' + this.getLastName();
			},
			/**
			 * getter for first name
			 * @return {String}
			 */
			getFirstName : function() {
				return this.get('first_name');
			},
			/**
			 * getter for last name
			 * @return {String}
			 */
			getLastName : function() {
				return this.get('last_name');
			},
			/**
			 * getter for mail
			 * @return {String}
			 */
			getMail : function() {
				return this.get('mail');
			},
			/**
			 * getter for gender
			 * @return {Integer}
			 */
			getGender : function() {
				return this.get('gender');
			},
			/**
			 * getter for state
			 * @return {String}
			 */
			getState : function() {
				return this.get('state');
			},
			/**
			 * getter for ocupation
			 * @return {String}
			 */
			getOccupation : function() {
				return this.get('occupation');
			},
			/**
			 * getter for university
			 * @return {String}
			 */
			getUniversity : function() {
				return this.get('university');
			},
			/**
			 * getter for where work
			 * @return {String}
			 */
			getWhereWork : function() {
				return this.get('where_work');
			},
			/**
			 * getter for phone
			 * @return {String}
			 */
			getPhone : function() {
				return this.get('phone');
			},
			getPhotoOrDefault : function() {
				var photo = this.getPhoto();
				if (!_.isString(photo) || _.isEmpty(photo)) {
					photo = this.getPhotoDefault();
				}
				return photo;
			},
			getPhotoDefault : function() {
				return this.getGender() == Alloy.CFG.constants.gender.male ? Alloy.Globals.Images.avatar_men : Alloy.Globals.Images.avatar_woman;
			},
			getPhotoBackgroundOrDefault : function() {
				var photo = this.getPhoto();
				if (!_.isString(photo) || _.isEmpty(photo)) {
					photo = this.getBackgroundDefault();
				}
				return photo;
			},
			getPhotoMini : function() {
				return this.get('photo_mini');
			},
			getPhotoMiniOrDefault : function() {
				var photo = this.getPhotoMini();
				if (!_.isString(photo) || _.isEmpty(photo)) {
					photo = this.getPhotoDefault();
				}
				return photo;
			},
			getBackgroundDefault : function() {
				return this.getGender() == Alloy.CFG.constants.gender.male ? Alloy.Globals.Images.avatar_background_men : Alloy.Globals.Images.avatar_background_female;
			},
			/**
			 * setter for favorite drinks
			 * @param {String} favoriteDrinks
			 */
			setFavoriteDrink : function(favoriteDrinks) {
				this.set('favorite_drink', favoriteDrinks);
			},
			/**
			 * getter for favorite drinks
			 * @return {String}
			 */
			getFavoriteDrink : function() {
				return this.get('favorite_drink');
			},
			/**
			 * getter for date of birth
			 * @return {String}
			 */
			getDateBirth : function() {
				return this.get('date_birth');
			},
			/**
			 * setter for gender
			 * @param {Integer} gender 
			 */
			setGender : function(gender) {
				this.set('gender', gender);
			},
			/**
			 * transform model to databinding
			 * @return JSON
			 */
			transform : function() {
				var STRING_SPACE = ' ';
				var transformed = this.toJSON();
				transformed.age_label = (require('JsUtils')).calculateAgeToStringDateBirth(this.get('date_birth')) + STRING_SPACE + L('years', 'Years').toLowerCase();
				
				if(this.get('gender') == Alloy.CFG.constants.gender.male){
					transformed.gender_description = L('male', 'Male') ;	
				} else if (this.get('gender') == Alloy.CFG.constants.gender.female){
					transformed.gender_description = L('female', 'Female');
				} else {
					transformed.gender_description = null;
				}
				
				transformed.fullname = this.getFullName();
				return transformed;
			}
		});

		return Model;
	},
	extendCollection : function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here

			// For Backbone v1.1.2, uncomment the following to override the
			// fetch method to account for a breaking change in Backbone.
			/*
			 fetch: function(options) {
			 options = options ? _.clone(options) : {};
			 options.reset = true;
			 return Backbone.Collection.prototype.fetch.call(this, options);
			 }
			 */
		});

		return Collection;
	}
};
