exports.definition = {
	config : {
		columns : {
			"id" : "INTEGER PRIMARY KEY",
			"description" : "TEXT",
			"establishment_id" : "TEXT",
			"price" : "REAL",
			"date_from" : "TEXT",
			"date_to" : "TEXT",
			"quantity" : "REAL",
			"transaction_type_id" : "INTEGER",
			"description_price" : "TEXT"
		},
		adapter : {
			type : "sql",
			collection_name : "concept",
			idAttribute : "id"
		}
	},
	extendModel : function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
			/**
			 * mapping local model with model service
			 * @param {Object} modelService
			 */
			mapping : function(modelService) {

				this.set('id', modelService.id);

				this.set('description', modelService.description);
				this.set('establishment_id', modelService.establishment_id);
				this.set('price', modelService.price);
				var formatUtils = require('FormatUtils');
				this.set('date_from', formatUtils.formatStringDBDateToDateTimeFormatService(modelService.date_from));
				this.set('date_to', formatUtils.formatStringDBDateToDateTimeFormatService(modelService.date_to));
				this.set('transaction_type_id', modelService.type_transaction_id);
				this.set('quantity', modelService.quantity_available);
				this.set('description_price', this.getDescription() + ' - ' + ' ' + Alloy.Globals.CurrentCurrencySymbol + ' ' + Alloy.Globals.FormatUtils.formatPrice(this.getPrice()));
				//gc
				formatUtils = null;

				this.save();
			},
			/**
			 * getter for id
			 * @return {Integer} id
			 */
			getID : function() {
				return this.get('id');
			},
			/**
			 * getter for description
			 * @return {String}
			 */
			getDescription : function() {
				return this.get('description');
			},
			/**
			 * getter for price
			 */
			getPrice : function() {
				return Alloy.Globals.FormatUtils.formatFloatTwoDecimal(this.get('price'));
			},
			/**
			 * getter for transaction type id
			 */
			getTransactionTypeID : function() {
				return this.get('transaction_type_id');
			},
			/**
			 * getter for quantity
			 * @return {Integer}
			 */
			getQuantity : function() {
				return this.get('quantity');
			}
		});

		return Model;
	},
	extendCollection : function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here

			// For Backbone v1.1.2, uncomment the following to override the
			// fetch method to account for a breaking change in Backbone.
			/*
			 fetch: function(options) {
			 options = options ? _.clone(options) : {};
			 options.reset = true;
			 return Backbone.Collection.prototype.fetch.call(this, options);
			 }
			 */
			deleteAll : function() {

				var collection = this;

				var sql = "DELETE FROM " + collection.config.adapter.collection_name;
				db = Ti.Database.open(collection.config.adapter.db_name);
				db.execute(sql);
				db.close();

				collection.trigger('sync');

			}
		});

		return Collection;
	}
};
