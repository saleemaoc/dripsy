exports.definition = {
	config : {
		columns : {
			"id" : "INTEGER PRIMARY KEY AUTOINCREMENT",
			"user_id" : "INTEGER",
			"tag_id" : "INTEGER",
			/**
			 * performance field
			 */
			"tag_name" : "TEXT"
		},
		adapter : {
			type : "sql",
			collection_name : "user_tag",
			idAttribute : "id"
		}
	},
	extendModel : function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
			mapping : function(modelService, userID) {
				this.setUserID(userID);
				this.setTagID(modelService.id);
				var tag = Alloy.Globals.DaoBuilder.getTagDao().findByServerID(modelService.id);
				//need mapping
				tag.mapping(modelService);
				this.setTagName(tag.getName());
				//gc
				tag = null;
				this.save();
			},
			/**
			 * setter for property user_id
			 * @param {Integer} userID
			 */
			setUserID : function(userID) {
				this.set('user_id', userID);
			},
			/**
			 * setter for property tag_id
			 * @param {Integer} tagID
			 */
			setTagID : function(tagID) {
				this.set('tag_id', tagID);
			},
			/**
			 * setter for property tag_name
			 * @param {String} tagName
			 */
			setTagName : function(tagName) {
				this.set('tag_name', tagName);
			}
		});

		return Model;
	},
	extendCollection : function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here

			// For Backbone v1.1.2, uncomment the following to override the
			// fetch method to account for a breaking change in Backbone.
			/*
			 fetch: function(options) {
			 options = options ? _.clone(options) : {};
			 options.reset = true;
			 return Backbone.Collection.prototype.fetch.call(this, options);
			 }
			 */
		});

		return Collection;
	}
};
