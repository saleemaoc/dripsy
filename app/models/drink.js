exports.definition = {
	config : {
		columns : {
			"id" : "INTEGER PRIMARY KEY",
			"name" : "TEXT",
			"description" : "TEXT",
			"price" : "REAL",
			"image_mini" : "TEXT",
			"image" : "TEXT",
			"recommended" : "INTEGER",
			"establishment_id" : "INTEGER"
		},
		defaults : {
			"price" : 0,
			"recommended" : 0
		},
		adapter : {
			type : "sql",
			collection_name : "drink",
			idAttribute : "id"
		}
	},
	extendModel : function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
			/**
			 * mapping local model with model service
			 * @param {Object} modelService
			 */
			mapping : function(modelService) {
				this.set('id', modelService.id);
				this.set('name', modelService.name);
				this.set('description', modelService.description);
				if (_.isString(modelService.image_name)) {
					this.set('image', Alloy.Globals.JsUtils.createURLToCDN(modelService.image_name, Alloy.Globals.Dimens.drink_image_cdn_size));
					this.set('image_mini', Alloy.Globals.JsUtils.createURLToCDN(modelService.image_name, Alloy.Globals.Dimens.drink_image_mini_cdn_size));
				}
				this.set('price', modelService.price);
				this.set('recommended', modelService.recommended ? 1 : 0);
				this.set('establishment_id', modelService.establishment_id);
				this.save();
			},
			/**
			 * getter for id
			 * @return {Integer}
			 */
			getID : function() {
				return this.get('id');
			},
			/**
			 * getter for type
			 * @return {Integer}
			 */
			getType : function() {
				return this.get('type');
			},
			/**
			 * getter for price
			 * @return {Double}
			 */
			getPrice : function() {
				return Alloy.Globals.FormatUtils.formatFloatTwoDecimal(this.get('price'));
			},
			/**
			 * getter for name
			 * @return {String}
			 */
			getName : function() {
				return this.get('name');
			},
			/**
			 * getter for description
			 * @return {String}
			 */
			getDescription : function() {
				return this.get('description');
			},
			/**
			 * getter for currency and price
			 * @return {String}
			 */
			getCurrencyPrice : function() {
				return Alloy.Globals.CurrentCurrencySymbol + " " + Alloy.Globals.FormatUtils.formatPrice(this.getPrice());
			},
			/**
			 * getter for image
			 * @return {String}
			 */
			getImage : function() {
				var image = this.get('image');
				return _.isString(image) && Alloy.Globals.JsUtils.validateURL(image) ? image : Alloy.CFG.constants.mock_url_broken_link_image;
			},
			/**
			 * getter for image mini
			 * @return {String}
			 */
			getImageMini : function() {
				var imageMini = this.get('image_mini');
				return _.isString(imageMini) && Alloy.Globals.JsUtils.validateURL(imageMini) ? imageMini : Alloy.CFG.constants.mock_url_broken_link_image;
			},
			isRecommended : function() {
				return this.get('recommended') == 1;
			},
			/**
			 * transform data to databinding in componentes
			 */
			transform : function() {
				var transformed = this.toJSON();
				transformed.currency_price = this.getCurrencyPrice();
				transformed.is_recommended = this.isRecommended();
				return transformed;
			}
		});

		return Model;
	},
	extendCollection : function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here

			// For Backbone v1.1.2, uncomment the following to override the
			// fetch method to account for a breaking change in Backbone.
			/*
			 fetch: function(options) {
			 options = options ? _.clone(options) : {};
			 options.reset = true;
			 return Backbone.Collection.prototype.fetch.call(this, options);
			 }
			 */
			deleteAll : function() {

				var collection = this;

				var sql = "DELETE FROM " + collection.config.adapter.collection_name;
				db = Ti.Database.open(collection.config.adapter.db_name);
				db.execute(sql);
				db.close();

				collection.trigger('sync');

			}
		});

		return Collection;
	}
};exports.definition = {
	config : {
		columns : {
			"id" : "INTEGER PRIMARY KEY",
			"name" : "TEXT",
			"description" : "TEXT",
			"price" : "REAL",
			"image_mini" : "TEXT",
			"image" : "TEXT",
			"recommended" : "INTEGER",
			"establishment_id" : "INTEGER"
		},
		defaults : {
			"price" : 0,
			"recommended" : 0
		},
		adapter : {
			type : "sql",
			collection_name : "drink",
			idAttribute : "id"
		}
	},
	extendModel : function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
			/**
			 * mapping local model with model service
			 * @param {Object} modelService
			 */
			mapping : function(modelService) {
				this.set('id', modelService.id);
				this.set('name', modelService.name);
				this.set('description', modelService.description);
				if (_.isString(modelService.image_name)) {
					this.set('image', Alloy.Globals.JsUtils.createURLToCDN(modelService.image_name, Alloy.Globals.Dimens.drink_image_cdn_size));
					this.set('image_mini', Alloy.Globals.JsUtils.createURLToCDN(modelService.image_name, Alloy.Globals.Dimens.drink_image_mini_cdn_size));
				}
				this.set('price', modelService.price);
				this.set('recommended', modelService.recommended ? 1 : 0);
				this.set('establishment_id', modelService.establishment_id);
				this.save();
			},
			/**
			 * getter for id
			 * @return {Integer}
			 */
			getID : function() {
				return this.get('id');
			},
			/**
			 * getter for type
			 * @return {Integer}
			 */
			getType : function() {
				return this.get('type');
			},
			/**
			 * getter for price
			 * @return {Double}
			 */
			getPrice : function() {
				return Alloy.Globals.FormatUtils.formatFloatTwoDecimal(this.get('price'));
			},
			/**
			 * getter for name
			 * @return {String}
			 */
			getName : function() {
				return this.get('name');
			},
			/**
			 * getter for description
			 * @return {String}
			 */
			getDescription : function() {
				return this.get('description');
			},
			/**
			 * getter for currency and price
			 * @return {String}
			 */
			getCurrencyPrice : function() {
				return Alloy.Globals.CurrentCurrencySymbol + " " + Alloy.Globals.FormatUtils.formatPrice(this.getPrice());
			},
			/**
			 * getter for image
			 * @return {String}
			 */
			getImage : function() {
				var image = this.get('image');
				return _.isString(image) && Alloy.Globals.JsUtils.validateURL(image) ? image : Alloy.CFG.constants.mock_url_broken_link_image;
			},
			/**
			 * getter for image mini
			 * @return {String}
			 */
			getImageMini : function() {
				var imageMini = this.get('image_mini');
				return _.isString(imageMini) && Alloy.Globals.JsUtils.validateURL(imageMini) ? imageMini : Alloy.CFG.constants.mock_url_broken_link_image;
			},
			isRecommended : function() {
				return this.get('recommended') == 1;
			},
			/**
			 * transform data to databinding in componentes
			 */
			transform : function() {
				var transformed = this.toJSON();
				transformed.currency_price = this.getCurrencyPrice();
				transformed.is_recommended = this.isRecommended();
				return transformed;
			}
		});

		return Model;
	},
	extendCollection : function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here

			// For Backbone v1.1.2, uncomment the following to override the
			// fetch method to account for a breaking change in Backbone.
			/*
			 fetch: function(options) {
			 options = options ? _.clone(options) : {};
			 options.reset = true;
			 return Backbone.Collection.prototype.fetch.call(this, options);
			 }
			 */
			deleteAll : function() {

				var collection = this;

				var sql = "DELETE FROM " + collection.config.adapter.collection_name;
				db = Ti.Database.open(collection.config.adapter.db_name);
				db.execute(sql);
				db.close();

				collection.trigger('sync');

			}
		});

		return Collection;
	}
};