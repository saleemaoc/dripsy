exports.definition = {
	config : {
		columns : {
			"id" : "INTEGER PRIMARY KEY",
			"transaction_id" : "INTEGER",
			"concept_id" : "INTEGER",
			"drink_id" : "INTEGER",
			"title" : "TEXT",
			"quantity" : "INTEGER",
			"unit_price" : "REAL",
			"currency_code" : "TEXT",
			"currency_symbol" : "TEXT",
			"subtotal" : "REAL",
			/**
			 * performance attributes
			 */
			"concept_transaction_type" : "INTEGER"
		},
		adapter : {
			type : "sql",
			collection_name : "n2n_transaction_detail",
			idAttribute : "id"
		}
	},
	extendModel : function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
			/**
			 * Mapping local model with model service and save local model
			 * @param {Object} modelService
			 * @param {Integer} transactionID @Nullable
			 */
			mapping : function(modelService, transactionID) {
				this.set('id', modelService.id);
				this.set('transaction_id', _.isNumber(transactionID) ? transactionID : modelService.transaction_id);

				if (_.isObject(modelService.concept)) {

					this.set('concept_id', modelService.concept_id);
					this.set('title', modelService.concept.description);
					this.set('concept_transaction_type', modelService.concept.type_transaction_id);

				} else if (_.isObject(modelService.drink)) {

					this.set('drink_id', modelService.drink_id);
					this.set('title', modelService.drink.name);

				}

				this.set('quantity', modelService.quantity);
				this.set('unit_price', modelService.unit_price);
				this.set('currency_code', modelService.currency_code);
				this.set('currency_symbol', modelService.currency_symbol);
				this.set('subtotal', modelService.subtotal);

				this.save();
			},
			/**
			 * generate transaction detail to send service
			 * @return {Object}
			 */
			toService : function() {
				return {
					quantity : this.getQuantity(),
					subtotal : this.getSubtotal(),
					currency_code : this.getCurrencyCode(),
					currency_symbol : this.getCurrencySymbol(),
					subtotal : this.getSubtotal(),
					unit_price : this.getUnitPrice(),
					concept_id : this.getConceptID(),
					drink_id : this.getDrinkID()

				};
			},
			/**
			 * to paypal
			 * @return {Object}
			 */
			toPaypal : function() {

				var name = "";
				if (_.isNumber(this.getConceptID()) && this.getConceptID() > 0) {
					name = this.getConcept().getDescription();
				} else {
					name = this.getDrink().getName();
				}
				return {
					totalPrice : this.getSubtotal(),
					itemPrice : this.getUnitPrice(),
					itemCount : this.getQuantity(),
					name : name
				};
			},
			/**
			 * setter for drink id
			 * @param {Integer} drinkID
			 */
			setDrinkID : function(drinkID) {
				this.set('drink_id', drinkID);
			},
			/**
			 * getter for drink id
			 * @return {Integer}
			 */
			getDrinkID : function() {
				return this.get('drink_id');
			},
			/**
			 * setter for concept id
			 * @param {Integer} drinkID
			 */
			setConceptID : function(conceptID) {
				this.set('concept_id', conceptID);
			},
			/**
			 * getter for concept id
			 * @return {Integer}
			 */
			getConceptID : function() {
				return this.get('concept_id');
			},
			/**
			 * setter for concept
			 * @param {Concept} concept
			 */
			setConcept : function(concept) {
				if (_.isObject(concept)) {
					this.setConceptID(concept.getID());
					this.setUnitPrice(concept.getPrice());
					this.setConceptTransactionType(concept.getTransactionTypeID());
				} else {
					this.setConceptID(null);
					this.setUnitPrice(null);
					this.setConceptTransactionType(null);
				}
				//gc
				concept = null;
			},
			/**
			 * getter for concept
			 * @return {Concept}
			 */
			getConcept : function() {
				if (_.isNumber(this.getConceptID())) {
					return Alloy.Globals.DaoBuilder.getConceptDao().findByID(this.getConceptID());
				} else {
					return null;
				}
			},
			/**
			 * setter for drink
			 * @param {Object} drink
			 */
			setDrink : function(drink) {
				if (_.isObject(drink)) {
					this.setDrinkID(drink.getID());
					this.setUnitPrice(drink.getPrice());
				} else {
					this.setDrinkID(null);
					this.setUnitPrice(null);
				}
				//gc
				drink = null;
			},
			/**
			 * getter for drink
			 * @return {Concept}
			 */
			getDrink : function() {
				if (_.isNumber(this.getDrinkID())) {
					return Alloy.Globals.DaoBuilder.getDrinkDao().findByID(this.getDrinkID());
				} else {
					return null;
				}
			},
			/**
			 * setter for quantity
			 * @param {Integer} quantity
			 */
			setQuantity : function(quantity) {

				this.set('quantity', quantity);
				this.calculateSubtotal();
			},
			/**
			 * getter for quantity
			 * @return {Integer}
			 */
			getQuantity : function() {
				return this.get('quantity');
			},
			/**
			 * get currency symbol
			 */
			getCurrencySymbol : function() {
				return this.get('currency_symbol');
			},
			getCurrencyCode : function() {
				return this.get('currency_code');
			},
			/**
			 * calculate subtotal
			 */
			calculateSubtotal : function() {
				var unitPrice = this.getUnitPrice();
				var subtotal = 0;
				if (_.isNumber(this.getConceptID())) {
					var price = 0;
					if (_.isNumber(unitPrice) && unitPrice > 0) {
						price = unitPrice;
					} else {
						var concept = this.getConcept();
						if (_.isObject(concept)) {
							price = concept.getPrice();
							//gc
							concept = null;
						}
					}
					subtotal = price * this.getQuantity();
				} else if (_.isNumber(this.getDrinkID())) {
					var price = _.isNumber(unitPrice) && unitPrice > 0 ? unitPrice : this.getDrink().getPrice();
					subtotal = Alloy.Globals.FormatUtils.formatFloatTwoDecimal(price * this.getQuantity());
				}
				this.setSubtotal(subtotal);
			},
			/**
			 * setter for title
			 * @param {String} title
			 */
			setTitle : function(title) {
				this.set('title', title);
			},
			/**
			 * getter for title
			 * @return {String}
			 */
			getTitle : function() {
				return this.get('title');
			},
			/**
			 * setter for subtotal
			 * @param {Double} subtotal
			 */
			setSubtotal : function(subtotal) {
				this.set('subtotal', subtotal);
			},
			/**
			 * get subtotal
			 */
			getSubtotal : function() {
				return this.get('subtotal');
			},
			/**
			 * setter for unit price
			 * @parma {Double} unitPrice
			 */
			setUnitPrice : function(unitPrice) {
				this.set('unit_price', unitPrice);
			},
			/**
			 * get unit price
			 * @return {Double}
			 */
			getUnitPrice : function() {
				return this.get('unit_price');
			},
			/**
			 * setter for currency code
			 * @param {String} currencyCode
			 */
			setCurrencyCode : function(currencyCode) {
				this.set('currency_code', currencyCode);
			},
			/**
			 * getter for currency code
			 * @return {String}
			 */
			getCurrencyCode : function() {
				return this.get('currency_code');
			},
			/**
			 * setter for currency symbol
			 * @param {String} currencySymbol
			 */
			setCurrencySymbol : function(currencySymbol) {
				this.set('currency_symbol', currencySymbol);
			},
			/**
			 * getter for currency symbol
			 * @return {String}
			 */
			getCurrencySymbol : function() {
				return this.get('currency_symbol');
			},
			/**
			 * setter for concept transaction type
			 * @param {Integer} conceptTransactionType
			 */
			setConceptTransactionType : function(conceptTransactionType) {
				this.set('concept_transaction_type', conceptTransactionType);
			},
			/**
			 * getter for concept transaction type
			 * @return {Integer}
			 */
			getConceptTransactionType : function() {
				return this.get('concept_transaction_type');
			},
			/**
			 * get icon
			 * @return {String}
			 */
			getIcon : function() {
				return _.isNumber(this.getConceptID()) ? this.getConceptTransactionType() == Alloy.CFG.constants.transaction_type.ticket ? Alloy.Globals.Images.ic_ticket_24dp : Alloy.Globals.Images.ic_table_24dp : Alloy.Globals.Images.ic_drink_24dp;
			},
			/**
			 * validate transaction detail
			 */
			validateRequired : function() {
				return this.validateConcept() && _.isNumber(this.getQuantity()) && this.getQuantity() > 0;
			},
			validateConcept : function() {
				return _.isNumber(this.getConceptID()) || _.isNumber(this.getDrinkID());
			},
			/**
			 * validate quantity
			 * @return {Boolean}
			 */
			validateQuantity : function() {
				var concept = this.getConcept();
				if (_.isObject(concept)) {
					return concept.getQuantity() >= this.getQuantity();
				} else {
					return true;
				}

			},
			/**
			 * generate default values to transaction detail
			 */
			generate : function() {
				this.setCurrencySymbol(Alloy.Globals.CurrentCurrencySymbol);
				this.setCurrencyCode(Alloy.Globals.CurrentCurrencyCode);
			},
			/**
			 * generate transaction detail to drink
			 * @param {Integer} quantity
			 * @param {Object} drink
			 */
			generateToDrink : function(quantity, drink) {
				this.generate();
				this.setDrink(drink);
				this.setQuantity(quantity);
			},
			/**
			 * generate transaction detail to concept
			 * @param {Integer} quantity
			 * @param {Object} drink
			 */
			generateToConcept : function(quantity, concept) {
				this.generate();
				this.setQuantity(quantity);
				this.setConcept(concept);
			},
			/**
			 * transform data to databinding in componentes
			 */
			transform : function() {
				var transformed = this.toJSON();
				transformed.quantity_title = this.getQuantity() + ' ' + this.getTitle();
				transformed.currency_amount = Alloy.Globals.CurrentCurrencySymbol + " " + Alloy.Globals.FormatUtils.formatPrice(this.getSubtotal());
				transformed.icon = this.getIcon();
				return transformed;
			}
		});

		return Model;
	},
	extendCollection : function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here

			// For Backbone v1.1.2, uncomment the following to override the
			// fetch method to account for a breaking change in Backbone.
			/*
			 fetch: function(options) {
			 options = options ? _.clone(options) : {};
			 options.reset = true;
			 return Backbone.Collection.prototype.fetch.call(this, options);
			 }
			 */
		});

		return Collection;
	}
};
