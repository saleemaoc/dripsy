exports.definition = {
	config : {
		columns : {
			"id" : "INTEGER PRIMARY KEY",
			"from_user_id" : "INTEGER",
			"to_user_id" : "INTEGER",
			"transaction_id" : "INTEGER",
			"status" : "INTEGER",
			"date_acceptance" : "TEXT",
			"show" : "INTEGER",
			"status_name" : "TEXT",
			/**
			 * performance fields
			 */
			"from_user_photo" : "TEXT",
			"from_user_full_name" : "TEXT",
			"from_user_gender" : "INTEGER",
			"transaction_establishment_name" : "TEXT",
			"transaction_transaction_date" : "TEXT",
			"transaction_transaction_date_ab" : "TEXT",
			"transaction_status" : "INTEGER",
			"transaction_code" : "TEXT"
		},
		defaults : {
			"modified" : ""
		},
		adapter : {
			type : "sql",
			collection_name : "gift",
			idAttribute : "id"
		}
	},
	extendModel : function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
			//@override
			save : function(attrs, options) {
				this.calculateUIAttributes();
				var date = (require('FormatUtils')).getStringDBToDate();
				this.set('modified', date);
				// Proxy the call to the original save function
				Backbone.Model.prototype.save.call(this, attrs, options);
			},
			/**
			 * mapping model service to local model
			 * @param {Object} modelService
			 */
			mapping : function(modelService) {
				//check if need update
				var needUpdated = true;

				if (!_.isEmpty(this.get('modified'))) {
					var formatUtils = require('FormatUtils');
					var currentModified = formatUtils.getDateByStringDB(this.get('modified'));
					var serverModified = formatUtils.getDateToString(modelService.modified);
					formatUtils = null;
					needUpdated = (serverModified.valueOf() >= currentModified.valueOf());
				}

				if (needUpdated) {

					var userDao = Alloy.Globals.DaoBuilder.getUserDao();

					this.set('id', modelService.id);

					this.set('status', modelService.status);
					this.set('show', modelService.show ? 1 : 0);

					if (_.isString(modelService.acceptance_date)) {
						this.set('date_acceptance', (require('FormatUtils')).getStringDBToDate(modelService.acceptance_date));
					}

					/**
					 * mapping transaction
					 */
					if (_.isObject(modelService.transaction)) {
						this.set('transaction_id', modelService.transaction_id);
						var transaction = Alloy.Globals.DaoBuilder.getTransactionDao().findByID(modelService.transaction.id);
						transaction.mapping(modelService.transaction, true);
						this.set('transaction_establishment_name', transaction.getEstablishmentName());
						this.set('transaction_transaction_date', transaction.getTransactionDate());
						this.set('transaction_status', transaction.getStatus());
						this.set('transaction_code', transaction.getTransactionCode());
						//gc
						transaction = null;

						/**
						 * mapping users
						 */

						if (_.isObject(modelService.transaction.buyer_user)) {

							this.set('from_user_id', modelService.transaction.buyer_user_id);

							var fromUser = userDao.findByID(modelService.transaction.buyer_user.id);

							fromUser.mapping(modelService.transaction.buyer_user);
							this.set('from_user_full_name', fromUser.getFullName());
							this.set('from_user_gender', fromUser.getGender());
							this.set('from_user_photo', fromUser.getPhotoMini());
							this.set('from_user_gender', fromUser.getGender());
							fromUser = null;
						}

					}

					/**
					 * mapping users
					 */

					if (_.isObject(modelService.to_user)) {
						this.set('to_user_id', modelService.to_user_id);
						userDao.findByID(modelService.to_user.id).mapping(modelService.to_user);
					}

					//gc
					userDao = null;

					this.save();
				}

			},
			/**
			 * return model to service
			 * @param {Object} transactionToService
			 * @param {Boolean} withoutTransaction
			 */
			toService : function(transactionToService, withoutTransaction) {

				var toService = {
					id : this.getID(),
					to_user_id : this.getToUserID(),
					status : this.getStatus()
				};

				if (_.isNumber(this.getTransactionID())) {
					toService.transaction_id = this.getTransactionID();
				}

				if (!withoutTransaction) {
					toService.transaction = _.isObject(transactionToService) ? transactionToService : Alloy.Globals.DaoBuilder.getTransactionDao().findByID(this.getTransactionID()).toService();
				}

				if (_.isString(this.getDateAcceptance())) {
					toService.acceptance_date = (require('FormatUtils')).formatStringDBDateToDateTimeFormatService(this.getDateAcceptance());
				}

				return toService;
			},
			toHideService : function() {
				return {
					show : false,
				};
			},
			/**
			 * getter for status accepted
			 * @return {Boolean}
			 */
			isAccept : function() {
				return this.get('status') == Alloy.CFG.constants.gift_status.accepted;
			},
			/**
			 * getter for status rejected
			 * @return {Boolean}
			 */
			isRejected : function() {
				return this.get('status') == Alloy.CFG.constants.gift_status.rejected;
			},
			isDefeated : function() {
				return this.get('status') == Alloy.CFG.constants.gift_status.defeated;
			},
			/**
			 * getter for id
			 * @return {Integer}
			 */
			getID : function() {
				return this.get('id');
			},
			/**
			 * getter for transaction date
			 * @return {String}
			 */
			getTransactionDate : function() {
				return this.get('transaction_transaction_date');
			},
			/**
			 * calculate UI attributes
			 */
			calculateUIAttributes : function() {
				this.set('status_name', this.getStatusName());
				this.set('transaction_date_ab', this.getCalculateTransactionDateAB());
			},
			/**
			 * get calculate transaction date with AB format
			 * @return {String}
			 */
			getCalculateTransactionDateAB : function() {
				return (require('FormatUtils')).getAbStringToDate(this.getTransactionDate());
			},
			/**
			 * return {String}
			 */
			getStatusName : function() {
				var statusName = "";
				if (this.get('transaction_status') == Alloy.CFG.constants.transaction_status.accept) {
					statusName = L('used', 'Redeemed');
				} else if (this.get('transaction_status') == Alloy.CFG.constants.transaction_status.rejected) {
					statusName = L('rejected', 'Rejected');
				} else if (this.get('transaction_status') == Alloy.CFG.constants.transaction_status.cancel) {
					statusName = L('canceled', 'Cancelled');
				} else {
					switch(this.get('status')) {
					case Alloy.CFG.constants.gift_status.accepted:
						statusName = L('accepted', 'Accepted');
						break;
					case Alloy.CFG.constants.gift_status.rejected:
						statusName = L('canceled', 'Not accepted');
						break;
					case Alloy.CFG.constants.gift_status.pending:
						statusName = L('pending', 'Pending');
						break;
					case Alloy.CFG.constants.gift_status.defeated:
						statusName = L('defeated', 'Expired');
						break;
					}
				}
				return statusName.toUpperCase();

			},
			/**
			 * setter for to user id
			 * @param {Integer} toUserID
			 */
			setToUserID : function(toUserID) {
				this.set('to_user_id', toUserID);
			},
			/**
			 * getter for to user id
			 * @return {Integer}
			 */
			getToUserID : function() {
				return this.get('to_user_id');
			},
			/**
			 * setter for from user id
			 * @param {Integer} fromUserID
			 */
			setFromUserID : function(fromUserID) {
				this.set('from_user_id', fromUserID);
			},
			/**
			 * getter for from user id
			 * @return {Integer}
			 */
			getFromUserID : function() {
				return this.get('from_user_id');
			},
			/**
			 * setter for transaction id
			 * @param {Integer} transactionID
			 */
			setTransactionID : function(transactionID) {
				this.set('transaction_id', transactionID);
			},
			/**
			 * getter for transaction id
			 * @return {Integer}
			 */
			getTransactionID : function() {
				return this.get('transaction_id');
			},
			/**
			 * setter for date acceptance
			 * @param {String} dateAcceptance
			 */
			setDateAcceptance : function(dateAcceptance) {
				this.set('date_acceptance', dateAcceptance);
			},
			/**
			 * getter for date acceptance
			 * @return {Integer}
			 */
			getDateAcceptance : function() {
				return this.get('date_acceptance');
			},
			/**
			 * setter for status
			 * @param {Integer} status
			 */
			setStatus : function(status) {
				this.set('status', status);
			},
			/**
			 * getter for status
			 * @return {Integer}
			 */
			getStatus : function() {
				return this.get('status');
			},
			/**
			 * generate new gift
			 * @param {Integer} fromUserID
			 * @param {Integer} toUserID
			 */
			generateNewGift : function(fromUserID, toUserID) {
				this.setFromUserID(fromUserID);
				this.setToUserID(toUserID);
			},
			/**
			 * get pretty transaction date
			 * @return {String}
			 */
			getPrettyTransactionDate : function() {
				return (require('pretty')).prettyDate((require('FormatUtils')).getDateToString(this.get('transaction_transaction_date')));
			},
			/**
			 * getter from user photo
			 * @return {String}
			 */
			getFromUserPhoto : function() {
				var fromUserPhoto = this.get('from_user_photo');
				return _.isString(fromUserPhoto) && Alloy.Globals.JsUtils.validateURL(fromUserPhoto) ? fromUserPhoto : Alloy.CFG.constants.mock_url_broken_link_image;
			},
			getFromUserDefaultPhoto : function() {
				return ((this.getFromUserGender() == Alloy.CFG.constants.gender.male) ? Alloy.Globals.Images.avatar_men : Alloy.Globals.Images.avatar_woman);
			},
			/**
			 * getter for transaction
			 * @return {Transaction}
			 */
			getTransaction : function() {
				return Alloy.Globals.DaoBuilder.getTransactionDao().findByID(this.getTransactionID());
			},
			/**
			 * getter for user gender
			 * @return {Integer}
			 */
			getFromUserGender : function() {
				return this.get('from_user_gender');
			},
			/**
			 * getter for transaction establishment name
			 * @return {String}
			 */
			getTransactionEstablishmentName : function() {
				return this.get('transaction_establishment_name');
			},
			/**
			 * getter for description
			 * @return {String}
			 */
			getDescription : function() {
				if (this.getFromUserGender() == Alloy.CFG.constants.gender.male) {
					return String.format(L('he_gave_you_a_gift_at_establishment', 'He sent you a gift for %s'), this.getTransactionEstablishmentName());
				} else {
					return String.format(L('she_gave_you_a_gift_at_establishment', 'She sent you a gift for %s'), this.getTransactionEstablishmentName());
				}

			},
			/**
			 * transform data to databinding in componentes
			 */
			transform : function() {
				var transformed = this.toJSON();
				transformed.transaction_pretty_transaction_date = this.getPrettyTransactionDate();
				//Indicacion para el widget co.clarika.label que tiene que usar los id de gift y no los de transaction
				transformed.from_user_photo = this.getFromUserPhoto();
				transformed.from_user_default_photo = this.getFromUserDefaultPhoto();
				var transactionStatus = this.get('transaction_status');

				if (transactionStatus == Alloy.CFG.constants.transaction_status.rejected || transactionStatus == Alloy.CFG.constants.transaction_status.cancel) {
					transformed.status = transactionStatus;
				}
				transformed.isgift = true;
				transformed.description = this.getDescription();
				return transformed;
			}
		});

		return Model;
	},
	extendCollection : function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here

			// For Backbone v1.1.2, uncomment the following to override the
			// fetch method to account for a breaking change in Backbone.
			/*
			 fetch: function(options) {
			 options = options ? _.clone(options) : {};
			 options.reset = true;
			 return Backbone.Collection.prototype.fetch.call(this, options);
			 }
			 */
			deleteAll : function() {

				var collection = this;

				var sql = "DELETE FROM " + collection.config.adapter.collection_name;
				db = Ti.Database.open(collection.config.adapter.db_name);
				db.execute(sql);
				db.close();

				collection.trigger('sync');

			}
		});

		return Collection;
	}
};
