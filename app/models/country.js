exports.definition = {
	config : {
		columns : {
			"id" : "INTEGER PRIMARY KEY",
			"name" : "TEXT",
			"enabled" : "INTEGER",
			"currency_symbol" : "TEXT",
			"currency_code" : "TEXT",
			"distance_symbol" : "TEXT",
			"distance_code" : "TEXT",
			"payment_methods" : "TEXT"
		},
		defaults : {
			"distance_code" : "M",
			"distance_symbol" : "mi"
		},
		adapter : {
			type : "sql",
			collection_name : "country",
			idAttribute : "id"
		}
	},
	extendModel : function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
			mapping : function(modelService) {

				this.set('id', modelService.id);
				this.set('name', modelService.name);
				this.set('enabled', modelService.enabled ? 1 : 0);
				this.set('currency_symbol', modelService.currency_symbol);
				this.set('currency_code', modelService.currency_code);
				this.set('distance_symbol', modelService.distance_symbol);
				this.set('distance_code', modelService.distance_code);
				this.set('payment_methods', JSON.stringify(modelService.payment_methods));
				this.save();
			},
			/**
			 * get distance symbol
			 * @return {String}
			 */
			getDistanceSymbol : function() {
				return this.get('distance_symbol');
			},
			/**
			 * get distance code
			 * @return {String}
			 */
			getDistanceCode : function() {
				return this.get('distance_code');
			},
			/**
			 * get currency code
			 * @return {String}
			 */
			getCurrencyCode : function() {
				return this.get('currency_code');
			},
			/**
			 * get currency symbol
			 * @return {String}
			 */
			getCurrencySymbol : function() {
				return this.get('currency_symbol');
			},
			getPaymentMethods : function() {
				var paymentMethods = this.get('payment_methods');
				if (_.isString(paymentMethods) && !_.isEmpty(paymentMethods)) {
					return JSON.parse(paymentMethods);
				} else {
					return null;
				}

			}
		});

		return Model;
	},
	extendCollection : function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here

			// For Backbone v1.1.2, uncomment the following to override the
			// fetch method to account for a breaking change in Backbone.
			/*
			 fetch: function(options) {
			 options = options ? _.clone(options) : {};
			 options.reset = true;
			 return Backbone.Collection.prototype.fetch.call(this, options);
			 }
			 */
		});

		return Collection;
	}
};
