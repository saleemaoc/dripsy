exports.definition = {
	config : {
		columns : {
			"id" : "INTEGER PRIMARY KEY AUTOINCREMENT",
			"conversation_id" : "INTEGER",
			"text" : "TEXT",
			"date" : "TEXT",
			"state" : "INTEGER",
			"from_user_id" : "INTEGER",
			"to_user_id" : "INTEGER",
			"server_id" : "INTEGER",
			/**
			 * Performance field
			 */
			"from_user_photo" : "TEXT"

		},
		default : {
			server_id : -1
		},
		adapter : {
			type : "sql",
			collection_name : "message",
			idAttribute : "id"
		}
	},
	extendModel : function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
			/**
			 * mapping local model with model service
			 * @param {Object} modelService
			 */
			mapping : function(modelService) {
				this.setServerID(modelService.id);
				this.setConversationID(modelService.conversation_id);
				this.setText(modelService.text);
				this.setDate((require('FormatUtils')).getStringDBToDate(modelService.date));
				this.setFromUserID(modelService.from_user_id);
				if (_.isObject(modelService.from_user)) {

					var fromUser = Alloy.Globals.DaoBuilder.getUserDao().getModel();
					fromUser.mapping(modelService.from_user);
					this.setFromUserPhoto(fromUser.getPhotoMiniOrDefault());
				}
				this.setState(Alloy.CFG.constants.message_state.received);
				this.setToUserID(modelService.to_user_id);
				this.save();
			},
			/**
			 * send model to service
			 * @return {Object}
			 */
			toService : function() {
				var modelService = {
					text : this.getText(),
					conversation_id : this.getConversationID(),
					from_user_id : this.getFromUserID(),
					to_user_id : this.getToUserID(),
					from_user_message_temp_id : this.getID(),
					date : (require('FormatUtils')).formatStringDBDateToDateTimeFormatService(this.getDate()),
					state : Alloy.CFG.constants.message_state.sent
				};

				return modelService;
			},
			/**
			 * getter for id
			 * @return {Integer}
			 */
			getID : function() {
				return this.get('id');
			},
			/**
			 * setter for server id
			 * @param {Integer} serverID
			 */
			setServerID : function(serverID) {
				this.set('server_id', serverID);
			},
			/**
			 * getter for server id
			 */
			getServerID : function() {
				return this.get('server_id');
			},
			/**
			 * setter for conversation id
			 * @param {Integer} conversationID
			 */
			setConversationID : function(conversationID) {
				this.set('conversation_id', conversationID);
			},
			/**
			 * getter for conversation id
			 * @return {Integer}
			 */
			getConversationID : function() {
				return this.get('conversation_id');
			},
			/**
			 * setter for text
			 * @param {String} text
			 */
			setText : function(text) {
				this.set('text', text);
			},
			/**
			 * getter for text
			 * @return {String}
			 */
			getText : function() {
				return this.get('text');
			},
			/**
			 * setter for date
			 * @param {String} date
			 */
			setDate : function(date) {
				this.set('date', date);
			},
			/**
			 * getter for date
			 * @return {String}
			 */
			getDate : function() {
				return this.get('date');
			},
			/**
			 * setter for from user id
			 * @param {Integer} fromUserID
			 */
			setFromUserID : function(fromUserID) {
				this.set('from_user_id', fromUserID);
			},
			/**
			 * getter for from user id
			 * @return {Integer}
			 */
			getFromUserID : function() {
				return this.get('from_user_id');
			},
			/**
			 * setter for to user id
			 * @param {Integer} toUserID
			 */
			setToUserID : function(toUserID) {
				this.set('to_user_id', toUserID);
			},
			/**
			 * getter for to user id
			 * @return {Integer}
			 */
			getToUserID : function() {
				return this.get('to_user_id');
			},
			/**
			 * setter from user photo
			 * @param {String} fromUserPhoto
			 */
			setFromUserPhoto : function(fromUserPhoto) {
				this.set('from_user_photo', fromUserPhoto);
			},
			/**
			 * getter from user photo
			 * @return {String}
			 */
			getFromUserPhoto : function() {
				return this.get('from_user_photo');
			},
			/**
			 * setter for state
			 * @param {Integer} state
			 */
			setState : function(state) {
				this.set('state', state);
			},
			/**
			 * getter for state
			 * @return {Integer}
			 */
			getState : function() {
				return this.get('state');
			},
			/**
			 * getter date in pretty format
			 * @return {String}
			 */
			getPrettyDateHour : function() {
				if (_.isNumber(Alloy.Globals.LastIdMessageDrawInCurrentConversation) && this.getID() <= Alloy.Globals.LastIdMessageDrawInCurrentConversation) {
					Alloy.Globals.LastDateMessageDrawInCurrentConversation = null;
				}
				Alloy.Globals.LastIdMessageDrawInCurrentConversation = this.getID();

				var date = (require('FormatUtils')).getDateToString(this.getDate());
				var dateToCompare = new Date(date.getTime());
				var lastDate = Alloy.Globals.LastDateMessageDrawInCurrentConversation;

				if (_.isDate(lastDate) && Alloy.Globals.JsUtils.compareTwoDatesWithoutTime(dateToCompare, lastDate)) {
					return (require('pretty')).prettyDate(date, true);
				} else {
					Alloy.Globals.LastDateMessageDrawInCurrentConversation = date;
					return (require('FormatUtils')).getStringToDateTimeWithoutSeconds(date);
				}
			},
			/**
			 * transform data to databinding in componentes
			 */
			transform : function() {
				var transformed = this.toJSON();
				//set template for listview

				var isMe = this.getFromUserID() == Alloy.Globals.UserID;
				transformed.template = isMe ? 'message_me' : 'message_other';

				if (isMe && (_.isNull(this.getFromUserPhoto()) || _.isEmpty(this.getFromUserPhoto()))) {
					transformed.from_user_photo = Alloy.Globals.UserLogged.getGender() == Alloy.CFG.constants.gender.male ? Alloy.Globals.Images.avatar_men : Alloy.Globals.Images.avatar_woman;
				}

				transformed.pretty_date_hour = this.getPrettyDateHour();
				return transformed;
			}
		});

		return Model;
	},
	extendCollection : function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here

			// For Backbone v1.1.2, uncomment the following to override the
			// fetch method to account for a breaking change in Backbone.
			/*
			 fetch: function(options) {
			 options = options ? _.clone(options) : {};
			 options.reset = true;
			 return Backbone.Collection.prototype.fetch.call(this, options);
			 }
			 */
			comparator : function(m) {
				return (require('FormatUtils')).getDateToString(m.getDate()).getTime();
			}
		});

		return Collection;
	}
};
