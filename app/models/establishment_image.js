exports.definition = {
	config : {
		columns : {
			"id" : "INTEGER PRIMARY KEY",
			"photo" : "TEXT",
			"establishment_id" : "INTEGER",
			"is_primary" : "INTEGER"
		},
		adapter : {
			type : "sql",
			collection_name : "establishment_image",
			idAttribute : "id"
		}
	},
	extendModel : function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
			/**
			 * Mapping local model with model service and save local model
			 * @param {Object} modelService
			 */
			mapping : function(modelService) {
				this.set('id', modelService.id);
				this.set('photo', modelService.file);
				this.set('establishment_id', modelService.establishment_id);
				this.set('is_primary', modelService.primary ? 1 : 0);
				this.save();
			},
			/**
			 * transform data to databinding in componentes
			 */
			transform : function() {
				var transformed = this.toJSON();
				if (OS_IOS) {
					transformed.photo = encodeURI(transformed.photo);
				}
				return transformed;
			}
		});

		return Model;
	},
	extendCollection : function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here
			deleteAll : function() {

				var collection = this;

				var sql = "DELETE FROM " + collection.config.adapter.collection_name;
				db = Ti.Database.open(collection.config.adapter.db_name);
				db.execute(sql);
				db.close();

				collection.trigger('sync');

			}
		});

		return Collection;
	}
};
