exports.definition = {
	config : {
		columns : {
			"id" : "INTEGER PRIMARY KEY",
			"establishment_id" : "INTEGER",
			"transaction_type_id" : "INTEGER",
			"transaction_date" : "TEXT",
			"transaction_code" : "TEXT",
			"transaction_event_date" : "TEXT",
			"payment_method" : "INTEGER",
			"payment_operation_code" : "TEXT",
			"status" : "INTEGER",
			"total_amount" : "REAL",
			"tax_amount" : "REAL",
			"admin_fee" : "REAL",
			"currency_code" : "TEXT",
			"currency_symbol" : "TEXT",
			"description" : "TEXT",
			"show" : "INTEGER",
			"is_gift" : "INTEGER",
			"modified" : "TEXT",
			"tip_amount" : "REAL",
			"future_payment":"INTEGER",
			"credit_card_token":"TEXT",
			/**
			 * user final
			 */
			"consumer_user_id" : "INTEGER",
			/**
			 * user buy tickets
			 */
			"buyer_user_id" : "INTEGER",
			/**
			 * checker user in bar
			 */
			"checker_user_id" : "INTEGER",

			/**
			 * UI attributes calculables
			 */
			"title" : "TEXT",
			"image" : "TEXT",
			"transaction_date_ab" : "TEXT",
			"pretty_transaction_date" : "TEXT",
			"status_name" : "TEXT",
			"establishment_name" : "TEXT",
			"establishment_image" : "TEXT",
			"buyer_user_full_name" : "TEXT",
			/**
			 * only gift
			 */
			"to_user_full_name" : "TEXT",
			"to_user_gender" : "INTEGER",			
		},
		defaults : {
			"is_gift" : 0,
			"modified" : "",
			"tip_amount" : 0
		},
		adapter : {
			type : "sql",
			collection_name : "n2n_transaction",
			idAttribute : "id"
		}
	},
	extendModel : function(Model) {
		_.extend(Model.prototype, {
			//@override
			save : function(attrs, options) {
				this.calculateUIAttributes();
				var date = (require('FormatUtils')).getStringDBToDate();
				this.set('modified', date);
				// Proxy the call to the original save function
				Backbone.Model.prototype.save.call(this, attrs, options);
			},
			// extended functions and properties go here
			/**
			 * Mapping local model with model service and save local model
			 * @param {Object} modelService
			 */
			mapping : function(modelService) {

				//check if need update
				var needUpdated = true;

				if (!_.isEmpty(this.get('modified'))) {

					var formatUtils = require('FormatUtils');
					var currentModified = formatUtils.getDateByStringDB(this.get('modified'));
					var serverModified = formatUtils.getDateToString(modelService.modified);

					formatUtils = null;
					needUpdated = (serverModified.valueOf() >= currentModified.valueOf());
				}
				if (needUpdated) {

					var transactionID = modelService.id;

					this.set('id', transactionID);
					this.set('transaction_type_id', modelService.type_transaction_id);
					this.set('transaction_date', (require('FormatUtils')).getStringDBToDate(modelService.transaction_date));
					this.set('transaction_code', modelService.transaction_code);
					this.set('show', modelService.show ? 1 : 0);
					this.set('payment_method', modelService.payment_method);
					this.set('payment_operation_code', modelService.payment_operation_code);
					this.set('status', modelService.status);
					this.set('total_amount',modelService.total_amount);
					this.set('description', modelService.description);
					this.set('tax_amount', modelService.tax_amount);
					this.set('admin_fee', Alloy.CFG.constants.admin_fee);

					this.set('currency_symbol', modelService.currency_symbol);
					this.set('currency_code', modelService.currency_code);

					this.setTipAmount(modelService.tip_amount);
					this.setAdminFee(Alloy.CFG.constants.admin_fee);
					if (modelService.establishment_id) {
						this.set('establishment_id', modelService.establishment_id);
						var establishment = Alloy.Globals.DaoBuilder.getEstablishmentDao().updateModel(modelService.establishment);
						this.set('establishment_name', establishment.getName());
						this.set('establishment_image', establishment.getImageMini());
						//gc
						establishment = null;
					}
					var userDao = Alloy.Globals.DaoBuilder.getUserDao();

					if (_.isObject(modelService.gift_drink)) {
						this.set('is_gift', 1);
						var toUserGift = userDao.updateModel(modelService.gift_drink.to_user);
						this.set('to_user_full_name', toUserGift.getFullName());
						this.set('to_user_gender', toUserGift.getGender());
						//gc
						toUserGift = null;

					}

					if (_.isNumber(modelService.consumer_user_id)) {
						this.set('consumer_user_id', modelService.consumer_user_id);
						if (_.isObject(modelService.consumer_user)) {
							userDao.updateModel(modelService.consumer_user);
						}
					}

					if (_.isNumber(modelService.buyer_user_id)) {
						this.set('buyer_user_id', modelService.buyer_user_id);
						if (_.isObject(modelService.buyer_user)) {
							var buyerUser = userDao.updateModel(modelService.buyer_user);
							this.set('buyer_user_full_name', buyerUser.getFullName());
							//gc
							buyerUser = null;
						}
					}

					if (_.isNumber(modelService.checker_user_id)) {
						this.set('checker_user_id', modelService.checker_user_id);
						if (_.isObject(modelService.checker_user)) {
							userDao.updateModel(modelService.checker_user);
						}
					}

					//gc
					userDao = null;

					if (_.isArray(modelService.transactions_detail)) {
						Alloy.Globals.DaoBuilder.getTransactionDetailDao().updateCollection(modelService.transactions_detail);
					}

					this.save();
				}

			},
			/**
			 * return model to service
			 * @param {Array} transactionDetailsService @Nullable
			 * @return {Object}
			 */
			toService : function(transactionDetailsService, withoutPayment) {
				
				var transactionType=this.getTransactionTypeID();
				
				var toService = {
					establishment_id : this.getEstablishmentID(),
					total_amount : this.getTotalAmount(),
					tax_amount: this.getTaxAmount(),
					buyer_user_id : this.getBuyerUserID(),
					currency_symbol : this.getCurrencySymbol(),

					currency_code : this.getCurrencyCode(),
					transactions_detail : _.isArray(transactionDetailsService) ? transactionDetailsService : this.getTransactionDetailsToService(),
					show : this.getShow(),
					transaction_date : (require('FormatUtils')).formatStringDBDateToDateTimeFormatService(this.getTransactionDate()),
					type_transaction_id : transactionType,
					tip_amount : this.getTipAmount(),
					admin_fee : this.getAdminFee(),
					
				};
				
				if(transactionType == Alloy.CFG.constants.transaction_type.table || transactionType == Alloy.CFG.constants.transaction_type.ticket){
					toService.event_date = (require('FormatUtils')).formatDateToDateFormatService(this.getTransactionEventDate());
				}

				if (_.isNumber(this.getID())) {
					toService.id = this.getID();
				}

				if (!withoutPayment) {
					toService.payment_operation_code = this.getPaymentOperationCode();
					toService.credit_card_token= this.getCreditCardToken();
					toService.payment_method = this.getPaymentMethod();
					toService.future_payment=(this.getFuturePayment()==1)
				}
				return toService;
			},
			/**
			 * generate payment to send a paypal
			 */
			toPaypal : function(paypalItems) {

				var description = this.getItemsDescriptionToPaypal(paypalItems);
				var customID = this.getBuyerUserID() + '_' + this.getEstablishmentID();

				var toPaypal= {
					tax : parseFloat(0),
					shipping : parseFloat(0),
					currency : Alloy.Globals.CurrentCurrencyCode,
					recipient : Alloy.CFG.Paypal.recipient,
					customID : customID,
					invoiceItems : paypalItems,
					ipnUrl : Alloy.CFG.web.url,
					merchantName : 'Dripsy',
					description : description
				};
				return toPaypal;
			},
			getItemsDescriptionToPaypal : function(items) {
				var description = "";
				if (_.isArray(items)) {
					var first = true;
					_.map(items, function(item) {

						if (!first) {
							description = description.concat(", ");
						}

						first = false;
						description = description.concat(item.name);
					});

				}
				return description;
			},
			/**
			 * get transaction detail to service format
			 * @return {Array}
			 */
			getTransactionDetailsToService : function() {
				//TODO develop
			},
			getParamsToUpdatePayment : function() {
				return {
					establishment_id : this.getEstablishmentID(),
					payment_method : this.getPaymentMethod(),
					payment_operation_code : this.getPaymentOperationCode(),
					credit_card_token : this.getCreditCardToken(),
					future_payment:(this.getFuturePayment()==1)
				};
			},
			/**
			 * calculate UI attributes
			 */
			calculateUIAttributes : function() {
				this.set('title', this.getCalculateTitle());
				this.set('image', this.getCalculateImage());
				this.set('transaction_date_ab', this.getCalculateTransactionDateAB());
				this.set('status_name', this.getStatusName());
				this.set('pretty_transaction_date', this.getPrettyTransactionDate());
			},
			/**
			 * setter for id
			 * @param {Integer} id
			 */
			setID : function(id) {
				this.set('id', id);

			},
			/**
			 * getter for id
			 * @rerturn {Integer}
			 */
			getID : function() {
				return this.get('id');
			},
			/**
			 * setter for establishment id
			 * @param {Integer} establishmentID
			 */
			setEstablishmentID : function(establishmentID) {
				this.set('establishment_id', establishmentID);
			},
			/**
			 * getter for establishment id
			 * @return {Integer}
			 */
			getEstablishmentID : function() {
				return this.get('establishment_id');
			},
			/**
			 * getter for establishment name
			 * @return {String}
			 */
			getEstablishmentName : function() {
				return this.get('establishment_name');
			},
			/**
			 * getter for establishment image
			 * @return {String}
			 */
			getEstablishmentImage : function() {
				return this.get('establishment_image');
			},
			/**
			 * setter for total amount
			 * @param {Double} totalAmount
			 */
			setTotalAmount : function(totalAmount) {
				this.set('total_amount', totalAmount);
			},
			/**
			 * getter for total amount
			 * @return {Double}
			 */
			getTotalAmount : function() {
				return this.get('total_amount');
			},
			/**
			 * getter for tax amount
			 * @return {Double}
			 */
			getTaxAmount : function() {
				return this.get('tax_amount');
			},
			/**
			 * getter for total amount without tip
			 */
			getTotalAmountWithoutTip : function() {
				return this.getTotalAmount() - this.getTipAmount();
			},
			/**
			 * setter for buyer user id
			 * @param {Integer} totalAmount
			 */
			setBuyerUserID : function(buyerUserID) {
				this.set('buyer_user_id', buyerUserID);
			},
			/**
			 * getter for buyer user id
			 * @return {Integer}
			 */
			getBuyerUserID : function() {
				return this.get('buyer_user_id');
			},
			/**
			 * setter for payment method
			 * @param {Integer} paymentMethod
			 */
			setPaymentMethod : function(paymentMethod) {
				this.set('payment_method', paymentMethod);
			},
			/**
			 * getter for payment method
			 * @return {Integer}
			 */
			getPaymentMethod : function() {
				return this.get('payment_method');
			},
			/**
			 * setter for payment operation code
			 * @param {String} paymentOperationCode
			 */
			setPaymentOperationCode : function(paymentOperationCode) {
				this.set('payment_operation_code', paymentOperationCode);
			},
			/**
			 * getter for payment operation code
			 * @return {String}
			 */
			getPaymentOperationCode : function() {
				return this.get('payment_operation_code');
			},
			/**
			 * setter for credit card token
			 * @param {String} creditCardToken
			 */
			setCreditCardToken:function(creditCardToken){
				this.set('credit_card_token', creditCardToken);
			},
			/**
			 * getter for credit card token
			 * @return {String}
			 */
			getCreditCardToken:function(){
				return this.get('credit_card_token');
			},
			/**
			 * setter for transaction type id
			 * @param {Integer} transactionTypeID
			 */
			setTransactionTypeID : function(transactionTypeID) {
				this.set('transaction_type_id', transactionTypeID);
			},
			/**
			 * setter for future payment
			 * @param {boolean} futurePayment
			 */
			setFuturePayment:function(futurePayment){
				this.set('future_payment', futurePayment?1:0);
			},
			/**
			 * getter for future payment
			 * @return {boolean}
			 */
			getFuturePayment : function(){
				return this.get('future_payment');
			},
			/**
			 * getter for transaction type id
			 * @return {Integer}
			 */
			getTransactionTypeID : function() {
				return this.get('transaction_type_id');
			},
			/**
			 * setter for transaction date
			 * @param {String} transactionDate
			 */
			setTransactionDate : function(transactionDate) {
				this.set('transaction_date', transactionDate);
			},
			/**
			 * getter for transaction_date
			 * @return {String}
			 */
			getTransactionDate : function() {
				return this.get('transaction_date');
			},
			/**
			 * setter for transaction event date
			 * @param {String} transactionEventDate
			 */
			setTransactionEventDate : function(transactionEventDate) {
				this.set('transaction_event_date', transactionEventDate);
			},
			/**
			 * getter for transaction event date
			 * @return {String}
			 */
			getTransactionEventDate : function() {
				return this.get('transaction_event_date');
			},
			/**
			 * setter for show
			 * @param {Boolean} show
			 */
			setShow : function(show) {
				this.set('show', show ? 1 : 0);
			},
			/**
			 * getter for show
			 * @return {Boolean}
			 */
			getShow : function() {
				return this.get('show') == 1;
			},
			/**
			 * setter for currency symbol
			 * @param {String} currencySymbol
			 */
			setCurrencySymbol : function(currencySymbol) {
				this.set('currency_symbol', currencySymbol);
			},
			/**
			 * getter for currency symbol
			 * @return {String}
			 */
			getCurrencySymbol : function() {
				return this.get('currency_symbol');
			},
			/**
			 * setter for currency code
			 * @param {String} currencyCode
			 */
			setCurrencyCode : function(currencyCode) {
				this.set('currency_code', currencyCode);
			},
			/**
			 * getter for currency code
			 * @return {String}
			 */
			getCurrencyCode : function() {
				return this.get('currency_code');
			},
			/**
			 * @return {Establishment}
			 */
			getEstablishment : function() {
				return Alloy.Globals.DaoBuilder.getEstablishmentDao().findByID(this.get('establishment_id'));
			},
			getTransactionCode : function() {
				return this.get('transaction_code');
			},
			/**
			 * get image
			 * @param {String}
			 */
			getImage : function() {
				return this.get('image');
			},
			/**
			 * get pretty transaction date
			 * @return {String}
			 */
			getPrettyTransactionDate : function() {
				var date = (require('FormatUtils')).getDateToString(this.getTransactionDate());
				var pretty = (require('pretty')).prettyDate(date);
				return pretty;
			},
			/**
			 * get buyer user
			 * @return {User}
			 */
			getBuyerUser : function() {
				return Alloy.Globals.DaoBuilder.getUserDao().findByID(this.get('buyer_user_id'));
			},
			/**
			 * get consumer user
			 * @return {User}
			 */
			getConsumerUser : function() {
				return Alloy.Globals.DaoBuilder.getUserDao().findByID(this.get('consumer_user_id'));
			},
			/**
			 * return {String}
			 */
			getStatusName : function() {
				var statusName = "";
				switch(this.get('status')) {
				case Alloy.CFG.constants.transaction_status.accept:
					statusName = L('used', 'Redeemed');
					break;
				case Alloy.CFG.constants.transaction_status.cancel:
					statusName = L('canceled', 'Cancelled');
					break;
				case Alloy.CFG.constants.transaction_status.pending:
					statusName = L('pending', 'Pending');
					break;
				case Alloy.CFG.constants.transaction_status.rejected:
					statusName = L('not_accepted', 'Not accepted');
					break;
					case Alloy.CFG.constants.transaction_status.defeated:
					statusName = L('defeated', 'Expired');
					break;
					case Alloy.CFG.constants.transaction_status.pending_accept:
					statusName = L('pending_accept', 'Pending to accept');
					break;
					case Alloy.CFG.constants.transaction_status.gift_rejected:
					statusName = L('gift_rejected', 'Not accepted');
					break;
					case Alloy.CFG.constants.transaction_status.gift_accepted:
					statusName = L('gift_accepted', 'Accepted');
					break;
				}
				return statusName.toUpperCase();

			},
			/**
			 * setter for property tip_amount
			 * @param {Double}
			 */
			setTipAmount : function(tipAmount) {
				this.set('tip_amount', tipAmount);
			},
			/**
			 * setter for property admin_fee
			 * @param {Double}
			 */
			setAdminFee : function(adminFee) {
				this.set('admin_fee', adminFee);
			},
			/**
			 * setter for property tax_amount
			 * @param {Double}
			 */
			setTaxAmount : function(taxAmount) {
				this.set('tax_amount', taxAmount);
			},
			/**
			 * getter for tip amount
			 * @return {Double}
			 */
			getTipAmount : function() {
				return this.get('tip_amount');
			},
			/**
			 * getter for admin fee
			 * @return {Double}
			 */
			getAdminFee : function() {
				return this.get('admin_fee');
			},
			/**
			 * get calculate title
			 * @return {String}
			 */
			getCalculateTitle : function() {
				return this.getEstablishmentName();
			},
			/**
			 * get calculate image
			 * @return {String}
			 */
			getCalculateImage : function() {
				return this.getEstablishmentImage();
			},
			/**
			 * getter for description
			 * @return {String}
			 */
			getDescription : function() {
				var description = "";
				switch(this.getTransactionTypeID()) {
				case Alloy.CFG.constants.transaction_type.ticket:
					description = L('bought_a_ticket', 'Bought a ticket');
					break;
				case Alloy.CFG.constants.transaction_type.table:
					description = L('bought_a_table', 'Bought a table');
					break;
				case Alloy.CFG.constants.transaction_type.drink:
					description = L('bought_a_drink', 'Bought a drink');
					break;
				case Alloy.CFG.constants.transaction_type.drink_gift:
					description = String.format(L('sent_a_drink_to', 'Sent a drink to %s'), this.getToUserFullName());
					break;
				}
				return description;
			},
			/**
			 * get calculate transaction date with AB format
			 * @return {String}
			 */
			getCalculateTransactionDateAB : function() {
				return (require('FormatUtils')).getAbStringToDate(this.getTransactionDate());
			},
			/**
			 * get is pending status
			 */
			isPending : function() {
				if(!this.getIsGift()){
					return this.get('status') == Alloy.CFG.constants.transaction_status.pending;	
				}else{
					return this.get('status') == Alloy.CFG.constants.transaction_status.gift_accepted;
				}
			},
			getIsGift : function() {
				return this.get('is_gift') == 1;
			},
			/**
			 * getter to user full name
			 * @return {String}
			 */
			getToUserFullName : function() {
				return this.get('to_user_full_name');
			},
			/**
			 * getter to user gender
			 * @return {Integer}
			 */
			getToUserGender : function() {
				return this.get('to_user_gender');
			},
			/**
			 * getter for status
			 */
			getStatus:function(){
				return this.get('status');
			},
			toHideService : function() {
				return {
					establishment_id : this.get('establishment_id'),
					show : false,
				};
			},
			/**
			 * generate transaction with informacion
			 * @param {Integer} establishmentID
			 * @param {Integer} buyerUserID
			 * @param {Integer} transactionTypeID
			 * @param {Double} totalAmount
			 * @param {Double} tipAmount
			 * @param {Data} eventDate
			 */
			generateTransaction : function(establishmentID, buyerUserID, transactionTypeID, totalAmount, tipAmount, eventDate, taxAmount, adminFee) {

				this.setEstablishmentID(establishmentID);
				this.setBuyerUserID(buyerUserID);
				this.setTransactionTypeID(transactionTypeID);
				this.setTotalAmount(totalAmount);
				this.setTipAmount(tipAmount);
				this.setTaxAmount(taxAmount);
				this.setAdminFee(adminFee);

				this.setPaymentMethod(Alloy.CFG.Paypal.method);
				this.setTransactionDate((require('FormatUtils')).getStringDBToDate());
				this.setCurrencySymbol(Alloy.Globals.CurrentCurrencySymbol);
				this.setCurrencyCode(Alloy.Globals.CurrentCurrencyCode);
				this.setShow(true);

				if (_.isDate(eventDate)) {
					this.setTransactionEventDate((require('FormatUtils')).getStringDBToDate(eventDate));
				}
			},
			/**
			 * generate transaction for gift
			 * @param {Integer} establishmentID
			 * @param {Integer} buyerUserID
			 * @param {Double} totalAmount
			 * @param {Double} tipAmount
			 */
			generateTransactionGift : function(establishmentID, buyerUserID, totalAmount, tipAmount, taxAmount, adminFee) {
				this.generateTransaction(establishmentID, buyerUserID, Alloy.CFG.constants.transaction_type.drink_gift, totalAmount, tipAmount, "", taxAmount, adminFee);
			},
			generateTransactionDetailPaypalForTip : function() {
				var tipAmount = parseFloat(this.getTipAmount());
				if (tipAmount > 0) {
					return {
						name : L('tip', 'Tip'),
						itemCount : 1,
						itemPrice : tipAmount,
						totalPrice : tipAmount
					};
				} else {
					return null;
				}
			},
			/**
			 * get currency with amount
			 * @return {String}
			 */
			getCurrencyAndTotalAmount:function(){
				return Alloy.Globals.CurrentCurrencySymbol + " " + Alloy.Globals.FormatUtils.formatPrice(this.getTotalAmount());
			},
			/**
			 * process information received for onlinepayment to send transaction 
			 *@param {Object} onlinePayment
			 */
			processOnlinePayment:function(onlinePayment){
				this.setPaymentMethod(onlinePayment.paymentMethod);
				switch(onlinePayment.paymentMethod){
					case Alloy.CFG.Paypal.method:
					this.setPaymentOperationCode(onlinePayment.code);
					break;
					case Alloy.CFG.Stripe.method:
					this.setCreditCardToken(onlinePayment.code);
					this.setFuturePayment(true);
					break;
				}
			},
			/**
			 * transform data to databinding in componentes
			 */
			transform : function() {
				var transformed = this.toJSON();
				transformed.description = this.getDescription();
				transformed.tax_amount_price = Alloy.Globals.CurrentCurrencySymbol + " " + Alloy.Globals.FormatUtils.formatPrice(transformed.tax_amount);
				transformed.tip_amount_price = Alloy.Globals.CurrentCurrencySymbol + " " + Alloy.Globals.FormatUtils.formatPrice(transformed.tip_amount);
				transformed.admin_fee_price = Alloy.Globals.CurrentCurrencySymbol + " " + Alloy.Globals.FormatUtils.formatPrice(transformed.admin_fee);
				transformed.total_amount_price = Alloy.Globals.CurrentCurrencySymbol + " " + Alloy.Globals.FormatUtils.formatPrice(transformed.total_amount);
				return transformed;
			}
		});

		return Model;
	},
	extendCollection : function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here

			// For Backbone v1.1.2, uncomment the following to override the
			// fetch method to account for a breaking change in Backbone.
			/*
			 fetch: function(options) {
			 options = options ? _.clone(options) : {};
			 options.reset = true;
			 return Backbone.Collection.prototype.fetch.call(this, options);
			 }
			 */
			deleteAll : function() {

				var collection = this;

				var sql = "DELETE FROM " + collection.config.adapter.collection_name;
				db = Ti.Database.open(collection.config.adapter.db_name);
				db.execute(sql);
				db.close();

				collection.trigger('sync');

			}
		});

		return Collection;
	}
};
